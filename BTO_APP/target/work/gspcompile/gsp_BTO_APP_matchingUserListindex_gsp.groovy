import com.bto.customer.MatchingUserSkipReason
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_matchingUserListindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/matchingUserList/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(0)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'http-equiv':("Content-Type"),'content':("text/html; charset=ISO-8859-1")],-1)
printHtmlPart(0)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(0)
createTagBody(2, {->
createClosureForHtmlPart(2, 3)
invokeTag('captureTitle','sitemesh',7,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',7,[:],2)
printHtmlPart(3)
expressionOut.print(resource(dir:'assets/plugins/footable/css',file:'footable.core.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/bootstrap-select/dist/css',file:'bootstrap-select.min.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'buttons.bootstrap.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css'))
printHtmlPart(6)
expressionOut.print(resource(dir:'machingUserAssets',file:'jcarousel.responsive.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'machingUserAssets',file:'sliderStyle.css'))
printHtmlPart(7)
})
invokeTag('captureHead','sitemesh',59,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(8)
if(true && (flash.message)) {
printHtmlPart(9)
expressionOut.print(flash.message)
printHtmlPart(10)
}
printHtmlPart(11)
if(true && (flash.message1)) {
printHtmlPart(12)
expressionOut.print(flash.message1)
printHtmlPart(10)
}
printHtmlPart(11)
createTagBody(2, {->
printHtmlPart(13)
expressionOut.print(tripId)
printHtmlPart(14)
expressionOut.print(resource(dir:'images',file:'refresh.png'))
printHtmlPart(15)
})
invokeTag('form','g',78,['method':("post"),'action':("findMatchingUser"),'controller':("BookingConsole"),'role':("form")],2)
printHtmlPart(11)
if(true && ((jsonObject)!=null && jsonObject.status!=false)) {
printHtmlPart(16)
loop:{
int i = 0
for( user in (jsonObject.users) ) {
printHtmlPart(17)

String[] color = ["#003366", "#330033", "#ff9900","#003366"]

printHtmlPart(18)
expressionOut.print(color[i])
printHtmlPart(19)
expressionOut.print(resource(dir:'assets/images/users',file:'avatar-1.jpg'))
printHtmlPart(20)
expressionOut.print(user.firstName)
printHtmlPart(21)
expressionOut.print(user.lastName)
printHtmlPart(22)

if(i==3){
														i=0;
													}

printHtmlPart(23)
expressionOut.print(resource(dir:'images',file:'from_to_icon.png'))
printHtmlPart(24)
expressionOut.print(user.startLocation)
printHtmlPart(25)
expressionOut.print(user.endLocation)
printHtmlPart(26)
expressionOut.print(user.tripTime)
printHtmlPart(27)
expressionOut.print(user.vendorCarType)
printHtmlPart(28)
expressionOut.print(user.eta)
printHtmlPart(29)
expressionOut.print(user.vendorName)
printHtmlPart(30)
expressionOut.print(user.department)
printHtmlPart(31)
expressionOut.print(user.designation)
printHtmlPart(32)
createTagBody(4, {->
printHtmlPart(33)
expressionOut.print(session['userID'])
printHtmlPart(34)
expressionOut.print(user.userID)
printHtmlPart(35)
expressionOut.print(tripId)
printHtmlPart(36)
expressionOut.print(user.tripID)
printHtmlPart(37)
expressionOut.print(user.phone)
printHtmlPart(38)
expressionOut.print(user.phone)
printHtmlPart(39)
if(true && (user.isInvited==false)) {
printHtmlPart(40)
}
else {
printHtmlPart(41)
}
printHtmlPart(42)
})
invokeTag('form','g',164,['controller':("InviteUser"),'action':("inviteUser"),'method':("post")],4)
printHtmlPart(43)
i++
}
}
printHtmlPart(44)
if(true && (jsonObject.invitedUserCount>0)) {
printHtmlPart(45)
createTagBody(4, {->
printHtmlPart(46)
expressionOut.print(session['userID'])
printHtmlPart(47)
expressionOut.print(tripId)
printHtmlPart(48)
})
invokeTag('form','g',186,['method':("post"),'action':("bookTripWithSkipReason"),'controller':("MatchingUserList"),'role':("form")],4)
printHtmlPart(49)
}
else {
printHtmlPart(50)
}
printHtmlPart(51)
createTagBody(3, {->
printHtmlPart(52)
expressionOut.print(session['userID'])
printHtmlPart(53)
expressionOut.print(tripId)
printHtmlPart(54)
for( reason in (MatchingUserSkipReason.list()) ) {
printHtmlPart(55)
expressionOut.print(reason.reasonDetails)
printHtmlPart(56)
expressionOut.print(reason.reasonDetails)
printHtmlPart(57)
}
printHtmlPart(58)
})
invokeTag('form','g',221,['method':("post"),'action':("bookTripWithSkipReason"),'controller':("MatchingUserList"),'role':("form")],3)
printHtmlPart(59)
}
else {
printHtmlPart(60)
expressionOut.print(resource(dir:'images',file:'no_matching_user.png'))
printHtmlPart(61)
createTagBody(3, {->
printHtmlPart(62)
expressionOut.print(session['userID'])
printHtmlPart(63)
expressionOut.print(tripId)
printHtmlPart(64)
})
invokeTag('form','g',243,['controller':("MatchingUserList"),'action':("bookTripWithoutMachingUser"),'method':("post")],3)
printHtmlPart(65)
}
printHtmlPart(66)
})
invokeTag('captureBody','sitemesh',246,[:],1)
printHtmlPart(67)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471534366301L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
