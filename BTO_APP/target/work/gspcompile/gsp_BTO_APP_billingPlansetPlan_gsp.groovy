import com.bto.customer.VendorDetails
import com.bto.customer.BillingPlan
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_billingPlansetPlan_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/billingPlan/setPlan.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
invokeTag('captureMeta','sitemesh',1,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(0)
printHtmlPart(0)
printHtmlPart(1)
if(true && (flash.message)) {
printHtmlPart(2)
expressionOut.print(flash.message)
printHtmlPart(3)
}
printHtmlPart(4)
if(true && (flash.message1)) {
printHtmlPart(5)
expressionOut.print(flash.message1)
printHtmlPart(3)
}
printHtmlPart(6)
createTagBody(1, {->
printHtmlPart(7)
loop:{
int i = 0
for( vendorInstance in (VendorDetails.list()) ) {
printHtmlPart(8)
expressionOut.print(vendorInstance.id)
printHtmlPart(9)
expressionOut.print(vendorInstance.companyName)
printHtmlPart(10)
i++
}
}
printHtmlPart(11)
})
invokeTag('form','g',57,['action':("setPlan"),'method':("post")],1)
printHtmlPart(12)
createTagBody(1, {->
printHtmlPart(13)
invokeTag('hiddenField','g',75,['name':("packageType"),'value':("1")],-1)
printHtmlPart(13)
invokeTag('hiddenField','g',76,['name':("vendor"),'value':(session["vendor"])],-1)
printHtmlPart(14)
invokeTag('set','g',87,['var':("j"),'value':(1)],-1)
printHtmlPart(15)
loop:{
int i = 0
for( billInstance in (packagesInstance) ) {
printHtmlPart(16)
if(true && (billInstance.packageType==1)) {
printHtmlPart(17)
expressionOut.print(j++)
printHtmlPart(18)
expressionOut.print(billInstance.amount)
printHtmlPart(18)
expressionOut.print(billInstance.kilometer)
printHtmlPart(19)
invokeTag('radio','g',101,['name':("flatRate"),'value':(billInstance.id)],-1)
printHtmlPart(20)
}
printHtmlPart(15)
i++
}
}
printHtmlPart(21)
})
invokeTag('form','g',114,['action':("savePlan"),'method':("post")],1)
printHtmlPart(22)
createTagBody(1, {->
printHtmlPart(13)
invokeTag('hiddenField','g',116,['name':("packageType"),'value':("2")],-1)
printHtmlPart(13)
invokeTag('hiddenField','g',117,['name':("vendor"),'value':(session["vendor"])],-1)
printHtmlPart(23)
invokeTag('set','g',129,['var':("j"),'value':(1)],-1)
printHtmlPart(16)
loop:{
int k = 0
for( billInstance1 in (packagesInstance) ) {
printHtmlPart(24)
if(true && (billInstance1.packageType==2)) {
printHtmlPart(25)
expressionOut.print(j++)
printHtmlPart(26)
expressionOut.print(billInstance1.amount)
printHtmlPart(26)
expressionOut.print(billInstance1.kilometer)
printHtmlPart(26)
expressionOut.print(billInstance1.hour)
printHtmlPart(27)
invokeTag('checkBox','g',146,['name':("customRatePlan"),'value':(billInstance1.id),'checked':("false")],-1)
printHtmlPart(28)
}
printHtmlPart(16)
k++
}
}
printHtmlPart(29)
})
invokeTag('form','g',159,['action':("savePlan"),'method':("post")],1)
printHtmlPart(30)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429497979L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
