import com.bto.trip.TravelPurpose
import java.lang.String
import com.bto.customer.VendorDetails
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_eventsbookEvent_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/events/bookEvent.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(0)
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(0)
invokeTag('captureMeta','sitemesh',7,['gsp_sm_xmlClosingForEmptyTag':("/"),'http-equiv':("Content-Type"),'content':("text/html; charset=ISO-8859-1")],-1)
printHtmlPart(0)
invokeTag('captureMeta','sitemesh',8,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(0)
createTagBody(2, {->
createClosureForHtmlPart(2, 3)
invokeTag('captureTitle','sitemesh',9,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',9,[:],2)
printHtmlPart(3)
expressionOut.print(resource(dir:'assets/plugins/timepicker',file:'bootstrap-timepicker.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/mjolnic-bootstrap-colorpicker/dist/css',file:'bootstrap-colorpicker.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/bootstrap-datepicker/dist/css',file:'bootstrap-datepicker.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/clockpicker/dist',file:'jquery-clockpicker.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/bootstrap-daterangepicker',file:'daterangepicker.css'))
printHtmlPart(5)
})
invokeTag('captureHead','sitemesh',61,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(6)
if(true && (flash.message)) {
printHtmlPart(7)
expressionOut.print(flash.message)
printHtmlPart(8)
}
printHtmlPart(9)
if(true && (flash.message1)) {
printHtmlPart(10)
expressionOut.print(flash.message1)
printHtmlPart(8)
}
printHtmlPart(9)
createTagBody(2, {->
printHtmlPart(11)
expressionOut.print(session.userID)
printHtmlPart(12)
expressionOut.print(eventVenue)
printHtmlPart(13)
for( purpose in (TravelPurpose.list()) ) {
printHtmlPart(14)
expressionOut.print(purpose.travelPurpose)
printHtmlPart(15)
expressionOut.print(purpose.travelPurpose)
printHtmlPart(16)
}
printHtmlPart(17)
expressionOut.print(costCenter)
printHtmlPart(18)
expressionOut.print(eventCode)
printHtmlPart(19)
expressionOut.print(eventDate)
printHtmlPart(20)
})
invokeTag('form','g',186,['controller':("BookingConsole"),'action':("bookEventTrip"),'method':("post")],2)
printHtmlPart(21)
expressionOut.print(resource(dir:'assets/js',file:'jquery.min.js'))
printHtmlPart(22)
expressionOut.print(jsonObject)
printHtmlPart(23)
})
invokeTag('captureBody','sitemesh',267,['onload':("check()")],1)
printHtmlPart(24)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429498556L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
