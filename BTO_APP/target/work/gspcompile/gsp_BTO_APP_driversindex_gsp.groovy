import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_driversindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/drivers/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
invokeTag('captureMeta','sitemesh',1,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("vendorMaster")],-1)
printHtmlPart(0)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css'))
printHtmlPart(3)
expressionOut.print(resource(dir:'assets/js',file:'jquery.min.js'))
printHtmlPart(4)
createClosureForHtmlPart(5, 1)
invokeTag('link','g',12,['controller':("Drivers"),'action':("addDriver"),'class':("btn btn-success waves-effect waves-light"),'style':("float:right;")],1)
printHtmlPart(6)
if(true && (flash.message)) {
printHtmlPart(7)
expressionOut.print(flash.message)
printHtmlPart(8)
expressionOut.print(session["popUpDataCancel"])
printHtmlPart(9)

session['popUpDataCancel']="";

printHtmlPart(10)
}
printHtmlPart(11)
if(true && (flash.message1)) {
printHtmlPart(12)
expressionOut.print(flash.message1)
printHtmlPart(13)
expressionOut.print(session["popUpData"])
printHtmlPart(14)

session['popUpData']="";

printHtmlPart(15)
}
printHtmlPart(16)
loop:{
int i = 0
for( driverInstance in (driverInstanceList) ) {
printHtmlPart(17)
expressionOut.print(resource(dir:'assets/images/users',file:'avatar-2.jpg'))
printHtmlPart(18)
expressionOut.print(driverInstance?.firstName)
printHtmlPart(19)
expressionOut.print(driverInstance?.lastName)
printHtmlPart(19)
expressionOut.print(driverInstance?.phone)
printHtmlPart(20)
createClosureForHtmlPart(21, 2)
invokeTag('link','g',100,['action':("editDriver"),'id':(driverInstance?.id),'title':("Edit")],2)
printHtmlPart(22)
createClosureForHtmlPart(23, 2)
invokeTag('link','g',101,['action':("deleteDriver"),'id':(driverInstance?.id),'title':("Delete"),'onclick':("return confirm(' You want to delete?');")],2)
printHtmlPart(24)
i++
}
}
printHtmlPart(25)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js'))
printHtmlPart(26)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js'))
printHtmlPart(26)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js'))
printHtmlPart(27)

session['sidebarActive']="";
session['sidebarActive']="drivers/index";

printHtmlPart(28)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429498443L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
