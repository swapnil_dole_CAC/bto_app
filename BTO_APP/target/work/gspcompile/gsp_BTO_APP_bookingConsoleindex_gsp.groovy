import com.bto.trip.TravelPurpose
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.lang.String
import com.bto.customer.VendorDetails
import com.bto.customer.CostCenter
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_bookingConsoleindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bookingConsole/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(0)
printHtmlPart(0)
printHtmlPart(0)
printHtmlPart(0)
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',18,['gsp_sm_xmlClosingForEmptyTag':("/"),'http-equiv':("Content-Type"),'content':("text/html; charset=ISO-8859-1")],-1)
printHtmlPart(0)
invokeTag('captureMeta','sitemesh',19,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(0)
createTagBody(2, {->
createClosureForHtmlPart(3, 3)
invokeTag('captureTitle','sitemesh',20,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',20,[:],2)
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/timepicker',file:'bootstrap-timepicker.min.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/plugins/mjolnic-bootstrap-colorpicker/dist/css',file:'bootstrap-colorpicker.min.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/plugins/bootstrap-datepicker/dist/css',file:'bootstrap-datepicker.min.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/plugins/clockpicker/dist',file:'jquery-clockpicker.min.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/plugins/bootstrap-daterangepicker',file:'daterangepicker.css'))
printHtmlPart(6)

Date date = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	String strDate = sdf.format(date);

printHtmlPart(7)
out.print(strDate.toString())
printHtmlPart(8)
})
invokeTag('captureHead','sitemesh',104,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(12)
if(true && (flash.message1)) {
printHtmlPart(13)
expressionOut.print(flash.message1)
printHtmlPart(11)
}
printHtmlPart(14)
createTagBody(2, {->
printHtmlPart(15)
expressionOut.print(session.userID)
printHtmlPart(16)
for( purpose in (TravelPurpose.list()) ) {
printHtmlPart(17)
expressionOut.print(purpose.travelPurpose)
printHtmlPart(18)
expressionOut.print(purpose.travelPurpose)
printHtmlPart(19)
}
printHtmlPart(20)
expressionOut.print(session['costCenter'])
printHtmlPart(21)
})
invokeTag('form','g',253,['controller':("BookingConsole"),'action':("bookTrip"),'method':("post")],2)
printHtmlPart(22)
expressionOut.print(resource(dir:'assets/js',file:'jquery.min.js'))
printHtmlPart(23)
expressionOut.print(jsonObject)
printHtmlPart(24)

session['sidebarActive']="";
session['sidebarActive']="bookingConsole/index";

printHtmlPart(25)
})
invokeTag('captureBody','sitemesh',292,[:],1)
printHtmlPart(26)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429498137L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
