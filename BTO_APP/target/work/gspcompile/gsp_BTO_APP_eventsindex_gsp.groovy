import java.lang.System
import com.bto.trip.EventDetails
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_eventsindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/events/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(0)
invokeTag('captureMeta','sitemesh',3,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css'))
printHtmlPart(3)
expressionOut.print(resource(dir:'assets/js',file:'jquery.min.js'))
printHtmlPart(4)
if(true && (eventDetailsInstanceList!=null)) {
printHtmlPart(5)
createClosureForHtmlPart(6, 2)
invokeTag('link','g',42,['controller':("Events"),'action':("createEvent"),'class':("btn btn-success waves-effect waves-light"),'style':("float:right;")],2)
printHtmlPart(7)
}
printHtmlPart(8)
if(true && (flash.message)) {
printHtmlPart(9)
expressionOut.print(flash.message)
printHtmlPart(10)
}
printHtmlPart(11)
if(true && (flash.message1)) {
printHtmlPart(12)
expressionOut.print(flash.message1)
printHtmlPart(10)
}
printHtmlPart(13)
if(true && (eventDetailsInstanceList)) {
printHtmlPart(14)
loop:{
int i = 0
for( event in (eventDetailsInstanceList) ) {
printHtmlPart(15)
expressionOut.print(event.eventName)
printHtmlPart(16)
expressionOut.print(event.eventType)
printHtmlPart(17)
expressionOut.print(event.eventDate)
printHtmlPart(18)
expressionOut.print(event.eventTime)
printHtmlPart(19)
expressionOut.print(event.eventVenue)
printHtmlPart(20)
expressionOut.print(event.eventCode)
printHtmlPart(21)
expressionOut.print(event.costCenter)
printHtmlPart(22)

def id=event.user.id;

printHtmlPart(23)
if(true && (id.toString()==session['userID'].toString())) {
printHtmlPart(24)
createTagBody(4, {->
printHtmlPart(25)
expressionOut.print(event.eventName)
printHtmlPart(26)
expressionOut.print(event.eventVenue)
printHtmlPart(27)
expressionOut.print(event.eventDate)
printHtmlPart(28)
expressionOut.print(event.eventCode)
printHtmlPart(29)
expressionOut.print(event.costCenter)
printHtmlPart(30)
})
invokeTag('form','g',125,['method':("post"),'action':("bookThisEvent"),'controller':("Events"),'role':("form")],4)
printHtmlPart(31)
createTagBody(4, {->
printHtmlPart(32)
expressionOut.print(event.id)
printHtmlPart(33)
})
invokeTag('form','g',129,['method':("post"),'action':("cancelEvent"),'controller':("Events"),'role':("form")],4)
printHtmlPart(34)
}
else {
printHtmlPart(35)
createTagBody(4, {->
printHtmlPart(25)
expressionOut.print(event.eventName)
printHtmlPart(26)
expressionOut.print(event.eventVenue)
printHtmlPart(27)
expressionOut.print(event.eventDate)
printHtmlPart(28)
expressionOut.print(event.eventCode)
printHtmlPart(29)
expressionOut.print(event.costCenter)
printHtmlPart(36)
})
invokeTag('form','g',143,['method':("post"),'action':("bookThisEvent"),'controller':("Events"),'role':("form")],4)
printHtmlPart(37)
}
printHtmlPart(38)
i++
}
}
printHtmlPart(39)
}
else {
printHtmlPart(40)
expressionOut.print(resource(dir:'images',file:'no_events.png'))
printHtmlPart(41)
createClosureForHtmlPart(42, 2)
invokeTag('link','g',167,['controller':("Events"),'action':("createEvent"),'class':("btn btn-success waves-effect waves-light")],2)
printHtmlPart(43)
}
printHtmlPart(44)
expressionOut.print(resource(dir:'assets/js',file:'jquery.min.js'))
printHtmlPart(45)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js'))
printHtmlPart(45)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js'))
printHtmlPart(45)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js'))
printHtmlPart(46)
expressionOut.print(session["popUpData"])
printHtmlPart(47)

session['popUpData']="";

printHtmlPart(48)

session['sidebarActive']="";
session['sidebarActive']="events/index";

printHtmlPart(49)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429498676L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
