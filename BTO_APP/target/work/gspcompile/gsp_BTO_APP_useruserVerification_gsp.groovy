import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_useruserVerification_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/user/userVerification.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'http-equiv':("Content-Type"),'content':("text/html; charset=ISO-8859-1")],-1)
printHtmlPart(2)
createTagBody(2, {->
createClosureForHtmlPart(3, 3)
invokeTag('captureTitle','sitemesh',5,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/css',file:'bootstrap.min.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/css',file:'components.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/css',file:'core.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/css',file:'icons.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/css',file:'responsive.css'))
printHtmlPart(6)
expressionOut.print(resource(dir:'images',file:'verifiction-bg.png'))
printHtmlPart(7)
})
invokeTag('captureHead','sitemesh',49,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(8)
if(true && (flash.message)) {
printHtmlPart(9)
expressionOut.print(flash.message)
printHtmlPart(10)
}
printHtmlPart(11)
if(true && (flash.message1)) {
printHtmlPart(12)
expressionOut.print(flash.message1)
printHtmlPart(10)
}
printHtmlPart(13)
createTagBody(2, {->
printHtmlPart(14)
expressionOut.print(session.emailID)
printHtmlPart(15)
expressionOut.print(session.phone)
printHtmlPart(16)
expressionOut.print(session.firstName)
printHtmlPart(17)
})
invokeTag('form','g',107,['class':("form-horizontal "),'controller':("User"),'action':("validateUser"),'method':("post")],2)
printHtmlPart(18)
createTagBody(2, {->
printHtmlPart(14)
expressionOut.print(session.emailID)
printHtmlPart(15)
expressionOut.print(session.phone)
printHtmlPart(16)
expressionOut.print(session.firstName)
printHtmlPart(19)
createClosureForHtmlPart(20, 3)
invokeTag('link','g',122,['class':("text-primary")],3)
printHtmlPart(21)
})
invokeTag('form','g',123,['controller':("User"),'action':("resendVerificationCode"),'method':("post")],2)
printHtmlPart(22)
})
invokeTag('captureBody','sitemesh',126,[:],1)
printHtmlPart(23)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429500344L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
