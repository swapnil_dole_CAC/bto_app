import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_useruserProfile_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/user/userProfile.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'http-equiv':("Content-Type"),'content':("text/html; charset=ISO-8859-1")],-1)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
createTagBody(2, {->
createClosureForHtmlPart(2, 3)
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(3)
expressionOut.print(resource(dir:'assets/plugins/timepicker',file:'bootstrap-timepicker.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/mjolnic-bootstrap-colorpicker/dist/css',file:'bootstrap-colorpicker.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/bootstrap-datepicker/dist/css',file:'bootstrap-datepicker.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/clockpicker/dist',file:'jquery-clockpicker.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/bootstrap-daterangepicker',file:'daterangepicker.css'))
printHtmlPart(5)
})
invokeTag('captureHead','sitemesh',82,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(6)
loop:{
int i = 0
for( user in (userInstanceList) ) {
printHtmlPart(7)
if(true && (flash.message)) {
printHtmlPart(8)
expressionOut.print(flash.message)
printHtmlPart(9)
}
printHtmlPart(10)
if(true && (flash.message1)) {
printHtmlPart(11)
expressionOut.print(flash.message1)
printHtmlPart(9)
}
printHtmlPart(10)
if(true && (user?.profilePicture.toString().equals(''))) {
printHtmlPart(10)
invokeTag('set','g',105,['var':("profilePic"),'value':("user.png")],-1)
printHtmlPart(10)
}
else {
printHtmlPart(10)
invokeTag('set','g',107,['var':("profilePic"),'value':(user?.profilePicture)],-1)
printHtmlPart(10)
}
printHtmlPart(12)
expressionOut.print(resource(dir:'images/userProfilePictures',file:profilePic))
printHtmlPart(13)
expressionOut.print(session['firstName'])
printHtmlPart(14)
expressionOut.print(session['lastName'])
printHtmlPart(15)
expressionOut.print(session['emailID'])
printHtmlPart(16)
expressionOut.print(session['phone'])
printHtmlPart(17)
expressionOut.print(user?.dob)
printHtmlPart(18)
expressionOut.print(user?.gender)
printHtmlPart(19)
expressionOut.print(user?.department)
printHtmlPart(20)
expressionOut.print(user?.designation)
printHtmlPart(21)
expressionOut.print(session['costCenter'])
printHtmlPart(22)
createTagBody(3, {->
printHtmlPart(23)
expressionOut.print(resource(dir:'images/userProfilePictures',file:profilePic))
printHtmlPart(24)
expressionOut.print(session['firstName'])
printHtmlPart(14)
expressionOut.print(session['lastName'])
printHtmlPart(25)
expressionOut.print(session['emailID'])
printHtmlPart(26)
expressionOut.print(session['firstName'])
printHtmlPart(27)
expressionOut.print(session['lastName'])
printHtmlPart(28)
expressionOut.print(session['emailID'])
printHtmlPart(29)
expressionOut.print(session['phone'])
printHtmlPart(30)
expressionOut.print(user?.dob)
printHtmlPart(31)
expressionOut.print(user?.department)
printHtmlPart(32)
expressionOut.print(user?.designation)
printHtmlPart(33)
expressionOut.print(session['costCenter'])
printHtmlPart(34)
})
invokeTag('uploadForm','g',252,['action':("updateUserProfile"),'controller':("User"),'method':("post")],3)
printHtmlPart(35)
i++
}
}
printHtmlPart(36)

session['sidebarActive']="";
session['sidebarActive']="user/userProfile";

printHtmlPart(37)
})
invokeTag('captureBody','sitemesh',267,[:],1)
printHtmlPart(38)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471436016512L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
