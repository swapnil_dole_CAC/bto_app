import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_bookedTripHistoryindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bookedTripHistory/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
invokeTag('captureMeta','sitemesh',1,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(0)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/js',file:'jquery.min.js'))
printHtmlPart(3)
if(true && (flash.message)) {
printHtmlPart(4)
expressionOut.print(flash.message)
printHtmlPart(5)
}
printHtmlPart(6)
if(true && (flash.message1)) {
printHtmlPart(7)
expressionOut.print(flash.message1)
printHtmlPart(5)
}
printHtmlPart(8)
if(true && (jsonObject.bookedTripHistory)) {
printHtmlPart(9)
loop:{
int i = 0
for( trips in (jsonObject.bookedTripHistory) ) {
printHtmlPart(10)
if(true && (trips.tripStatus=="requested")) {
printHtmlPart(11)
expressionOut.print(resource(dir:'images',file:'from_to_icon.png'))
printHtmlPart(12)
expressionOut.print(trips.startLocation)
printHtmlPart(13)
expressionOut.print(trips.endLocation)
printHtmlPart(14)
expressionOut.print(trips.tripDate)
printHtmlPart(15)
expressionOut.print(trips.tripTime)
printHtmlPart(16)
expressionOut.print(trips.vendorCarType)
printHtmlPart(17)
expressionOut.print(trips.vendorName)
printHtmlPart(18)
createTagBody(4, {->
printHtmlPart(19)
expressionOut.print(trips.tripID)
printHtmlPart(20)
})
invokeTag('form','g',106,['method':("post"),'action':("cancelBookedTrip"),'controller':("BookedTripHistory"),'role':("form")],4)
printHtmlPart(21)
}
printHtmlPart(22)
i++
}
}
printHtmlPart(23)
}
else {
printHtmlPart(24)
expressionOut.print(resource(dir:'images',file:'no_trips.png'))
printHtmlPart(25)
createClosureForHtmlPart(26, 2)
invokeTag('link','g',126,['controller':("BookingConsole"),'action':("index"),'class':("btn btn-success waves-effect waves-light")],2)
printHtmlPart(27)
}
printHtmlPart(28)
if(true && (jsonObject.bookedTripHistory)) {
printHtmlPart(29)
loop:{
int i = 0
for( trips in (jsonObject.bookedTripHistory) ) {
printHtmlPart(30)
if(true && (trips.tripStatus=="scheduled")) {
printHtmlPart(31)
expressionOut.print(resource(dir:'images',file:'from_to_icon.png'))
printHtmlPart(32)
expressionOut.print(trips.startLocation)
printHtmlPart(33)
expressionOut.print(trips.endLocation)
printHtmlPart(34)
expressionOut.print(trips.tripDate)
printHtmlPart(35)
expressionOut.print(trips.tripTime)
printHtmlPart(36)
expressionOut.print(trips.carType)
printHtmlPart(37)
expressionOut.print(trips.vendorName)
printHtmlPart(38)
createTagBody(4, {->
printHtmlPart(39)
expressionOut.print(trips.tripID)
printHtmlPart(40)
})
invokeTag('form','g',175,['method':("post"),'action':("cancelBookedTrip"),'controller':("BookedTripHistory"),'role':("form")],4)
printHtmlPart(41)
}
printHtmlPart(42)
i++
}
}
printHtmlPart(43)
}
else {
printHtmlPart(24)
expressionOut.print(resource(dir:'images',file:'no_trips.png'))
printHtmlPart(25)
createClosureForHtmlPart(26, 2)
invokeTag('link','g',197,['controller':("BookingConsole"),'action':("index"),'class':("btn btn-success waves-effect waves-light")],2)
printHtmlPart(27)
}
printHtmlPart(44)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js'))
printHtmlPart(45)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js'))
printHtmlPart(45)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js'))
printHtmlPart(46)
expressionOut.print(session["popUpData"])
printHtmlPart(47)

session['popUpData']="";

printHtmlPart(48)
expressionOut.print(session["popUpDataCancel"])
printHtmlPart(49)

session['popUpDataCancel']="";

printHtmlPart(50)

session['sidebarActive']="";
session['sidebarActive']="bookedTripHistory/index";

printHtmlPart(51)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471534760367L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
