import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_driverseditDriver_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/drivers/editDriver.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
invokeTag('captureMeta','sitemesh',1,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("vendorMaster")],-1)
printHtmlPart(0)
if(true && (flash.message)) {
printHtmlPart(1)
expressionOut.print(flash.message)
printHtmlPart(2)
}
printHtmlPart(3)
if(true && (flash.message1)) {
printHtmlPart(4)
expressionOut.print(flash.message1)
printHtmlPart(2)
}
printHtmlPart(5)
createTagBody(1, {->
printHtmlPart(6)
invokeTag('hiddenField','g',22,['name':("id"),'value':(driverInstance?.id)],-1)
printHtmlPart(7)
expressionOut.print(driverInstance?.firstName)
printHtmlPart(8)
expressionOut.print(driverInstance?.lastName)
printHtmlPart(9)
expressionOut.print(driverInstance?.phone)
printHtmlPart(10)
createClosureForHtmlPart(11, 2)
invokeTag('link','g',55,['action':("index"),'class':("btn btn-inverse waves-effect waves-light")],2)
printHtmlPart(12)
})
invokeTag('form','g',61,['action':("updateDriver"),'controller':("Drivers"),'method':("post")],1)
printHtmlPart(13)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429498418L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
