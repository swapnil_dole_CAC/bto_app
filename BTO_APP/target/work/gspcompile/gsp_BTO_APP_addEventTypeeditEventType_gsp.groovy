import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_addEventTypeeditEventType_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/addEventType/editEventType.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
invokeTag('captureMeta','sitemesh',1,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(0)
if(true && (flash.message)) {
printHtmlPart(1)
expressionOut.print(flash.message)
printHtmlPart(2)
}
printHtmlPart(3)
if(true && (flash.message1)) {
printHtmlPart(4)
expressionOut.print(flash.message1)
printHtmlPart(2)
}
printHtmlPart(5)
createTagBody(1, {->
printHtmlPart(6)
invokeTag('hiddenField','g',36,['name':("id"),'value':(eventTypeInstance?.id)],-1)
printHtmlPart(7)
expressionOut.print(eventTypeInstance?.eventType)
printHtmlPart(8)
createClosureForHtmlPart(9, 2)
invokeTag('link','g',48,['controller':("AddVendor"),'action':("index"),'class':("btn btn-inverse waves-effect waves-light")],2)
printHtmlPart(10)
})
invokeTag('form','g',54,['action':("updateEventType"),'controller':("AddEventType"),'method':("post")],1)
printHtmlPart(11)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429497810L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
