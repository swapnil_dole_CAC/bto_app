import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_carTypeindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/carType/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
invokeTag('captureMeta','sitemesh',1,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(0)
if(true && (flash.message)) {
printHtmlPart(1)
expressionOut.print(flash.message)
printHtmlPart(2)
}
printHtmlPart(3)
if(true && (flash.message1)) {
printHtmlPart(4)
expressionOut.print(flash.message1)
printHtmlPart(2)
}
printHtmlPart(5)
createClosureForHtmlPart(6, 1)
invokeTag('link','g',21,['controller':("CarType"),'action':("add_type")],1)
printHtmlPart(7)
invokeTag('set','g',40,['var':("j"),'value':(1)],-1)
printHtmlPart(8)
loop:{
int i = 0
for( carTypeInstance in (carTypeInstanceList) ) {
printHtmlPart(9)
expressionOut.print(j++)
printHtmlPart(10)
expressionOut.print(carTypeInstance?.capacity)
printHtmlPart(10)
expressionOut.print(carTypeInstance?.type)
printHtmlPart(10)
expressionOut.print(carTypeInstance?.petrolMileage)
printHtmlPart(10)
expressionOut.print(carTypeInstance?.dieselMileage)
printHtmlPart(10)
expressionOut.print(carTypeInstance?.cngMileage)
printHtmlPart(11)
createClosureForHtmlPart(12, 2)
invokeTag('link','g',49,['action':("edit"),'id':(carTypeInstance?.id),'title':("Edit")],2)
printHtmlPart(13)
createClosureForHtmlPart(14, 2)
invokeTag('link','g',50,['action':("deleteCarType"),'id':(carTypeInstance?.id),'title':("Delete"),'onclick':("return confirm(' You want to delete?');")],2)
printHtmlPart(15)
i++
}
}
printHtmlPart(16)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429498214L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
