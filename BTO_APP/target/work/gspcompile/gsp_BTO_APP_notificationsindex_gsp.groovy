import com.bto.vendor.Driver
import com.bto.vendor.RejectionReason
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_notificationsindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/notifications/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':("/"),'http-equiv':("Content-Type"),'content':("text/html; charset=ISO-8859-1")],-1)
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',7,['gsp_sm_xmlClosingForEmptyTag':(""),'http-equiv':("refresh"),'content':("100")],-1)
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',8,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(3)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css'))
printHtmlPart(5)
createTagBody(2, {->
createClosureForHtmlPart(6, 3)
invokeTag('captureTitle','sitemesh',14,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',14,[:],2)
printHtmlPart(7)
})
invokeTag('captureHead','sitemesh',41,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(8)
if(true && (flash.message)) {
printHtmlPart(9)
expressionOut.print(flash.message)
printHtmlPart(10)
}
printHtmlPart(11)
if(true && (flash.message1)) {
printHtmlPart(12)
expressionOut.print(flash.message1)
printHtmlPart(10)
}
printHtmlPart(13)
if(true && (jsonObject.alerts)) {
printHtmlPart(14)
loop:{
int i = 0
for( trips in (jsonObject.alerts.data) ) {
printHtmlPart(15)
if(true && (trips.alertType.equals("RequestArrives"))) {
printHtmlPart(16)
if(true && (trips.status.equals("Unread"))) {
printHtmlPart(17)
expressionOut.print(resource(dir:'assets/images/users',file:'avatar-1.jpg'))
printHtmlPart(18)
expressionOut.print(trips.sender_name)
printHtmlPart(19)
expressionOut.print(trips.designation)
printHtmlPart(20)
expressionOut.print(trips.carType)
printHtmlPart(21)
expressionOut.print(trips.date)
printHtmlPart(22)
expressionOut.print(trips.time)
printHtmlPart(23)
expressionOut.print(resource(dir:'images',file:'from_to_icon.png'))
printHtmlPart(24)
expressionOut.print(trips.source_location)
printHtmlPart(25)
expressionOut.print(trips.destination_location)
printHtmlPart(26)
createTagBody(6, {->
printHtmlPart(27)
expressionOut.print(trips.invitationId)
printHtmlPart(28)
expressionOut.print(trips.alert_id)
printHtmlPart(29)
})
invokeTag('form','g',127,['method':("post"),'action':("performInvitationAction"),'controller':("Notifications"),'role':("form")],6)
printHtmlPart(30)
createTagBody(6, {->
printHtmlPart(27)
expressionOut.print(trips.invitationId)
printHtmlPart(31)
expressionOut.print(trips.alert_id)
printHtmlPart(32)
})
invokeTag('form','g',135,['method':("post"),'action':("performInvitationAction"),'controller':("Notifications"),'role':("form")],6)
printHtmlPart(33)
}
printHtmlPart(34)
}
printHtmlPart(15)
if(true && (trips.alertType.equals("RejectedByVendor"))) {
printHtmlPart(35)
expressionOut.print(resource(dir:'assets/images/users',file:'avatar-1.jpg'))
printHtmlPart(36)
expressionOut.print(trips.sender_name)
printHtmlPart(37)
expressionOut.print(trips.message)
printHtmlPart(38)
expressionOut.print(trips.tripDate)
printHtmlPart(39)
expressionOut.print(trips.tripTime)
printHtmlPart(40)
expressionOut.print(trips.startLocation)
printHtmlPart(41)
expressionOut.print(trips.endLocation)
printHtmlPart(42)
if(true && (trips.status=="Unread")) {
printHtmlPart(43)
createTagBody(6, {->
printHtmlPart(44)
expressionOut.print(trips.alert_id)
printHtmlPart(45)
expressionOut.print(trips.tripId)
printHtmlPart(46)
})
invokeTag('form','g',176,['method':("post"),'action':("edit"),'controller':("BookingConsole"),'role':("form")],6)
printHtmlPart(47)
createTagBody(6, {->
printHtmlPart(48)
expressionOut.print(trips.tripId)
printHtmlPart(49)
})
invokeTag('form','g',183,['method':("post"),'action':("cancelBookedTrip"),'controller':("BookedTripHistory"),'role':("form")],6)
printHtmlPart(50)
}
printHtmlPart(51)
}
printHtmlPart(52)
if(true && (trips.alertType.equals("RejectedReqest"))) {
printHtmlPart(53)
expressionOut.print(resource(dir:'assets/images/users',file:'avatar-1.jpg'))
printHtmlPart(36)
expressionOut.print(trips.sender_name)
printHtmlPart(54)
expressionOut.print(trips.message)
printHtmlPart(38)
expressionOut.print(trips.tripDate)
printHtmlPart(39)
expressionOut.print(trips.tripTime)
printHtmlPart(40)
expressionOut.print(trips.startLocation)
printHtmlPart(41)
expressionOut.print(trips.endLocation)
printHtmlPart(42)
if(true && (trips.status=="Unread")) {
printHtmlPart(55)
createTagBody(6, {->
printHtmlPart(44)
expressionOut.print(trips.alert_id)
printHtmlPart(45)
expressionOut.print(trips.tripId)
printHtmlPart(56)
})
invokeTag('form','g',223,['method':("post"),'action':("findMatchingUser"),'controller':("BookingConsole"),'role':("form")],6)
printHtmlPart(57)
createTagBody(6, {->
printHtmlPart(58)
expressionOut.print(trips.invitationId)
printHtmlPart(59)
expressionOut.print(trips.tripId)
printHtmlPart(60)
expressionOut.print(trips.alert_id)
printHtmlPart(61)
})
invokeTag('form','g',231,['method':("post"),'action':("cancelBookedTrip"),'controller':("BookedTripHistory"),'role':("form")],6)
printHtmlPart(50)
}
printHtmlPart(62)
}
printHtmlPart(52)
if(true && (trips.alertType.equals("RequestAccepted"))) {
printHtmlPart(35)
expressionOut.print(resource(dir:'assets/images/users',file:'avatar-1.jpg'))
printHtmlPart(36)
expressionOut.print(trips.sender_name)
printHtmlPart(63)
expressionOut.print(trips.message)
printHtmlPart(38)
expressionOut.print(trips.tripDate)
printHtmlPart(39)
expressionOut.print(trips.tripTime)
printHtmlPart(40)
expressionOut.print(trips.startLocation)
printHtmlPart(41)
expressionOut.print(trips.endLocation)
printHtmlPart(64)
}
printHtmlPart(52)
if(true && (trips.alertType.equals("AcceptedByVendor"))) {
printHtmlPart(65)
expressionOut.print(resource(dir:'assets/images/users',file:'avatar-1.jpg'))
printHtmlPart(36)
expressionOut.print(trips.sender_name)
printHtmlPart(66)
expressionOut.print(trips.message)
printHtmlPart(38)
expressionOut.print(trips.tripDate)
printHtmlPart(39)
expressionOut.print(trips.tripTime)
printHtmlPart(40)
expressionOut.print(trips.startLocation)
printHtmlPart(41)
expressionOut.print(trips.endLocation)
printHtmlPart(67)
}
printHtmlPart(52)
if(true && (trips.alertType.equals("EventCreated"))) {
printHtmlPart(35)
expressionOut.print(resource(dir:'assets/images/users',file:'avatar-1.jpg'))
printHtmlPart(36)
expressionOut.print(trips.sender_name)
printHtmlPart(66)
expressionOut.print(trips.sender_name)
printHtmlPart(68)
expressionOut.print(trips.message)
printHtmlPart(69)
expressionOut.print(trips.eventName)
printHtmlPart(70)
expressionOut.print(trips.eventVenue)
printHtmlPart(71)
expressionOut.print(trips.eventDate)
printHtmlPart(72)
expressionOut.print(trips.eventTime)
printHtmlPart(73)
createTagBody(5, {->
printHtmlPart(74)
expressionOut.print(trips.eventName)
printHtmlPart(75)
expressionOut.print(trips.eventVenue)
printHtmlPart(76)
expressionOut.print(trips.eventDate)
printHtmlPart(77)
expressionOut.print(trips.eventCode)
printHtmlPart(78)
expressionOut.print(trips.costCenter)
printHtmlPart(79)
})
invokeTag('form','g',325,['method':("post"),'action':("bookThisEvent"),'controller':("Events"),'role':("form")],5)
printHtmlPart(80)
}
printHtmlPart(81)
i++
}
}
printHtmlPart(82)
}
else {
printHtmlPart(83)
expressionOut.print(resource(dir:'images',file:'no_notofications.png'))
printHtmlPart(84)
}
printHtmlPart(85)
expressionOut.print(resource(dir:'assets/js',file:'jquery.min.js'))
printHtmlPart(86)
expressionOut.print(session["popUpDataAccept"])
printHtmlPart(87)

session['popUpDataAccept']="";

printHtmlPart(88)
expressionOut.print(session["popUpDataReject"])
printHtmlPart(89)

session['popUpDataReject']="";

printHtmlPart(90)

session['sidebarActive']="";
session['sidebarActive']="notifications/index";

printHtmlPart(91)
})
invokeTag('captureBody','sitemesh',419,[:],1)
printHtmlPart(92)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471537823291L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
