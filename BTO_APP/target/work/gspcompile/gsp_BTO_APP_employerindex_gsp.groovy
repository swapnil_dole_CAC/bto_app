import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_employerindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/employer/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'http-equiv':("Content-Type"),'content':("text/html; charset=ISO-8859-1")],-1)
printHtmlPart(2)
createTagBody(2, {->
createClosureForHtmlPart(3, 3)
invokeTag('captureTitle','sitemesh',5,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/css',file:'bootstrap.min.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/css',file:'components.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/css',file:'core.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/css',file:'icons.css'))
printHtmlPart(6)
expressionOut.print(resource(dir:'assets/css',file:'responsive.css'))
printHtmlPart(7)
expressionOut.print(resource(dir:'images',file:'verifiction-bg.png'))
printHtmlPart(8)
})
invokeTag('captureHead','sitemesh',44,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(12)
if(true && (flash.message1)) {
printHtmlPart(13)
expressionOut.print(flash.message1)
printHtmlPart(11)
}
printHtmlPart(14)
createClosureForHtmlPart(15, 2)
invokeTag('form','g',98,['class':("form-horizontal m-t-30"),'controller':("Employer"),'action':("authenticateEmployer"),'method':("post")],2)
printHtmlPart(16)
expressionOut.print(resource(dir:'assets/js',file:'jquery.min.js'))
printHtmlPart(17)
expressionOut.print(resource(dir:'assets/js',file:'bootstrap.min.js'))
printHtmlPart(18)
expressionOut.print(resource(dir:'assets/plugins/custombox/dist',file:'custombox.min.js'))
printHtmlPart(18)
expressionOut.print(resource(dir:'assets/plugins/custombox/dist',file:'legacy.min.js'))
printHtmlPart(19)
createClosureForHtmlPart(20, 2)
invokeTag('form','g',151,['method':("post"),'action':("forgotPassword"),'role':("form")],2)
printHtmlPart(21)
})
invokeTag('captureBody','sitemesh',152,[:],1)
printHtmlPart(22)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429498490L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
