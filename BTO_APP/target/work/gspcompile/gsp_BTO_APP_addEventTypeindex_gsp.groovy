import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_addEventTypeindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/addEventType/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
invokeTag('captureMeta','sitemesh',1,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(0)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css'))
printHtmlPart(2)
createClosureForHtmlPart(3, 1)
invokeTag('link','g',11,['controller':("AddEventType"),'action':("addEventType"),'class':("btn btn-success waves-effect waves-light"),'style':("float:right;")],1)
printHtmlPart(4)
if(true && (flash.message)) {
printHtmlPart(5)
expressionOut.print(flash.message)
printHtmlPart(6)
}
printHtmlPart(7)
if(true && (flash.message1)) {
printHtmlPart(8)
expressionOut.print(flash.message1)
printHtmlPart(6)
}
printHtmlPart(9)
loop:{
int i = 0
for( eventTypeInstance in (eventTypeInstanceList) ) {
printHtmlPart(10)
expressionOut.print(eventTypeInstance?.eventType)
printHtmlPart(11)
createClosureForHtmlPart(12, 2)
invokeTag('link','g',43,['action':("editEventType"),'id':(eventTypeInstance?.id),'title':("Edit")],2)
printHtmlPart(13)
createClosureForHtmlPart(14, 2)
invokeTag('link','g',46,['action':("deleteEventType"),'id':(eventTypeInstance?.id),'title':("Delete"),'onclick':("return confirm(' You want to delete?');")],2)
printHtmlPart(15)
i++
}
}
printHtmlPart(16)
expressionOut.print(resource(dir:'assets/js',file:'jquery.min.js'))
printHtmlPart(17)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js'))
printHtmlPart(17)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js'))
printHtmlPart(17)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js'))
printHtmlPart(18)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429497837L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
