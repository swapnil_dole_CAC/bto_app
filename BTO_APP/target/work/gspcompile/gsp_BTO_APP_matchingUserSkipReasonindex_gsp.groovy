import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_matchingUserSkipReasonindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/matchingUserSkipReason/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
invokeTag('captureMeta','sitemesh',1,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(0)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css'))
printHtmlPart(2)
createClosureForHtmlPart(3, 1)
invokeTag('link','g',21,['controller':("MatchingUserSkipReason"),'action':("addTripSkipReason"),'class':("btn btn-success waves-effect waves-light"),'style':("float:right;")],1)
printHtmlPart(4)
if(true && (flash.message)) {
printHtmlPart(5)
expressionOut.print(flash.message)
printHtmlPart(6)
}
printHtmlPart(7)
if(true && (flash.message1)) {
printHtmlPart(8)
expressionOut.print(flash.message1)
printHtmlPart(6)
}
printHtmlPart(9)
invokeTag('set','g',46,['var':("j"),'value':(1)],-1)
printHtmlPart(10)
loop:{
int i = 0
for( matchingUserSkipReasonInstance in (matchingUserSkipReasonInstanceList) ) {
printHtmlPart(11)
expressionOut.print(j++)
printHtmlPart(12)
expressionOut.print(matchingUserSkipReasonInstance?.reasonDetails)
printHtmlPart(13)
createClosureForHtmlPart(14, 2)
invokeTag('link','g',52,['action':("editSkipReason"),'id':(matchingUserSkipReasonInstance?.id),'title':("Edit")],2)
printHtmlPart(15)
createClosureForHtmlPart(16, 2)
invokeTag('link','g',53,['action':("deleteSkipReason"),'id':(matchingUserSkipReasonInstance?.id),'title':("Delete"),'onclick':("return confirm(' You want to delete?');")],2)
printHtmlPart(17)
i++
}
}
printHtmlPart(18)
expressionOut.print(resource(dir:'assets/js',file:'jquery.min.js'))
printHtmlPart(19)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js'))
printHtmlPart(19)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js'))
printHtmlPart(19)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js'))
printHtmlPart(20)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429499898L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
