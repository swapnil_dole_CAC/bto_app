import com.bto.vendor.Driver
import com.bto.vendor.RejectionReason
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_tripRequestsindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/tripRequests/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':("/"),'http-equiv':("Content-Type"),'content':("text/html; charset=ISO-8859-1")],-1)
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',7,['gsp_sm_xmlClosingForEmptyTag':(""),'http-equiv':("refresh"),'content':("60")],-1)
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',8,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("vendorMaster")],-1)
printHtmlPart(2)
createTagBody(2, {->
createClosureForHtmlPart(3, 3)
invokeTag('captureTitle','sitemesh',9,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',9,[:],2)
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css'))
printHtmlPart(6)
createTagBody(2, {->
createClosureForHtmlPart(3, 3)
invokeTag('captureTitle','sitemesh',15,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',15,[:],2)
printHtmlPart(7)
expressionOut.print(resource(dir:'assets/js',file:'jquery.min.js'))
printHtmlPart(8)
})
invokeTag('captureHead','sitemesh',104,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(12)
if(true && (flash.message1)) {
printHtmlPart(13)
expressionOut.print(flash.message1)
printHtmlPart(11)
}
printHtmlPart(12)
if(true && (requestedTrips)) {
printHtmlPart(14)
loop:{
int i = 0
for( trips in (requestedTrips.requestedTrips) ) {
printHtmlPart(15)
expressionOut.print(resource(dir:'assets/images/users',file:'avatar-1.jpg'))
printHtmlPart(16)
expressionOut.print(trips.name)
printHtmlPart(17)
expressionOut.print(trips.designation)
printHtmlPart(18)
expressionOut.print(trips.vendorCarType)
printHtmlPart(19)
expressionOut.print(trips.tripDate)
printHtmlPart(20)
expressionOut.print(trips.tripTime)
printHtmlPart(21)
expressionOut.print(resource(dir:'images',file:'from_to_icon.png'))
printHtmlPart(22)
expressionOut.print(trips.startLocation)
printHtmlPart(23)
expressionOut.print(trips.endLocation)
printHtmlPart(24)
expressionOut.print(i)
printHtmlPart(25)
expressionOut.print(i)
printHtmlPart(26)
expressionOut.print(i)
printHtmlPart(27)
expressionOut.print(i)
printHtmlPart(28)
expressionOut.print(i)
printHtmlPart(29)
createTagBody(4, {->
printHtmlPart(30)
expressionOut.print(trips.tripID)
printHtmlPart(31)
expressionOut.print(trips.name)
printHtmlPart(32)
expressionOut.print(trips.phone)
printHtmlPart(33)
expressionOut.print(trips.startLocation)
printHtmlPart(34)
expressionOut.print(trips.endLocation)
printHtmlPart(35)
for( driver in (Driver.list()) ) {
printHtmlPart(36)
expressionOut.print(driver.firstName)
printHtmlPart(37)
expressionOut.print(driver.lastName)
printHtmlPart(38)
expressionOut.print(driver.firstName)
printHtmlPart(37)
expressionOut.print(driver.lastName)
printHtmlPart(39)
}
printHtmlPart(40)
})
invokeTag('form','g',248,['method':("post"),'action':("acceptRequest"),'controller':("TripRequests"),'role':("form")],4)
printHtmlPart(41)
expressionOut.print(i)
printHtmlPart(42)
createTagBody(4, {->
printHtmlPart(30)
expressionOut.print(trips.tripID)
printHtmlPart(31)
expressionOut.print(trips.name)
printHtmlPart(32)
expressionOut.print(trips.phone)
printHtmlPart(33)
expressionOut.print(trips.startLocation)
printHtmlPart(34)
expressionOut.print(trips.endLocation)
printHtmlPart(43)
})
invokeTag('form','g',277,['method':("post"),'action':("acceptRequest"),'controller':("TripRequests"),'role':("form")],4)
printHtmlPart(44)
expressionOut.print(i)
printHtmlPart(45)
expressionOut.print(i)
printHtmlPart(46)
createTagBody(4, {->
printHtmlPart(30)
expressionOut.print(trips.tripID)
printHtmlPart(47)
expressionOut.print(trips.name)
printHtmlPart(48)
expressionOut.print(trips.phone)
printHtmlPart(49)
expressionOut.print(trips.startLocation)
printHtmlPart(50)
expressionOut.print(trips.endLocation)
printHtmlPart(51)
for( reason in (RejectionReason.list()) ) {
printHtmlPart(52)
expressionOut.print(reason.reasonDetails)
printHtmlPart(53)
expressionOut.print(reason.reasonDetails)
printHtmlPart(54)
}
printHtmlPart(55)
})
invokeTag('form','g',333,['method':("post"),'action':("rejectRequest"),'controller':("TripRequests"),'role':("form")],4)
printHtmlPart(56)
if(true && (trips.CoPasscount)) {
printHtmlPart(57)
loop:{
int j = 0
for( coTravellers in (trips.coTravellers) ) {
printHtmlPart(58)
expressionOut.print(coTravellers.name)
printHtmlPart(59)
expressionOut.print(coTravellers.startLocation)
printHtmlPart(60)
expressionOut.print(coTravellers.phone)
printHtmlPart(61)
j++
}
}
printHtmlPart(62)
}
printHtmlPart(63)
if(true && (trips.note!="")) {
printHtmlPart(64)
expressionOut.print(trips.note)
printHtmlPart(65)
}
printHtmlPart(66)
i++
}
}
printHtmlPart(67)
}
else {
printHtmlPart(68)
expressionOut.print(resource(dir:'images',file:'no_trip_requests.png'))
printHtmlPart(69)
}
printHtmlPart(70)
expressionOut.print(session["popUpData"])
printHtmlPart(71)

session['popUpData']="";

printHtmlPart(72)
expressionOut.print(session["popUpDataReject"])
printHtmlPart(73)

session['popUpDataReject']="";

printHtmlPart(74)

session['sidebarActive']="";
session['sidebarActive']="tripRequests/index";

printHtmlPart(75)
})
invokeTag('captureBody','sitemesh',455,[:],1)
printHtmlPart(76)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471431397853L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
