import com.bto.vendor.VendorBilling
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_vendorBillingeditPackage_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/vendorBilling/editPackage.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
invokeTag('captureMeta','sitemesh',1,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("vendorMaster")],-1)
printHtmlPart(0)
printHtmlPart(1)
if(true && (flash.message)) {
printHtmlPart(2)
expressionOut.print(flash.message)
printHtmlPart(3)
}
printHtmlPart(4)
if(true && (flash.message1)) {
printHtmlPart(5)
expressionOut.print(flash.message1)
printHtmlPart(3)
}
printHtmlPart(6)
createTagBody(1, {->
printHtmlPart(7)
invokeTag('hiddenField','g',31,['name':("id"),'value':(billInstance.id)],-1)
printHtmlPart(8)
if(true && (billInstance.packageType==1)) {
printHtmlPart(9)
}
else {
printHtmlPart(10)
}
printHtmlPart(11)
expressionOut.print(billInstance.kilometer)
printHtmlPart(12)
expressionOut.print(billInstance.hour)
printHtmlPart(13)
expressionOut.print(billInstance.amount)
printHtmlPart(14)
createClosureForHtmlPart(15, 2)
invokeTag('link','g',82,['action':("index"),'class':("btn btn-inverse waves-effect waves-light")],2)
printHtmlPart(16)
})
invokeTag('form','g',88,['action':("updateBillPackage"),'method':("post")],1)
printHtmlPart(17)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429500916L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
