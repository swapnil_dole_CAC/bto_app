import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_employerDashboardindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/employerDashboard/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',48,['gsp_sm_xmlClosingForEmptyTag':("/"),'http-equiv':("Content-Type"),'content':("text/html; charset=ISO-8859-1")],-1)
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',49,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
createTagBody(2, {->
createClosureForHtmlPart(3, 3)
invokeTag('captureTitle','sitemesh',50,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',50,[:],2)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',51,[:],1)
printHtmlPart(2)
createTagBody(1, {->
printHtmlPart(4)
expressionOut.print(createLinkTo(dir:'images',file:'active_user.png'))
printHtmlPart(5)
expressionOut.print(dashboardInstance.users)
printHtmlPart(6)
expressionOut.print(createLinkTo(dir:'images',file:'my_trips.png'))
printHtmlPart(7)
expressionOut.print(dashboardInstance.trips)
printHtmlPart(8)
expressionOut.print(createLinkTo(dir:'images',file:'distance-travelled.png'))
printHtmlPart(9)
expressionOut.print(dashboardInstance.distance)
printHtmlPart(10)
expressionOut.print(createLinkTo(dir:'images',file:'savings.png'))
printHtmlPart(11)
expressionOut.print(dashboardInstance.saving)
printHtmlPart(12)
expressionOut.print(createLinkTo(dir:'images',file:'Fuel-saved.png'))
printHtmlPart(13)
expressionOut.print(dashboardInstance.fuel)
printHtmlPart(14)
expressionOut.print(createLinkTo(dir:'images',file:'CO2-saving.png'))
printHtmlPart(15)
expressionOut.print(dashboardInstance.co2)
printHtmlPart(16)
})
invokeTag('captureBody','sitemesh',155,[:],1)
printHtmlPart(17)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1470906160920L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
