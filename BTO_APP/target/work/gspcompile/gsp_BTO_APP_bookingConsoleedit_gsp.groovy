import java.lang.String
import com.bto.customer.VendorDetails
import com.bto.trip.TravelPurpose
import com.bto.trip.TripDetails
import com.bto.customer.CostCenter
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_bookingConsoleedit_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bookingConsole/edit.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(0)
printHtmlPart(0)
printHtmlPart(0)
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(0)
invokeTag('captureMeta','sitemesh',9,['gsp_sm_xmlClosingForEmptyTag':("/"),'http-equiv':("Content-Type"),'content':("text/html; charset=ISO-8859-1")],-1)
printHtmlPart(0)
invokeTag('captureMeta','sitemesh',10,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(0)
createTagBody(2, {->
createClosureForHtmlPart(2, 3)
invokeTag('captureTitle','sitemesh',11,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',11,[:],2)
printHtmlPart(3)
expressionOut.print(resource(dir:'assets/plugins/timepicker',file:'bootstrap-timepicker.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/mjolnic-bootstrap-colorpicker/dist/css',file:'bootstrap-colorpicker.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/bootstrap-datepicker/dist/css',file:'bootstrap-datepicker.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/clockpicker/dist',file:'jquery-clockpicker.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/plugins/bootstrap-daterangepicker',file:'daterangepicker.css'))
printHtmlPart(5)
})
invokeTag('captureHead','sitemesh',65,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(6)
if(true && (flash.message)) {
printHtmlPart(7)
expressionOut.print(flash.message)
printHtmlPart(8)
}
printHtmlPart(9)
if(true && (flash.message1)) {
printHtmlPart(10)
expressionOut.print(flash.message1)
printHtmlPart(8)
}
printHtmlPart(9)
createTagBody(2, {->
printHtmlPart(11)
invokeTag('hiddenField','g',93,['name':("tripId"),'value':(tripInstance?.id)],-1)
printHtmlPart(12)
expressionOut.print(session.userID)
printHtmlPart(13)
expressionOut.print(tripInstance?.startLocation)
printHtmlPart(14)
expressionOut.print(tripInstance?.endLocation)
printHtmlPart(15)
for( purpose in (TravelPurpose.list()) ) {
printHtmlPart(16)
if(true && (tripInstance.purpose.equals(purpose.travelPurpose))) {
printHtmlPart(17)
expressionOut.print(purpose.travelPurpose)
printHtmlPart(18)
expressionOut.print(purpose.travelPurpose)
printHtmlPart(19)
}
else {
printHtmlPart(17)
expressionOut.print(purpose.travelPurpose)
printHtmlPart(20)
expressionOut.print(purpose.travelPurpose)
printHtmlPart(19)
}
printHtmlPart(21)
}
printHtmlPart(22)
expressionOut.print(tripInstance?.tripDate)
printHtmlPart(23)
expressionOut.print(tripTime)
printHtmlPart(24)
expressionOut.print(session['costCenter'])
printHtmlPart(25)
})
invokeTag('form','g',175,['controller':("BookingConsole"),'action':("updateTrip"),'method':("post")],2)
printHtmlPart(26)
expressionOut.print(resource(dir:'assets/js',file:'jquery.min.js'))
printHtmlPart(27)
expressionOut.print(jsonObject)
printHtmlPart(28)
})
invokeTag('captureBody','sitemesh',255,['onload':("check()")],1)
printHtmlPart(29)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429498082L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
