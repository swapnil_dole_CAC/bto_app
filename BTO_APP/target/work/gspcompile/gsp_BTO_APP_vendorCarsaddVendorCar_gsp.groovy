import com.bto.customer.CarType
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_vendorCarsaddVendorCar_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/vendorCars/addVendorCar.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
invokeTag('captureMeta','sitemesh',2,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("vendorMaster")],-1)
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/bootstrap-tagsinput/dist',file:'bootstrap-tagsinput.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/plugins/switchery/dist',file:'switchery.min.css'))
printHtmlPart(3)
expressionOut.print(resource(dir:'assets/plugins/multiselect/css',file:'multi-select.css'))
printHtmlPart(3)
expressionOut.print(resource(dir:'assets/plugins/select2',file:'select2.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/plugins/bootstrap-select/dist/css',file:'bootstrap-select.min.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/plugins/bootstrap-touchspin/dist',file:'jquery.bootstrap-touchspin.min.css'))
printHtmlPart(4)
if(true && (flash.message)) {
printHtmlPart(5)
expressionOut.print(flash.message)
printHtmlPart(6)
}
printHtmlPart(7)
if(true && (flash.message1)) {
printHtmlPart(8)
expressionOut.print(flash.message1)
printHtmlPart(6)
}
printHtmlPart(9)
createTagBody(1, {->
printHtmlPart(10)
for( carType in (CarType.list()) ) {
printHtmlPart(11)
expressionOut.print(carType.type)
printHtmlPart(12)
expressionOut.print(carType.type)
printHtmlPart(13)
}
printHtmlPart(14)
createClosureForHtmlPart(15, 2)
invokeTag('link','g',43,['action':("index"),'class':("btn btn-inverse waves-effect waves-light")],2)
printHtmlPart(16)
})
invokeTag('form','g',49,['action':("saveVendorCar"),'method':("post")],1)
printHtmlPart(17)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429501171L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
