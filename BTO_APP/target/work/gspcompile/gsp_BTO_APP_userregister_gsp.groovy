import com.bto.customer.CostCenter
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_userregister_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/user/register.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(0)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'http-equiv':("Content-Type"),'content':("text/html; charset=ISO-8859-1")],-1)
printHtmlPart(2)
createTagBody(2, {->
createClosureForHtmlPart(3, 3)
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',9,[:],2)
printHtmlPart(4)
expressionOut.print(resource(dir:'assets/css',file:'bootstrap.min.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/css',file:'components.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/css',file:'core.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/css',file:'icons.css'))
printHtmlPart(5)
expressionOut.print(resource(dir:'assets/css',file:'responsive.css'))
printHtmlPart(6)
expressionOut.print(resource(dir:'images',file:'reg-bg.jpg'))
printHtmlPart(7)
})
invokeTag('captureHead','sitemesh',88,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(8)
if(true && (flash.message)) {
printHtmlPart(9)
expressionOut.print(flash.message)
printHtmlPart(10)
}
printHtmlPart(11)
if(true && (flash.message1)) {
printHtmlPart(12)
expressionOut.print(flash.message1)
printHtmlPart(10)
}
printHtmlPart(13)
createTagBody(2, {->
printHtmlPart(14)
invokeTag('hiddenField','g',115,['name':("deviceType"),'value':("Web")],-1)
printHtmlPart(15)
for( costCenter in (CostCenter.list()) ) {
printHtmlPart(16)
expressionOut.print(costCenter.costCenterName)
printHtmlPart(17)
expressionOut.print(costCenter.costCenterName)
printHtmlPart(18)
}
printHtmlPart(19)
})
invokeTag('form','g',172,['class':("form-horizontal m-t-10"),'controller':("User"),'action':("saveUser"),'method':("post"),'onsubmit':("return validate();")],2)
printHtmlPart(20)
createClosureForHtmlPart(21, 2)
invokeTag('link','g',179,['class':("text-primary "),'action':("index")],2)
printHtmlPart(22)
})
invokeTag('captureBody','sitemesh',201,[:],1)
printHtmlPart(23)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429500263L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
