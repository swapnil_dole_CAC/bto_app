import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_rejectionReasonindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/rejectionReason/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
invokeTag('captureMeta','sitemesh',1,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("vendorMaster")],-1)
printHtmlPart(0)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/js',file:'jquery.min.js'))
printHtmlPart(3)
createClosureForHtmlPart(4, 1)
invokeTag('link','g',12,['controller':("RejectionReason"),'action':("addRejectionReason"),'class':("btn btn-success waves-effect waves-light"),'style':("float:right;")],1)
printHtmlPart(5)
if(true && (flash.message)) {
printHtmlPart(6)
expressionOut.print(flash.message)
printHtmlPart(7)
expressionOut.print(session["popUpDataCancel"])
printHtmlPart(8)

session['popUpDataCancel']="";

printHtmlPart(9)
}
printHtmlPart(10)
if(true && (flash.message1)) {
printHtmlPart(11)
expressionOut.print(flash.message1)
printHtmlPart(12)
expressionOut.print(session["popUpData"])
printHtmlPart(13)

session['popUpData']="";

printHtmlPart(14)
}
printHtmlPart(15)
loop:{
int i = 0
for( rejectionReasonInstance in (rejectionReasonInstanceList) ) {
printHtmlPart(16)
expressionOut.print(rejectionReasonInstance?.reasonDetails)
printHtmlPart(17)
createClosureForHtmlPart(18, 2)
invokeTag('link','g',96,['action':("editRejectionReason"),'id':(rejectionReasonInstance?.id),'title':("Edit")],2)
printHtmlPart(19)
createClosureForHtmlPart(20, 2)
invokeTag('link','g',97,['action':("deleteRejectionReason"),'id':(rejectionReasonInstance?.id),'title':("Delete"),'onclick':("return confirm(' You want to delete?');")],2)
printHtmlPart(21)
i++
}
}
printHtmlPart(22)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js'))
printHtmlPart(23)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js'))
printHtmlPart(23)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js'))
printHtmlPart(24)

session['sidebarActive']="";
session['sidebarActive']="rejectionReason/index";

printHtmlPart(25)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429500043L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
