import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_carTypeedit_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/carType/edit.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
invokeTag('captureMeta','sitemesh',1,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('hiddenField','g',10,['name':("id"),'value':(carTypeInstance?.id)],-1)
printHtmlPart(2)
expressionOut.print(carTypeInstance?.type)
printHtmlPart(3)
expressionOut.print(carTypeInstance?.capacity)
printHtmlPart(4)
expressionOut.print(carTypeInstance?.petrolMileage)
printHtmlPart(5)
expressionOut.print(carTypeInstance?.dieselMileage)
printHtmlPart(6)
expressionOut.print(carTypeInstance?.cngMileage)
printHtmlPart(7)
createClosureForHtmlPart(8, 2)
invokeTag('link','g',35,['action':("index"),'class':("btn btn-inverse waves-effect waves-light")],2)
printHtmlPart(9)
})
invokeTag('form','g',40,['action':("update")],1)
printHtmlPart(10)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471429498192L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
