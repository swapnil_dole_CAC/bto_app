import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_BTO_APP_tripRequestsrequestDetails_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/tripRequests/requestDetails.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
invokeTag('captureMeta','sitemesh',1,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("vendorMaster")],-1)
printHtmlPart(0)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css'))
printHtmlPart(1)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css'))
printHtmlPart(2)
expressionOut.print(resource(dir:'assets/js',file:'jquery.min.js'))
printHtmlPart(3)
if(true && (flash.message)) {
printHtmlPart(4)
expressionOut.print(flash.message)
printHtmlPart(5)
}
printHtmlPart(6)
if(true && (flash.message1)) {
printHtmlPart(7)
expressionOut.print(flash.message1)
printHtmlPart(5)
}
printHtmlPart(8)
if(true && (acceptedTrips.acceptedTrips!=null)) {
printHtmlPart(9)
loop:{
int i = 0
for( trips in (acceptedTrips.acceptedTrips) ) {
printHtmlPart(10)
expressionOut.print(resource(dir:'assets/images/users',file:'avatar-1.jpg'))
printHtmlPart(11)
expressionOut.print(trips.firstName)
printHtmlPart(12)
expressionOut.print(trips.lastName)
printHtmlPart(13)
expressionOut.print(resource(dir:'images',file:'from_to_icon.png'))
printHtmlPart(14)
expressionOut.print(trips.startLocation)
printHtmlPart(15)
expressionOut.print(trips.endLocation)
printHtmlPart(16)
expressionOut.print(trips.tripDate)
printHtmlPart(17)
expressionOut.print(trips.tripTime)
printHtmlPart(18)
expressionOut.print(trips.vendorCarType)
printHtmlPart(19)
expressionOut.print(i)
printHtmlPart(20)
expressionOut.print(i)
printHtmlPart(21)
createTagBody(3, {->
printHtmlPart(22)
expressionOut.print(trips.tripID)
printHtmlPart(23)
expressionOut.print(trips.firstName)
printHtmlPart(24)
expressionOut.print(trips.phone)
printHtmlPart(25)
expressionOut.print(trips.startLocation)
printHtmlPart(26)
expressionOut.print(trips.endLocation)
printHtmlPart(27)
})
invokeTag('form','g',156,['method':("post"),'action':("cancelAcceptedTrip"),'controller':("TripRequests"),'role':("form")],3)
printHtmlPart(28)
if(true && (trips.CoPasscount)) {
printHtmlPart(29)
loop:{
int j = 0
for( coTravellers in (trips.coTravellers) ) {
printHtmlPart(30)
expressionOut.print(coTravellers.name)
printHtmlPart(31)
expressionOut.print(coTravellers.startLocation)
printHtmlPart(32)
expressionOut.print(coTravellers.phone)
printHtmlPart(33)
j++
}
}
printHtmlPart(34)
}
printHtmlPart(35)
i++
}
}
printHtmlPart(36)
}
else {
printHtmlPart(37)
expressionOut.print(resource(dir:'images',file:'no_trips.png'))
printHtmlPart(38)
}
printHtmlPart(39)
if(true && (rejectedTrips.rejectedTrips!=null)) {
printHtmlPart(9)
loop:{
int i = 0
for( trips in (rejectedTrips.rejectedTrips) ) {
printHtmlPart(40)
expressionOut.print(resource(dir:'assets/images/users',file:'avatar-1.jpg'))
printHtmlPart(11)
expressionOut.print(trips.firstName)
printHtmlPart(41)
expressionOut.print(resource(dir:'images',file:'from_to_icon.png'))
printHtmlPart(14)
expressionOut.print(trips.startLocation)
printHtmlPart(15)
expressionOut.print(trips.endLocation)
printHtmlPart(16)
expressionOut.print(trips.tripDate)
printHtmlPart(17)
expressionOut.print(trips.tripTime)
printHtmlPart(18)
expressionOut.print(trips.vendorCarType)
printHtmlPart(42)
i++
}
}
printHtmlPart(43)
}
else {
printHtmlPart(37)
expressionOut.print(resource(dir:'images',file:'no_trips.png'))
printHtmlPart(44)
}
printHtmlPart(45)
if(true && (tripDetailsInstanceList!=null)) {
printHtmlPart(9)
loop:{
int i = 0
for( trips in (tripDetailsInstanceList) ) {
printHtmlPart(10)
expressionOut.print(resource(dir:'assets/images/users',file:'avatar-1.jpg'))
printHtmlPart(11)
expressionOut.print(trips.user.firstName)
printHtmlPart(12)
expressionOut.print(trips.user.lastName)
printHtmlPart(13)
expressionOut.print(resource(dir:'images',file:'from_to_icon.png'))
printHtmlPart(14)
expressionOut.print(trips?.startLocation)
printHtmlPart(15)
expressionOut.print(trips?.endLocation)
printHtmlPart(16)
expressionOut.print(trips?.tripDate)
printHtmlPart(17)
expressionOut.print(trips?.tripTime)
printHtmlPart(18)
expressionOut.print(trips?.carType)
printHtmlPart(46)
i++
}
}
printHtmlPart(36)
}
else {
printHtmlPart(37)
expressionOut.print(resource(dir:'images',file:'no_trips.png'))
printHtmlPart(47)
}
printHtmlPart(48)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js'))
printHtmlPart(49)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js'))
printHtmlPart(49)
expressionOut.print(resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js'))
printHtmlPart(50)

session['sidebarActive']="";
session['sidebarActive']="tripRequests/requestDetails";

printHtmlPart(51)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1471441135957L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
