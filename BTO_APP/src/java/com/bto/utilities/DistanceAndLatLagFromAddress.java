package com.bto.utilities;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.io.InputStreamReader;
import java.io.BufferedReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathConstants;

import org.w3c.dom.Document;

public class DistanceAndLatLagFromAddress {

	public static String[] getDisatnceLatlagFromAddress(String startLocation, String endLocation)throws Exception
	{
		String[] startlatLongs = getLatLongPositions(startLocation);
		String[] endlatLongs = getLatLongPositions(endLocation);
		double distance= getDisatnce(Double.parseDouble(startlatLongs[0]), Double.parseDouble(startlatLongs[1]), Double.parseDouble(endlatLongs[0]), Double.parseDouble(endlatLongs[1]));
		
		return new String[] {startlatLongs[0], startlatLongs[1],endlatLongs[0],endlatLongs[1],Double.toString(distance)};
		
	}
	
	// calculate distance for given lat long
	//http://maps.googleapis.com/maps/api/distancematrix/json?origins=kalyani%20nagar&destinations=karve%20nagar&mode=driving&sensor=false
	public static final double R = 6372.8; // In kilometers
	public static double getDisatnce(double lat1, double lon1, double lat2, double lon2) {
		double dLat = Math.toRadians(lat2 - lat1);
		double dLon = Math.toRadians(lon2 - lon1);
		lat1 = Math.toRadians(lat1);
		lat2 = Math.toRadians(lat2);
 
		double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
		double c = 2 * Math.asin(Math.sqrt(a));
		return R * c;
	}
	//==================end====================
	
	public static String[] getLatLongPositions(String address) throws Exception
	  {
	    int responseCode = 0;
	    String api = "https://maps.googleapis.com/maps/api/geocode/xml?address=" + URLEncoder.encode(address, "UTF-8") + "&sensor=true&key=AIzaSyBI9iSbSEo3h0LdqlqRFwnayYApQfmNXuE";
	   // System.out.print(api);
	    URL url = new URL(api);
	    HttpURLConnection httpConnection = (HttpURLConnection)url.openConnection();
	    httpConnection.connect();
	    responseCode = httpConnection.getResponseCode();
	    if(responseCode == 200)
	    {
	      DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();;
	      Document document = builder.parse(httpConnection.getInputStream());
	      XPathFactory xPathfactory = XPathFactory.newInstance();
	      XPath xpath = xPathfactory.newXPath();
	      XPathExpression expr = xpath.compile("/GeocodeResponse/status");
	      String status = (String)expr.evaluate(document, XPathConstants.STRING);
	      if(status.equals("OK"))
	      {
	         expr = xpath.compile("//geometry/location/lat");
	         String latitude = (String)expr.evaluate(document, XPathConstants.STRING);
	         expr = xpath.compile("//geometry/location/lng");
	         String longitude = (String)expr.evaluate(document, XPathConstants.STRING);
	         return new String[] {latitude, longitude};
	      }
	      else
	      {
	         throw new Exception("Error from the API - response status: "+status);
	      }
	    }
	    return null;
	  }
	
	public static void main(String[] args) throws Exception{
		// https://maps.googleapis.com/maps/api/directions/json?origin=karve%20nagar&destination=swarget
		// TODO Auto-generated method stub
		// System.out.println(getDisatnce(18.5018322, 73.8635912, 18.4897587, 73.8202962));
		System.out.println(Arrays.toString(getDisatnceLatlagFromAddress("Karve nagar, Pune", "swarget, Pune")));
	}

}
