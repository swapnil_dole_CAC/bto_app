package com.bto.utilities;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONException;
import org.codehaus.groovy.grails.web.json.JSONObject;

public class GetKMLFile {

	// Declaring all parameters
	static String start_address = "";
	static String startCoordinates = "";
	static String listcoordinates = "";
	static String end_address = "";
	static String endCoordinates = "";
	static String polyline = "";
	static String summary = "";
	static String fileName = "";
	
	//read and convert input stream
	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	// passing url and getting json data and generating KML File 
	public static String genarateKML(String startLocation,String endLocation, String tripID) throws IOException,JSONException {
		String URL="https://maps.googleapis.com/maps/api/directions/json?origin="+URLEncoder.encode(startLocation, "UTF-8")+"&destination="+URLEncoder.encode(endLocation, "UTF-8")+"";
		InputStream is = new URL(URL).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			
			
			JSONObject json = new JSONObject(jsonText);
			JSONObject jsonObject = new JSONObject(jsonText);
			
			// reference http://stackoverflow.com/questions/7237290/json-parsing-of-google-maps-api-in-android-app
			
			// routesArray contains ALL routes
			JSONArray routesArray = jsonObject.getJSONArray("routes");
			
			// Grab the first route
			JSONObject route = routesArray.getJSONObject(0);
			
			// Take all legs from the route
			JSONArray legs = route.getJSONArray("legs");
			
			// Grab first leg
			JSONObject leg = legs.getJSONObject(0);
			// get start address and end address from leg
			start_address = leg.getString("start_address");
			end_address = leg.getString("end_address");
			
			// get lat lng of start location
			JSONObject start_locationObject = leg.getJSONObject("start_location");
			startCoordinates = start_locationObject.getString("lat") + ","+ start_locationObject.getString("lng");

			// get lat lng of end location
			JSONObject end_locationObject = leg.getJSONObject("end_location");
			endCoordinates = end_locationObject.getString("lat") + ","+ end_locationObject.getString("lng");
			
			// Grab route and get points
			JSONObject overview_polyline = route.getJSONObject("overview_polyline");
			polyline = overview_polyline.getString("points");
		
			// Grab route and get summary
			summary = route.getString("summary");
			
			// array to gate all coordinates from steps
			JSONArray steps = leg.getJSONArray("steps");

			for (int i = 0; i < steps.length(); i++) {
				JSONObject step = steps.getJSONObject(i);
				listcoordinates += step.getJSONObject("start_location").getString("lat")+ ","+ step.getJSONObject("start_location").getString("lng")+ "\n ";
			}
		
			//write data in String
			String xmlString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
					+ "<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n"
					+ "<Document>\n\t<Placemark id=\"start\">" + "\n\t\t<name>"
					+ start_address
					+ "</name>\n"
					+ "\n\t\t<Point>"
					+ "<coordinates>"
					+ startCoordinates
					+ "</coordinates>"
					+ "</Point>\n"
					+ "\t</Placemark>\n\n"
					+ "<Placemark id=\"selectedRoute\">\n"
					+ "<name>Line</name>\n"
					+ "\t<Style>\n"
					+ "\t\t<LineStyle>\n"
					+ "\t\t<color>ffff0000</color>\n"
					+ "\t\t<width>5.000000</width>\n"
					+ "\t\t</LineStyle>\n"
					+ "\t</Style>\n"
					+ "<LineString>\n"
					+ "<altitudeMode>relative</altitudeMode>\n"
					+ "<coordinates>"
					+ listcoordinates
					+ "</coordinates>\n"
					+ "</LineString>\n</Placemark>\n"
					+ "<Placemark id=\"end\">\n"
					+ "<name>"
					+ end_address
					+ "</name>\n"
					+ "<Point>\n"
					+ "<coordinates>"
					+ endCoordinates
					+ "</coordinates>\n"
					+ "</Point>\n"
					+ "</Placemark>\n"
					+ "<Placemark id=\"polyline\">\n"
					+ "<name>"
					+ polyline
					+ "</name>\n"
					+ "</Placemark><Placemark id=\"routeName\">\n"
					+ "<name>"
					+ summary
					+ "</name>"
					+ "</Placemark>"
					+ "</Document>\n"
					+ "</kml>";
				return xmlString;
		} finally {
			is.close();
		}
	}

	public static void main(String[] args) {
		try {
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}