package com.bto.demo;

public class DistanceDemo {

	public static final double R = 6372.8; // In kilometers
	public static double haversine(double lat1, double lon1, double lat2, double lon2) {
		double dLat = Math.toRadians(lat2 - lat1);
		double dLon = Math.toRadians(lon2 - lon1);
		lat1 = Math.toRadians(lat1);
		lat2 = Math.toRadians(lat2);
 
		double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
		double c = 2 * Math.asin(Math.sqrt(a));
		return R * c;
	}
	public static void main(String[] args) {
		System.out.println(haversine(18.4897587, 73.8202962, 18.5018322, 73.8635912));
	}
	//https://maps.googleapis.com/maps/api/directions/json?origin=karve%20nagar&destination=swarget
	//http://maps.googleapis.com/maps/api/distancematrix/json?origins=Karve%20nagar&destinations=swarget&mode=car&language=en-EN&sensor=false
	//http://maps.googleapis.com/maps/api/distancematrix/json?origins=Karve%20nagar&destinations=swarget&sensor=false
}
