/*package com.bto.demo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONException;
import org.codehaus.groovy.grails.web.json.JSONObject;
 public class DirectionsJSONParser{
	 
public List<List<HashMap<String,String>>> parse(JSONObject jObject){

    List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String,String>>>() ;
    JSONArray jRoutes = null;
    JSONArray jLegs = null;
    JSONArray jSteps = null;    

    try {           

        jRoutes = jObject.getJSONArray("routes");

        *//** Traversing all routes *//*
        for(int i=0;i<jRoutes.length();i++){            
            jLegs = ( (JSONArray)jRoutes.get(i)).getJSONArray("legs");
            List path = new ArrayList<HashMap<String, String>>();

            *//** Traversing all legs *//*
            for(int j=0;j<jLegs.length();j++){
                jSteps = ( (JSONArray)jLegs.get(j)).getJSONArray("steps");

                *//** Traversing all steps *//*
                for(int k=0;k<jSteps.length();k++){

                    String html_instructions = jSteps.get(k).getString("html_instructions");
                    String travel_mode = jSteps.get(k).getString("travel_mode");
                    String maneuver = jSteps.get(k).getString("maneuver");

                    String distance_text = jSteps.get(k).getJSONObject("distance").getString("text");
                    String distance_value = jSteps.get(k).getJSONObject("distance").getString("value");

                    String duration_text = jSteps.get(k).getJSONObject("duration").getString("text");
                    String duration_value = jSteps.get(k).getJSONObject("duration").getString("value");

                    String start_lat = jSteps.get(k).getJSONObject("start_location").getString("lat");
                    String start_lon = jSteps.get(k).getJSONObject("start_location").getString("lng");

                    String end_lat = jSteps.get(k).getJSONObject("end_location").getString("lat");
                    String end_lon = jSteps.get(k).getJSONObject("end_location").getString("lng");

                    String polyline = "";
                    polyline = (String)((JSONObject)((JSONObject)jSteps.get(k)).get("polyline")).get("points");
                    List<LatLng> list = decodePoly(polyline);


                    *//** Traversing all points *//*
                    for(int l=0;l<list.size();l++){
                        HashMap<String, String> hm = new HashMap<String, String>();
                        hm.put("lat", Double.toString(((LatLng)list.get(l)).latitude) );
                        hm.put("lng", Double.toString(((LatLng)list.get(l)).longitude) );
                        path.add(hm);                       
                    }                               
                }
                routes.add(path);
            }
        }

    } catch (JSONException e) {         
        e.printStackTrace();
    }catch (Exception e){           
    }


    return routes;
}
}*/