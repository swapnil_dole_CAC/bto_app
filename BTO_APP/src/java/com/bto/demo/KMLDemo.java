package com.bto.demo;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONException;
import org.codehaus.groovy.grails.web.json.JSONObject;

public class KMLDemo {

	// Declaring all parameteres
	static String start_address = "";
	static String startCoordinates = "";
	static String listcoordinates = "";
	static String end_address = "";
	static String endCoordinates = "";
	static String polyline = "";
	static String summary = "";

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	public static JSONObject readJsonFromUrl(String url) throws IOException,
			JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONObject json = new JSONObject(jsonText);
			// JsonSlurper jslurp=new JsonSlurper();
			// Object obj=jslurp.parseText(jsonText);
			// Map jsonData=(Map)obj;
			// System.out.println(""+jsonData.get("routes[0]"));

			JSONObject jsonObject = new JSONObject(jsonText);
			// http://stackoverflow.com/questions/7237290/json-parsing-of-google-maps-api-in-android-app
			// routesArray contains ALL routes
			JSONArray routesArray = jsonObject.getJSONArray("routes");
			// Grab the first route
			JSONObject route = routesArray.getJSONObject(0);
			// Take all legs from the route
			JSONArray legs = route.getJSONArray("legs");
			// Grab first leg
			JSONObject leg = legs.getJSONObject(0);
			start_address = leg.getString("start_address");
			end_address = leg.getString("end_address");
			// System.out.println(leg.getString("start_address"));
			// System.out.println(leg.getString("end_address"));

			JSONObject start_locationObject = leg
					.getJSONObject("start_location");
			startCoordinates = start_locationObject.getString("lat") + ","
					+ start_locationObject.getString("lng");

			JSONObject end_locationObject = leg.getJSONObject("end_location");
			endCoordinates = end_locationObject.getString("lat") + ","
					+ end_locationObject.getString("lng");
			// System.out.println(endCoordinates);

			JSONObject overview_polyline = route
					.getJSONObject("overview_polyline");
			polyline = overview_polyline.getString("points");
			// System.out.println(polyline);

			summary = route.getString("summary");

			// System.out.println(summary);

			JSONArray steps = leg.getJSONArray("steps");

			for (int i = 0; i < steps.length(); i++) {
				JSONObject step = steps.getJSONObject(i);
				listcoordinates += step.getJSONObject("start_location")
						.getString("lat")
						+ ","
						+ step.getJSONObject("start_location").getString("lng")
						+ "\n ";
				// System.out.println(step.getJSONObject("start_location").getString("lat")+","+step.getJSONObject("start_location").getString("lng"));
			}
			// System.out.println(listcoordinates);

			String xmlString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
					+ "<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n"
					+ "<Document>\n\t<Placemark id=\"start\">" + "\n\t\t<name>"
					+ start_address
					+ "</name>\n"
					+ "\n\t\t<Point>"
					+ "<coordinates>"
					+ startCoordinates
					+ "</coordinates>"
					+ "</Point>\n"
					+ "\t</Placemark>\n\n"
					+ "<Placemark id=\"selectedRoute\">\n"
					+ "<name>Line</name>\n"
					+ "\t<Style>\n"
					+ "\t\t<LineStyle>\n"
					+ "\t\t<color>ffff0000</color>\n"
					+ "\t\t<width>5.000000</width>\n"
					+ "\t\t</LineStyle>\n"
					+ "\t</Style>\n"
					+ "<LineString>\n"
					+ "<altitudeMode>relative</altitudeMode>\n"
					+ "<coordinates>"
					+ listcoordinates
					+ "</coordinates>\n"
					+ "</LineString>\n</Placemark>\n"
					+ "<Placemark id=\"end\">\n"
					+ "<name>"
					+ end_address
					+ "</name>\n"
					+ "<Point>\n"
					+ "<coordinates>"
					+ endCoordinates
					+ "</coordinates>\n"
					+ "</Point>\n"
					+ "</Placemark>\n"
					+ "<Placemark id=\"polyline\">\n"
					+ "<name>"
					+ polyline
					+ "</name>\n"
					+ "</Placemark><Placemark id=\"routeName\">\n"
					+ "<name>"
					+ summary
					+ "</name>"
					+ "</Placemark>"
					+ "</Document>\n"
					+ "</kml>"

			;

			FileWriter out;
			try {
				out = new FileWriter("A.xml");

				out.write(xmlString);
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return json;
		} finally {
			is.close();
		}
	}

	public static void main(String[] args) {
		try {
			JSONObject json = readJsonFromUrl("https://maps.googleapis.com/maps/api/directions/json?origin=Kalyani%20Nagar,%20Pune,%20Maharashtra,%20India&destination=swarget");
			// System.out.println(json.toString());
			// System.out.println(json.get("steps"));
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}