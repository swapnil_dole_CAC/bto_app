package com.bto.trip

import java.util.Date;

import com.bto.user.User;

class EventDetails {

	//foreign key
	User user
	
	String eventName
	String eventType
	String eventVenue
	String eventDate
	String eventTime
	String costCenter
	String eventCode
	String eventStatus
	String description
	
	Date createdOn
	Date lastUpdatedOn
	
    static constraints = {
		user nullable:true
		eventType nullable:true 
		eventVenue nullable:true 
		eventDate nullable:true 
		eventTime nullable:true 
		costCenter nullable:true 
		eventCode nullable:true
		eventStatus nullable:true  
		description nullable:true 
		createdOn nullable:true
		lastUpdatedOn nullable:true
    }
}
