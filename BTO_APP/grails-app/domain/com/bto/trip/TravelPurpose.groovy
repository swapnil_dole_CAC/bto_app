package com.bto.trip

import java.util.Date;

class TravelPurpose {

	String travelPurpose
	Date createdOn
	Date lastUpdatedOn
	
    static constraints = {
		travelPurpose nullable:true 
		createdOn nullable:true
		lastUpdatedOn nullable:true
    }
}
