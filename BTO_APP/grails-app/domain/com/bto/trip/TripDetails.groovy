package com.bto.trip

import java.util.Date;

import com.bto.customer.CarType;
import com.bto.customer.VendorDetails;
import com.bto.user.User;

class TripDetails {

   //foreign key
	User user
	VendorDetails vendor
	String carType
	//table columns
	String startLocation
	String  startLat
	String  startLog
	String  endLocation
	String  endLat
	String  endLog
	String purpose
	Float tripDistance
	String tripStatus  //requested or booked or cancel or completed or scheduled
	String tripDate
	String  tripTime
	String routeName
	String costCenter
	String reason
	String eventCode
	String note
	
	Date createdOn
	Date lastUpdatedOn

	
	static constraints = {
		user nullable:true 
		vendor nullable:true 
		carType nullable:true 
		startLat nullable: true
		endLat nullable:true 
		startLog nullable: true
		endLog nullable:true  
		purpose nullable:true 
		tripDistance nullable:true
		tripStatus nullable:true
		tripDate nullable:true 
		tripTime nullable:true 
		routeName nullable:true 
		reason nullable:true 
		eventCode nullable:true
		note nullable:true  
		createdOn nullable:true 
		lastUpdatedOn nullable:true 
	}
	
}
