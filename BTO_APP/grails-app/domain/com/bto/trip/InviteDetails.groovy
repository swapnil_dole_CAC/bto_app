package com.bto.trip

import java.util.Date;

import com.bto.customer.VendorDetails;
import com.bto.user.User;

class InviteDetails {

   //foreign key
	TripDetails invitedByTripID
	TripDetails invitedToTripID
	User invitedByUserID
	User invitedToUserID
	VendorDetails vendor
	String status       //invite (default ,when invited), accept if 1 or reject if 0
	
	Date lastUpdatedOn
	Date createdOn
	
    static constraints = {
		invitedByTripID nullable:true 
		invitedToTripID nullable:true 
		invitedToUserID nullable:true
		status nullable:true 
		createdOn nullable:true
		lastUpdatedOn nullable:true
		vendor nullable:true
    }
		
}
