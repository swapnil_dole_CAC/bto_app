package com.bto.trip

import java.util.Date;

class LiveTripDetails {

  //foreign key
	TripDetails trip
	//table columns
	Boolean status
	
	Date lastUpdatedOn
	Date createdOn
	
    static constraints = {
		trip nullable:true 
		status nullable:true 
		createdOn nullable:true
		lastUpdatedOn nullable:true
    }
}
