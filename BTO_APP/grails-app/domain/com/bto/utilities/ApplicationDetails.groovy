package com.bto.utilities

import java.util.Date;

class ApplicationDetails {

   String androidVersion
	String iosVersion
	Integer androidDownloadCount
	Integer iosDownloadCount
	Date createdOn
	Date lastUpdatedOn
	
    static constraints = {
		androidVersion nullable:true 
		iosVersion nullable:true 
		androidDownloadCount nullable:true 
		iosDownloadCount nullable:true 
		createdOn nullable:true
		lastUpdatedOn nullable:true
    }
}
