package com.bto.utilities

class SMSGateway {

   
	String gatewayName
	String apiKey;
	String senderId
	String username
	String password
	String serviceName
	String url
	
	
    static constraints = {
		
		 gatewayName(nullable:true)
		 apiKey(nullable:true)
		 senderId(nullable:true)
		 username(nullable:true)
		 password(nullable:true)
		 serviceName(nullable:true)
		 url(nullable:true)
    }
}
