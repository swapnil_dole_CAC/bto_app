package com.bto.utilities

import java.util.Date;

class AppVersion {

    String deviceType
	String versionCode
	Date lastUpdatedOn
	Date createdOn
	
    static constraints = {
		deviceType nullable:true 
		versionCode nullable:true
		createdOn nullable:true
		lastUpdatedOn nullable:true
    }
}
