package com.bto.customer

import java.util.Date;

class MatchingUserSkipReason {

    String reasonDetails
	Date createdOn
	Date lastUpdatedOn
    static constraints = {
		reasonDetails nullable:true 
		createdOn nullable:true
		lastUpdatedOn nullable:true
    }
}
