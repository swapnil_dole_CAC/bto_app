package com.bto.customer

import java.util.Date;

class BillingPlan {	
	VendorDetails vendor
	Double amount
	Double kilometer
	Double hour
	Integer packageType
	Date createdOn
	Date lastUpdatedOn
	
    static constraints = {
		amount nullable:true
		kilometer nullable:true
		hour nullable:true
		packageType nullable:false
		createdOn nullable:false
		lastUpdatedOn nullable:false
    }
}
