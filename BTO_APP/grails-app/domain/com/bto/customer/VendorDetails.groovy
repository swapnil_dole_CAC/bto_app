package com.bto.customer

import java.util.Date;

class VendorDetails {

    String companyName
	String email
	String password
	String mobile
	String address
	String image
	String POCName
	String POCEmail
	String POCMobile
	String carTypes
	Date lastUpdatedOn
	Date createdOn
	
    static constraints = {
		carTypes nullable:true
		companyName nullable:true 
		email nullable:true
		password nullable:true  
		mobile nullable:true 
		address nullable:true 
		image nullable:true
		createdOn nullable:true
		lastUpdatedOn nullable:true
		carTypes nullable:true 
    }
}
