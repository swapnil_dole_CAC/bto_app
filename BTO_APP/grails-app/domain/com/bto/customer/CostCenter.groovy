package com.bto.customer

import java.util.Date;

class CostCenter {

   String costCenterName
	Date createdOn
	Date lastUpdatedOn
	
    static constraints = {
		costCenterName nullable:true 
		createdOn nullable:true
		lastUpdatedOn nullable:true
    }
}
