package com.bto.customer

import java.util.Date;

class CarType {

    //table columns
	String type
	Double petrolMileage
	Double dieselMileage
	Double cngMileage
	Date createdOn
	Date lastUpdatedOn
	Integer capacity
	
    static constraints = {
		petrolMileage nullable:true
		dieselMileage nullable:true
		cngMileage nullable:true
		createdOn nullable:true
		lastUpdatedOn nullable:true
		capacity nullable:true
    }
	
	//Default values to table
	static mapping = {
		petrolMileage defaultValue: 15
		dieselMileage defaultValue: 17
		cngMileage defaultValue: 18
	}
}
