package com.bto.customer

import java.util.Date;

class Employer {

	//	login credentials
	String emailID
	String password
	String role
// personal Details
	String firstName	
	String lastName
	String logo
	String gender
	String phone
	String city
	boolean enabled = true
	boolean accountExpired = false
	boolean passwordExpired = false
	boolean accountLocked = false
	Date createdOn
	Date lastUpdatedOn
	
   static constraints = {
		password blank: true,nullable:true
		firstName (nullable:true)
		lastName (nullable:true)
		emailID (nullable:false, unique: true)
		gender (nullable:true, inList:['Male','Female','Other'])
		phone (nullable:true)
		accountLocked (nullable:true)
		createdOn (nullable:true)
		lastUpdatedOn (nullable:true)
		logo (nullable:true)
		city nullable:true 
	}
}
