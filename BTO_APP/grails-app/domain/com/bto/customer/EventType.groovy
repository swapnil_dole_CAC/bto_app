package com.bto.customer

import java.util.Date;

class EventType {

    String eventType
	Date createdOn
	Date lastUpdatedOn
	
    static constraints = {
		eventType nullable:true 
		createdOn nullable:true
		lastUpdatedOn nullable:true
    }
}
