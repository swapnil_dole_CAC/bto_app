package com.bto.customer

class VehicleDetails {

    CarType car
	String carNumber
	String carPic
	String fuelType
	Date lastUpdatedOn
	Date createdOn
	
    static constraints = {
		carPic nullable:true
		createdOn nullable:true
		lastUpdatedOn nullable:true
		fuelType inList:['Petrol', 'Diesel','CNG']
    }
}
