package com.bto.user

class CorporateUser {

  String emailID
	String firstName
	String lastName
	Integer status
	
    static constraints = {
		emailID nullable:false,unique: true, blank:false, email:true
		firstName blank:false
		lastName blank:false
		status nullable:true
		}
}
