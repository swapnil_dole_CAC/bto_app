package com.bto.user

import java.lang.String.CaseInsensitiveComparator;
import java.util.Date;

class User {

    transient springSecurityService
	static transients = ['springSecurityService']
	
	// Login credentials
	String emailID
	String password
	// Personal Details
	String firstName
	String lastName
	String phone
	String costCenter
	Boolean status
	
	//after registration required fields
	String gender
	String countryCodePhone
	String dob
	String linkedInID
	String facebookID
	String profilePicture
	String department
	String designation
	String timeZone
	String address
	
	Date createdOn
	Date lastUpdatedOn

	static constraints = {
		emailID nullable:false,unique: true, blank:false, email:true
		password blank:false
		firstName blank:false,  nullable:true
		lastName blank:false,  nullable:true
		phone nullable:true
		costCenter  nullable:true
		status nullable:true
		
		gender nullable:true 
		countryCodePhone nullable:true 
		dob nullable:true 
		linkedInID nullable:true 
		facebookID nullable:true 
		profilePicture nullable:true 
		department nullable:true 
		designation nullable:true 
		timeZone nullable:true 
		address nullable:true 
		
		createdOn nullable:true
		lastUpdatedOn nullable:true
	}
	
	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		//password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}
}
