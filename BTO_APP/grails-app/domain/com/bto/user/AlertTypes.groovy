package com.bto.user

class AlertTypes {

   String alertType
	String message
	Date createdOn
	Date lastUpdatedOn

    static constraints = {
		
		alertType(nullable:false)
		message(nullable:false)
		createdOn(nullable:false)
		lastUpdatedOn(nullable:false)
		
    }
}
