package com.bto.user

import java.util.Date;

class UserDeviceToken {

   User user
	String deviceType
	String deviceToken
	Boolean status
	Date createdOn
	Date lastUpdatedOn
	String apitoken

		static constraints = {

		deviceType nullable:true
		deviceToken nullable:true
		status nullable:true
		createdOn nullable:true
		lastUpdatedOn nullable:true
		apitoken nullable:true		
	}
}
