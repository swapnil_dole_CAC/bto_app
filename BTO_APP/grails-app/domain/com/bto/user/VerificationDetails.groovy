package com.bto.user

import java.util.Date;

class VerificationDetails {

   User user
	String emailID
	String phoneVerificationCode
	Boolean isPhoneVerified
	Date createdOn
	Date lastUpdatedOn
	
    static constraints = {
		user nullable:true
		emailID nullable:true
		phoneVerificationCode nullable:true 
		isPhoneVerified nullable:true 
		createdOn nullable:true 
		lastUpdatedOn nullable:true  
    }
	
}
