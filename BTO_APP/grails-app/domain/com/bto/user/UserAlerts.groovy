package com.bto.user

import java.util.Date;

import com.bto.customer.VendorDetails;
import com.bto.trip.EventDetails;
import com.bto.trip.InviteDetails;
import com.bto.trip.TripDetails

class UserAlerts {

    User sender
	User receiver
	AlertTypes alertType
	String smsText
	String type
	TripDetails trip
	VendorDetails vendor
	InviteDetails invite
	String status="Unread"
	EventDetails event
	Date createdOn
	Date lastUpdatedOn
	
    static constraints = {
		
		sender(nullable:true)
		receiver(nullable:false)
		alertType(nullable:true)
		smsText(nullable:true)
		type(nullable:true,inList:['TripAlert', 'SMS','EventAlert','Account'])
		createdOn(nullable:true)
		trip(nullable:true)
		status(nullable:true,inList:['Read', 'Unread'])
		lastUpdatedOn(nullable:true)
		vendor nullable:true
		invite nullable:true
		event nullable:true 
    }
}
