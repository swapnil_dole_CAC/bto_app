package com.bto.vendor

import java.util.Date;

class Driver {

	
	// Personal Details
	String firstName
	String lastName
	String phone
	String image
	Date createdOn
	Date lastUpdatedOn
    static constraints = {
		firstName blank:false
		lastName blank:true
		phone nullable:true
		image nullable:true 
		createdOn nullable:true
		lastUpdatedOn nullable:true
    }
}
