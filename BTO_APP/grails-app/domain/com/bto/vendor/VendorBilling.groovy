package com.bto.vendor

import com.bto.customer.VendorDetails;

class VendorBilling {
	
	VendorDetails vendor
	Double amount
	Double kilometer
	Double hour
	Integer packageType
	Date createdOn
	Date lastUpdatedOn
	
    static constraints = {
		amount nullable:true
		kilometer nullable:true 
		hour nullable:true 
		packageType nullable:false 
		createdOn nullable:false 
		lastUpdatedOn nullable:false 
    }
}
