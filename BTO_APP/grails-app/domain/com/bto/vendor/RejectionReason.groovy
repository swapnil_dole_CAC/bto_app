package com.bto.vendor

import java.util.Date;

class RejectionReason {

    String reasonDetails
	Date createdOn
	Date lastUpdatedOn
    static constraints = {
		reasonDetails nullable:true 
		createdOn nullable:true
		lastUpdatedOn nullable:true
    }
}
