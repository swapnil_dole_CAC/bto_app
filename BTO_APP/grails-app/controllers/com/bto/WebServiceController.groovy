package com.bto

import com.bto.booking.BookingService
import com.bto.booking.EventService
import com.bto.booking.GetVendorAndCarDetailsService
import com.bto.booking.TripNotificationService
import com.bto.customer.VendorDetails
import com.bto.trip.InviteDetails
import com.bto.trip.TripDetails
import com.bto.user.User
import com.bto.user.UserAlerts
import com.bto.user.UserDeviceToken
import com.bto.user.VerificationDetails
import com.bto.utilities.SmsService
import com.bto.utilities.UtilitiesService
import com.google.android.gcm.server.Message
import com.google.android.gcm.server.Result
import com.google.android.gcm.server.Sender
import com.google.gson.JsonArray
import org.apache.commons.codec.binary.Base64;
import grails.converters.JSON
import java.nio.charset.Charset
import java.text.SimpleDateFormat
import javapns.Push
import javapns.notification.PushNotificationPayload
import org.apache.commons.lang.RandomStringUtils
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsWebRequest
import org.springframework.web.context.request.RequestContextHolder

class WebServiceController {

	def userService
	def bookingService
	def getVendorAndCarDetailsService
	def tripDetailsService
	def matchingUserService
	def tripAnalyticsService
	def eventService
	def smsService
	def tripNotificationService
	def dateTimeHelperService
	def grailsApplication
	def index() { }

	def registerUser={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json
		try {
			if(!params?.emailID){
				status=false
				responseData ="failed"
				errorList.add("Mail Required")
			}
			else if(!params?.password){
				status=false
				responseData="failed"
				errorList.add("Password Required")
			}
			else if(!params?.firstName){
				status=false
				responseData="failed"
				errorList.add("First Name Required")
			}
			else if(!params?.lastName){
				status=false
				responseData="failed"
				errorList.add("Last Name Required")
			}
			else if(!params?.phone){
				status=false
				responseData="failed"
				errorList.add("Mobile Number Required")
			}
			else if(!params?.deviceType){
				status=false
				responseData="failed"
				errorList.add("deviceType Required")
			}
			else if(!params?.costCenter){
				status=false
				responseData="failed"
				errorList.add("costCenter Required")
			}
			/*else if(!params?.gender){
				status=false
				responseData="failed"
				errorList.add("Gender Required")
			}*/
			else {

				def phone=params?.phone.toString()
				//def gender=params?.gender.toString()
				def email=params?.emailID.toString()
				//Email pattern against which given Email address will be check
				def emailPattern = /[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})/
				if(!email==emailPattern){
					status=false
					responseData="failed"
					errorList.add("Email Format Is Invalid")
				}
				else if(!(phone.isNumber())){
					status=false
					responseData="failed"
					errorList.add("Mobile number must be in number format")
				}
				else if(!(phone.length()==10)){
					status=false
					responseData="failed"
					errorList.add("Mobile number must be 10 digit")
				}
			}

			if(status) {
				json=userService.registerUser(params?.emailID,params?.password, params?.firstName,params?.lastName,params?.phone,params?.costCenter,params?.deviceType,params?.deviceToken)

			}
		}
		catch (Exception e) {
			e.printStackTrace()
		}
		finally {
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
				//return jsonData
			}
			else {
				render json as JSON
			}
		}
	}

	def authenticateUser={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json
		try {

			if(!params?.emailID){
				status=false
				responseData ="failed"
				errorList.add("Mail Required")
			}
			else if(!params?.password){
				status=false
				responseData="failed"
				errorList.add("Password Required")
			}
			else if(!params?.deviceToken){
				status=false
				responseData="failed"
				errorList.add("Device Token Required")
			}
			else if(!params?.deviceType){
				status=false
				responseData="failed"
				errorList.add("Device Type Required")
			}

			if(status) {
				json=userService.authenticateUser(params?.emailID,params?.password,params?.deviceToken,params?.deviceType)				
			}
		} catch (Exception e) {
			e.printStackTrace()
		}
		finally{
			if(status==false){
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
			}else{
				render json as JSON
			}
		}
	}

	def changePassword={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json
		try {

			if(!params?.emailID){
				status=false
				responseData="failed"
				errorList.add("Mail Required")
			}else if(!params?.oldPassword)
			{
				status=false
				responseData="failed"
				errorList.add("Old password incorrect")
			}
			else if(!params?.newPassword)
			{
				status=false
				responseData="failed"
				errorList.add("New password incorrect")
			}
			else if(!params?.confirmPassword)
			{
				status=false
				responseData="failed"
				errorList.add("Confirm password incorrect")
			}else if((params?.confirmPassword)!=(params?.newPassword))
			{
				status=false
				responseData="failed"
				errorList.add("Old and confirm password does not match..!")
			}else if((params?.oldPassword)==(params?.newPassword))
			{
				status=false
				responseData="failed"
				errorList.add("Old and new password must not be same..!")
			}
			
			if(status){
				//json=userService.changePassword(params?.emailID,params?.oldPassword,params?.newPassword,params?.confirmPassword)
				json=userService.changePassword(params?.emailID,params?.oldPassword,params?.newPassword)
			}

		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			if(status==false){
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
			}else{
				render json as JSON
			}
		}
	}

	def forgotPassword={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json
		try {

			if(!params?.emailID){
				status=false
				responseData="failed"
				errorList.add("Mail Required")
			}
			
			if(status){
				json=userService.forgetPassword(params?.emailID)
			}

		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			if(status==false){
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
			}else{
				render json as JSON
			}
		}
	}
	
	def userVerification={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			if(!params?.emailID){
				status=false
				responseData="failed"
				errorList.add("emailID Required")
			}else if(!params?.verificationCode){
				status=false
				responseData="failed"
				errorList.add("Verification Code Required")

			}else if(!params?.phone){
				status=false
				responseData="failed"
				errorList.add("phone number Required")
			}
			if(status) {
				json=userService.userVerification(params?.emailID,params?.verificationCode,params?.phone,params?.firstName)
			}

		} catch (Exception e) {
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
				//return jsonData
			}
			else {
				render json as JSON
			}
		}
	}

	def resendVerificationCode={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			if(!params?.emailID){
				status=false
				responseData="failed"
				errorList.add("emailID Required")
			}
			else if(!params?.phone){
				status=false
				responseData="failed"
				errorList.add("emailID Required")
			}
			else if(!params?.firstName){
				status=false
						responseData="failed"
						errorList.add("firstName Required")
			}
			if(status) {
				json=userService.resendVerificationCode(params?.emailID,params?.phone,params?.firstName)
			}
		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
				//return jsonData
			}
			else {
				render json as JSON
			}
		}
	}

	def bookTrip={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			if(!params?.startLocation){
				status=false
				responseData="failed"
				errorList.add("Start Location Required")
			}else if(!params?.endLocation){
				status=false
				responseData="failed"
				errorList.add("End Location Required")
			}else if(!params?.purpose){
				status=false
				responseData="failed"
				errorList.add("Purpose Required")
			}else if(!params?.tripDate){
				status=false
				responseData="failed"
				errorList.add("Trip Date Required")
			}else if(!params?.tripTime){
				status=false
				responseData="failed"
				errorList.add("Trip Time Required")
			}else if(!params?.userID){
				status=false
				responseData="failed"
				errorList.add("UserID Required")
			}else if(!params?.vendorID){
				status=false
				responseData="failed"
				errorList.add("vendorID Required")
			}else if(!params?.carType){
				status=false
				responseData="failed"
				errorList.add("carType Required")
			}else if(!params?.costCenter){
				status=false
				responseData="failed"
				errorList.add("Cost Center Required")
			}
			
			
			if(status) {
				json=bookingService.bookTrip(params?.startLocation,params?.endLocation,params?.purpose,params?.tripDate,params?.tripTime,params?.userID,params?.vendorID,params?.carType,params?.costCenter,params?.eventCode,params?.note)
			}

		} catch (Exception e) {
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
			}
			else {
				render json as JSON
			}
		}
	}
	
	def updateTrip={
		def trip
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			if(!params?.startLocation){
				status=false
				responseData="failed"
				errorList.add("Start Location Required")
			}else if(!params?.endLocation){
				status=false
				responseData="failed"
				errorList.add("End Location Required")
			}else if(!params?.purpose){
				status=false
				responseData="failed"
				errorList.add("Purpose Required")
			}else if(!params?.tripDate){
				status=false
				responseData="failed"
				errorList.add("Trip Date Required")
			}else if(!params?.tripTime){
				status=false
				responseData="failed"
				errorList.add("Trip Time Required")
			}else if(!params?.userID){
				status=false
				responseData="failed"
				errorList.add("UserID Required")
			}else if(!params?.vendorID){
				status=false
				responseData="failed"
				errorList.add("vendorID Required")
			}else if(!params?.carType){
				status=false
				responseData="failed"
				errorList.add("carType Required")
			}else if(!params?.costCenter){
				status=false
				responseData="failed"
				errorList.add("Cost Center Required")
			}
			else if(!params?.tripID)
			{
				status=false
				responseData="failed"
				errorList.add("Invalid Trip Id")
			}
			else
			{
				trip=TripDetails.get(Long.valueOf(params?.tripID))
				if(!trip)
				{
					status=false
					responseData="failed"
					errorList.add("Invalid Trip")
				}
			}
			
			if(status) {
				json=bookingService.updateTrip(params?.startLocation,params?.endLocation,params?.purpose,params?.tripDate,params?.tripTime,params?.userID,params?.vendorID,params?.carType,params?.costCenter,params?.eventCode,trip,params?.alertId)
			}

		} catch (Exception e) {
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
			}
			else {
				render json as JSON
			}
		}
	}
	
	def createEvent={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			if(!params?.userID){
				status=false
				responseData="failed"
				errorList.add("userID Required")
			}else if(!params?.eventName){
				status=false
				responseData="failed"
				errorList.add("eventName Required")
			}else if(!params?.eventType){
				status=false
				responseData="failed"
				errorList.add("eventType Required")
			}else if(!params?.eventVenue){
				status=false
				responseData="failed"
				errorList.add("eventVenue Required")
			}else if(!params?.eventDate){
				status=false
				responseData="failed"
				errorList.add("eventDate Required")
			}else if(!params?.eventTime){
				status=false
				responseData="failed"
				errorList.add("eventTime Required")
			}else if(!params?.costCenter){
				status=false
				responseData="failed"
				errorList.add("costCenter Required")
			}else if(!params?.eventCode){
				status=false
				responseData="failed"
				errorList.add("eventCode Required")
			}
			else if(!params?.description){
				status=false
				responseData="failed"
				errorList.add("description Required")
			}

			if(status) {
				json=eventService.createEvent(params?.userID,params?.eventName,params?.eventType,params?.eventVenue,params?.eventDate,params?.eventTime,params?.costCenter,params?.eventCode,params?.description)
			}

		} catch (Exception e) {
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
				//return jsonData
			}
			else {
				render json as JSON
			}
		}
	}
	
	def getEvents={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			if(!params?.userID){
				status=false
				responseData="failed"
				errorList.add("userID Required")
			}
			if(status) {
				json=eventService.getEvents(params?.userID)
				
			}

		} catch (Exception e) {
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
				//return jsonData
			}
			else {
				render json as JSON
			}
		}
	}
	
	def getDropDownList={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			if(status) {
				json=getVendorAndCarDetailsService.getDropDownList()				
			}

		} catch (Exception e) {
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
				//return jsonData
			}
			else {
				render json as JSON
			}
		}
	}

	def getPreviousTripDetails={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			
			if(!params?.userID){
				status=false
				responseData="failed"
				errorList.add("UserID Required")
			}
			
			if(status) {
				json=tripDetailsService.getPreviousTripDetails(params?.userID)
			}

		} catch (Exception e) {
		
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
				//return jsonData
			}
			else {
				render json as JSON
			}
		}
		
	}
		
	def getBookedTripHistory={
			response.setHeader("Access-Control-Allow-Origin", "*")
			def jsonData=[:]
			def requestData=[:]
			def responseData
			def status=true
			def errorList=[]
			def json
	
			try {
				
				if(!params?.userID){
					status=false
					responseData="failed"
					errorList.add("UserID Required")
				}
				
				if(status) {
					json=tripDetailsService.getBookedTripHistory(params?.userID)					
				}
	
			} catch (Exception e) {
			
				e.printStackTrace()
			}
			finally{
				if(status==false) {
					jsonData['request']=requestData
					jsonData['response']=responseData
					jsonData['status']=status
					jsonData['errorList']=errorList
					render jsonData as JSON
					//return jsonData
				}
				else {
					render json as JSON
				}
			}		
	}
	
	def cancelBookedTrip={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			
			if(!params?.tripID){
				status=false
				responseData="failed"
				errorList.add("tripID Required")
			}
			
			if(status) {
				json=tripDetailsService.cancelBookedTrip(params?.tripID)
			}

		} catch (Exception e) {
		
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
				//return jsonData
			}
			else {
				render json as JSON
			}
		}
}
	
	def scheduleTripWithReason={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			
			if(!params?.userID){
				status=false
				responseData="failed"
				errorList.add("UserID Required")
			}else if(!params?.tripId){
				status=false
				responseData="failed"
				errorList.add("tripId Required")
			}else if(!params?.reason){
				status=false
				responseData="failed"
				errorList.add("reason Required")
			}
			
			if(status) {
				json=tripDetailsService.scheduleTripWithReason(params?.userID,params?.tripId,params?.reason)
				
			}

		} catch (Exception e) {
		
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
				//return jsonData
			}
			else {
				render json as JSON
			}
		}	
}

	def deleteUser={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			if(!params?.emailID){
				status=false
				responseData="failed"
				errorList.add("emailID Required")
			}
			if(status) {
				json=userService.deleteUser(params?.emailID)
			}

		} catch (Exception e) {
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
				//return jsonData
			}
			else {
				render json as JSON
			}
		}
	}
	
	def getMatchingUsers={
		def jsonData = [:]
		def responseData
		def errorList = []
		def status = true
		
		def user,trip,requestedTrip
		def trips=[]
		def isValid=true,userCommunity
		def hasUsers=false
		
		List parsedList
		List tripList
		HashMap<String, String> map
		ArrayList<HashMap<String, String>> responseList=new ArrayList<HashMap<String, String>>()
		def InvitedUserCount=0
		def finalUsersCount=0
		try{
			if(!params?.tripId)
			{
				status=false
				responseData="failed"
				errorList.add("Trip Id not provided")
			}
			if(!params?.userID)
			{
				status=false
				responseData="failed"
				errorList.add("User Id not provided")
			}
			else
			{
				user=User.get(params?.userID)
				if(!user)
				{
					status=false
					responseData="failed"
					errorList.add("Invalid User")
				}
				else
				{
					requestedTrip=TripDetails.get(params?.tripId)
					if(!requestedTrip)
					{	status=false
						responseData="failed"
						errorList.add("Invalid Trip")
					}
				}
			}
			
			if(status)
			{
				//2.get Trip date
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				Date tripDate= formatter.parse(requestedTrip.tripDate)
				//3.Get Trip Time
				//4.Setting Time to tripDAte

				//5.Creating Current Time calendar object
				Calendar currentCal=Calendar.getInstance()
				currentCal.add(Calendar.MINUTE, -30)

				//6. Creating Trip_date Calendar Object
				Calendar tripCal=Calendar.getInstance()
				tripCal.setTime(tripDate)
				//7. Compare if tripDate is greater than currentDAte - 30 minutes
				if(tripCal.before(currentCal))
				{
					status=true
					responseData="Success"
					isValid=false
				}
				HashMap<String, Object> resultMap=matchingUserService.getMatchingUsers(requestedTrip)
				if(params?.alertId)
				{
					
					def isAlert=UserAlerts.get(Long.valueOf(params?.alertId))
					if(isAlert)
					{
						isAlert.status="Read"
						isAlert.lastUpdatedOn=new Date()
						isAlert.save(flush:true,failOnError:true)						
					}
				}
				ArrayList<TripDetails> userList=resultMap.get("trips")
				if(userList.size()== 0)
				{
					hasUsers=false
					status=false
				}
				else
				{
					for(int i=0 ; i<userList.size(); i++)
					{
						//hasUsers=true
						map =new HashMap<String,String>()
						def tempTrip=userList.get(i)
						def tempuser=User.get(tempTrip.user.id)
						
						//getting vendor name and car type from trip id
						def tripID=TripDetails.get(tempTrip.id)
						def tripVendor=VendorDetails.get(tripID.vendor.id)
						
						map.put("userID",tempuser.id )
						map.put("firstName", tempuser.firstName)
						map.put('lastName', tempuser.lastName)
						map.put("department",tempuser.department)
						map.put("designation",tempuser.designation)
						map.put("userProfilePic",tempuser.profilePicture)
						map.put("tripTime", tripDetailsService.convertTimeFromTwentyFourHoursToTwelveHours(tempTrip.tripTime))
						map.put('phone', tempuser.phone)
						map.put('tripID', tempTrip.id)
						map.put('vendorName', tripVendor.companyName)
						map.put('vendorImage', tripVendor.image)
						map.put('vendorCarType', tripID.carType)
						map.put("startLocation", tempTrip.startLocation)
						map.put("endLocation", tempTrip.endLocation)
						map.put("traveledBefore",tripAnalyticsService.travelledBeforeYou(user, tempuser)==true ? "Yes" : "NO")
					
						def minutes=matchingUserService.checkETA(requestedTrip.startLocation,tempTrip.startLocation)
						def isInvitedList=InviteDetails.findAllByInvitedByTripIDAndInvitedToTripID(requestedTrip,tempTrip)
						def flg=0,flg1=0	
						println requestedTrip
						for(InviteDetails isInvited:isInvitedList)
						{								
							if(isInvited.status=="reject" || isInvited.status=="Rejected"){
								flg1=1
								break;
							}
							else
							{
								if((minutes+15)<=Integer.parseInt(grailsApplication.config.grails.MaxETA))
								{
									finalUsersCount++
									InvitedUserCount++
								}
								else
									continue						
							}
							map.put('eta', minutes)
							if(isInvited.status=="Canceled")
							{
								flg=1
								break
								map.put("isInvited",false)
							}
							else
							{ 
								flg=1
								map.put("isInvited",true)
								break
								
							}
						}
						if(flg1==1)
						continue;
									
						if(flg==0)
						{	
							if(tempTrip.tripStatus=="reject" || tempTrip.tripStatus=="Rejected"){							
								continue
							}
							else
							{
								if((minutes+15)<=Integer.parseInt(grailsApplication.config.grails.MaxETA))
								{
									map.put("isInvited",false)
									finalUsersCount++
								}
								else
									continue
									
								map.put('eta', minutes)
							}
						}
						responseList.add(map)
					}
				}
				responseData="success"
			}
			jsonData['invitedUserCount']=InvitedUserCount
			if(finalUsersCount>0){
				hasUsers=true
				status=true
			}
			else{
				hasUsers=false
				status=false
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
		finally{
			jsonData['status'] = status
			jsonData['response'] = responseData
			jsonData['errorList'] = errorList
			if(status== true )
			{
				if(hasUsers)
					jsonData['users'] = responseList
			}
			render jsonData as JSON
			return
		}
	}
	
	def inviteUser={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			
			if(!params?.invitedByUserID){
				status=false
				responseData="failed"
				errorList.add("invitedByUserID Required")
			}else if(!params?.invitedToUserID){
				status=false
				responseData="failed"
				errorList.add("invitedToUserID Required")
			}else if(!params?.invitedByTripID){
				status=false
				responseData="failed"
				errorList.add("invitedByTripID Required")
			}else if(!params?.invitedToTripID){
				status=false
				responseData="failed"
				errorList.add("invitedToTripID Required")
			}
			
			if(status) {
				json=tripDetailsService.inviteUser(params?.invitedByUserID,params?.invitedToUserID,params?.invitedByTripID,params?.invitedToTripID)				
			}

		} catch (Exception e) {
		
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
			}
			else {
				render json as JSON
			}
		}
}
	
	def performInvitationAction={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			if(!params?.invitationId){
				status=false
				responseData="failed"
				errorList.add("invitationId Required")
			}
			else if(!params?.statusAction){
				status=false
				responseData="failed"
				errorList.add("statusAction Required")
			}else if(!(params?.statusAction=="0" || params?.statusAction=="1")){
				status=false
				responseData="failed"
				errorList.add("statusAction must be 0 or 1")
			}
			
			if(status) {
				json=tripDetailsService.performInvitationAction(params?.invitationId,params?.statusAction)
				def responseString=json.response
				
				if(responseString.equals("success"))
				{
					if(params?.alert_id)
					{
						InputStream is1 = new URL(grailsApplication.config.grails.webSite+"WebService/updateAlertRecord?alertId="+params?.alert_id).openStream()
						BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1, Charset.forName("UTF-8")));
						String jsonText1=UtilitiesService.readAll(rd1);
						JSONObject jsonBookData = new JSONObject(jsonText1);
						
						if(jsonBookData.getString("response")=="success") {
						}
					}
				}
			}

		} catch (Exception e) {		
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
				//return jsonData
			}
			else {
				render json as JSON
			}
		}
}
	
	def getUserProfile={
		def jsonData = [:]
		def requestData = [:]
		def userInfo = [:]
		def tripPref = [:]
		def travelPref = [:]
		def container = [:]
		def responseData
		def status = true
		def errorList = []
		def user
		def loginStatus

		try{
			if(!params?.userID)
			{
				status=false
				responseData="failed"
				errorList.add("User Id not provided")
			}
			else
			{
				user=User.get(params?.userID)
				if(!user)
				{
					status=false
					responseData="failed"
					errorList.add("User not found")
				}
			}
			if(status)
			{
				//On login success create JSON response with all user details
				status = true
				responseData = "success"
				def verifications= VerificationDetails.findByUser(user)
				userInfo['userID']=user.id				
				userInfo['emailID']= user.emailID
				
				// creating UseriIfo Json Object
				userInfo['firstName']=user.firstName
				userInfo['lastName']=user.lastName
				
				userInfo['phone']=user.phone

				if(user.department)
				userInfo['department']=user.department
				else
				userInfo['department']=""
			
				if(user.designation)
				userInfo['designation']=user.designation
				else
				userInfo['designation']= ""
				
				if(user.costCenter)
				userInfo['costCenter']=user.costCenter
				else
				userInfo['costCenter']= ""
				
				if(user.gender)
				userInfo['gender']=user.gender
				else
				userInfo['gender']=""
				
				if(user.dob)
				userInfo['dob']=user.dob
				else
				userInfo['dob']= ""
				
				if(user.address)
					userInfo['address']=user.address
				else
					userInfo['address']=""
				
				userInfo['isPhoneVerified']= verifications.isPhoneVerified
					
					
				if(user.countryCodePhone)
				userInfo['countryCodePhone']=user.countryCodePhone
				else
				userInfo['countryCodePhone']=""

				if(user.profilePicture )
				{
					def url=grailsApplication.config.grails.webSite+"images/userProfilePictures/"+user.profilePicture
					userInfo['profilePic']=url
				}
				else
					userInfo['profilePic']=""
					
				//Adding Account Status in response
				if(verifications.isPhoneVerified==true)
				{
					userInfo['accountStatus']=true
				}
				else
				{
					userInfo['accountStatus']=false
				}
				requestData['profileDetails']= userInfo
				//requestData['userData']= container
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
		finally{
			jsonData['status'] = status
			jsonData['request'] = requestData
			jsonData['response'] = responseData
			jsonData['errorList'] = errorList
			render jsonData as JSON
			return
		}
	}

	//Web Service to add or updated user profile
	def updateUserProfile={
		def jsonData = [:]
		def requestData= [:]
		def responseData
		def errorList = []
		def status = true
		def isPhoneChanged=false
		def isEmaileChanged=false
		def profilePic=false
		def random = new Random()
		byte[] imageByteArray
		FileOutputStream imageOutFile
		String imageDataString
		byte[] imageByte
		def user
		try
		{
			// code to auto-generate characters to rename image files..for Security and uniqueness
			def charset = (('A'..'Z') + ('0'..'9')).join()
			def length = 5
			def randomString= RandomStringUtils.random(length, charset.toCharArray())
			def emailPattern = /[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})/

			if(!params?.emailID)
			{
				status = false
				responseData = "failed"
				errorList.add("Email not Provided")
			}
			else if(params?.profileImage)
			{
				if(!params?.profilePicContentType)
				{
					status = false
					responseData = "failed"
					errorList.add("Image Content Type not Provided")
				}
			}
			
			user=User.findByEmailID(params?.emailID)
			if(user && status)
			{
				// gender update
				if(params?.gender)
				{
					if('male'?.equalsIgnoreCase(params?.gender))
						user.gender = "Male"
					else if('female'?.equalsIgnoreCase(params?.gender))
						user.gender = "Female"
					else if('other'?.equalsIgnoreCase(params?.gender))
						user.gender = "Other"
				}


				if(params?.firstName)
					user.firstName=params?.firstName

				if(params?.lastName)
					user.lastName=params?.lastName

				if(params?.designation)
					user.designation=params?.designation

				if(params?.costCenter)
					user.costCenter=params?.costCenter

				if(params?.department)
					user.department=params?.department

				if(params?.countryCodePhone)
					user.countryCodePhone=params?.countryCodePhone

				if(params?.dob)
					user.dob=params?.dob

				if(params?.address)
					user.address=params?.address


				def verifications= VerificationDetails.findByEmailID(params?.emailID)
				//Checking phone number changed or not
				if(params?.phone.equals(user.phone))
				{}
				else if(params?.phone)
				{
					user.phone=params?.phone
					verifications.isPhoneVerified=false
					def randomInt = random.nextInt(2000-1000+1)+1000
					verifications.phoneVerificationCode=randomInt.toString()
					verifications.isPhoneVerified=false
					verifications.save(flush: true,failonError:true)
					smsService.sendSMS("Dear "+user.firstName+", You have changed your mobile Number !\n Please reverify your mobile number. \n Your mobile verification code is "+ randomInt+"\n Thank you!"+"\n cloudacar.org Team.", params?.phone,"91")
				}
						
					if(params?.profileImage)
					{
						if(params?.profilePicContentType)
						{
							//Checking whether Profile_Pic already Provided,If YES then deleting that one first
							if(user.profilePicture)
							{
								try
								{ 
									//Deleting old profile pic
									File imageFileTOBeDeleted=new File(getServletContext().getRealPath("/")+"images/userProfilePictures/"+user.profilePicture)
									imageFileTOBeDeleted.delete()
								}
								catch(Exception e)
								{
									println "Exception occured,while deleting Profile_Pic file From Server"
								}
							}
							//Create name for profile pic
							def profilePicturefileName= user.id+"."+params?.profilePicContentType
							user.profilePicture=profilePicturefileName
							//Decode Base64 image bytes
							imageDataString =params?.profileImage
							imageByteArray = Base64.decodeBase64(imageDataString.replaceAll("\\s",""));
							imageOutFile = new FileOutputStream(getServletContext().getRealPath("/")+"images/userProfilePictures/"+profilePicturefileName);
							profilePic=true
						}
						else
						{
							status = false
							responseData = "failed"
							errorList.add("profilePicContentType not Provided")
						}
					}
										
					user.lastUpdatedOn=new Date()

					if(user){
						if(user.save(flush:true,failOnError:true))
						{
							if(profilePic)
							{
								//Save profile pic
								imageOutFile.write(imageByteArray);
								imageOutFile.close();
							}
							status = true
							responseData = "Success"
						}
						else
						{
							status = false
							responseData = "failed"
							errorList.add("Failed to Update data")
						}
					}
					else
					{
						status = false
						responseData = "failed"
						errorList.add("Problem in Update data")
					}		
			}
			else
			{
				status = false
				responseData = "failed"
				errorList.add("User not Found,Invalid Email")
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
			status = false
			errorList.add("Internal server error")
		}
		finally{

			jsonData['status'] = status
			jsonData['response'] = responseData
			jsonData['errorList'] = errorList
			jsonData['request'] = requestData
			render jsonData as JSON
			return
		}
	}
	
	def getNotification={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			if(!params?.userID){
				status=false
				responseData="failed"
				errorList.add("userID Required")
			}
			if(status) {
				json=tripNotificationService.getNotification(params?.userID)
			}

		} catch (Exception e) {
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
				//return jsonData
			}
			else {
				render json as JSON
			}
		}
	}
	
	def getInviteDetails={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			if(!params?.userID){
				status=false
				responseData="failed"
				errorList.add("userID Required")
			}
			if(status) {
				json=tripDetailsService.getInviteDetails(params?.userID)				
			}

		} catch (Exception e) {
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
				//return jsonData
			}
			else {
				render json as JSON
			}
		}
	}
	
	def getAllRequests={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			if(!params?.vendorID){
				status=false
				responseData="failed"
				errorList.add("vendorID Required")
			}
			if(status) {
				json=tripDetailsService.getAllRequests(params?.vendorID)				
			}

		} catch (Exception e) {
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
				//return jsonData
			}
			else {
				render json as JSON
			}
		}
	}
	
	//For Vendor Panel
	def getAcceptedTrips={
			response.setHeader("Access-Control-Allow-Origin", "*")
			def jsonData=[:]
			def requestData=[:]
			def responseData
			def status=true
			def errorList=[]
			def json
	
			try {
				if(!params?.vendorID){
					status=false
					responseData="failed"
					errorList.add("vendorID Required")
				}
				if(status) {
					json=tripDetailsService.getAcceptedTrips(params?.vendorID)
					
				}
	
			} catch (Exception e) {
				e.printStackTrace()
			}
			finally{
				if(status==false) {
					jsonData['request']=requestData
					jsonData['response']=responseData
					jsonData['status']=status
					jsonData['errorList']=errorList
					render jsonData as JSON
					//return jsonData
				}
				else {
					render json as JSON
				}
			}
	}
	
	//For Vendor Panel	
	def getRejectedTrips={
			response.setHeader("Access-Control-Allow-Origin", "*")
			def jsonData=[:]
			def requestData=[:]
			def responseData
			def status=true
			def errorList=[]
			def json
	
			try {
				if(!params?.vendorID){
					status=false
					responseData="failed"
					errorList.add("vendorID Required")
				}
				if(status) {
					json=tripDetailsService.getRejectedTrips(params?.vendorID)
					
				}
	
			} catch (Exception e) {
				e.printStackTrace()
			}
			finally{
				if(status==false) {
					jsonData['request']=requestData
					jsonData['response']=responseData
					jsonData['status']=status
					jsonData['errorList']=errorList
					render jsonData as JSON
					//return jsonData
				}
				else {
					render json as JSON
				}
			}
	}
	
	def updateAlertRecord={
		def jsonData=[:]
		def responseData
		def status=true
		def errorList=[]
		def alert
		try{
			if(!params?.alertId)
			{
				status=false
				responseData="failed"
				errorList.add("Invalid Alert ID")
			}
			else
			{
				alert=UserAlerts.get(Long.parseLong(params?.alertId))
				if(!alert)
				{
					status=false
					responseData="failed"
					errorList.add("Invalid Alert")
				}	
			}
			if(status)
			{
				alert.status="Read"
				alert.lastUpdatedOn=new Date()
				if(alert.save(flush:true,failOnError:true))
				{
					responseData="success"					
				}
			}
		}
		catch(Exception e){
			e.printStackTrace()
		}
		finally{
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			render jsonData as JSON
			return
		}
	}
	
	def cancelEvent={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json

		try {
			
			if(!params?.eventId){
				status=false
				responseData="failed"
				errorList.add("eventId Required")
			}
			
			if(status) {
				json=eventService.cancelEvent(params?.eventId)
			}

		} catch (Exception e) {
		
			e.printStackTrace()
		}
		finally{
			if(status==false) {
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
				//return jsonData
			}
			else {
				render json as JSON
			}
		}
}
}
