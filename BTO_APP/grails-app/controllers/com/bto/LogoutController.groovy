package com.bto

class LogoutController {

	def index() {


		//flash.message="Please Login...!"
		if(session['role']=="User"){
			session.invalidate()
			redirect(controller:"User",action:"index")
		}else if(session['role']=="vendorAdmin"){
			session.invalidate()
			redirect(controller:"Vendor",action:"index")
		}else if(session['role']=="employerAdmin"){
			session.invalidate()
			redirect(controller:"Employer",action:"index")
		}


	}
}
