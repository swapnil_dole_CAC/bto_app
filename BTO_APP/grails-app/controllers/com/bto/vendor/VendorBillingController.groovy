package com.bto.vendor

import com.bto.customer.VendorDetails;

import groovy.json.JsonSlurper

class VendorBillingController {

	def billingService
    def index() { 
		try{
			def vendorId=session["userID"]
			def vendor=VendorDetails.get(vendorId)
			if(vendor)
			{
				def billPlans=VendorBilling.findAllByVendor(vendor)
				respond 'c', model:[billPlans: billPlans]
			}
		}catch(Exception e)
		{
			e.printStackTrace()
		}
	}
	
	def addBillPackage(){}
	
	def saveBillPackage(){
		def vendor
		try{
			if(session['userID'])
			{
				vendor=VendorDetails.get(session['userID'])
				if(!vendor)
				{
					flash.message="Session time out"
					redirect controller:"Logout"
				}
				else
				{
					if(params?.packageType && params?.packageType.equals("1"))
					{
						def json=billingService.saveBillPackage(params?.packageType,params?.kilometer,params?.amount,vendor,"")
						if(json.response=="success")
						{
							flash.message1="Package Added"
							redirect action:"index"
						}	
						else
						{
							flash.message=json.errorList[0]
							redirect action:"addBillPackage"
						}
					}
					else
					{
						def json=billingService.saveBillPackage(params?.packageType,params?.kilometer,params?.amount,vendor,params?.hour)
						if(json.response=="success")
						{
							flash.message1="Package Added"
							redirect action:"index"
						}
						else
						{
							flash.message=json.errorList[0]
							redirect action:"addBillPackage"
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}

	def editPackage(VendorBilling billInstance){
		respond "c", model: [billInstance:billInstance]
	}	
	
	def updateBillPackage(VendorBilling billInstance)
	{
		def vendor
		try{
			if(session['userID'])
			{
				vendor=VendorDetails.get(session['userID'])
				if(!vendor)
				{
					flash.message="Session time out"
					redirect controller:"Logout"
				}
				else
				{
					if(params?.packageType && params?.packageType.equals("1"))
					{
						def json=billingService.updateBillPackage(billInstance,params?.packageType,params?.kilometer,params?.amount,vendor,"")
						if(json.response=="success")
						{
							flash.message1="Package Updated"
							redirect action:"editPackage"
						}
						else
						{
							flash.message=json.errorList[0]
							redirect action:"addBillPackage"
						}
					}
					else
					{
						def json=billingService.updateBillPackage(billInstance,params?.packageType,params?.kilometer,params?.amount,vendor,params?.hour)
						if(json.response=="success")
						{
							flash.message1="Package Updated"
							redirect action:"index"
						}
						else
						{
							flash.message=json.errorList[0]
							redirect action:"addBillPackage"
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}

	def deleteBillPackage(VendorBilling billInstance)
	{
		try{
			def json=billingService.deleteBillPackage(billInstance)
			if(json.response=="success")
			{
				flash.message1="Package Deleted"
				redirect action:"index"
			}
			else
			{
				flash.message=json.errorList[0]
				redirect action:"index"
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}	
}
