package com.bto.vendor
import com.bto.customer.VendorDetails;
import com.bto.trip.TripDetails;
import com.bto.utilities.SmsService;

import groovy.json.JsonSlurper

import java.nio.charset.Charset;

import org.codehaus.groovy.grails.web.json.JSONObject

import com.bto.customer.VendorDetails;
import com.bto.trip.InviteDetails;
import com.bto.trip.TripDetails;
import com.bto.user.AlertTypes;
import com.bto.user.User;
import com.bto.user.UserAlerts
import com.bto.utilities.UtilitiesService;
class TripRequestsController {

	def smsService
	def notificationService
	
	def index() {
		try{
			def user=User.get(session['userID'])
				
			InputStream is1 = new URL(grailsApplication.config.grails.webSite+"WebService/getAllRequests?vendorID="+session['userID']).openStream();
			BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1, Charset.forName("UTF-8")));
			String jsonText1=UtilitiesService.readAll(rd1);
			JSONObject jsonBookData = new JSONObject(jsonText1);
			if(jsonBookData.getString("response")=="success")
			{
				respond "c", model: [requestedTrips:jsonBookData]
			}
		}
		catch(Exception e){
			e.printStackTrace()
		}
	}
	
	def requestDetails(){
		def user=VendorDetails.get(session['userID'])
			
		InputStream is1 = new URL(grailsApplication.config.grails.webSite+"WebService/getAcceptedTrips?vendorID="+session['userID']).openStream();
		BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1, Charset.forName("UTF-8")));
		String jsonText1=UtilitiesService.readAll(rd1);
		JSONObject jsonBookData = new JSONObject(jsonText1);
		
		InputStream is = new URL(grailsApplication.config.grails.webSite+"WebService/getRejectedTrips?vendorID="+session['userID']).openStream();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
		String jsonText=UtilitiesService.readAll(rd);
		JSONObject jsonBookData1 = new JSONObject(jsonText);
		
		if(jsonBookData.getString("response")=="success" && jsonBookData1.getString("response")=="success") {
			
			
			respond TripDetails.findAllByVendorAndTripStatus(user,"Canceled"), model: [TripDetailsInstanceCount: TripDetails.count(),acceptedTrips:jsonBookData,rejectedTrips:jsonBookData1]
			//respond TripDetails.findAllByUserAndTripStatus(user,"scheduled"), model:[TripDetailsInstanceCount: TripDetails.count(),jsonObject:jsonBookData]
		}else{
			respond TripDetails.findAllByVendorAndTripStatus(user,"Canceled"), model: [TripDetailsInstanceCount: TripDetails.count(),acceptedTrips:jsonBookData,rejectedTrips:jsonBookData1]		
		}
		
	}
	
	def acceptRequest(){
		try{
			
			def vendor
			def trip=TripDetails.get(params?.tripID)		
			if(trip){				
				trip.tripStatus="scheduled"
				trip.lastUpdatedOn=new Date()
				def allInvitedByTrips=InviteDetails.findAllByInvitedToTripID(trip)
				for(InviteDetails invitedTrip:allInvitedByTrips)
				{
					def isTripFound=TripDetails.get(invitedTrip.invitedByTripID.id)
					if(isTripFound)
					{
						if(invitedTrip.status.equals("accept"))
						{
							isTripFound.tripStatus="scheduled"
							isTripFound.lastUpdatedOn=new Date()
							isTripFound.save(flush:true,failOnError:true)
							def driverName=params?.driverName
							def phone=params?.phone
							
							def userAlert=new UserAlerts()
							userAlert.type="TripAlert"
							def alertId= AlertTypes.findByAlertType("AcceptedByVendor")
							if(alertId)
								userAlert.alertType=alertId
								userAlert.createdOn=new Date()
								userAlert.status="Unread"
								userAlert.trip=isTripFound
								userAlert.receiver=invitedTrip.invitedByUserID
								if(invitedTrip.vendor)
									userAlert.vendor=invitedTrip.vendor
									userAlert.createdOn=new Date()
									userAlert.lastUpdatedOn=new Date()
									userAlert.smsText=trip.vendor.companyName+" "+alertId.message
								if(userAlert.save(flush:true,failOnError:true))
								{
									Map userData=new HashMap<String,String>()
									userData.put("alertType", userAlert.alertType.alertType)
									userData.put("tripId", userAlert.trip.id)
									userData.put("notificationID", userAlert.id)
									def message= userAlert.alertType.message
									userData.put("message",trip.vendor.companyName+" "+message)
									notificationService.sendPushNotification(message, "acceptedByVendor", invitedTrip.invitedByUserID,userData)
								}
						}
					}
				}
				if(trip.save(flush:true,failOnError:true))
				{
					def driverName=params?.driverName
					def phone=params?.phone
					
					def inviteDetails=InviteDetails.findByInvitedByTripID(trip)
					if(inviteDetails)
					{
						//Save and send alert
						def userAlert=new UserAlerts()
							userAlert.type="TripAlert"
							def alertId= AlertTypes.findByAlertType("AcceptedByVendor")
							if(alertId)
								userAlert.alertType=alertId
								userAlert.createdOn=new Date()
								userAlert.status="Unread"
								userAlert.trip=inviteDetails.invitedByTripID
								userAlert.receiver=inviteDetails.invitedByUserID
								if(inviteDetails.vendor)
									userAlert.vendor=inviteDetails.vendor
									userAlert.createdOn=new Date()
									userAlert.lastUpdatedOn=new Date()
									userAlert.smsText=inviteDetails.invitedByTripID.vendor.companyName+" "+alertId.message
								if(userAlert.save(flush:true,failOnError:true))
								{
									Map userData=new HashMap<String,String>()
									userData.put("alertType", userAlert.alertType.alertType)
									userData.put("tripId", userAlert.trip.id)
									def message= userAlert.alertType.message
									userData.put("message",inviteDetails.invitedByTripID.vendor.companyName+" "+message)
									notificationService.sendPushNotification(message, "acceptedByVendor", trip.user,userData)
								}
					}
					smsService.sendSMS("Mr/Ms. "+params?.firstName+", \n Your Trip "+params?.startLocation+" To " +params?.endLocation+ "  has been successfully scheduled.\n\n Driver Name :"+driverName+" \n Mobile: "+phone+" \n\n Thank you!\n cloudacar.org Team.",params?.userPhone,"91")
					session["popUpData"]="show"
					//flash.message1="Trip accepted"					
					redirect(action: "index")
				}
			}else{		
				flash.message="Trip Not Found!"
				redirect(action: "index")		
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
	
	def rejectRequest(){
		try
		{
			def vendor=VendorDetails.get(Long.parseLong(session["userID"].toString()))
			if(vendor)
			{
				def trip=TripDetails.get(params?.tripID)		
				if(trip){
					trip.tripStatus="Rejected"
					trip.lastUpdatedOn=new Date()
					if(trip.save(flush:true,failOnError:true))
					{
						def userAlert=new UserAlerts()
						userAlert.vendor=vendor
						userAlert.receiver=trip.user
						userAlert.type="TripAlert"
						userAlert.status="Unread"
						def alertId= AlertTypes.findByAlertType("RejectedByVendor")
						if(alertId)
							userAlert.alertType=alertId
							userAlert.createdOn=new Date()
							userAlert.trip= trip
							userAlert.createdOn=new Date()
							userAlert.lastUpdatedOn=new Date()
							if(userAlert.save(flush:true,failOnError:true))
							{
								Map userData=new HashMap<String,String>()
								userData.put("tripId", userAlert.trip.id)
								userData.put("alert_id", userAlert.alertType.id)
								userData.put("alertType", userAlert.alertType.alertType)
								userData.put("badge", UserAlerts.countByReceiver(trip.user))							
								def message= alertId.message
								notificationService.sendPushNotification(message, "notification_trip_rejectedByVendor_request", trip.user,userData)
							}
						smsService.sendSMS("Mr/Ms. "+params?.firstName+", \n Your Trip "+params?.startLocation+" To " +params?.endLocation+ " has been unfortunately canceled.\n Sorry for the inconvenience \n Thank you! \n cloudacar.org Team.",params?.phone,"91")
						def mainTrip=InviteDetails.findByInvitedByTripIDAndVendorAndStatus(trip,vendor,"invite")
						if(mainTrip)
						{
							mainTrip.status="Canceled"
							mainTrip.lastUpdatedOn=new Date()
							mainTrip.save(flush:true)
						}
						def isCoPassExists=InviteDetails.findAllByInvitedToTripID(trip)
						for(InviteDetails invite:isCoPassExists)
						{
							//When Vendor Rejects parent trip then all co-passengers will get rejected
							invite.invitedByTripID.tripStatus="Rejected"
							invite.invitedByTripID.lastUpdatedOn=new Date()
							if(invite.invitedByTripID.save(flush:true,failOnError:true))
							{		
								if(invite.status=="Canceled")
								continue						
								userAlert=new UserAlerts()
								userAlert.vendor=vendor
								userAlert.receiver=invite.invitedByTripID.user
								userAlert.type="TripAlert"
								userAlert.status="Unread"
								userAlert.createdOn=new Date()
								userAlert.lastUpdatedOn=new Date()
								alertId= AlertTypes.findByAlertType("RejectedByVendor")
								if(alertId)
									userAlert.alertType=alertId
									userAlert.createdOn=new Date()
									userAlert.trip= invite.invitedByTripID
									if(userAlert.save(flush:true,failOnError:true))
									{
										//Remove co-passenger from parent trip 
										invite.status="Canceled"
										invite.lastUpdatedOn=new Date()
										invite.save(flush:true,failOnError:true)
										
										def alerts=UserAlerts.findByTrip(invite.invitedByTripID)
										for(UserAlerts alert:alerts)
										{
											if(alert.invite)
											{
												if(alert.invite.id.equals(invite.id))
												{
													alert.delete(flush:true,failOnError:true)
													break
												}
											}
										}
										
										Map userData=new HashMap<String,String>()
										userData.put("tripId", userAlert.trip.id)
										userData.put("alert_id", userAlert.alertType.id)
										userData.put("alertType", userAlert.alertType.alertType)
										userData.put("badge", UserAlerts.countByReceiver(trip.user))
										def message= alertId.message
										notificationService.sendPushNotification(message, "notification_trip_rejectedByVendor_request", invite.invitedByTripID.user,userData)
									}
							}
						}
						session["popUpDataReject"]="show"
						//flash.message1="Trip canceled"
						redirect(action: "index")
					}
					
				}else{
					flash.message="Invalid trip!"
					redirect(action: "index")
				}
			}
			else{
				flash.message="Invalid Vendor!"
				redirect(action: "index")
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
	
	def cancelAcceptedTrip(){
		try
		{
			def vendor=VendorDetails.get(Long.parseLong(session["userID"].toString()))
			if(vendor)
			{
				def trip=TripDetails.get(params?.tripID)
				if(trip){
					trip.tripStatus="Canceled"
					trip.lastUpdatedOn=new Date()
					if(trip.save(flush:true,failOnError:true))
					{
						def userAlert=new UserAlerts()
						userAlert.vendor=vendor
						userAlert.receiver=trip.user
						userAlert.type="TripAlert"
						userAlert.status="Unread"
						def alertId= AlertTypes.findByAlertType("RejectedByVendor")
						if(alertId)
							userAlert.alertType=alertId
							userAlert.createdOn=new Date()
							userAlert.trip= trip
							userAlert.createdOn=new Date()
							userAlert.lastUpdatedOn=new Date()
							if(userAlert.save(flush:true,failOnError:true))
							{
								Map userData=new HashMap<String,String>()
								userData.put("tripId", userAlert.trip.id)
								userData.put("alert_id", userAlert.alertType.id)
								userData.put("alertType", userAlert.alertType.alertType)
								userData.put("badge", UserAlerts.countByReceiver(trip.user))
								def message= alertId.message
								notificationService.sendPushNotification(message, "notification_trip_rejectedByVendor_request", trip.user,userData)
							}
						smsService.sendSMS("Mr/Ms. "+params?.firstName+", \n Your Trip "+params?.startLocation+" To " +params?.endLocation+ " has been unfortunately canceled.\n Sorry for the inconvenience \n Thank you! \n cloudacar.org Team.",params?.phone,"91")
						def mainTrip=InviteDetails.findByInvitedByTripIDAndVendorAndStatus(trip,vendor,"invite")
						if(mainTrip)
						{
							mainTrip.status="Canceled"
							mainTrip.lastUpdatedOn=new Date()
							mainTrip.save(flush:true)
						}
						def isCoPassExists=InviteDetails.findAllByInvitedToTripID(trip)
						for(InviteDetails invite:isCoPassExists)
						{
							//When Vendor Rejects parent trip then all co-passengers will get rejected
							invite.invitedByTripID.tripStatus="Rejected"
							invite.invitedByTripID.lastUpdatedOn=new Date()
							if(invite.invitedByTripID.save(flush:true,failOnError:true))
							{
								if(invite.status=="Canceled")
								continue
								userAlert=new UserAlerts()
								userAlert.vendor=vendor
								userAlert.receiver=invite.invitedByTripID.user
								userAlert.type="TripAlert"
								userAlert.status="Unread"
								userAlert.createdOn=new Date()
								userAlert.lastUpdatedOn=new Date()
								alertId= AlertTypes.findByAlertType("RejectedByVendor")
								if(alertId)
									userAlert.alertType=alertId
									userAlert.createdOn=new Date()
									userAlert.trip= invite.invitedByTripID
									if(userAlert.save(flush:true,failOnError:true))
									{
										//Remove co-passenger from parent trip
										invite.status="Canceled"
										invite.lastUpdatedOn=new Date()
										invite.save(flush:true,failOnError:true)
										
										def alerts=UserAlerts.findByTrip(invite.invitedByTripID)
										for(UserAlerts alert:alerts)
										{
											if(alert.invite)
											{
												if(alert.invite.id.equals(invite.id))
												{
													alert.delete(flush:true,failOnError:true)
													break
												}
											}
										}
										
										Map userData=new HashMap<String,String>()
										userData.put("tripId", userAlert.trip.id)
										userData.put("alert_id", userAlert.alertType.id)
										userData.put("alertType", userAlert.alertType.alertType)
										userData.put("badge", UserAlerts.countByReceiver(trip.user))
										def message= alertId.message
										notificationService.sendPushNotification(message, "notification_trip_rejectedByVendor_request", invite.invitedByTripID.user,userData)
									}
							}
						}
						session["popUpDataReject"]="show"
						//flash.message1="Trip canceled"
						redirect(action: "requestDetails")
					}
					
				}else{
					flash.message="Invalid trip!"
					redirect(action: "requestDetails")
				}
			}
			else{
				flash.message="Invalid Vendor!"
				redirect(action: "requestDetails")
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
	
}
