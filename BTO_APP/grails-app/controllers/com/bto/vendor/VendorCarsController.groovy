package com.bto.vendor

import com.bto.customer.CarType;
import com.bto.customer.VendorDetails;

import grails.transaction.Transactional;

class VendorCarsController {

	def index() {
		def vendorId=session["userID"]
		ArrayList carsInstance=new ArrayList()
		if(vendorId)
		{
			def vendor=VendorDetails.get(vendorId)
			if(vendor)
			{				
				def vendorCars=vendor.carTypes
				if(vendorCars)
				{
					def carTypes=vendorCars.tokenize(",")
					for(def i=0;i<carTypes.size();i++)
					{
						carsInstance.add(carTypes[i])
					}
				}	
			}
		}
		respond "c", model:[carsInstance: carsInstance]
	}
	
	def addVendorCar(){}
	
	def saveVendorCar(){
		try {
			
			def isExists=VendorDetails.findByCarTypesAndEmail(params?.carTypes,session["emailID"])
			if(!isExists){
				
				def vendorDetails = VendorDetails.get(session["userID"])
				def p=(params?.carTypes).toString().replace("[", "").replace("]", "")
				vendorDetails.carTypes =p
				vendorDetails.createdOn=new Date()
				vendorDetails.lastUpdatedOn =new Date()
					
				if(vendorDetails.save(flush:true,failOnError:true)) {
					flash.message1="Car Types Added Successfully"
					redirect action:"index"
				}
				else {
					flash.message="Error in save error Car Types"
					redirect action:"index"
				}
			}
			else
			{
				flash.message="Car Types with this name is already exist"
				redirect action:"index"
			}
			
		} catch (Exception e) {
			e.printStackTrace()
		}
		
	}
	
	@Transactional
	def updateVendorCar(VendorDetails vendorDetailsInstance){
		try{
				vendorDetailsInstance.carTypes=params?.carTypes
				vendorDetailsInstance.lastUpdatedOn =new Date()
				if(vendorDetailsInstance.save(flush:true,failOnError:true))
				{
					flash.message1="Car Types Updated Successfully"
					redirect action:"index"
				}
				else{
					flash.message="ERROR in Car Types update"
					redirect action:"index"
				}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
	
	def editVendorCar(VendorDetails vendorDetailsInstance)
	{
		respond vendorDetailsInstance
	}
	
	@Transactional
	def deleteVendorCar(VendorDetails vendorDetailsInstance)
	{
		try{
			vendorDetailsInstance.carTypes=""
			vendorDetailsInstance.lastUpdatedOn =new Date()
			if(vendorDetailsInstance.save(flush:true,failOnError:true))
			{
				flash.message1="Car Types Deleted Successfully"
				redirect action:"index"
			}
			else{
				flash.message="ERROR in Car Types Delete"
				redirect action:"index"
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
}
