package com.bto.vendor

import grails.transaction.Transactional;


class DriversController {

	def index() {

		respond Driver.list(params), model:[driverInstanceCount: Driver.count()]
	}

	def addDriver(){}

	def saveDriver(){
		try {
			def isExists=Driver.findByPhone(params?.phone)
			if(!isExists){
				def driver=new Driver()
				driver.firstName=params?.firstName
				driver.lastName=params?.lastName
				driver.phone=params?.phone
				driver.image=params?.image
				driver.createdOn=new Date()
				driver.lastUpdatedOn =new Date()

				if(driver.save(flush:true,failOnError:true)) {
					flash.message1="Driver Added Successfully"
					redirect action:"index"
				}
				else {
					flash.message="Error in save error Driver details"
					redirect action:"index"
				}
			}
			else {
				flash.message="Driver with this phone is already exist"
				redirect action:"index"
			}
		} catch (Exception e) {
			e.printStackTrace()
		}
	}

	@Transactional
	def updateDriver(Driver driverInstance){

		try{
			driverInstance.firstName=params?.firstName
			driverInstance.lastName=params?.lastName
			driverInstance.phone=params?.phone
			driverInstance.image=params?.image
			driverInstance.createdOn=new Date()
			driverInstance.lastUpdatedOn =new Date()
			if(driverInstance.save(flush:true,failOnError:true)) {
				flash.message1="Driver Updated Successfully"
				redirect action:"index"
			}else{
				flash.message="ERROR in Driver update"
				redirect action:"index"
			}
		}
		catch(Exception e) {
			e.printStackTrace()
		}
	}

	def editDriver(Driver driverInstance) {
		respond driverInstance
	}

	@Transactional
	def deleteDriver(Driver driverInstance) {
		try{
			driverInstance.delete(flush:true)
			flash.message1="Driver Deleted"
			redirect action:"index"
		}
		catch(Exception e) {
			e.printStackTrace()
		}
	}
}
