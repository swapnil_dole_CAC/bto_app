package com.bto.vendor

import grails.transaction.Transactional;


class RejectionReasonController {

    
    def index() { 
		respond RejectionReason.list(params), model:[rejectionReasonInstanceCount: RejectionReason.count()]
	}
	
	def addRejectionReason(){
		
	}
	
	def saveRejectionReason(){
		try {
			def isExists=RejectionReason.findByReasonDetails(params?.reasonDetails)
			if(!isExists){
				def rejectionReason=new RejectionReason()
				rejectionReason.reasonDetails=params?.reasonDetails				
				rejectionReason.createdOn=new Date()
				rejectionReason.lastUpdatedOn =new Date()
				
				if(rejectionReason.save(flush:true,failOnError:true)) {
					flash.message1="reason Added Successfully"
					redirect action:"index"
				}
				else {
					flash.message="Error in save error reason details"
					redirect action:"index"
				}
			}
			else
			{
				flash.message="reason with this name is already exist"
				redirect action:"index"
			}
			
		} catch (Exception e) {
			e.printStackTrace()
		}
	}
	
	def updateRejectionReason(RejectionReason rejectionReasonInstance){
		try{
			rejectionReasonInstance.reasonDetails=params?.reasonDetails
			rejectionReasonInstance.lastUpdatedOn =new Date()
			if(rejectionReasonInstance.save(flush:true,failOnError:true))
			{
				flash.message1="Reason Updated Successfully"
				redirect action:"index"
			}else{
				flash.message="ERROR in Reason update"
				redirect action:"index"
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
	
	def editRejectionReason(RejectionReason rejectionReasonInstance)
	{
		respond rejectionReasonInstance
	}
	
	@Transactional
	def deleteRejectionReason(RejectionReason rejectionReasonInstance)
	{
		try{
			rejectionReasonInstance.delete(flush:true)
				flash.message1="Reason Deleted"
				redirect action:"index"
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
	
}
