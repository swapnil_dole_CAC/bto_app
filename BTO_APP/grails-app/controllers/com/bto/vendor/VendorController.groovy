package com.bto.vendor

import org.apache.commons.lang.RandomStringUtils;

import grails.gsp.PageRenderer;

import com.bto.customer.VendorDetails;
import com.bto.utilities.UtilitiesService

class VendorController {

	PageRenderer groovyPageRenderer

	def index() { }
	
	def dashboard(){}
	
	def resetPassword(){
		def vendor=VendorDetails.findByEmail(params?.emailID)
		if(vendor.password=="" || vendor.password==null){
			def emailID=params?.emailID
			respond "emailID",model:[emailID:emailID]
		}else{
			flash.message="You had already reset password"
			redirect(controller:"Vendor" ,action: "index")
		}
	}

	def authenticateVendor(){

		try {
			def regvendor=VendorDetails.findByEmail(params?.email)
			if(regvendor){
				def vendor=VendorDetails.findByEmailAndPassword(params?.email,params?.password)
				if(vendor){
					session["userID"]=vendor.id
					session["companyName"]=vendor.companyName
					session["emailID"]=vendor.email
					session["mobile"]=vendor.mobile
					session["role"]="vendorAdmin"

					//flash.message1="Login Successful"
					redirect(controller:"TripRequests", action: "index")
				}else{
					flash.message="Email address or password does not match"
					redirect(action: "index")
				}
			}else{
				flash.message="Email address not registered"
				redirect(action: "index")
			}
		} catch (Exception e) {
			e.printStackTrace()
		}
	}

	def setPassword(){
		try {

			def status=true
			def vendor=VendorDetails.findByEmail(params?.emailID)
			if(vendor){

				if(!params?.newPassword) {
					status=false
					flash.message="New password incorrect"
					redirect(action: "resetPassword")
				}
				else if(!params?.confirmPassword) {
					status=false
					flash.message="Confirm password incorrect"
					redirect(action: "resetPassword")
				}else if((params?.confirmPassword)!=(params?.newPassword)) {
					status=false
					flash.message="Old and confirm password does not match..!"
					redirect(action: "resetPassword")
				}

				if(status){

					vendor.password=params?.confirmPassword
					vendor.lastUpdatedOn=new Date()

					if(vendor.save(flush:true,failOnError:true)){
						flash.message="Login to your account"
						redirect(controller:"Vendor", action: "index")
					}else{
						flash.message="Error in reset password"
						redirect(action: "resetPassword")
					}
				}
			}else{
				flash.message="Email address not registered"
				redirect(action: "resetPassword")
			}
		} catch (Exception e) {
			e.printStackTrace()
		}
	}

	def forgotPassword(){
		try{
			def vendor = VendorDetails.findByEmail(params?.emailID)

			if(!vendor) {
				flash.message="Invalid email address"
				redirect(controller:"Vendor" ,action: "index")
			}
			if(vendor) {
				//Generating Random characters as a Password
				def charset = (('A'..'Z')).join()
				def length = 5
				def randomString = RandomStringUtils.random(length, charset.toCharArray())

				//Saving password in database for that user
				vendor.password=randomString

				// failOnError:true
				if(vendor.save(flush: true))
				{
					// Sending new password to Email address
					Map data=new HashMap<String, String>()
					data.put("name", vendor.companyName)
					data.put("password", randomString)
					data.put("email", vendor.email)
					def content=groovyPageRenderer.render(view: '/htmlEmailTemplates/forgotPasswordEmailTemplate', model : [ userData : data] )
					sendMail {
						from 'info@cloudacar.org'
						to vendor.email
						subject ("BTO account new password")
						html content
					}

					flash.message1="Password is Sent to your Email"
					redirect(controller:"Vendor" ,action: "index")

				}
				else
				{
					flash.message="Error in forget password"
					redirect(controller:"Vendor" ,action: "index")
				}
			}

		}
		catch(Exception e)
		{
			e.printStackTrace()
			flash.message="Error in forget password"
			redirect(controller:"Vendor" ,action: "index")
		}
	}

}
