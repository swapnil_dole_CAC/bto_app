package com.bto.booking
import grails.plugins.rest.client.RestBuilder
import groovy.json.JsonSlurper;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;

import org.codehaus.groovy.grails.web.json.JSONObject

import com.bto.utilities.UtilitiesService;

class InviteUserController {

	def grailsApplication
	
    def index() { }
	
	def inviteUser(){	
		
			InputStream is = new URL(grailsApplication.config.grails.webSite+"WebService/inviteUser?invitedByUserID="+params?.invitedByUserID+"&invitedToUserID="+params?.invitedToUserID+"&invitedByTripID="+params?.invitedByTripID+"&invitedToTripID="+params?.invitedToTripID).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText =UtilitiesService.readAll(rd);
			JSONObject json = new JSONObject(jsonText);
			JsonSlurper jslurp=new JsonSlurper()
			Object obj=jslurp.parseText(jsonText)
			Map jsonData=(Map)obj
			def t=obj.tripId
			if(json.getString("response")=="success") {
			
				InputStream is1 = new URL(grailsApplication.config.grails.webSite+"WebService/getMatchingUsers?tripId="+t+"&userID="+session['userID']).openStream();
				BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1, Charset.forName("UTF-8")));
				String jsonText1=UtilitiesService.readAll(rd1);
				JSONObject json1 = new JSONObject(jsonText1);
				
				if(json1.getString("response")=="success") {
					chain(controller:"MatchingUserList",action: "index", model: [jsonObject:json1,tripId:t])
				}
			}
			else{
				redirect(controller:"BookingConsole", action: "index")
			}
	}
}
