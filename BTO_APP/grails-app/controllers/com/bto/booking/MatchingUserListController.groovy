package com.bto.booking

import grails.plugins.rest.client.RestBuilder
import groovy.json.JsonSlurper;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;

import org.codehaus.groovy.grails.web.json.JSONObject

import com.bto.utilities.UtilitiesService;

class MatchingUserListController {

	def grailsApplication
	
    def index() { }
	
	def bookTripWithoutMachingUser(){
		InputStream is = new URL(grailsApplication.config.grails.webSite+"WebService/scheduleTripWithReason?userID="+session['userID']+"&tripId="+params?.tripId+"&reason="+URLEncoder.encode(params?.reason, "UTF-8")).openStream();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
		String jsonText =UtilitiesService.readAll(rd);
		JSONObject json = new JSONObject(jsonText);
		JsonSlurper jslurp=new JsonSlurper()
		Object obj=jslurp.parseText(jsonText)
		Map jsonData=(Map)obj
		session["popUpData"]="show"
		if(json.getString("response")=="success") {
			
			InputStream is1 = new URL(grailsApplication.config.grails.webSite+"WebService/getBookedTripHistory?userID="+session['userID']).openStream();
			BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1, Charset.forName("UTF-8")));
			String jsonText1=UtilitiesService.readAll(rd1);
			JSONObject jsonBookData = new JSONObject(jsonText1);
			
			if(jsonBookData.getString("response")=="success") {
				chain(controller:"BookedTripHistory",action: "index", model: [jsonObject:jsonBookData])
			}
			
		}else{
			redirect(controller:"BookingConsole", action: "index")
		}
	}
	
	def bookTripWithSkipReason(){
		InputStream is = new URL(grailsApplication.config.grails.webSite+"WebService/scheduleTripWithReason?userID="+session['userID']+"&tripId="+params?.tripId+"&reason="+URLEncoder.encode(params?.reason, "UTF-8")).openStream();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
		String jsonText =UtilitiesService.readAll(rd);
		JSONObject json = new JSONObject(jsonText);
		JsonSlurper jslurp=new JsonSlurper()
		Object obj=jslurp.parseText(jsonText)
		Map jsonData=(Map)obj
		session["popUpData"]="show"
		if(json.getString("response")=="success") {
			
			InputStream is1 = new URL(grailsApplication.config.grails.webSite+"WebService/getBookedTripHistory?userID="+session['userID']).openStream();
			BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1, Charset.forName("UTF-8")));
			String jsonText1=UtilitiesService.readAll(rd1);
			JSONObject jsonBookData = new JSONObject(jsonText1);
			
			if(jsonBookData.getString("response")=="success") {
				
				chain(controller:"BookedTripHistory",action: "index", model: [jsonObject:jsonBookData])
			}
		}else{
			redirect(controller:"BookingConsole", action: "index")
		}
	}
	
}
