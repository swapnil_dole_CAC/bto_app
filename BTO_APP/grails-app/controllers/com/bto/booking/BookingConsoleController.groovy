package com.bto.booking

import grails.plugins.rest.client.RestBuilder
import groovy.json.JsonSlurper

import java.nio.charset.Charset;
import java.text.SimpleDateFormat

import org.codehaus.groovy.grails.web.json.JSONObject

import com.bto.trip.TripDetails;
import com.bto.utilities.UtilitiesService;

class BookingConsoleController {

	def grailsApplication

	def index() { 
		
		if(session['userID'])
		{
			InputStream is1 = new URL(grailsApplication.config.grails.webSite+"WebService/getDropDownList").openStream();
			BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1, Charset.forName("UTF-8")));
			String jsonText1=UtilitiesService.readAll(rd1);
			JSONObject jsonData = new JSONObject(jsonText1);
			
			if(jsonData.getString("response")=="success") {
				respond "c", model: [jsonObject:jsonText1]
			}else{
				respond "c", model: [jsonObject:jsonText1]
			}
		}
		else
		{
			redirect controller:"User"
		}
	}
	
	def bookTrip(){
		def eventCode=""
		def purpose
		
		if((params?.purpose!="") && (params?.purpose!="other")){
			purpose=params?.purpose
		}else if(params?.purpose=="other"){
			purpose=params?.otherPurpose
		}
		def costCenter=""
		if(params?.costCenter!=session['costCenter']){
			costCenter=params?.costCenter	
		}
		else{
			costCenter=session['costCenter']
		}
		
		try {
			RestBuilder rest=new RestBuilder()
			def resp = rest.get(grailsApplication.config.grails.webSite+"WebService/bookTrip?startLocation="+params?.startLocation+"&endLocation="+params?.endLocation+"&purpose="+purpose+"&tripDate="+params?.tripDate+"&tripTime="+params?.tripTime+"&userID="+params?.userID+"&vendorID="+params?.vendorID+"&carType="+params?.carType+"&costCenter="+costCenter.toString()+"&eventCode="+eventCode.toString()+"&note="+params?.note)
			resp.json instanceof JSONObject
			
			//parse JSON response using JsonSlurper
			def JsonSlurper=new JsonSlurper()
			def object=JsonSlurper.parseText(resp.json.toString())
			
			//Get parent ArrayList called "Result"
			def resultList=object.errorList
			
			//Get 1st Object from ResultList
			def error=resultList[0]
			def t=object.tripId
				
			if(resp.json.response=="success") {
			
				InputStream is = new URL(grailsApplication.config.grails.webSite+"WebService/getMatchingUsers?tripId="+t+"&userID="+session['userID']).openStream();
				BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
				String jsonText=UtilitiesService.readAll(rd);
				JSONObject json = new JSONObject(jsonText);
				if(json.getString("response")=="success") {
					chain(controller:"MatchingUserList",action: "index", model: [jsonObject:json,tripId:t])
				}
				else {
					flash.message=error
					redirect(controller:"BookingConsole", action: "index")
				}
			}
			else {
				flash.message=error
				redirect(controller:"BookingConsole", action: "index")
			}
		} catch (Exception e) {
			e.printStackTrace()
		}
	}
	
	def bookEventTrip(){
				try {
					def purpose
					
					if((params?.purpose!="") && (params?.purpose!="other")){
						purpose=params?.purpose
					}else if(params?.purpose=="other"){
						purpose=params?.otherPurpose
					}
					
					RestBuilder rest=new RestBuilder()
					def resp = rest.get(grailsApplication.config.grails.webSite+"WebService/bookTrip?startLocation="+params?.startLocation+"&endLocation="+params?.endLocation+"&purpose="+purpose+"&tripDate="+params?.tripDate+"&tripTime="+params?.tripTime+"&userID="+params?.userID+"&vendorID="+params?.vendorID+"&carType="+params?.carType+"&costCenter="+params?.costCenter+"&eventCode="+params?.eventCode)
					resp.json instanceof JSONObject
					
					//parse JSON response using JsonSlurper
					def JsonSlurper=new JsonSlurper()
					def object=JsonSlurper.parseText(resp.json.toString())
					
					//Get parent ArrayList called "Result"
					def resultList=object.errorList
					
					//Get 1st Object from ResultList
					def error=resultList[0]
					def t=object.tripId
					
					if(resp.json.response=="success") {
					
						InputStream is = new URL(grailsApplication.config.grails.webSite+"WebService/getMatchingUsers?tripId="+t+"&userID="+session['userID']).openStream();
						BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
						String jsonText=UtilitiesService.readAll(rd);
						JSONObject json = new JSONObject(jsonText);
						
						if(json.getString("response")=="success") {
							chain(controller:"MatchingUserList",action: "index", model: [jsonObject:json,tripId:t])
						}
						else {
							flash.message=error
							redirect(controller:"BookingConsole", action: "index")
						}
					}
					else {
						flash.message=error
						redirect(controller:"BookingConsole", action: "index")
					}
				} catch (Exception e) {
					e.printStackTrace()
				}
			}
	
	def edit(){
		try{
			def tripInstance=TripDetails.get(Long.parseLong(params?.tripID))
			if(tripInstance)
			{
				InputStream is1 = new URL(grailsApplication.config.grails.webSite+"WebService/getDropDownList").openStream();
				BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1, Charset.forName("UTF-8")));
				String jsonText1=UtilitiesService.readAll(rd1);
				JSONObject jsonData = new JSONObject(jsonText1);
				
				session['alertId']=params?.alert_id
				respond "c", model: [tripInstance:tripInstance,tripTime:convertTimeFromTwentyFourHoursToTwelveHours(tripInstance.tripTime),jsonObject:jsonText1]
			}
			else
			{
				flash.message="Invalid Trip"
				redirect action:"index"
			}
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}

	def updateTrip(TripDetails tripInstance){
		try{
			def eventCode=""
			def purpose
			if((params?.purpose!="") && (params?.purpose!="other")){
				purpose=params?.purpose
			}else if(params?.purpose=="other"){
				purpose=params?.otherPurpose
			}
			def costCenter=""
			if(params?.costCenter!=session['costCenter']){
				costCenter=params?.costCenter
			}
			else{
				costCenter=session['costCenter']
			}
			RestBuilder rest=new RestBuilder()
			def resp = rest.get(grailsApplication.config.grails.webSite+"WebService/updateTrip?startLocation="+params?.startLocation+"&endLocation="+params?.endLocation+"&purpose="+purpose+"&tripDate="+params?.tripDate+"&tripTime="+params?.tripTime+"&userID="+params?.userID+"&vendorID="+params?.vendorID+"&carType="+params?.carType+"&costCenter="+costCenter.toString()+"&eventCode="+eventCode.toString()+"&tripID="+params?.tripId+"&alertId="+session['alertId'])
			resp.json instanceof JSONObject

			//parse JSON response using JsonSlurper
			def JsonSlurper=new JsonSlurper()
			def object=JsonSlurper.parseText(resp.json.toString())

			//Get parent ArrayList called "Result"
			def resultList=object.errorList

			//Get 1st Object from ResultList
			def error=resultList[0]
			def t=object.tripId

			if(resp.json.response=="success") {

				InputStream is = new URL(grailsApplication.config.grails.webSite+"WebService/getMatchingUsers?tripId="+t+"&userID="+session['userID']).openStream();
				BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
				String jsonText=UtilitiesService.readAll(rd);
				JSONObject json = new JSONObject(jsonText);

				if(json.getString("response")=="success") {
					
					chain(controller:"MatchingUserList",action: "index", model: [jsonObject:json,tripId:t])
				}
				else {
					flash.message=error
					redirect(controller:"BookingConsole", action: "index")
				}
			}
			else {
				flash.message=error
				redirect(controller:"BookingConsole", action: "index")
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()	
		}
	}	

	def findMatchingUser(){
		try{
			InputStream is
			if(params?.alertId)
			 	is= new URL(grailsApplication.config.grails.webSite+"WebService/getMatchingUsers?tripId="+params?.tripID+"&userID="+session['userID']+"&alertId="+params?.alert_id).openStream();
			else
				is= new URL(grailsApplication.config.grails.webSite+"WebService/getMatchingUsers?tripId="+params?.tripID+"&userID="+session['userID']).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText=UtilitiesService.readAll(rd);
			JSONObject json = new JSONObject(jsonText);
			
			if(json.getString("response")=="success") {
					chain(controller:"MatchingUserList",action: "index", model: [jsonObject:json,tripId:params?.tripID])
			}
			else {
				flash.message="Error to found Matching user"
				redirect(controller:"BookingConsole", action: "index")
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
	
	static String convertTimeFromTwentyFourHoursToTwelveHours(String tripStartTime)
	{
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
			Date _24HourDt = _24HourSDF.parse(tripStartTime);
			SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
			return _12HourSDF.format(_24HourDt)
	}
	
}
