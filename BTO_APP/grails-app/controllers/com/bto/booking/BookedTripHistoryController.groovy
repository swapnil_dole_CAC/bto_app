package com.bto.booking

import groovy.json.JsonSlurper

import java.nio.charset.Charset;

import org.codehaus.groovy.grails.web.json.JSONObject

import com.bto.customer.VendorDetails;
import com.bto.trip.InviteDetails;
import com.bto.trip.TripDetails;
import com.bto.user.User;
import com.bto.utilities.UtilitiesService;

class BookedTripHistoryController {

    def index() {		
		def user=User.get(session['userID'])
			
		InputStream is1 = new URL(grailsApplication.config.grails.webSite+"WebService/getBookedTripHistory?userID="+session['userID']).openStream();
		BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1, Charset.forName("UTF-8")));
		String jsonText1=UtilitiesService.readAll(rd1);
		JSONObject jsonBookData = new JSONObject(jsonText1);
		
		if(jsonBookData.getString("response")=="success") {
			//respond "c", model: [jsonObject:jsonBookData]
			respond TripDetails.findAllByUserAndTripStatus(user,"scheduled"), model:[TripDetailsInstanceCount: TripDetails.count(),jsonObject:jsonBookData]
		}else{
			respond TripDetails.findAllByUserAndTripStatus(user,"scheduled"), model:[TripDetailsInstanceCount: TripDetails.count(),jsonObject:jsonBookData]
		}
	}
	
	
	def cancelBookedTrip={
		session["popUpDataCancel"]="show"
		InputStream is1 = new URL(grailsApplication.config.grails.webSite+"WebService/cancelBookedTrip?tripID="+params?.tripID).openStream();
		BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1, Charset.forName("UTF-8")));
		String jsonText1=UtilitiesService.readAll(rd1);
		JSONObject jsonData = new JSONObject(jsonText1);
		if(jsonData.getString("response")=="success") {
			//flash.message1="Trip canceled...!"
			redirect(action:"index")
		}else{
		 flash.message="Error in cancel trip...!"
		 redirect(action:"index")
		}
	}
	
}
