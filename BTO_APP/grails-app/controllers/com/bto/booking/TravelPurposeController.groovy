package com.bto.booking

import grails.gsp.PageRenderer;
import grails.transaction.Transactional;

import com.bto.trip.TravelPurpose;
import com.bto.utilities.UtilitiesService

class TravelPurposeController {
	PageRenderer groovyPageRenderer
	def grailsApplication

	def index() {
		
		respond TravelPurpose.list(params), model:[travelPurposeInstanceCount: TravelPurpose.count()]
	}
	
	def addTravelPurpose(){}
	
	def saveTravelPurpose(){
		try {
			def isExists=TravelPurpose.findByTravelPurpose(params?.travelPurpose)
			if(!isExists){
				def travelPurpose=new TravelPurpose()
				travelPurpose.travelPurpose=params?.travelPurpose
				travelPurpose.createdOn=new Date()
				travelPurpose.lastUpdatedOn =new Date()
				
				if(travelPurpose.save(flush:true,failOnError:true)) {
					flash.message1="Travel purpose Added Successfully"
					redirect action:"index"
				}
				else {
					flash.message="Error in save travel purpose details"
					redirect action:"index"
				}
			}
			else
			{
				flash.message="Travel purpose already exist"
				redirect action:"index"
			}
			
		} catch (Exception e) {
			e.printStackTrace()
		}
		
	}
	
	@Transactional
	def updateTravelPurpose(TravelPurpose travelPurposeInstance){
		try{
				travelPurposeInstance.travelPurpose=params?.travelPurpose
				travelPurposeInstance.lastUpdatedOn =new Date()
				if(travelPurposeInstance.save(flush:true,failOnError:true))
				{
					flash.message1="Travel purpose Updated Successfully"
					redirect action:"index"
				}else{
					flash.message=println "ERROR in Travel purpose update"
					redirect action:"index"
				}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
	
	def editTravelPurpose(TravelPurpose travelPurposeInstance)
	{
		respond travelPurposeInstance
	}
	
	@Transactional
	def deleteTravelPurpose(TravelPurpose travelPurposeInstance)
	{
		try{
			travelPurposeInstance.delete(flush:true)
				flash.message1="Travel purpose deleted"
				redirect action:"index"
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
}
