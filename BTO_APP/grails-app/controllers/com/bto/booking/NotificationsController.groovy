package com.bto.booking

import groovy.json.JsonSlurper

import java.nio.charset.Charset;

import org.codehaus.groovy.grails.web.json.JSONObject

import com.bto.customer.VendorDetails;
import com.bto.trip.InviteDetails;
import com.bto.trip.TripDetails;
import com.bto.user.User;
import com.bto.utilities.UtilitiesService;

class NotificationsController {

    def index() { 
		if(session['userID'])
		{
			InputStream is1 = new URL(grailsApplication.config.grails.webSite+"WebService/getNotification?userID="+session['userID']).openStream();
			BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1, Charset.forName("UTF-8")));
			String jsonText1=UtilitiesService.readAll(rd1);
			JSONObject jsonBookData = new JSONObject(jsonText1);
			
			if(jsonBookData.getString("response")=="success") {			
				respond "c", model: [jsonObject:jsonBookData]
			}else{
				respond "c", model: [jsonObject:jsonBookData]
			}
		}
		else
		{
			redirect controller:"User"
		}
	}


	def performInvitationAction(){	
		try{	
			InputStream is = new URL(grailsApplication.config.grails.webSite+"WebService/performInvitationAction?invitationId="+params?.invitationId+"&statusAction="+params?.statusAction).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText=UtilitiesService.readAll(rd);
			JSONObject jsonBookData = new JSONObject(jsonText);
			
			if(params?.statusAction=="1"){
				session["popUpDataAccept"]="show"
			}else{
				session["popUpDataReject"]="show"
			}
			
			if(jsonBookData.getString("response")=="success") {
				InputStream is1 = new URL(grailsApplication.config.grails.webSite+"WebService/updateAlertRecord?alertId="+params?.alert_id).openStream();
				BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1, Charset.forName("UTF-8")));
				def jsonText1=UtilitiesService.readAll(rd1);
				JSONObject json1 = new JSONObject(jsonText1);
				if(json1.getString("response")=="success") {
					redirect action:"index"
				}
			}
			else{
				flash.message=jsonBookData.getString("errorList")
				redirect action:"index"
			}
		}catch(Exception e){
		e.printStackTrace()}
	}

}