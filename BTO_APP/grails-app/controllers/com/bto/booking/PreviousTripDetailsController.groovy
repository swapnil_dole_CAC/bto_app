package com.bto.booking
import groovy.json.JsonSlurper

import java.nio.charset.Charset;

import org.codehaus.groovy.grails.web.json.JSONObject

import com.bto.trip.TripDetails;
import com.bto.user.User;
import com.bto.utilities.UtilitiesService;

class PreviousTripDetailsController {

    def index() {
		
		def user=User.get(session['userID'])
		respond TripDetails.findAllByUserAndTripStatus(user,"completed"), model:[TripDetailsInstanceCount: TripDetails.count()]
		
	}
}
