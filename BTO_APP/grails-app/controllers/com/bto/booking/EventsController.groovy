package com.bto.booking

import grails.plugins.rest.client.RestBuilder
import groovy.json.JsonSlurper

import java.nio.charset.Charset;

import org.codehaus.groovy.grails.web.json.JSONObject

import com.bto.trip.EventDetails;
import com.bto.utilities.UtilitiesService;
class EventsController {

    def index() {		
		respond EventDetails.list(params), model:[eventDetailsInstanceCount: EventDetails.count()]		
	}
	
	def createEvent(){ }
	
	def bookEvent(){ }
	
	def bookThisEvent(){
		String eventName=params?.eventName
		String eventVenue=params?.eventVenue
		String eventDate=params?.eventDate
		String eventCode=params?.eventCode
			
		String costCenter=""
		if(params?.costCenter=="NONE"){
			costCenter=params?.eventCode
		}else{
			costCenter=params?.costCenter
		}
		InputStream is1 = new URL(grailsApplication.config.grails.webSite+"WebService/getDropDownList").openStream();
		BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1, Charset.forName("UTF-8")));
		String jsonText1=UtilitiesService.readAll(rd1);
		JSONObject jsonData = new JSONObject(jsonText1);
		
		  chain (controller:"Events", action: "bookEvent",  model: [eventDate:eventDate, eventVenue:eventVenue , costCenter: costCenter,eventCode:eventCode, eventName:eventName,jsonObject:jsonText1]) 
	}
	
	
	def addEvent(){
		// if cost center equals to NONE then use event code as a cost center
		def costCenter=""
		if(params?.costCenter=="NONE"){
			costCenter=params?.eventCode
		}else{
			costCenter=params?.costCenter
		}
		
		RestBuilder rest=new RestBuilder()
		def resp = rest.get(grailsApplication.config.grails.webSite+"WebService/createEvent?userID="+session['userID']+"&eventName="+params?.eventName+"&eventType="+params?.eventType+"&eventVenue="+params?.eventVenue+"&eventDate="+params?.eventDate+"&eventTime="+params?.eventTime+"&costCenter="+costCenter+"&eventCode="+params?.eventCode+"&description="+params?.description)
		resp.json instanceof JSONObject
		
		//parse JSON response using JsonSlurper
		def JsonSlurper=new JsonSlurper()
		def object=JsonSlurper.parseText(resp.json.toString())
		
		//Get parent ArrayList called "Result"
		def resultList=object.errorList
		
		//Get 1st Object from ResultList
		def error=resultList[0]
		def sid=object.eventId
		def uid=object.userId
	
			if(resp.json.response=="success") {
				session["popUpData"]="show"
				redirect(controller:"Events", action: "index")
			}else{
			 flash.message=error
			 redirect(controller:"Events", action: "createEvent")
			}
	}
	
	def cancelEvent(){
		if(session['userID'])
		{
			InputStream is1 = new URL(grailsApplication.config.grails.webSite+"WebService/cancelEvent?eventId="+params?.eventId).openStream();
			BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1, Charset.forName("UTF-8")));
			String jsonText1=UtilitiesService.readAll(rd1);
			JSONObject jsonData = new JSONObject(jsonText1);
			
			if(jsonData.getString("response")=="success") {
				redirect(controller:"Events", action: "index")
			}else{
				redirect(controller:"Events", action: "index")
			}
		}
		else
		{
			redirect controller:"User"
		}
	}
	
	
}
