package com.bto
import java.sql.Date;

import org.codehaus.groovy.grails.web.json.JSONObject;

import com.bto.user.User;
import com.bto.utilities.NotificationService;

import grails.converters.JSON

class DemoServiceController {

	def demoService
	def notificationService
	
    def index() { }
	
	def notification={
		response.setHeader("Access-Control-Allow-Origin", "*")
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def json
		try {
				
			def u=User.get(1)
			if(status){
				json=notificationService.sendPushNotification("Hello User", "notification From WEBAPP",u,null)
			}

		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			if(status==false){
				jsonData['request']=requestData
				jsonData['response']=responseData
				jsonData['status']=status
				jsonData['errorList']=errorList
				render jsonData as JSON
			}else{
				render json as JSON
			}
		}
	}
}
