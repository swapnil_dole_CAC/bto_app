package com.bto.customer

import com.bto.utilities.UtilitiesService

import grails.gsp.PageRenderer;
import grails.transaction.Transactional;

class AddVendorController {
	PageRenderer groovyPageRenderer
	def grailsApplication

	def index() {

		respond VendorDetails.list(params), model:[vendorDetailsInstanceCount: VendorDetails.count()]
	}

	def addVendor(){}

	def saveVendor(){
		try {
			def isExists=VendorDetails.findByEmail(params?.email)
			if(!isExists){
				def isExistsMobile=VendorDetails.findByMobile(params?.mobile)
				if(!isExistsMobile){
					def vendorDetails=new VendorDetails()
					vendorDetails.email=params?.email
					vendorDetails.password=""
					vendorDetails.companyName=params?.companyName
					vendorDetails.mobile=params?.mobile
					vendorDetails.address=params?.address
					vendorDetails.POCName=params?.POCName
					vendorDetails.POCEmail=params?.POCEmail
					vendorDetails.POCMobile=params?.POCMobile
					vendorDetails.carTypes=""
					vendorDetails.password=""
					vendorDetails.image=params?.image
					vendorDetails.createdOn=new Date()
					vendorDetails.lastUpdatedOn =new Date()

					if(vendorDetails.save(flush:true,failOnError:true)) {
						def randomInt=new UtilitiesService().getRandomNumber()
						// Sending new password to Email address
						Map data=new HashMap<String, String>()
						data.put("name", vendorDetails.companyName)
						data.put("password", randomInt)
						data.put("email", vendorDetails.email)
						data.put("link", grailsApplication.config.grails.webSite+"vendor/resetPassword.gsp?emailID="+vendorDetails.email)
						def content=groovyPageRenderer.render(view: '/htmlEmailTemplates/employerActivation', model : [ userData : data] )
						sendMail {
							from 'info@cloudacar.org'
							to vendorDetails.email
							subject ("BTO vendor account new password")
							html content
						}
						flash.message1="Vendor Added Successfully"
						redirect action:"index"
					}
					else {
						flash.message="Error in save error vendor details"
						redirect action:"index"
					}
				}else
				{
					flash.message="Vendor with this mobile is already exist"
					redirect action:"index"
				}
			}
			else
			{
				flash.message="Vendor with this email is already exist"
				redirect action:"index"
			}

		} catch (Exception e) {
			e.printStackTrace()
		}

	}

	@Transactional
	def updateVendor(VendorDetails vendorDetailsInstance){
		try{
			vendorDetailsInstance.email=params?.email
			vendorDetailsInstance.companyName=params?.companyName
			vendorDetailsInstance.mobile=params?.mobile
			vendorDetailsInstance.address=params?.address
			vendorDetailsInstance.POCName=params?.POCName
			vendorDetailsInstance.POCEmail=params?.POCEmail
			vendorDetailsInstance.POCMobile=params?.POCMobile
			vendorDetailsInstance.image=params?.image
			vendorDetailsInstance.lastUpdatedOn =new Date()
			if(vendorDetailsInstance.save(flush:true,failOnError:true))
			{
				flash.message1="Vendor Updated Successfully"
				redirect action:"index"
			}else{
				flash.message="ERROR in vendor update"
				redirect action:"index"
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}

	def editVendor(VendorDetails vendorDetailsInstance)
	{
		respond vendorDetailsInstance
	}

	@Transactional
	def deleteVendorDetails(VendorDetails vendorDetailsInstance)
	{
		try{
			vendorDetailsInstance.delete(flush:true)
			flash.message1="Vendor Deleted"
			redirect action:"index"
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
}
