package com.bto.customer

import org.apache.commons.lang.RandomStringUtils;

import com.bto.trip.TripDetails;
import com.bto.user.User;
import com.bto.utilities.UtilitiesService

import grails.gsp.PageRenderer;

class EmployerController {

	PageRenderer groovyPageRenderer

	def index() {
		try{
		}
		catch(Exception e){
			e.printStackTrace()
		}
	}

	def authenticateEmployer(){
		try {
			def regemployer=Employer.findByEmailID(params?.email)
			if(regemployer){
				def employer=Employer.findByEmailIDAndPassword(params?.email,params?.password)
				if(employer){
					session["userID"]=employer.id
					session["firstName"]=employer.firstName
					session["lastName"]=employer.lastName
					session["emailID"]=employer.emailID
					session["mobile"]=employer.phone
					session["role"]="employerAdmin"
					//flash.message1="Login Successful"
					redirect(controller:"EmployerDashboard", action: "index")
				}else{
					flash.message="Email address or password does not match"
					redirect(action: "index")
				}
			}else{
				flash.message="Email address not registered"
				redirect(action: "index")
			}
		} catch (Exception e) {
			e.printStackTrace()
		}
	}

	def forgotPassword(){
		try{
			def employer = Employer.findByEmailID(params?.emailID)

			if(!employer) {
				flash.message="Invalid email address"
				redirect(controller:"Employer" ,action: "index")
			}
			if(employer) {
				//Generating Random characters as a Password
				def charset = (('A'..'Z')).join()
				def length = 5
				def randomString = RandomStringUtils.random(length, charset.toCharArray())

				//Saving password in database for that user
				employer.password=randomString

				// failOnError:true
				if(employer.save(flush: true))
				{
					// Sending new password to Email address
					Map data=new HashMap<String, String>()
					data.put("name", employer.firstName)
					data.put("password", randomString)
					data.put("email", employer.emailID)
					def content=groovyPageRenderer.render(view: '/htmlEmailTemplates/forgotPasswordEmailTemplate', model : [ userData : data] )
					sendMail {
						from 'info@cloudacar.org'
						to employer.emailID
						subject ("BTO account new password")
						html content
					}
					flash.message1="Password is Sent to your Email"
					redirect(controller:"Employer" ,action: "index")
				}
				else
				{
					flash.message="Error in forget password"
					redirect(controller:"Employer" ,action: "index")
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
			flash.message="Error in forget password"
			redirect(controller:"Employer" ,action: "index")
		}
	}

	def resetPassword(){
		def employer=Employer.findByEmailID(params?.emailID)
		if(employer.password=="" || employer.password==null){
			def emailID=params?.emailID
			respond "emailID",model:[emailID:emailID]
		}else{
			flash.message="You had already reset password"
			redirect(controller:"Employer" ,action: "index")
		}
	}

	def setPassword(){
		try {

			def status=true
			def employer=Employer.findByEmailID(params?.emailID)
			if(employer){

				if(!params?.newPassword) {
					status=false
					flash.message="New password incorrect"
					redirect(action: "resetPassword")
				}
				else if(!params?.confirmPassword) {
					status=false
					flash.message="Confirm password incorrect"
					redirect(action: "resetPassword")
				}else if((params?.confirmPassword)!=(params?.newPassword)) {
					status=false
					flash.message="Old and confirm password does not match..!"
					redirect(action: "resetPassword")
				}

				if(status){

					employer.password=params?.confirmPassword
					employer.lastUpdatedOn=new Date()

					if(employer.save(flush:true,failOnError:true)){
						flash.message="Login to your account"
						redirect(controller:"Employer", action: "index")
					}else{
						flash.message="Error in reset password"
						redirect(action: "resetPassword")
					}
				}
			}else{
				flash.message="Email address not registered"
				redirect(action: "resetPassword")
			}
		} catch (Exception e) {
			e.printStackTrace()
		}
	}

}

class dashboard{
	Integer users
	Integer trips
	Double saving
	Double fuel
	Double co2
	Double distance
}