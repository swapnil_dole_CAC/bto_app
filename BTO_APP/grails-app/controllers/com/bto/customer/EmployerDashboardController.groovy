package com.bto.customer

import java.text.DecimalFormat
import java.util.concurrent.TimeUnit;

import org.hibernate.SessionFactory;

import com.bto.trip.InviteDetails;
import com.bto.trip.TripDetails;
import com.bto.user.User;

class EmployerDashboardController {

	def matchingUserService
	SessionFactory sessionFactory
	def grailsApplication
    def index() {
		try{
			def dashboardInstance=new dashboard()
			//Set default value to zero
			dashboardInstance.users=0
			dashboardInstance.trips=0
			dashboardInstance.saving=0
			
			//Active User Count
			def usercount=User.countByStatus(true)
			dashboardInstance.users=usercount
			
			//Total Trips Count
			def tripsCount=TripDetails.count()
			dashboardInstance.trips=tripsCount
			
			//Saving Count
			def allTrips=TripDetails.findAllByTripStatus("scheduled")
			def savingCount=0,distanceTraveled=0,TotalfuelSaved=0
			def unmatchedTripList=[]
			for(TripDetails trip:allTrips)
			{
				def fuelSavedForTrip=0
				def packageDetails=BillingPlan.findByVendor(trip.vendor)
				def copassengers=InviteDetails.findAllByInvitedToTripIDAndStatus(trip,"accept")
				def lastBillPlan=0
				if(packageDetails)
				{
					for(InviteDetails invite:copassengers)
					{
						distanceTraveled=distanceTraveled+invite.invitedByTripID.tripDistance
						if(packageDetails.packageType==1)
						{
							savingCount=savingCount+(invite.invitedByTripID.tripDistance*packageDetails.amount)
						}
						else
						{
							def eta=matchingUserService.checkETA(invite.invitedByTripID.startLocation, invite.invitedByTripID.endLocation)
							def hr=TimeUnit.MINUTES.toHours(eta)
							def minimunHrLimit=0
							def flg=false,finalFlg
	
							def session1=sessionFactory.currentSession
							def result=session1.createSQLQuery("select id,vendor_id,amount,hour,kilometer,package_type from billing_plan order by hour asc").list()
							for(def i=0;i<result.size();i++)
							{
								lastBillPlan=result.get(i).getAt(0)
								if(invite.invitedByTripID.tripDistance<=result.get(i).getAt(4) && hr<=result.get(i).getAt(3))
								{
									if(unmatchedTripList.contains(invite.invitedByTripID))
										unmatchedTripList.remove(invite.invitedByTripID)
									savingCount=savingCount+(invite.invitedByTripID.tripDistance*result.get(i).getAt(2))
									break
								}
								else
								{
									if(!unmatchedTripList.contains(invite.invitedByTripID))
										unmatchedTripList.add(invite.invitedByTripID)
								}
							}
						}
					}
					def avg=CarType.findByType(trip.carType)
					if(avg)
						fuelSavedForTrip=distanceTraveled/avg.petrolMileage
						
					TotalfuelSaved=TotalfuelSaved+fuelSavedForTrip
					def billPlan=BillingPlan.get(lastBillPlan)
					if(billPlan)
					{
						for(TripDetails unmatchedTrip:unmatchedTripList)
						{
							savingCount=savingCount+(unmatchedTrip.tripDistance*billPlan.amount)
						}
					}
				}
				else
				{
					continue;
				}
			}
			DecimalFormat df = new DecimalFormat("#.##");
			dashboardInstance.distance=Double.parseDouble(df.format(distanceTraveled))			
			dashboardInstance.fuel=Double.parseDouble(df.format(TotalfuelSaved))
			dashboardInstance.co2=Double.parseDouble(df.format(dashboardInstance.fuel*Double.parseDouble(grailsApplication.config.grails.co2emission)))
			dashboardInstance.saving=Math.round(savingCount)
			respond "c", model: [dashboardInstance:dashboardInstance]
		}
		catch(Exception e)
		{
			e.printStackTrace()	
		}
	}
}
