package com.bto.customer


import javax.transaction.Transactional;

class CarTypeController {

    def index() {
		respond CarType.list(params), model:[carTypeInstanceCount: CarType.count()]
	}
	
	def add_type(){}
	
	def saveCarType(){
		try{
			
			def isExists=CarType.findByType(params?.type_name)
			if(!isExists)
			{
				def carTypeObj=new CarType()
				carTypeObj.type=params?.type_name
				carTypeObj.capacity=Integer.parseInt(params?.capacity)
				carTypeObj.petrolMileage=Double.parseDouble(params?.petrol)
				carTypeObj.dieselMileage=Double.parseDouble(params?.diesel)
				carTypeObj.cngMileage=Double.parseDouble(params?.cng)
				carTypeObj.createdOn=new Date()
				carTypeObj.lastUpdatedOn=new Date()
				
				if(carTypeObj.save(flush:true,failOnError:true))
				{
					flash.message1="Car Type Added Successfully"
					redirect action:"index"
				}
				
			}
			else
			{
				flash.message="Car Type with this name is already present"
				redirect action:"index"
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}

	def edit(CarType carTypeInstance)
	{
		respond carTypeInstance
	}
	
	@Transactional	
	def update(CarType carTypeInstance){
		try{
			println params?.capacity
				carTypeInstance.type=params?.type_name
				carTypeInstance.capacity=Integer.parseInt(params?.capacity)
				carTypeInstance.petrolMileage=Double.parseDouble(params?.petrol)
				carTypeInstance.dieselMileage=Double.parseDouble(params?.diesel)
				carTypeInstance.cngMileage=Double.parseDouble(params?.cng)
				carTypeInstance.lastUpdatedOn=new Date()
				
				if(carTypeInstance.save(flush:true,failOnError:true))
				{
					flash.message1="Car Type Updated Successfully"
					redirect action:"index"
				}				
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
	
	@Transactional
	def deleteCarType(CarType carTypeInstance)	
	{
		try{
			carTypeInstance.delete(flush:true)
				flash.message1="Car Type Deleted"
				redirect action:"index"
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
}
