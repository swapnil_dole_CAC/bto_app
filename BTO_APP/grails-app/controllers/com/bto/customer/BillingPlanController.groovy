package com.bto.customer

import com.bto.vendor.VendorBilling;

class BillingPlanController {

    def index() {
		respond BillingPlan.list(params), model:[billingPlanInstanceCount: BillingPlan.count()]
	}
	
	def setPlan(){
		try{
			if(params?.vendor){
				def vendor=VendorDetails.get(params?.vendor)
				if(vendor) {
					def packagesInstance=VendorBilling.findAllByVendor(vendor)
					session["vendor"]=vendor.id			
					respond "c", model: [packagesInstance:packagesInstance]
				}
				else
				{
					flash.message="No Vendor Found"
					redirect action:"index"
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
	
	def getBillingPlans()
	{
		try{
			if(!params?.vendor)
			{
				flash.message="Invalid Vendor"
				redirect action:"setPlan"
			}
			else
			{
				def vendor=VendorDetails.get(params?.vendor)
				if(vendor)
				{
					def packagesList=VendorBilling.findAllByVendor(vendor)
					redirect action:"setPlan",model:[packageListInstance: packagesList]
				}
				else
				{
					flash.message="Invalid Vendor"
					redirect action:"setPlan"
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}

	def savePlan(){
		def vendor
		try{
			vendor=VendorDetails.get(params?.vendor)
			if(vendor)
			{	
				def packageExists=BillingPlan.findAllByVendor(vendor)
				for(BillingPlan plan:packageExists)
				{
					plan.delete(flush:true,failOnError:true)
				}
				if(params?.packageType=="1")
				{	
					def vendorPlan=VendorBilling.get(params?.flatRate)
					if(vendorPlan)
					{
						def newPlan=new BillingPlan()
						newPlan.vendor=vendor
						newPlan.kilometer=vendorPlan?.kilometer
						newPlan.amount=vendorPlan?.amount
						newPlan.packageType=1
						newPlan.createdOn=new Date()
						newPlan.lastUpdatedOn=new Date()
						if(newPlan.save(flush:true,failOnError:true))
						{
							flash.message1="Plan Added Successfully"
							redirect action:"index"
						}
					}
					else
					{
						flash.message="Vendor Plan Invalid"
						redirect action:"index"
					}
				}
				else
				{
					if(params?.customRatePlan.size()>0)
					{
						for(def i=0;i<params?.customRatePlan.size();i++)
						{
							def vendorPlan=VendorBilling.get(params?.customRatePlan)
							if(vendorPlan)
							{
								def newPlan=new BillingPlan()
								newPlan.vendor=vendor
								newPlan.kilometer=vendorPlan?.kilometer
								newPlan.amount=vendorPlan?.amount
								newPlan.hour=vendorPlan?.hour
								newPlan.packageType=2
								newPlan.createdOn=new Date()
								newPlan.lastUpdatedOn=new Date()
								if(newPlan.save(flush:true,failOnError:true))
								{
									flash.message1="Plan Added Successfully"
									redirect action:"index"
								}
							}
						}
					}
				}
			}
			else
			{
				flash.message="Invalid Vendor"
				redirect action:"index"
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}	
}
