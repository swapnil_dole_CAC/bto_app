package com.bto.customer

import grails.gsp.PageRenderer;
import grails.transaction.Transactional;

class AddEventTypeController {

  PageRenderer groovyPageRenderer
	def grailsApplication

    def index() { 
		
		respond EventType.list(params), model:[eventTypeInstanceCount: EventType.count()]
	}
	
	def addEventType(){}
	
	def saveEventType(){
		try {
			def isExists=EventType.findByEventType(params?.eventType)
			if(!isExists){
				def eventType=new EventType()
				eventType.eventType=params?.eventType
				eventType.createdOn=new Date()
				eventType.lastUpdatedOn =new Date()
				
				if(eventType.save(flush:true,failOnError:true)) {
					flash.message1="Event Type Added Successfully"
					redirect action:"index"
				}
				else {
					flash.message="Error in save error event type"
					redirect action:"index"
				}
			}
			else
			{
				flash.message="Event type with this name is already exist"
				redirect action:"index"
			}
			
		} catch (Exception e) {
			e.printStackTrace()
		}
		
	}
	
	@Transactional
	def updateEventType(EventType eventTypeInstance){
		
		try{
				eventTypeInstance.eventType=params?.eventType
				eventTypeInstance.lastUpdatedOn =new Date()
				if(eventTypeInstance.save(flush:true,failOnError:true))
				{
					flash.message1="Event type Updated Successfully"
					redirect action:"index"
				}else{
					flash.message="ERROR in event type update"
					redirect action:"index"
				}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
	
	def editEventType(EventType eventTypeInstance)
	{
		respond eventTypeInstance
	}
	
	@Transactional
	def deleteEventType(EventType eventTypeInstance)
	{
		try{
			eventTypeInstance.delete(flush:true)
				flash.message1="Event type Deleted"
				redirect action:"index"
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
}
