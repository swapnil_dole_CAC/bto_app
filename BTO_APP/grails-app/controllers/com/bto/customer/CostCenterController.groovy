package com.bto.customer

import javax.transaction.Transactional;

class CostCenterController {

	def index() {
		respond CostCenter.list(params), model:[CostCenterInstanceCount: CostCenter.count()]
	}

	def addCostCenter(){}

	def saveCostCenter(){
		try{
			def isExists=CostCenter.findByCostCenterName(params?.costCenterName)
			if(!isExists) {
				def CostCenterObj=new CostCenter()
				CostCenterObj.costCenterName=params?.costCenterName
				CostCenterObj.createdOn=new Date()
				CostCenterObj.lastUpdatedOn=new Date()

				if(CostCenterObj.save(flush:true,failOnError:true)) {
					flash.message1="cost center code added successfully"
					redirect action:"index"
				}
			}
			else {
				flash.message="Cost center code is already present"
				redirect action:"index"
			}
		}
		catch(Exception e) {
			e.printStackTrace()
		}
	}

	def editCostCenter(CostCenter CostCenterInstance) {
		respond CostCenterInstance
	}

	@Transactional
	def updateCostCenter(CostCenter CostCenterInstance){
		try{
			CostCenterInstance.costCenterName=params?.costCenterName
			CostCenterInstance.lastUpdatedOn=new Date()

			if(CostCenterInstance.save(flush:true,failOnError:true)) {
				flash.message1="cost center code updated successfully"
				redirect action:"index"
			}
		}
		catch(Exception e) {
			e.printStackTrace()
		}
	}

	@Transactional
	def deleteCostCenter(CostCenter CostCenterInstance) {
		try{
			CostCenterInstance.delete(flush:true)
			flash.message1="cost center code deleted successfully"
			redirect action:"index"
		}
		catch(Exception e) {
			e.printStackTrace()
		}
	}
}
