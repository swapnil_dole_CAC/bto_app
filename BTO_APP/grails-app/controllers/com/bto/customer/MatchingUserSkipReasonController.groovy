package com.bto.customer

import grails.transaction.Transactional;

class MatchingUserSkipReasonController {

   
    def index() { 
		respond MatchingUserSkipReason.list(params), model:[matchingUserSkipReasonInstanceCount: MatchingUserSkipReason.count()]
	}
	
	def addTripSkipReason(){
		
	}
	
	def saveSkipReason(){
		try {
			def isExists=MatchingUserSkipReason.findByReasonDetails(params?.reasonDetails)
			if(!isExists){
				def matchingUserSkipReason=new MatchingUserSkipReason()
				matchingUserSkipReason.reasonDetails=params?.reasonDetails				
				matchingUserSkipReason.createdOn=new Date()
				matchingUserSkipReason.lastUpdatedOn =new Date()
				
				if(matchingUserSkipReason.save(flush:true,failOnError:true)) {
					flash.message1="reason Added Successfully"
					redirect action:"index"
				}
				else {
					flash.message="Error in save error reason details"
					redirect action:"index"
				}
			}
			else
			{
				flash.message="reason with this name is already exist"
				redirect action:"index"
			}
			
		} catch (Exception e) {
			e.printStackTrace()
		}
	}
	
	def updateSkipReason(MatchingUserSkipReason matchingUserSkipReasonInstance){
		try{
			matchingUserSkipReasonInstance.reasonDetails=params?.reasonDetails
			matchingUserSkipReasonInstance.lastUpdatedOn =new Date()
			if(matchingUserSkipReasonInstance.save(flush:true,failOnError:true))
			{
				flash.message1="Reason Updated Successfully"
				redirect action:"index"
			}else{
				flash.message="ERROR in Reason update"
				redirect action:"index"
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
	
	def editSkipReason(MatchingUserSkipReason matchingUserSkipReasonInstance)
	{
		respond matchingUserSkipReasonInstance
	}
	
	@Transactional
	def deleteSkipReason(MatchingUserSkipReason matchingUserSkipReasonInstance)
	{
		try{
			matchingUserSkipReasonInstance.delete(flush:true)
				flash.message1="Reason Deleted"
				redirect action:"index"
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
	
}
