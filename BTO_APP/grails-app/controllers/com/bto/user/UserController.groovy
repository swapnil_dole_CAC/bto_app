package com.bto.user

import grails.plugins.rest.client.RestBuilder
import groovy.json.JsonSlurper;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.web.multipart.MultipartFile

import com.bto.booking.BookingConsoleController;
import com.bto.utilities.UtilitiesService;

class UserController {

	def grailsApplication
	def bookingConsole
	def index() { }

	def register(){}
	def userProfile(){
		def user=User.get(session['userID'])
		respond User.findAllByEmailID(user.emailID), model:[userInstanceCount: User.count()]
	}
	def userVerification(){}
	def changePassword(){}

	// method to call registerUser service
	def saveUser(){
		try {

			// if user is valid then call registerUser web service
			def web="web" // set device type as web
			InputStream is1 = new URL(grailsApplication.config.grails.webSite+"WebService/registerUser?emailID="+params?.emailID+"&password="+params?.password+"&firstName="+params?.firstName+"&lastName="+params?.lastName+"&phone="+params?.phone+"&costCenter="+params?.costCenter+"&deviceType="+web+"&deviceToken="+web).openStream();
			BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1, Charset.forName("UTF-8")));
			String jsonText1 =UtilitiesService.readAll(rd1);
			JSONObject json1 = new JSONObject(jsonText1);
			if(json1.getString("response")=="success") {
				flash.message1="Registration Successfull...!"
				session["userID"]=json1.getString("userId")
				session["emailID"]=json1.getString("emailID")
				session["phone"]=json1.getString("phone")
				session["firstName"]=json1.getString("firstName")
				session["lastName"]=json1.getString("lastName")
				session["designation"]=json1.getString("designation")
				session["costCenter"]=json1.getString("costCenter")
				session["role"]="User"
				session["profilePicture"]=json1.getString("profilePicture")
				redirect(action: "userVerification")
			}
			else {
				flash.message=json1.getString("errorList").toString()
				redirect action:"register"
			}
		}
		catch (Exception e) {
			e.printStackTrace()
		}
	}

	// method to send verification code to user mobile
	def validateUser(){
		//checking verification code
		InputStream is2 = new URL(grailsApplication.config.grails.webSite+"WebService/userVerification?emailID="+params?.emailID+"&verificationCode="+params?.verificationCode+"&phone="+params?.phone+"&firstName="+params?.firstName).openStream();
		BufferedReader rd2 = new BufferedReader(new InputStreamReader(is2, Charset.forName("UTF-8")));
		String jsonText2 =UtilitiesService.readAll(rd2);
		JSONObject json2 = new JSONObject(jsonText2);
		if(json2.getString("response")=="success") {
			//set session
			session["userID"]=json2.getString("userId")
			session["emailID"]=json2.getString("emailID")
			session["phone"]=json2.getString("phone")
			session["firstName"]=json2.getString("firstName")
			session["role"]="User"
			//flash.message1="Verification Successful...!"
			redirect(controller:"BookingConsole", action: "index")
		}
		else {

			flash.message="Error in verification...!"+json2.getString("errorList").toString()
			session["emailID"]=json2.getString("emailID")
			session["phone"]=json2.getString("phone")
			session["firstName"]=json2.getString("firstName")
			session["role"]="User"
			redirect(action: "userVerification")
		}
	}

	// method to resent verification code to user mobile
	def resendVerificationCode(){

		InputStream is = new URL(grailsApplication.config.grails.webSite+"WebService/resendVerificationCode?emailID="+params?.emailID+"&phone="+params?.phone+"&firstName="+params?.firstName).openStream();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
		String jsonText=UtilitiesService.readAll(rd);
		JSONObject json = new JSONObject(jsonText);
		if(json.getString("response")=="success") {
			flash.message1="Code sent successfully...!"
			session["emailID"]=json.getString("emailID")
			session["phone"]=json.getString("phone")
			session["firstName"]=json.getString("firstName")
			session["role"]="User"
			redirect(action: "userVerification")
		}
		else {
			session["emailID"]=json.getString("emailID")
			session["phone"]=json.getString("phone")
			session["firstName"]=json.getString("firstName")
			flash.message="Error in resending verification code...!"+json.getString("errorList").toString()
			redirect(action: "userVerification")
		}
	}

	// method to login user with given credentials
	def authenticateUser(){
		InputStream is = new URL(grailsApplication.config.grails.webSite+"WebService/authenticateUser?emailID="+params?.emailID+"&password="+params?.password+"&deviceToken="+params?.deviceToken+"&deviceType="+params?.deviceType).openStream();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
		String jsonText =UtilitiesService.readAll(rd);
		JSONObject json = new JSONObject(jsonText);
		JsonSlurper jslurp=new JsonSlurper()
		Object obj=jslurp.parseText(jsonText)
		Map jsonData=(Map)obj

		if(json.getString("response")=="success") {
			//set session
			session["userID"]=json.getString("userId")
			session["emailID"]=json.getString("emailID")
			session["phone"]=json.getString("phone")
			session["firstName"]=json.getString("firstName")
			session["lastName"]=json.getString("lastName")
			session["designation"]=json.getString("designation")
			session["costCenter"]=json.getString("costCenter")
			session["role"]="User"
			session["profilePicture"]=json.getString("profilePicture")

			if(json.getString("isPhoneVerified")=="false"){
				flash.message="Please Verify before Login...!"
				redirect(action: "userVerification")
			}else{
				redirect(controller:"BookingConsole", action: "index")
			}
		}
		else {

			flash.message=jsonData.get("errorList")[0]
			//redirect(action: "userVerification")
			redirect(action: "index")
		}
	}

	def changePass(){

		InputStream is = new URL(grailsApplication.config.grails.webSite+"WebService/changePassword?emailID="+params?.emailID+"&oldPassword="+params?.oldPassword+"&newPassword="+params?.newPassword+"&confirmPassword="+params?.confirmPassword).openStream();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
		String jsonText =UtilitiesService.readAll(rd);
		JSONObject json = new JSONObject(jsonText);
		JsonSlurper jslurp=new JsonSlurper()
		Object obj=jslurp.parseText(jsonText)
		Map jsonData=(Map)obj

		if(json.getString("response")=="success") {
			flash.message1="Password Changed Successfully...!"
			redirect(action: "userProfile")
		}
		else {
			flash.message=jsonData.get("errorList")[0]
			redirect(action: "changePassword")
		}
	}

	def forgotPassword(){
		InputStream is = new URL(grailsApplication.config.grails.webSite+"WebService/forgotPassword?emailID="+params?.emailID).openStream();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
		String jsonText =UtilitiesService.readAll(rd);
		JSONObject json = new JSONObject(jsonText);
		JsonSlurper jslurp=new JsonSlurper()
		Object obj=jslurp.parseText(jsonText)
		Map jsonData=(Map)obj

		if(json.getString("response")=="success") {
			flash.message1="Password sent to your email address..!"
			redirect(action: "index")
		}
		else {
			flash.message=jsonData.get("errorList")[0]
			redirect(action: "index")
		}
	}

	def updateUserProfile(){

		try {
			MultipartFile profilePicture
			session["phone"]=params?.phone
			session["costCenter"]=params?.costCenter
			def fileExt
			byte[] encoded
			String encodedString=""
			profilePicture=params?.profileImage
			//def contentType =profilePicture.contentType
			def picSize = profilePicture.size
			fileExt=profilePicture.getOriginalFilename().tokenize(".")
				encoded=Base64.encodeBase64URLSafeString(profilePicture.getBytes())
			encodedString=new String(encoded)
			RestBuilder rest=new RestBuilder()
			def resp = rest.get(grailsApplication.config.grails.webSite+"WebService/updateUserProfile?emailID="+params?.emailID+"&phone="+params?.phone+"&dob="+params?.dob+"&gender="+params?.gender+"&department="+params?.department+"&designation="+params?.designation+"&costCenter="+params?.costCenter+"&profileImage="+encodedString+"&profilePicContentType="+fileExt[1])
			resp.json instanceof JSONObject
			//parse JSON response using JsonSlurper
			def JsonSlurper=new JsonSlurper()
			def object=JsonSlurper.parseText(resp.json.toString())
			//Get parent ArrayList called "Result"
			def resultList=object.errorList
			//Get 1st Object from ResultList
			def error=resultList[0]

			if(resp.json.response=="success") {
				flash.message1="Profile updated successfully..!"
				redirect(action: "userProfile")
			}
			else {
				flash.message=error
				redirect(action: "userProfile")
			}
		} catch (Exception e) {
			e.printStackTrace()
		}


	}

}
