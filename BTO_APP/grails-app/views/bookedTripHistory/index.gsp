<meta name="layout" content="main"/>
<link href="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css')}" rel="stylesheet" type="text/css" />
<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>
<style>
	table, th, td {
	    background-color: #ffffff;
	    border: none;
	}
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td, .table > thead > tr > th, .table-bordered {
	    border-top: medium none;
	}
	.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
	    border-top: medium none;
	    line-height: 1.42857;
	    padding: 0;
	    vertical-align: top;
	}
</style>
<script type="text/javascript">
$(document).ready(function() {
    $('table.display').DataTable();
} );
</script>
<script type="text/javascript">
function changeBackground(color) {
		//alert(color)
	   document.body.style.background = color;
	}
</script>
<div class="row" style="">
	<g:if test="${flash.message}">
		<div class="message" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.message1}">
		<div class="message1" role="status">
			${flash.message1}
		</div>
	</g:if>
	<div class="col-lg-12"> 
         <ul class="nav nav-tabs tabs">
             <li class="active tab">
                 <a href="#requested" data-toggle="tab" aria-expanded="false"> 
                     <span class="visible-xs"><i class="fa fa-home"></i></span> 
                     <span class="hidden-xs">Requested</span> 
                 </a> 
             </li> 
             <li class="tab"> 
                 <a href="#booked" data-toggle="tab" aria-expanded="false"> 
                     <span class="visible-xs"><i class="fa fa-user"></i></span> 
                     <span class="hidden-xs">Booked</span> 
                 </a> 
             </li> 
            
         </ul>          	          
         <div class="tab-content"> 
             <div class="tab-pane active" id="requested"> 
             	<g:if test="${jsonObject.bookedTripHistory}">
		            <script type="text/javascript">
						changeBackground('#FFFFFF');
					</script>
					<table id="" class="table display">
						<thead>
						<tr>				
							<th style="display: none;"></th>				
						</tr>
						</thead>
						<tbody>							
		             <g:each var="trips" in="${jsonObject.bookedTripHistory}" status="i">
		             	<g:if test="${trips.tripStatus=="requested" }">
		             	 <tr>				
					  		 <td>
		             	 	<div class="col-md-12 shadow" style="background-color: #f2f3f4;">
			            		 <div class="col-md-4"> 
						             <p>LOCATION</p>
						             <p style="float: left;"><img class="img-responsive" alt="" src="${resource(dir:'images',file:'from_to_icon.png')}" style="margin: 0 auto;"></p>
						             <p>&nbsp;&nbsp; ${trips.startLocation}</p>
						             <p>&nbsp;&nbsp;${trips.endLocation}</p>
			             		</div>
			             		<div class="col-md-2">
						             <p>DATE</p>
						             <p>${trips.tripDate}</p>
			             		</div>
			             		<div class="col-md-2">
						             <p>TIME</p>
						             <p>${trips.tripTime}</p>
			             		</div>
			             		<div class="col-md-2">
						             <p>CAR TYPE</p>
						             <p>${trips.vendorCarType}</p>
			            		</div>
			             		<div class="col-md-1">
						             <p>VENDOR</p>
						             <p>${trips.vendorName}</p>
						        </div>
						        <div class="col-md-1" style="margin-top: 4%;">
						            <p>
						              	<g:form method="post" action="cancelBookedTrip" controller="BookedTripHistory" role="form">
											<input type="hidden" name="tripID" value="${trips.tripID}">
											<button class="btn btn-success waves-effect waves-light" type="submit" onclick="return confirm(' You want to cancel?');" >Cancel</button>
										</g:form>
									</p>
			             		</div>
			             	</div>
			             	<div style="background-color: white;margin: 0.5%;" class="col-md-12"></div>
			             	</td></tr>
		             	</g:if>             
		             </g:each>
		             </tbody></table>
             	</g:if>
				<g:else>
					<script type="text/javascript">
						changeBackground('#f2f3f4');
					</script>
					<div class="row" style="margin-left: 10%;">
						<div class="col-md-12" style="width:80%; text-align: center; ">
							<img class="img-responsive" alt="" src="${resource(dir:'images',file:'no_trips.png')}" style="margin: 0 auto; height: 320px;">
							<div style="margin: 4%;"></div>
							<h3 class="text-muted">No trips planned</h3>
							<h4 class="text-muted">You have not planned any trips</h4>
							<g:link controller="BookingConsole" action="index" class="btn btn-success waves-effect waves-light"> Plan a Trip</g:link>					
						</div>
					</div>
				</g:else>	
             </div>          
             <div class="tab-pane" id="booked">
             	<g:if test="${jsonObject.bookedTripHistory}">
              		<script type="text/javascript">
						changeBackground('#FFFFFF');
					</script> 
					<table id="" class="table display">
						<thead>
							<tr>				
								<th style="display: none;"></th>				
							</tr>
						</thead>
						<tbody>
		             		<g:each var="trips" in="${jsonObject.bookedTripHistory}" status="i">
		             			<g:if test="${trips.tripStatus=="scheduled" }">
			             			<tr>				
							  			<td>
					             			<div class="col-md-12 shadow" style="background-color: #f2f3f4;">
					             				<div class="col-md-4"> 
										             <p>LOCATION</p>
										             <p style="float: left;"><img class="img-responsive" alt="" src="${resource(dir:'images',file:'from_to_icon.png')}" style="margin: 0 auto;"></p>
										             <p>&nbsp;&nbsp;${trips.startLocation}</p>
										             <p>&nbsp;&nbsp;${trips.endLocation}</p>
					             				</div>
					             				<div class="col-md-2">
										             <p>DATE</p>
										             <p>${trips.tripDate}</p>
					             				</div>
					             				<div class="col-md-2">
										             <p>TIME</p>
										             <p>${trips.tripTime}</p>
					             				</div>
					             				<div class="col-md-2">
										             <p>CAR TYPE</p>
										             <p>${trips.carType}</p>
					             				</div>
					             				<div class="col-md-1">
										             <p>VENDOR</p>
										             <p>${trips.vendorName}</p>
										              </div>
										        <div class="col-md-1" style="margin-top: 4%;">
										             <p>
										              	<g:form method="post" action="cancelBookedTrip" controller="BookedTripHistory" role="form">
															<input type="hidden" name="tripID" value="${trips.tripID}">
															<button class="btn btn-success waves-effect waves-light" type="submit" onclick="return confirm(' You want to cancel?');">Cancel</button>
														</g:form>
													</p>            
					             				</div>
					            			</div>
			              					<div style="background-color: white;margin: 0.5%;" class="col-md-12"></div>
			              				</td>
			              			</tr>
				             	</g:if>             
				             </g:each>
		             </tbody>
		      	</table>
              </g:if>
				<g:else>
					<script type="text/javascript">
						changeBackground('#f2f3f4');
					</script>
					<div class="row" style="margin-left: 10%;">
						<div class="col-md-12" style="width:80%; text-align: center; ">
							<img class="img-responsive" alt="" src="${resource(dir:'images',file:'no_trips.png')}" style="margin: 0 auto; height: 320px;">
							<div style="margin: 4%;"></div>
							<h3 class="text-muted">No trips planned</h3>
							<h4 class="text-muted">You have not planned any trips</h4>
							<g:link controller="BookingConsole" action="index" class="btn btn-success waves-effect waves-light"> Plan a Trip</g:link>					
						</div>
					</div>
				</g:else>	 
             </div>           
         </div> 
     </div> 
	<div class="col-md-0"></div>
</div>
<script src="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js')}"></script>
<script type="text/javascript">
	$(document).ready(function() {
    	$('#datatable').dataTable();
	});
</script>
<div class="container">  
  <!-- Modal -->
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="myModal" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-sm">
           <div class="modal-content" style="text-align: center;">
               <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                   <h4 class="modal-title" id="mySmallModalLabel">Congratulations...!</h4>
               </div>
               <div class="modal-body">
               		Your request is submitted successfully.
               </div>
           </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
   </div>
</div>
<script>
$(document).ready(function(){
    // Show the Modal on load
 var d="${session["popUpData"] }";
  if(d=="show"){
    $("#myModal").modal("show");
    }
  <%
  session['popUpData']="";
  %>
});
</script>

<div class="container">  
  <!-- Modal -->
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="myModalCancel" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-sm">
           <div class="modal-content" style="text-align: center;">
               <div class="modal-header" style="border-bottom: medium none;margin: -15px;">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 
               </div>
               <div class="modal-body">
               Your trip has been canceled successfully.
               </div>
           </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
   </div>  
</div>
<script>
$(document).ready(function(){
    // Show the Modal on load
 var d="${session["popUpDataCancel"] }";
  if(d=="show"){
    $("#myModalCancel").modal("show");
    }
  <%
  session['popUpDataCancel']="";
  %>
});
</script>
<script type="text/javascript">
$(document).ready(function(){
<%
session['sidebarActive']="";
session['sidebarActive']="bookedTripHistory/index";
%>
});
</script>