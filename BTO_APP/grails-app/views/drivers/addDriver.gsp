<meta name="layout" content="vendorMaster" />
<script>
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Number"+charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
		document.getElementById("mob").innerHTML = "<span style='color:red;'>Only 0-9 digits allowed...!</span>"; 
	 return false;
	}
       
    else{
    document.getElementById("mob").innerHTML = "<span></span>";
	return true;
	}
}
function isNumberKey1(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Number"+charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
		document.getElementById("pmob").innerHTML = "<span style='color:red;'>Only 0-9 digits allowed...!</span>"; 
	 return false;
	}
       
    else{
    document.getElementById("pmob").innerHTML = "<span></span>";
	return true;
	}
}
function isCharacterKeyDot(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Char"+charCode);
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 46 || charCode == 40 || charCode == 41 || charCode == 32 || charCode == 47 || charCode == 45 || charCode == 8 || charCode == 13){
    	document.getElementById("fname").innerHTML = "<span></span>";    	
        return true;
    }else{
    	document.getElementById("fname").innerHTML = "<span style='color:red;'>Only a-z A-Z digits allowed...!</span>";
        return false;
    }
}
function isCharacterKeyDot1(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Char"+charCode);
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 46 || charCode == 40 || charCode == 41 || charCode == 32 || charCode == 47 || charCode == 45 || charCode == 8 || charCode == 13){
    	document.getElementById("lname").innerHTML = "<span></span>";    	
        return true;
    }else{
    	document.getElementById("lname").innerHTML = "<span style='color:red;'>Only 0-9 a-z A-Z digits allowed...!</span>";
        return false;
    }
}
</script>
<div class="row">
	<div class="col-sm-12">
		<div class="">
			<h4 class="m-t-0 header-title">
				<b>New Driver</b>
			</h4>
			<div class="col-md-2"></div>
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:if test="${flash.message1}">
				<div class="message1" role="status">
					${flash.message1}
				</div>
			</g:if>
			<div class="col-md-8">
				<g:form action="saveDriver" method="post">
					<div>

						<div class="form-group clearfix">
							<label class="col-lg-3 control-label " for="userName2">First Name *</label>
							<div class="col-lg-9">
								<input class="form-control" id="userName2" required="required" name="firstName" type="text" oninvalid="this.setCustomValidity('Enter driver name')"
								onchange="this.setCustomValidity('')" onkeypress="return isCharacterKeyDot(event);">
								<div id="fname" style="color:red;"></div>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label" for="name2">Last name </label>
							<div class="col-lg-9">
								<input id="name2" name="lastName" type="text" class=" form-control" oninvalid="this.setCustomValidity('Enter last name')"
								onchange="this.setCustomValidity('')" onkeypress="return isCharacterKeyDot1(event);">
								<div id="lname" style="color:red;"></div>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label " for="surname2">
								Mobile *</label>
							<div class="col-lg-9">
								<input id="surname2" name="phone" required="required" type="text" class=" form-control" maxlength="10" min="10" oninvalid="this.setCustomValidity('Enter mobile number')"
								onchange="this.setCustomValidity('')" onkeypress="return isNumberKey(event);">
								<div id="mob" style="color:red;"></div>

							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label " for="POCMobile">Image</label>
							<div class="col-lg-9">
								<input class="" id="image" name="image" type="file">
							</div>
						</div>
						<div class="form-group clearfix">
							<div class="col-lg-3" style="float: right;">
								<button class="btn btn-success waves-effect waves-light"
									type="submit">Submit</button>
									&nbsp;<g:link action="index" class="btn btn-inverse waves-effect waves-light">Cancel</g:link>
							</div>
						</div>


					</div>
				</g:form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</div>
<!-- End row -->