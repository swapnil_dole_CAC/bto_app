<meta name="layout" content="vendorMaster" />
<div class="row">
	<div class="col-sm-12">
		<div class="">
			<h4 class="m-t-0 header-title">
				<b>Update Driver</b>
			</h4>
			<div class="col-md-2"></div>
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:if test="${flash.message1}">
				<div class="message1" role="status">
					${flash.message1}
				</div>
			</g:if>
			<div class="col-md-8">
				<g:form action="updateDriver" controller="Drivers" method="post">
					<div>
						<g:hiddenField name="id" value="${driverInstance?.id}" />
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label " for="userName2">First Name *</label>
							<div class="col-lg-9">
								<input class="form-control" id="userName2" name="firstName" type="text" required="required" value="${driverInstance?.firstName}">
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label" for="name2">Last name </label>
							<div class="col-lg-9">
								<input id="name2" name="lastName" type="text" class=" form-control" value="${driverInstance?.lastName}">
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label " for="surname2">
								Mobile *</label>
							<div class="col-lg-9">
								<input id="surname2" name="phone" required="required" type="text" value="${driverInstance?.phone}" maxlength="10" min="10"
									class=" form-control">

							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label " for="POCMobile">Image</label>
							<div class="col-lg-9">
								<input class="" id="image" name="image" type="file">
							</div>
						</div>
						
						<div class="form-group clearfix">
							<div class="col-lg-3" style="float: right;">
								<button class="btn btn-success waves-effect waves-light"
									type="submit">Update</button>
									&nbsp;<g:link action="index" class="btn btn-inverse waves-effect waves-light">Cancel</g:link>
							</div>
						</div>


					</div>
				</g:form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</div>
<!-- End row -->