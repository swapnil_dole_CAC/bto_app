<meta name="layout" content="main" />
<script>
function isCharacterKeyDot(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Char"+charCode);
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 46 || charCode == 40 || charCode == 41 || charCode == 32 || charCode == 47 || charCode == 45 || charCode == 8 || charCode == 13){
    	document.getElementById("etype").innerHTML = "<span></span>";    	
        return true;
    }else{
    	document.getElementById("etype").innerHTML = "<span style='color:red;'>Only a-z A-Z digits allowed...!</span>";
        return false;
    }
}
</script>
<div class="row">
	<div class="col-sm-12">
		<div class="">
			<h4 class="m-t-0 header-title">
				<b>Update Event Type</b>
			</h4>
			<div class="col-md-2"></div>
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:if test="${flash.message1}">
				<div class="message1" role="status">
					${flash.message1}
				</div>
			</g:if>
			<div class="col-md-8">
				<g:form action="updateEventType" controller="AddEventType" method="post">
					<div>
						<g:hiddenField name="id" value="${eventTypeInstance?.id}" />
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label" for="name2">Event Type *</label>
							<div class="col-lg-9">
								<input id="name2" name="eventType" type="text" class="form-control" required="required" oninvalid="this.setCustomValidity('Enter event type')"
											onchange="this.setCustomValidity('')" onkeypress="return isCharacterKeyDot(event);" value="${eventTypeInstance?.eventType}">
											<div id="etype" style="color:red;"></div>
							</div>
						</div>
						<div class="form-group clearfix">
							<div class="col-lg-3" style="float: right;">
								<button class="btn btn-success waves-effect waves-light" type="submit">Update</button>
									&nbsp;<g:link controller="AddVendor" action="index" class="btn btn-inverse waves-effect waves-light">Cancel</g:link>
							</div>
						</div>


					</div>
				</g:form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</div>
<!-- End row -->