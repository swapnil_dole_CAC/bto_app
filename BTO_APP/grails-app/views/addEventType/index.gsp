<meta name="layout" content="main"/>
 <link href="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css')}" rel="stylesheet" type="text/css" />
<h4 class="m-t-0 header-title">
		<b>Event Type List</b>
</h4>
<div class="col-md-12" style="text-align:right">
	<g:link controller="AddEventType" action="addEventType" class="btn btn-success waves-effect waves-light" style="float:right;"> New Event Type</g:link>
<br><br>
</div>
<div class="row" style="margin-top: 5%;">
	<g:if test="${flash.message}">
		<div class="message" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.message1}">
		<div class="message1" role="status">
			${flash.message1}
		</div>
	</g:if>
	
	<div class="col-md-1"></div>
<div class="col-md-10">
	<table id="datatable" class="table table-striped table-bordered">
		<thead>
			<tr>
				
				<th>Event Type</th>
				<th>Actions</th>
			</tr>
			</thead>
			<tbody>
			<g:each in="${eventTypeInstanceList}" status="i" var="eventTypeInstance">
				<tr>
				<%--<td align="center" style="border:0">${eventTypeInstance?.id}</td>--%>
				<td>${eventTypeInstance?.eventType}</td>
				
				<td>
				<g:link action="editEventType" id="${eventTypeInstance?.id}" title="Edit"><i class="md  md-edit"></i></g:link>
				
				 | 
				<g:link action="deleteEventType" id="${eventTypeInstance?.id}" title="Delete" onclick="return confirm(' You want to delete?');"><i class="md  md-delete"></i></g:link>
				
				</td>
				</tr>
			</g:each>			
		</tbody>
	</table>
</div>
<div class="col-md-1"></div>
</div>
<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js')}"></script>
<script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').dataTable();
            });
          </script>