<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<%--<meta name="layout" content="main" />
--%>

<title>User Verification | BTO</title>
<link rel="stylesheet"
	href="${resource(dir:'assets/css',file:'bootstrap.min.css') }">
<link rel="stylesheet"
	href="${resource(dir:'assets/css',file:'components.css') }">
<link rel="stylesheet"
	href="${resource(dir:'assets/css',file:'core.css') }">
<link rel="stylesheet"
	href="${resource(dir:'assets/css',file:'icons.css') }">
<%--<link rel="stylesheet"
	href="${resource(dir:'assets/css',file:'pages.css') }">
--%><link rel="stylesheet"
	href="${resource(dir:'assets/css',file:'responsive.css') }">

<style>
html, body {
	background-image:
		url("${resource(dir:'images',file:'verifiction-bg.png')}");
	background-repeat: no-repeat;
	background-size: cover;
}


.card-box {
	background: rgba(255, 255, 255, 0.75) none repeat scroll 0 0;
	margin-top: 8%;
	padding: 0;
}

.message1 {
	color: green;
}

.message {
	color: red;
}

:-moz-ui-invalid:not(output) {
    box-shadow: 0 0 1.5px 1px red;
}

input[type=number]::-webkit-outer-spin-button,
input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

input[type=number] {
    -moz-appearance:textfield;
}
</style>
</head>
<body>
	<div class="body">
		<div class="account-pages"></div>
		<div class="clearfix"></div>
		<div class="wrapper-page">
			<div class="col-md-4"></div>
			<div class="card-box col-md-4">
				<div class="panel-heading text-center">
					<g:if test="${flash.message}">
						<div class="message" role="status">
							${flash.message}
						</div>
					</g:if>
					<g:if test="${flash.message1}">
						<div class="message1" role="status">
							${flash.message1}
						</div>
					</g:if>
					<h3 class="">Verify Mobile Number</h3>					
					<div class="panel-body">
						<g:form class="form-horizontal " controller="User"
							action="validateUser" method="post">
							<input type="hidden" name="emailID" value="${session.emailID}">
							<input type="hidden" name="phone" value="${session.phone}">
							<input type="hidden" name="firstName" value="${session.firstName}">
							
							<div class="form-group text-center">
								
								<div class="col-md-12">
									Enter the 4-digit verification code sent to your mobile
								</div>
								<div style="margin: 8%;"></div>
								
								<div class="col-md-12">
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<input class="form-control inputTranspernt"
										name="verificationCode" type="password" required="" maxlength="4"
										oninvalid="this.setCustomValidity('Enter code')"
												onchange="this.setCustomValidity('')"
										placeholder="Enter code" >
								</div>										
								<div class="col-md-2"></div>
								</div>
								
							</div>
							<div class="form-group text-center">
								<div class="col-md-12">
									<button class="btn btn-success waves-effect waves-light"
										type="submit" style="width: 40%;">Verify</button>
									<%--<g:link action="index"  class="btn btn-inverse waves-effect waves-light">Cancel</g:link>--%>
								</div>
							</div>

						</g:form>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12 text-center">
						<g:form controller="User" action="resendVerificationCode" method="post">
							<input type="hidden" name="emailID" value="${session.emailID}">
							<input type="hidden" name="phone" value="${session.phone}">
							<input type="hidden" name="firstName" value="${session.firstName}">
							<p>
								Code Not Received?
								<g:link class="text-primary">
									<button class="btn btn-link" type="submit" style="margin-bottom: 1%">Resend</button>
								</g:link>
							</p>
						</g:form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4"></div>
	</div>

</body>
</html>