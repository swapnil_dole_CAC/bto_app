<%@page import="com.bto.customer.CostCenter"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<%--<meta name="layout" content="main" />
--%>

<title>Register Here | BTO</title>
		<link rel="stylesheet" href="${resource(dir:'assets/css',file:'bootstrap.min.css') }">
		<link rel="stylesheet" href="${resource(dir:'assets/css',file:'components.css') }">
		<link rel="stylesheet" href="${resource(dir:'assets/css',file:'core.css') }">
		<link rel="stylesheet" href="${resource(dir:'assets/css',file:'icons.css') }">
		<%--<link rel="stylesheet" href="${resource(dir:'assets/css',file:'pages.css') }">
		--%><link rel="stylesheet" href="${resource(dir:'assets/css',file:'responsive.css') }">

		<style>
html,body {
	background-image: url("${resource(dir:'images',file:'reg-bg.jpg')}");
	background-repeat: no-repeat;
	background-size: cover;
	
}

.inputTranspernt{
//background: transparent;
}

.card-box{
 background: rgba(255, 255, 255, 0.75) none repeat scroll 0 0;
   margin-top:8%;
    padding: 0;
   
}
.panel-body {
    padding: 5px;
}

.message1 {
	color: green;
}


.message {
	color: red;
}

:-moz-ui-invalid:not(output) {
    box-shadow: 0 0 1.5px 1px red;
}


</style>
		
<script>
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Number"+charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
		document.getElementById("mob").innerHTML = "<span style='color:red;'>Only 0-9 digits allowed...!</span>"; 
	 return false;
	}
       
    else{
    document.getElementById("mob").innerHTML = "<span></span>";
	return true;
	}
}
function isCharacterKeyDot(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Char"+charCode);
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 46 || charCode == 40 || charCode == 41 || charCode == 32 || charCode == 47 || charCode == 45 || charCode == 8 || charCode == 13){
    	document.getElementById("fname").innerHTML = "<span></span>";    	
        return true;
    }else{
    	document.getElementById("fname").innerHTML = "<span style='color:red;'>Only a-z A-Z digits allowed...!</span>";
        return false;
    }
}
function isCharacterKeyDot1(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Char"+charCode);
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 46 || charCode == 40 || charCode == 41 || charCode == 32 || charCode == 47 || charCode == 45 || charCode == 8 || charCode == 13){
    	document.getElementById("lname").innerHTML = "<span></span>";    	
        return true;
    }else{
    	document.getElementById("lname").innerHTML = "<span style='color:red;'>Only a-z A-Z digits allowed...!</span>";
        return false;
    }
}
</script>
</head>
<body>
	<div class="body">
		<div class="account-pages"></div>
		<div class="clearfix"></div>
		<div class="wrapper-page">
			<div class=col-md-1></div>
			<div class="card-box col-md-5">
				<div class="panel-heading">
				<g:if test="${flash.message}">
						<div class="message" role="status">
							${flash.message}
						</div>
					</g:if>
					<g:if test="${flash.message1}">
						<div class="message1" role="status">
							${flash.message1}
						</div>
					</g:if>
					<h3 class="" style="text-align: center;">Register</h3>					
					<div class="panel-body">
						<g:form class="form-horizontal m-t-10" controller="User" action="saveUser" method="post" onsubmit="return validate();">
							<g:hiddenField name="deviceType" value="Web"/>
							<div class="form-group ">
								<div class="col-md-6">
									<label>First name*</label> 
									<input class="form-control inputTranspernt"
									oninvalid="this.setCustomValidity('Enter first name')"
												onchange="this.setCustomValidity('')"
										name="firstName" type="text" required="" onkeypress="return isCharacterKeyDot(event);"
										placeholder="Enter first name">
										<div id="fname" style="color:red;"></div>
								</div>
								<div class="col-md-6">
									<label>Last name*</label> <input class="form-control inputTranspernt" name="lastName" onkeypress="return isCharacterKeyDot1(event);"
										oninvalid="this.setCustomValidity('Enter last name')"
											onchange="this.setCustomValidity('')"
										type="text" required="" placeholder="Enter last name">
										<div id="lname" style="color:red;"></div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label>Email address*</label> <input class="form-control inputTranspernt" name="emailID"
									oninvalid="this.setCustomValidity('Enter email address')"
											onchange="this.setCustomValidity('')"
										type="email" required="" placeholder="Enter email address">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label>Password*</label> <input class="form-control inputTranspernt" name="password"
									oninvalid="this.setCustomValidity('Enter password')"
											onchange="this.setCustomValidity('')"
										type="password" required="" placeholder="Enter password">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6">
									<label>Mobile number *</label> <input class="form-control inputTranspernt" name="phone" maxlength="10" pattern=".{10,}"
									oninvalid="this.setCustomValidity('Enter mobile number (must be 10 digits)')"
												onchange="this.setCustomValidity('')"
										type="text" required="" placeholder="Enter mobile number" onkeypress="return isNumberKey(event);">
										<div id="mob" style="color:red;"></div>
								</div>
								<div class="col-md-6">
									<label>Cost center *</label> 
									<%--
									<input class="form-control inputTranspernt" name="costCenter" maxlength="10"
										type="text" required="" placeholder="Enter Cost Center Code">
									--%>
									<select name="costCenter" class=" form-control"
										data-style="btn-white" required="required">
										<option class="text-muted" value="" disabled selected>Select</option>
										<option value="N/A">N/A</option>
										<g:each var="costCenter" in="${CostCenter.list()}">
											<option value="${costCenter.costCenterName}">
												${costCenter.costCenterName}
											</option>
										</g:each>
									</select>
								</div>
							</div>
							<div class="form-group m-t-30" style="text-align: center;">
								<div class="col-md-12">
									<button class="btn btn-success waves-effect waves-light"
										type="submit" >Register</button>
										
								</div>
							</div>
						</g:form>
					</div>

				</div><div class="row">
				<div class="col-md-12 text-center">
					<p>
					 Already have account? <g:link class="text-primary " action="index">Login</g:link>
					</p>
				</div>
			</div>
			</div>
		</div>
		<div class=col-md-6></div>
	</div>
	<script type="text/javascript">
	function validate() {
		var a = 0, rdbtn = document.getElementsByName("gender")
		for (i = 0; i < rdbtn.length; i++) {
			if (rdbtn.item(i).checked == false) {
				a++;
			}
		}
		if (a == rdbtn.length) {
			//alert("Please select your gender");
			document.getElementById("type").style.border = "2px solid red";
			return false;
		} else {
			document.getElementById("type").style.border = "";
		}
	}
</script>
</body>
</html>