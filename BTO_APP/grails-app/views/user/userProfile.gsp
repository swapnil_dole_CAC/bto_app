<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="layout" content="main" />
<title>Profile | BTO</title>
<%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
--%>

<link rel="stylesheet"	href="${resource(dir:'assets/plugins/timepicker',file:'bootstrap-timepicker.min.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/mjolnic-bootstrap-colorpicker/dist/css',file:'bootstrap-colorpicker.min.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/bootstrap-datepicker/dist/css',file:'bootstrap-datepicker.min.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/clockpicker/dist',file:'jquery-clockpicker.min.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/bootstrap-daterangepicker',file:'daterangepicker.css') }">


<style type="text/css">
html, body {
	background: #FFFFFF;
}

.card-box {
	//box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0	rgba(0, 0, 0, 0.19);
	 box-shadow: 1px 1px 5px #aeaeae;
	 background-color: transparent;
}



</style>

 <script>
            function check(x) {
                var user = x;
               
                if (user == "disp") {
                    $("#editProfile").css("display", "");
                    $("#showProfile").css("display", "none");
                }
                if (user == "edit") {
                    $("#showProfile").css("display", "");
                    $("#editProfile").css("display", "none");

                }
            }
        </script>
<script>
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Number"+charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
		document.getElementById("mob").innerHTML = "<span style='color:red;'>Only 0-9 digits allowed...!</span>"; 
	 return false;
	}
       
    else{
    document.getElementById("mob").innerHTML = "<span></span>";
	return true;
	}
}
function isCharacterKeyDot(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Char"+charCode);
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 46 || charCode == 40 || charCode == 41 || charCode == 32 || charCode == 47 || charCode == 45 || charCode == 8 || charCode == 13){
    	document.getElementById("desig").innerHTML = "<span></span>";    	
        return true;
    }else{
    	document.getElementById("desig").innerHTML = "<span style='color:red;'>Only a-z A-Z digits allowed...!</span>";
        return false;
    }
}
function isCharacterKeyDot1(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Char"+charCode);
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 46 || charCode == 40 || charCode == 41 || charCode == 32 || charCode == 47 || charCode == 45 || charCode == 8 || charCode == 13){
    	document.getElementById("dept").innerHTML = "<span></span>";    	
        return true;
    }else{
    	document.getElementById("dept").innerHTML = "<span style='color:red;'>Only a-z A-Z digits allowed...!</span>";
        return false;
    }
}
</script>
</head>
<body>
	<div class="body">
		
					<g:each var="user" in="${userInstanceList}" status="i">
						<div class="profile-detail card-box" id="showProfile" style="width: 40%;margin-left: 30%;">
						<p onclick="check('disp');"><i class="glyphicon glyphicon-pencil" style="float: right;"></i></p>
							<div>
							<g:if test="${flash.message}">
								<div class="message" role="status">
									${flash.message}
								</div>
							</g:if>
							<g:if test="${flash.message1}">
								<div class="message1" role="status">
									${flash.message1}
								</div>
							</g:if>
							<g:if test="${user?.profilePicture.toString().equals('') }">
							<g:set var="profilePic" value="user.png"></g:set>
							</g:if>
							<g:else>
							<g:set var="profilePic" value="${user?.profilePicture}"></g:set>
							</g:else>
							<img src="${resource(dir:'images/userProfilePictures',file:profilePic)}" class="img-circle" alt="profile-image" />
								


								<h4 class="font-600">${session['firstName']} ${session['lastName'] }</h4>
	
								
								<div style="margin-left:-10px%;" class="panel-body form-horizontal m-t-10">
									
										<div class="form-group form-horizontal m-t-10">
											<div class="col-md-6" style="text-align: left;">
												<label>Email address</label>
												<p>${session['emailID']}</p>
											</div>											
											<div class="col-md-6" style="text-align: center;">
												
												<label>Mobile</label>
												<p>${session['phone']}</p>
											</div>
										</div>
										
															
										<div class="form-group form-horizontal m-t-10">
											<div class="col-md-6" style="text-align: left;">
												<label>Date of Birth</label>
												<p>${user?.dob}</p>
											</div>
											
											<div class="col-md-6" style="text-align: center;">
												<label>Gender</label>
												<p>${user?.gender}</p>
											</div>
										</div>
										<div class="form-group form-horizontal m-t-10">
											<div class="col-md-6" style="text-align: left;">
												<label>Department</label>
												<p>${user?.department}</p>
											</div>
											
											<div class="col-md-6" style="text-align: center;">
												<label>Designation</label>
												<p>${user?.designation}</p>
											</div>
										</div>	
										<div class="form-group form-horizontal m-t-10">
											<div class="col-md-6" style="text-align: left;">
											<label>Cost center</label>
												<p>${session['costCenter']}</p>
											</div>
											
											<div class="col-md-6" style="text-align: center;">
												
											</div>
										</div>	
								</div>
								
							</div>
						</div>
						
						<div class="profile-detail card-box" id="editProfile" style="width: 40%;margin-left: 30%;display: none">
						<p onclick="check('edit');"><i class="glyphicon  glyphicon-remove" style="float: right;"></i></p>
						<g:uploadForm action="updateUserProfile" controller="User" method="post">
						<div>
								<div class="show-image">
								<img src="${resource(dir:'images/userProfilePictures',file:profilePic)}" class="img-circle" alt="profile-image" />
								<input type="file" name="profileImage">
								</div>


								<h4 class="font-600">${session['firstName']} ${session['lastName'] }</h4>

				
								<div style="margin-left:-10px%;" class="panel-body form-horizontal m-t-0">
									
										<div class="form-group form-horizontal m-t-0">
											<div class="col-md-6" style="text-align: left;">
												<label>Email address</label>
												<p>${session['emailID']}</p>
												<input type="hidden" name="firstName" value="${session['firstName']}">
												<input type="hidden" name="lastName" value="${session['lastName']}">
												<input type="hidden" name="emailID" value="${session['emailID']}">
											</div>											
											<div class="col-md-6" style="text-align: center;">
												
												<label>Mobile</label>
												<p><input class="form-control"
												oninvalid="this.setCustomValidity('Enter mobile number')" value="${session['phone']}"
												onchange="this.setCustomValidity('')"
										name="phone" type="text" required="" maxlength="10"
										placeholder="Enter mobile number"></p>
											</div>
										</div>
										
															
										<div class="form-group form-horizontal m-t-0">
											<div class="col-md-6" style="text-align: left;">
											<label>Date of Birth</label>
												<p>
												
												<input type="text" class="form-control"
													data-date-format='dd/mm/yyyy' name="dob" value="${user?.dob}"
													placeholder="dd/mm/yyyy" id="datepicker">												
												</p>												
											</div>
											
											<div class="col-md-6" style="text-align: center;">
												<label>Gender</label>
												<p><input type="radio" name="gender" value="Male">Male &nbsp;&nbsp;&nbsp;<input type="radio" name="gender" value="Female">Female</p>
											</div>
										</div>
										<div class="form-group form-horizontal m-t-0">
											<div class="col-md-6" style="text-align: left;">
												<label>Department</label>
												<p><input class="form-control" name="department" type="text" 
										oninvalid="this.setCustomValidity('Enter department')" value="${user?.department}" onkeypress="return isCharacterKeyDot1(event);"
												onchange="this.setCustomValidity('')"
										placeholder="Enter department">
										<div id="dept" style="color:red;"></div></p>
											</div>
											
											<div class="col-md-6" style="text-align: center;">
												<label>Designation</label>
												<p><input class="form-control" name="designation" type="text" 
												oninvalid="this.setCustomValidity('Enter designation')" value="${user?.designation}" onkeypress="return isCharacterKeyDot(event);"
												onchange="this.setCustomValidity('')" placeholder="Enter designation">
												<div id="desig" style="color:red;"></div>
												</p>
											</div>
										</div>	
										<div class="form-group form-horizontal m-t-0">
											<div class="col-md-6" style="text-align: left;">
											<label>Cost center</label>
												<p><input class="form-control" name="costCenter" type="text" required=""
												oninvalid="this.setCustomValidity('Enter cost center')" value="${session['costCenter']}"
												onchange="this.setCustomValidity('')" placeholder="Enter cost center"></p>
											</div>
											
											<div class="col-md-6" style="text-align: center;">
												
											</div>
										</div>	
										<button class="btn btn-success waves-effect waves-light" style="width: 40%;"
											type="submit">Update</button>	
								</div>



						</div>
						</g:uploadForm>
					</div>
					</g:each>
				</div>
			<div class="col-md-4"></div>
<script type="text/javascript">
$(document).ready(function(){
<%
session['sidebarActive']="";
session['sidebarActive']="user/userProfile";
%>
});
</script>
</body>
</html>