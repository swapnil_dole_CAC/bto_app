<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="layout" content="main" />
<title>Change Password</title>
<style type="text/css">
html, body {
	background: #FFFFFF;
}

.card-box { //
	box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0
		rgba(0, 0, 0, 0.19);
	box-shadow: 1px 1px 5px #aeaeae;
	background-color: transparent;
}
</style>
<script>
function validatePassword() {
var oldPassword,newPassword,confirmPassword,output = true;

oldPassword = document.frmChange.oldPassword;
newPassword = document.frmChange.newPassword;
confirmPassword = document.frmChange.confirmPassword;

if(!oldPassword.value) {
	oldPassword.focus();
	document.getElementById("oldPassword").innerHTML = "required";
	output = false;
}
else if(!newPassword.value) {
	newPassword.focus();
	document.getElementById("newPassword").innerHTML = "required";
	output = false;
}
else if(!confirmPassword.value) {
	confirmPassword.focus();
	document.getElementById("confirmPassword").innerHTML = "required";
	output = false;
}
if(newPassword.value != confirmPassword.value) {
	newPassword.value="";
	confirmPassword.value="";
	newPassword.focus();
	document.getElementById("confirmPassword").innerHTML = "<span style=\"color:red\">New and confirm password does not match</span>";
	output = false;
} 	
return output;
}
</script>
</head>
<body>
	<div class="body">
				<div class=" card-box" style="padding: 0px;width: 34%;margin-left: 30%;">
			<div class="panel-heading">
				<h3 class="" style="text-align: center;">Change password</h3>
				
				<g:if test="${flash.message}">
						<div class="message" role="status">
							${flash.message}
						</div>
				</g:if>
				<g:if test="${flash.message1}">
						<div class="message1" role="status">
							${flash.message1}
						</div>
				</g:if>
				<div class="panel-body">
					<g:form class="form-horizontal m-t-20" controller="User" action="changePass" onSubmit="return validatePassword()" name="frmChange">
						<input type="hidden" name="emailID" value="${session['emailID'] }"/>
						<div class="form-group">
							<div class="col-md-12">
								<label>Old Password*</label> <input class="form-control" name="oldPassword"
								oninvalid="this.setCustomValidity('Enter old password')"
												onchange="this.setCustomValidity('')"
									type="password" required="" placeholder="Enter old password">
									<span id="oldPassword"  class="required"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<label>New Password*</label> <input class="form-control" name="newPassword"
								oninvalid="this.setCustomValidity('Enter new password')"
												onchange="this.setCustomValidity('')"
									type="password" required="" placeholder="Enter new password">
									<span id="newPassword"  class="required"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<label>Confirm Password*</label> <input class="form-control" name="confirmPassword"
								oninvalid="this.setCustomValidity('Confirm new password')"
								onchange="this.setCustomValidity('')"	type="password" required="" placeholder="Confirm new password">
									<span id="confirmPassword"  class="required"></span>
							</div>
						</div>
						
						<div class="form-group text-center m-t-30">
							<div class="col-md-12">
								<button	class="btn btn-success  text-uppercase waves-effect waves-light"
									type="submit" style="width: 40%;">Submit</button>
							</div>
						</div>
					</g:form>
					

			</div>
		</div>
				</div>
			</div>
<script type="text/javascript">
$(document).ready(function(){
<%
session['sidebarActive']="";
session['sidebarActive']="user/changePassword";
%>
});
</script>
</body>
</html>