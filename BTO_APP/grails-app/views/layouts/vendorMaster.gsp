<g:if test="${session['userID']!=null}">
</g:if>
<g:else>
	<g:javascript>
		window.location.href = '../vendor/index.gsp';
	</g:javascript>
</g:else>

<!DOCTYPE html>
<%@page import="org.h2.command.ddl.CreateLinkedTable"%>
<html lang="en" class="no-js">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="BTO" /></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
		<link rel="stylesheet" href="${resource(dir:'assets/css',file:'bootstrap.min.css') }">
		<link rel="stylesheet" href="${resource(dir:'assets/css',file:'components.css') }">
		<link rel="stylesheet" href="${resource(dir:'assets/css',file:'core.css') }">
		<link rel="stylesheet" href="${resource(dir:'assets/css',file:'icons.css') }">
		<link rel="stylesheet" href="${resource(dir:'assets/css',file:'pages.css') }">
		<link rel="stylesheet" href="${resource(dir:'assets/css',file:'responsive.css') }">
	
		<style type="text/css">
			html, body {
				background: #FFFFFF;
			}
			
			.shadow {
				box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);
				margin-bottom: 1%;
			}
			
			.side-menu.left {
				 background-color:#0EBAB8;
				 filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#0EBAB8, endColorstr=#267F9E);
				 background-image:-moz-linear-gradient(top, #0EBAB8 0%, #267F9E 100%);
				 background-image:-webkit-linear-gradient(top, #0EBAB8 0%, #267F9E 100%);
				 background-image:-ms-linear-gradient(top, #0EBAB8 0%, #267F9E 100%);
				 background-image:linear-gradient(top, #0EBAB8 0%, #267F9E 100%);
				 background-image:-o-linear-gradient(top, #0EBAB8 0%, #267F9E 100%);
				 background-image:-webkit-gradient(linear, right top, right bottom, color-stop(0%,#0EBAB8), color-stop(100%,#267F9E));}
			}
						
			#sidebar-menu > ul > li > a {
				color: #ffffff;
				border-left: 3px solid #36404A;
			}
			#sidebar-menu > ul > li > a {
    		border-left: medium none;
    		}
			#sidebar-menu > ul > li > a {
			    color: #ffffff;
			}
			
			#sidebar-menu>ul>li>a:hover {
				background-color: #f5f5f5;
				color: #141414;
			}
			#sidebar-menu > ul > li > a.active {
   			 background: #f5f5f5 none repeat scroll 0 0 !important;
   			 border-left: 3px solid #141414;
  			 color: #141414 !important;
			}
			
			.btn-success, .btn-success:hover, .btn-success:focus, .btn-success:active, .btn-success.active, .btn-success.focus, .btn-success:active, .btn-success:focus, .btn-success:hover, .open > .dropdown-toggle.btn-success {
			    background-color: #0ebab8 !important;
			    border: 1px solid #0ebab8 !important;
			}
			
			#sidebar-menu ul ul a {
			    color: #ffffff;
			    display: block;
			    padding: 10px 20px 10px 65px;
			}
			
			.message1 {
				color: green;
			}
			
			.message {
				color: red;
			}
			
			.shadow {
				margin-bottom: 2%;
				box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
			}
			
			.innerShadow {
				-webkit-box-shadow: inset 0px -1px 18px -10px rgba(0, 0, 0, 0.67);
				-moz-box-shadow: inset 0px -1px 18px -10px rgba(0, 0, 0, 0.67);
				box-shadow: inset 0px -1px 18px -10px rgba(0, 0, 0, 0.67);
				margin-bottom: 1%;
			}
			.page-title {
			    background-color: #f8f8f8;
			    border-bottom: 0.0625rem solid #e4e4e4;
			    padding: 0.3125rem 0.75rem;
			    margin-left: -2rem;
			    margin-right: -1.2rem;
			    margin-top: -2rem;
			    margin-bottom: 2.75rem; 
			   }
		</style>
		<g:layoutHead />
	</head>
<body>
	<g:if test="${session['sidebarActive'].toString().equals('tripRequests/index') }">
	<g:set var="tripRequestsIndex" value="active"></g:set>
	</g:if>
	<g:else>
	<g:set var="tripRequestsIndex" value=""></g:set>
	</g:else>
	<g:if test="${session['sidebarActive'].toString().equals('tripRequests/requestDetails') }">
	<g:set var="tripRequestsRequestDetails" value="active"></g:set>
	</g:if>
	<g:else>
	<g:set var="tripRequestsRequestDetails" value=""></g:set>
	</g:else>
	<g:if test="${session['sidebarActive'].toString().equals('drivers/index') }">
	<g:set var="driversIndex" value="active"></g:set>
	</g:if>
	<g:else>
	<g:set var="driversIndex" value=""></g:set>
	</g:else>
	<g:if test="${session['sidebarActive'].toString().equals('vendorCars/index') }">
	<g:set var="vendorCarsIndex" value="active"></g:set>
	</g:if>
	<g:else>
	<g:set var="vendorCarsIndex" value=""></g:set>
	</g:else>
	<g:if test="${session['sidebarActive'].toString().equals('rejectionReason/index') }">
	<g:set var="rejectionReasonIndex" value="active"></g:set>
	</g:if>
	<g:else>
	<g:set var="rejectionReasonIndex" value=""></g:set>
	</g:else>
	<g:if test="${session['sidebarActive'].toString().equals('vendorBilling/index') }">
	<g:set var="vendorBillingIndex" value="active"></g:set>
	</g:if>
	<g:else>
	<g:set var="vendorBillingIndex" value=""></g:set>
	</g:else>
	<div id="wrapper">
		<g:if test="${session['role']=="vendorAdmin"}">
			<!-- Top Bar Start -->
			<div class="topbar">
				<!-- LOGO -->
				<div class="topbar-left">
                    <div class="text-center">
                        <img src="${resource(dir:'images',file:'logo.png')}" alt="logo" class="" style="margin-top: 3.5%;margin-left: -2%;">
                    </div>
                </div>

				<!-- Button mobile view to collapse sidebar menu -->
				<div class="navbar navbar-default" role="navigation">
					<div class="container">
						<div class="">
							<div class="pull-left">
								<button class="button-menu-mobile open-left">
									<i class="ion-navicon"></i>
								</button>
								<span class="clearfix"></span>
							</div>
							<ul class="nav navbar-nav navbar-right pull-right">
								<!-- <li class="dropdown hidden-xs"><a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light"	data-toggle="dropdown" aria-expanded="true"> <i	class="icon-bell"></i> <span class="badge badge-xs badge-danger"></span></a></li> -->
								<li class="dropdown hidden-xs">
                                    <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                                        ${session["companyName"] } </a>                                    
                                </li>
								<li class="dropdown"><a href="#" class="dropdown-toggle profile" data-toggle="dropdown"	aria-expanded="true"> <img src="${resource(dir:'assets/images/users',file:'avatar-1.jpg')}" alt="user-img" class="img-circle"> </a>
									<ul class="dropdown-menu">
										<li><g:link controller="Vendor" action="userProfile"><i class="ti-user m-r-5"></i> Profile</g:link></li>
										<li><g:link controller="Vendor" action="changePassword"><i class="ti-pencil m-r-5 ${userChangePassword}"></i> Change password</g:link></li>
                                        <li><g:link controller="Logout" action="index"><i class="ti-power-off m-r-5"></i> Logout</g:link></li>
									</ul>
								</li>
							</ul>
						</div>
						<!--/.nav-collapse -->
					</div>
				</div>
			</div>
			<!-- Top Bar End -->

			<!-- ========== Left Sidebar Start ========== -->

			<div class="left side-menu">
				<div class="sidebar-inner slimscrollleft">
					<!--- Divider -->
					<div id="sidebar-menu">
						<!-- <ul style="margin-left: 5%;">
							<li>
								<img src="${resource(dir:'assets/images/users',file:'avatar-1.jpg')}" alt="user-img" class="img-circle" style="height: 36px; width: 36px; float: left; margin: 10px 0 10px 20px;"></li>
							<li class="menu-title waves-effect" style="color: #ffffff; font-size: 16px; margin-left: -6%;">
								${session["companyName"] }</li>
							<li class="text-muted menu-title" style="color: #777; margin-left: 20%; margin-top: -8%;">
								Vendor</li>
						</ul> -->
						<div style="margin: 2%;"></div>
						<ul>
							<%--<li><g:link controller="Vendor" action="dashboard" class="waves-effect"><i class="ti-dashboard"></i> <span>DashBoard</span></g:link></li>--%>
							<li><g:link controller="TripRequests" action="index" class="waves-effect ${tripRequestsIndex}"> <i class="ti-location-pin"></i><span>Trip Requests</span></g:link></li>
							<li><g:link controller="TripRequests" action="requestDetails" class="waves-effect ${tripRequestsRequestDetails }"><i class="ti-location-pin"></i><span>Requests Details</span></g:link></li>
							<li class="has_sub"><a href="#" class="waves-effect">
								<i class="ti-menu-alt"></i> <span> Options </span> </a>
								<ul class="list-unstyled">
									<li><g:link controller="Drivers" action="index" class="waves-effect ${driversIndex}">
											<i class="ti-user"></i>
											<span>Drivers</span>
										</g:link></li>
									<li><g:link controller="VendorCars" action="index" class="waves-effect ${vendorCarsIndex}">
											<i class="ti-car"></i>
											<span>Car Types</span>
										</g:link></li>
									<li><g:link controller="RejectionReason" action="index" class="waves-effect ${rejectionReasonIndex}">
											<i class="ti-help"></i>
											<span>Reasons</span>
										</g:link></li>
								</ul></li>
							<li><g:link controller="VendorBilling" action="index" class="waves-effect ${vendorBillingIndex}">
									<i class="ti-money"></i>
									<span>Create Billing Plan</span>
								</g:link></li>
							<li><g:link controller="Logout" action="index" class="waves-effect">
									<i class="icon-logout"></i>
									<span>Logout</span>
								</g:link>
							<li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- Left Sidebar End -->
		</g:if>
	</div>
	<div class="body">
		<div class="content-page">
			<!-- Start content -->
			<div class="content">
				<div class="container">
					<g:layoutBody />
				</div>
			</div>
		</div>
	</div>
	<div class="footer" role="contentinfo" style="display: none;"></div>
	<div id="spinner" class="spinner" style="display: none;">
		<g:message code="spinner.alt" default="Loading&hellip;" />
	</div>

	<script type="text/javascript">
		var resizefunc = [];
		</script>

	<!-- jQuery  -->
	<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>
	<script src="${resource(dir:'assets/js',file:'bootstrap.min.js')}"></script>
	<script src="${resource(dir:'assets/js',file:'detect.js')}"></script>
	<script src="${resource(dir:'assets/js',file:'fastclick.js')}"></script>
	<script src="${resource(dir:'assets/js',file:'jquery.slimscroll.js')}"></script>
	<script src="${resource(dir:'assets/js',file:'jquery.blockUI.js')}"></script>
	<script src="${resource(dir:'assets/js',file:'waves.js')}"></script>
	<script src="${resource(dir:'assets/js',file:'wow.min.js')}"></script>
	<script src="${resource(dir:'assets/js',file:'jquery.nicescroll.js')}"></script>
	<script src="${resource(dir:'assets/js',file:'jquery.scrollTo.min.js')}"></script>
	<script src="${resource(dir:'assets/js',file:'jquery.core.js')}"></script>
	<script src="${resource(dir:'assets/js',file:'jquery.app.js')}"></script>

	<!--FooTable      below files foe data table -->
	<!--End FooTable      below files foe data table -->

	<!--start pickers--- below files foe booking console date and time picker-->
	<script src="${resource(dir:'assets/plugins/moment',file:'moment.js')}"></script>
	<script src="${resource(dir:'assets/plugins/timepicker',file:'bootstrap-timepicker.min.js')}"></script>
	<script src="${resource(dir:'assets/plugins/mjolnic-bootstrap-colorpicker/dist/js',file:'bootstrap-colorpicker.min.js')}"></script>
	<script src="${resource(dir:'assets/plugins/bootstrap-datepicker/dist/js',file:'bootstrap-datepicker.min.js')}"></script>
	<script src="${resource(dir:'assets/plugins/clockpicker/dist',file:'jquery-clockpicker.min.js')}"></script>
	<script src="${resource(dir:'assets/plugins/bootstrap-daterangepicker',file:'daterangepicker.js')}"></script>

	<!-- start data table below files for data table -->
	<script src="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js')}"></script>
	<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js')}"></script>
	<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js')}"></script>
	<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.buttons.min.js')}"></script>
	<script src="${resource(dir:'assets/plugins/datatables',file:'buttons.bootstrap.min.js')}"></script>
	<script src="${resource(dir:'assets/plugins/datatables',file:'jszip.min.js')}"></script>
	<script src="${resource(dir:'assets/plugins/datatables',file:'pdfmake.min.js')}"></script>
	<script src="${resource(dir:'assets/plugins/datatables',file:'vfs_fonts.js')}"></script>
	<script	src="${resource(dir:'assets/plugins/datatables',file:'buttons.html5.min.js')}"></script>
	<script	src="${resource(dir:'assets/plugins/datatables',file:'buttons.print.min.js')}"></script>
	<script	src="${resource(dir:'assets/plugins/datatables',file:'dataTables.fixedHeader.min.js')}"></script>
	<script	src="${resource(dir:'assets/plugins/datatables',file:'dataTables.keyTable.min.js')}"></script>
	<script	src="${resource(dir:'assets/plugins/datatables',file:'dataTables.responsive.min.js')}"></script>
	<script	src="${resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.js')}"></script>
	<script	src="${resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js')}"></script>
	<script	src="${resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js')}"></script>
	<script	src="${resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.js')}"></script>
	<script src="${resource(dir:'assets/pages',file:'datatables.init.js')}"></script>

	<!--Form Validation-->
	<script	src="${resource(dir:'assets/plugins/bootstrapvalidator/dist/js',file:'bootstrapValidator.js')}"></script>
	<script	src="${resource(dir:'assets/plugins/jquery-validation/dist',file:'jquery.validate.min.js')}"></script>
	<!--start Code of Multi select -->
	<script type="text/javascript" src="${resource(dir:'assets/plugins/bootstrap-tagsinput/dist',file:'bootstrap-tagsinput.min.js')}"></script>
	<script	src="${resource(dir:'assets/plugins/switchery/dist',file:'switchery.min.js')}"></script>
	<script type="text/javascript" src="${resource(dir:'assets/plugins/multiselect/js',file:'jquery.multi-select.js')}"></script>
	<script type="text/javascript" src="${resource(dir:'assets/plugins/jquery-quicksearch',file:'jquery.quicksearch.js')}"></script>
	<script type="text/javascript" src="${resource(dir:'assets/plugins/select2',file:'select2.min.js')}"></script>
	<script type="text/javascript" src="${resource(dir:'assets/plugins/bootstrap-select/dist/js',file:'bootstrap-select.min.js')}"></script>
	<script type="text/javascript" src="${resource(dir:'assets/plugins/bootstrap-filestyle/src',file:'bootstrap-filestyle.min.js')}"></script>
	<script type="text/javascript" src="${resource(dir:'assets/plugins/bootstrap-maxlength',file:'bootstrap-maxlength.min.js')}"></script>
	<script	src="${resource(dir:'machingUserAssets',file:'jcarousel.responsive.js')}"></script>
	<script	src="${resource(dir:'machingUserAssets',file:'jquery.jcarousel.min.js')}"></script>
	<script type="text/javascript">
            jQuery(document).ready(function() {
                //advance multiselect start
                $('#my_multi_select3').multiSelect({
                    selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
                    selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
                    afterInit: function (ms) {
                        var that = this,
                            $selectableSearch = that.$selectableUl.prev(),
                            $selectionSearch = that.$selectionUl.prev(),
                            selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                            selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                        that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                            .on('keydown', function (e) {
                                if (e.which === 40) {
                                    that.$selectableUl.focus();
                                    return false;
                                }
                            });

                        that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                            .on('keydown', function (e) {
                                if (e.which == 40) {
                                    that.$selectionUl.focus();
                                    return false;
                                }
                            });
                    },
                    afterSelect: function () {
                        this.qs1.cache();
                        this.qs2.cache();
                    },
                    afterDeselect: function () {
                        this.qs1.cache();
                        this.qs2.cache();
                    }
                });

                // Select2
                $(".select2").select2();
                
                $(".select2-limiting").select2({
				  maximumSelectionLength: 2
				});
				
			   $('.selectpicker').selectpicker();
	            $(":file").filestyle({input: false});
	            });	          
	            
	            //Bootstrap-TouchSpin
	            $(".vertical-spin").TouchSpin({
		            verticalbuttons: true,
		            verticalupclass: 'ion-plus-round',
		            verticaldownclass: 'ion-minus-round'
		        });
		        var vspinTrue = $(".vertical-spin").TouchSpin({
		            verticalbuttons: true
		        });
		        if (vspinTrue) {
		            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
		        }
		
		        $("input[name='demo1']").TouchSpin({
		            min: 0,
		            max: 100,
		            step: 0.1,
		            decimals: 2,
		            boostat: 5,
		            maxboostedstep: 10,
		            postfix: '%'
		        });
		        $("input[name='demo2']").TouchSpin({
		            min: -1000000000,
		            max: 1000000000,
		            stepinterval: 50,
		            maxboostedstep: 10000000,
		            prefix: '$'
		        });
		        $("input[name='demo3']").TouchSpin();
		        $("input[name='demo3_21']").TouchSpin({
		            initval: 40
		        });
		        $("input[name='demo3_22']").TouchSpin({
		            initval: 40
		        });
		
		        $("input[name='demo5']").TouchSpin({
		            prefix: "pre",
		            postfix: "post"
		        });
		        $("input[name='demo0']").TouchSpin({});
		        
		        
		        //Bootstrap-MaxLength
		        $('input#defaultconfig').maxlength()
		        
		        $('input#thresholdconfig').maxlength({
                threshold: 20
            });

            $('input#moreoptions').maxlength({
                alwaysShow: true,
                warningClass: "label label-success",
                limitReachedClass: "label label-danger"
            });

            $('input#alloptions').maxlength({
                alwaysShow: true,
                warningClass: "label label-success",
                limitReachedClass: "label label-danger",
                separator: ' out of ',
                preText: 'You typed ',
                postText: ' chars available.',
                validate: true
            });

            $('textarea#textarea').maxlength({
                alwaysShow: true
            });

            $('input#placement') .maxlength({
                    alwaysShow: true,
                    placement: 'top-left'
                });
        </script>
        
         <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').dataTable();
                $('#datatable-keytable').DataTable( { keys: true } );
                $('#datatable-responsive').DataTable();
                $('#datatable-colvid').DataTable({
                    "dom": 'C<"clear">lfrtip',
                    "colVis": {
                        "buttonText": "Change columns"
                    }
                });
                $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
                var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
            } );
            TableManageButtons.init();

        </script>
                
	<script type="text/javascript">
			jQuery(document).ready(function() {

				// Time Picker
				jQuery('#timepicker').timepicker({
					defaultTIme : false,
					minuteStep : 01
				});
				jQuery('#timepicker2').timepicker({
					showMeridian : false
				});
				jQuery('#timepicker3').timepicker({
					minuteStep : 15
				});
				
				//colorpicker start

                $('.colorpicker-default').colorpicker({
                    format: 'hex'
                });
                $('.colorpicker-rgba').colorpicker();
                
                // Date Picker
               
                jQuery('#datepicker').datepicker();
                 var nowDate = new Date();
    			var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
                jQuery('#datepicker-autoclose').datepicker({
                 startDate: today,
                	autoclose: true,
                	todayHighlight: true
                });
                $("#datepicker-autoclose").datepicker().datepicker("setDate", new Date())
                jQuery('#datepicker-inline').datepicker();
                jQuery('#datepicker-multiple-date').datepicker({
                    format: "mm/dd/yyyy",
					clearBtn: true,
					multidate: true,
					multidateSeparator: ","
                });
                jQuery('#date-range').datepicker({
                    toggleActive: true
                });
                
                //Clock Picker
                $('.clockpicker').clockpicker({
                	donetext: 'Done'
                });
                
                $('#single-input').clockpicker({
				    placement: 'bottom',
				    align: 'left',
				    autoclose: true,
				    'default': 'now'
				});
				$('#check-minutes').click(function(e){
				    // Have to stop propagation here
				    e.stopPropagation();
				    $("#single-input").clockpicker('show')
				            .clockpicker('toggleView', 'minutes');
				});
				
				
				//Date range picker
				$('.input-daterange-datepicker').daterangepicker({
					buttonClasses: ['btn', 'btn-sm'],
		            applyClass: 'btn-default',
		            cancelClass: 'btn-white'
				});
		        $('.input-daterange-timepicker').daterangepicker({
		            timePicker: true,
		            format: 'MM/DD/YYYY h:mm A',
		            timePickerIncrement: 30,
		            timePicker12Hour: true,
		            timePickerSeconds: false,
		            buttonClasses: ['btn', 'btn-sm'],
		            applyClass: 'btn-default',
		            cancelClass: 'btn-white'
		        });
		        $('.input-limit-datepicker').daterangepicker({
		            format: 'MM/DD/YYYY',
		            minDate: '06/01/2015',
		            maxDate: '06/30/2015',
		            buttonClasses: ['btn', 'btn-sm'],
		            applyClass: 'btn-default',
		            cancelClass: 'btn-white',
		            dateLimit: {
		                days: 6
		            }
		        });
		
		        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
		
		        $('#reportrange').daterangepicker({
		            format: 'MM/DD/YYYY',
		            startDate: moment().subtract(29, 'days'),
		            endDate: moment(),
		            minDate: '01/01/2012',
		            maxDate: '12/31/2015',
		            dateLimit: {
		                days: 60
		            },
		            showDropdowns: true,
		            showWeekNumbers: true,
		            timePicker: false,
		            timePickerIncrement: 1,
		            timePicker12Hour: true,
		            ranges: {
		                'Today': [moment(), moment()],
		                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		                'This Month': [moment().startOf('month'), moment().endOf('month')],
		                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		            },
		            opens: 'left',
		            drops: 'down',
		            buttonClasses: ['btn', 'btn-sm'],
		            applyClass: 'btn-default',
		            cancelClass: 'btn-white',
		            separator: ' to ',
		            locale: {
		                applyLabel: 'Submit',
		                cancelLabel: 'Cancel',
		                fromLabel: 'From',
		                toLabel: 'To',
		                customRangeLabel: 'Custom',
		                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
		                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		                firstDay: 1
		            }
		        }, function (start, end, label) {
		            console.log(start.toISOString(), end.toISOString(), label);
		            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		        });
				
			});
		</script>
</body>
</html>