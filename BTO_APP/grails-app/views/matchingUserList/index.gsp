<%@page import="com.bto.customer.MatchingUserSkipReason"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="layout" content="main" />
<title>Matching Users | BTO</title>
<!--Footable-->
<link rel="stylesheet" href="${resource(dir:'assets/plugins/footable/css',file:'footable.core.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/bootstrap-select/dist/css',file:'bootstrap-select.min.css') }">
<!-- DataTables -->
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/datatables',file:'buttons.bootstrap.min.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css') }">

<%--Start Slider assets--%>
<link rel="stylesheet"	href="${resource(dir:'machingUserAssets',file:'jcarousel.responsive.css') }">
<link rel="stylesheet"	href="${resource(dir:'machingUserAssets',file:'sliderStyle.css') }">
<%--end Slider assets--%>

<style type="text/css">
.profile-widget .bg-profile {
    height: 65px;
}
</style>

<style type="text/css">
html, body {
	//background: #FFFFFF;
	//background: #f2f3f4;
}

table * {
	border: none !important;
}

.table-bordered {
	border: medium none;
}

table.dataTable thead .sorting::after {
	content: "";
	opacity: 0.2;
}

table.dataTable thead .sorting_asc::after {
	content: "";
}

table.dataTable thead .sorting_desc::after {
	content: "";
}
</style>
<script type="text/javascript">
function changeBackground(color) {
	   document.body.style.background = color;
	}
</script>
</head>
<body>
	<div class="body" style="margin-top: 2%;">
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:if test="${flash.message1}">
			<div class="message1" role="status">
				${flash.message1}
			</div>
		</g:if>
		<g:form method="post" action="findMatchingUser" controller="BookingConsole" role="form">
				<input type="hidden" name="tripID" value="${tripId }"/>
				<input type="hidden" name="alertId" value=""/>
				<button class="" type="submit" style="border:none; float: right; background-color: transparent;"><img src="${resource(dir:'images',file:'refresh.png')}" class="img-responsive" alt="refresh"/></button>
			</g:form>
		<g:if test="${(jsonObject)!=null && jsonObject.status!=false}">
			<script type="text/javascript">
				changeBackground('#FFFFFF');
			</script>		
			
			 <div class="wrapper" style="margin-left: 5%; margin-top: -3%; padding: 0 20px 27px;">
	           <div class="jcarousel-wrapper">
	                <div class="jcarousel row">
	                    <ul>
		                    <g:each var="user" in="${jsonObject.users}" status="i">
			                    <li>
			                        <div class="col-lg-12">
		                        		<div class="card-box p-0" style="background-color: #f2f2f2;">
		                        		<%
							 				String[] color = ["#003366", "#330033", "#ff9900","#003366"] 
							 			%>
		                        		
		                        			<div class="profile-widget ">
					                            <div class="bg-custom bg-profile" style="background-color: ${color[i]} !important;"></div>
					                            <img src="${resource(dir:'assets/images/users',file:'avatar-1.jpg')}" class="thumb-lg img-circle img-thumbnail" alt="profile-image" style="margin-left: 37%;"/>
					                            <h4 class="text-center">${user.firstName} ${user.lastName}</h4>
					                            <%												
													if(i==3){
														i=0;
													} 
												%>
					                           <p style="float: left;">
										<img class="img-responsive" alt="" src="${resource(dir:'images',file:'from_to_icon.png')}"	style="margin: 0 auto; ">
									</p>
									<p style="">
										${user.startLocation}
									</p>
									<p style="">
										${user.endLocation}
									</p>
									 <div style="margin: 5%;"></div>
					                            <div class="col-md-12">
					                            	<div class="col-md-5">TIME</div>
					                            	<div class="col-md-2"></div>
					                            	<div class="col-md-5">CAR TYPE</div>
					                            </div>
					                            <div style="margin: 10%;"></div>
					                            <div class="col-md-12">
					                            	<div class="col-md-5"><b>${user.tripTime}</b></div>
					                            	<div class="col-md-2"></div>
					                            	<div class="col-md-5"><b>${user.vendorCarType}</b></div>
					                            </div>
					                             <div style="margin: 25%;"></div>
					                            <div class="col-md-12">
					                            	<div class="col-md-5">ETA</div>
					                            	<div class="col-md-2"></div>
					                            	<div class="col-md-5">VENDOR</div>
					                            </div>
					                            <div style="margin: 10%;"></div>
					                            <div class="col-md-12">
					                            	<div class="col-md-5"><b>${user.eta} Min</b></div>
					                            	<div class="col-md-2"></div>
					                            	<div class="col-md-5"><b>${user.vendorName}</b></div>
					                            </div>
					                            <div style="margin: 45%;"></div>
					                            <div class="col-md-12">
						                            <div class="col-md-5">DEPARTMENT</div>
						                            <div class="col-md-2"></div>
						                            <div class="col-md-5">DESIGNATION</div>
					                            </div>
					                            <div style="margin: 10%;"></div>
					                            <div class="col-md-12">
					                            <div class="col-md-5"><b>${user.department}</b></div>
					                            <div class="col-md-2"></div>
					                            <div class="col-md-5"><b>${user.designation}</b></div>
					                            </div>
					                            <div style="margin: 65%;"></div>
					                           <g:form controller="InviteUser" action="inviteUser" method="post">
													<input type="hidden" name="invitedByUserID" value="${session['userID']}" />
													<input type="hidden" name="invitedToUserID" value="${user.userID}" />
													<input type="hidden" name="invitedByTripID" value="${tripId}" />
													<input type="hidden" name="invitedToTripID" value="${user.tripID}" />
													 <a href="tel:${user.phone}" style="margin-left: 30%;">
															${user.phone}
													</a>
													<g:if test="${user.isInvited==false }">
														<button type="submit" class="btn btn-success waves-effect waves-light" style="margin-left: 9%;">Join trip</button>
													</g:if>
													<g:else>
														<button type="button" class="btn btn-success waves-effect waves-light" style="margin-left: 9%;">Requested</button>
													</g:else>
												</g:form>
					                            <p class="m-t-20">
					                        </div>
		                        		</div>
		                        	</div> <!-- end col -->
		                        </li> 
	                        </g:each>                      
	                    </ul>
	                </div>
	                <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
	                <a href="#" class="jcarousel-control-next">&rsaquo;</a>
	                <p class="jcarousel-pagination"></p>
	            </div>
	        </div>
       
       	<g:if test="${jsonObject.invitedUserCount>0}">
       		<g:form method="post" action="bookTripWithSkipReason" controller="MatchingUserList" role="form">
       			<input type="hidden" name="userID" value="${session['userID']}" />
				<input type="hidden" name="tripId" value="${tripId}" />
				<input type="hidden" name="reason" value="WithMatchingUser" />       			
       				<button type="submit" class="btn btn-inverse waves-effect waves-light" style="margin-left:43%; width: 20%;">Done</button>
       		</g:form>
       	</g:if>
       	<g:else>
       		<a href="#" class="btn btn-inverse waves-effect waves-light" data-toggle="modal" data-target="#myModalNorm"
					style="margin-left:43%; width: 20%;">Skip and continue</a>
       	</g:else>
				<!-- Modal -->
				<div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content" style="width: 50%; text-align: center;">
							<g:form method="post" action="bookTripWithSkipReason" controller="MatchingUserList" role="form">
								<input type="hidden" name="userID" value="${session['userID']}" />
								<input type="hidden" name="tripId" value="${tripId}" />
								<!-- Modal Header -->
								<div class="">
									<h4 class="modal-title" id="myModalLabel">Select skip reason</h4>
								</div>
								<!-- Modal Body -->
								<div class="modal-body">
									<div class="row">
										<select class=" form-control" name="reason" oninvalid="this.setCustomValidity('Select skip reason')"
												onchange="this.setCustomValidity('');" data-style="btn-white" required="required">
											<option class="text-muted" value="" disabled selected>Select</option>
											<g:each var="reason" in="${MatchingUserSkipReason.list()}">
												<option value="${reason.reasonDetails}">
													${reason.reasonDetails}
												</option>
											</g:each>
										</select>
									</div>
								</div>
								<!-- Modal Footer -->
								<div class="" style="text-align: center;">
									<button class="btn btn-success waves-effect waves-light" type="submit" style="float: right;" onclick="return confirm(' Do you want to proceed?');">Proceed</button>
									<button class="btn btn-inverse waves-effect waves-light" data-dismiss="modal" type="reset">Cancel</button>
								</div>
							</g:form>
						</div>
					</div>
				</div>
		</g:if>
		<g:else>
			<script type="text/javascript">
				changeBackground('#f2f3f4');
			</script>
				<div class="row" style="margin-left: 10%;">
					<div class="col-md-12" style="width:80%; text-align: center; ">
						<img class="img-responsive" alt="" src="${resource(dir:'images',file:'no_matching_user.png')}" style="margin: 0 auto; height: 320px;">
						<div style="margin: 4%;"></div>
						<h3 class="text-muted">No matching users</h3>
						<h4 class="text-muted">Submit booking request</h4>
						<div style="margin: 2%;"></div>
						<g:form controller="MatchingUserList" action="bookTripWithoutMachingUser" method="post">
							<input type="hidden" name="userID" value="${session['userID']}" />
							<input type="hidden" name="tripId" value="${tripId}" />
							<input type="hidden" name="reason" value="NONE" />
							<button class="btn btn-success waves-effect waves-light" type="submit" style="width: 30%;" onclick="return confirm(' Do you want to proceed?');">Proceed</button>
						</g:form>
						<div style="margin: 6%;"></div>
					</div>
				</div>
			</g:else>	
		</div>		
</body>
</html>