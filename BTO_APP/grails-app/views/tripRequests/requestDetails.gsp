<meta name="layout" content="vendorMaster" />
<link href="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css')}" rel="stylesheet" type="text/css" />
<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>
<style>
	table, th, td {
	    background-color: #ffffff;
	    border: none;
	}
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td, .table > thead > tr > th, .table-bordered {
	    border-top: medium none;
	}
	.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
	    border-top: medium none;
	    line-height: 1.42857;
	    padding: 0;
	    vertical-align: top;
	}
</style>
<script type="text/javascript">
$(document).ready(function() {
    $('table.display').DataTable();
} );
</script>
<script type="text/javascript">
	function changeBackground(color) {
	   document.body.style.background = color;
	}
</script>

<div class="row" style="">
	<g:if test="${flash.message}">
		<div class="message" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.message1}">
		<div class="message1" role="status">
			${flash.message1}
		</div>
	</g:if>
	<div class="col-lg-12">
		<ul class="nav nav-tabs tabs">
			<li class="active tab"><a href="#accepted" data-toggle="tab" aria-expanded="false"> 
				<span class="visible-xs">
					<i class="fa fa-home"></i></span> <span class="hidden-xs">Accepted</span>
			</a></li>
			<li class="tab"><a href="#rejected" data-toggle="tab" aria-expanded="false"> <span class="visible-xs">
				<i class="fa fa-user"></i></span> <span class="hidden-xs">Rejected</span>
			</a></li>
			<li class="tab"><a href="#cancelled" data-toggle="tab" aria-expanded="false"> <span class="visible-xs">
				<i class="fa fa-user"></i></span> <span class="hidden-xs">Cancelled</span>
			</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="accepted">
				<g:if test="${acceptedTrips.acceptedTrips!=null}">
					<script type="text/javascript">
						changeBackground('#FFFFFF');
					</script>
					<table id="" class="table display">
					<thead>
					<tr>				
						<th style="display: none;"></th>				
					</tr>
					</thead>
					<tbody>		
					<g:each var="trips" in="${acceptedTrips.acceptedTrips}" status="i">
						<tr>				
					  		 <td>
						<div class="col-md-12 shadow" style="background-color: #f2f3f4;">
							<div class="col-md-1" style="text-align: center;">
								<p>
									<img style="height: 70px; width: 65px; margin-top: 10%;" src="${resource(dir:'assets/images/users',file:'avatar-1.jpg')}"
										alt="user-img" class="thumb-lg img-circle img-thumbnail">
								</p>
								<p>
									${trips.firstName} ${trips.lastName}
								</p>
							</div>
							<div class="col-md-5">
								<p>LOCATION</p>
								<p style="float: left;">
									<img class="img-responsive" alt="" src="${resource(dir:'images',file:'from_to_icon.png')}"
										style="margin: 0 auto;">
								</p>
								<p>
									&nbsp;&nbsp;${trips.startLocation}
								</p>
								<p>
									&nbsp;&nbsp;${trips.endLocation}
								</p>
							</div>
							<div class="col-md-2">
								<p>DATE</p>
								<p>
									${trips.tripDate}
								</p>
							</div>
							<div class="col-md-1">
								<p>TIME</p>
								<p>
									${trips.tripTime}
								</p>
							</div>
							<div class="col-md-2">
								<p>CAR TYPE</p>
								<p>
									${trips.vendorCarType}
								</p>
							</div>
							<div class="col-md-1">
								<p></p><br><br><br>
								<p>
								<button class="btn btn-inverse waves-effect waves-light"
									type="submit" data-toggle="modal"
									data-target="#myModalNorm${i}" style="">Reject</button>
							</p>
							<!-- Modal -->
							<div class="modal fade" id="myModalNorm${i}" tabindex="-1"
								role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">

									<div class="modal-content"
										style="width: 50%; text-align: center;">
										<g:form method="post" action="cancelAcceptedTrip"
											controller="TripRequests" role="form">
											<input type="hidden" name="tripID" value="${trips.tripID }">
											<input type="hidden" name=firstName	value="${trips.firstName}">
											<input type="hidden" name="phone" value="${trips.phone }">
											<input type="hidden" name="startLocation" value="${trips.startLocation }">
											<input type="hidden" name="endLocation" value="${trips.endLocation }">
											<!-- Modal Header -->
											<div class="">
												<h4 class="modal-title" id="myModalLabel">Enter reject reason</h4>
												<input type="text" name="reason" required="required">
											</div>

											<!-- Modal Body -->
											<div class="modal-body">
												<div class="row">
													
												</div>
											</div>

											<!-- Modal Footer -->
											<div class="" style="text-align: center;">
												<button class="btn btn-success waves-effect waves-light"
													type="submit" style="float: right;">Reject</button>
												<button class="btn btn-inverse waves-effect waves-light"
													data-dismiss="modal" type="reset">Cancel</button>
											</div>
										</g:form>
									</div>
								</div>
							</div>
							</div>
							<g:if test="${trips.CoPasscount }">
							<div class="col-md-12">
								<p><b>CO-TRAVELLERS</b></p>	
									<div class="col-md-12 innerShadow">
										<div class="col-md-12">
											<div class="col-md-3" style="text-align:center"><b>Name</b></div>
											<div class="col-md-6" style="text-align:center"><b>Pickup Location</b></div>
											<div class="col-md-3" style="text-align:center"><b>Mobile Number</b></div>
										</div>
										<g:each var="coTravellers" in="${trips.coTravellers}" status="j">
											<div class="col-md-3" style="text-align: center;">
												<p>
													${coTravellers.name}
												</p>
											</div>
											<div class="col-md-6" style="text-align:center">
												<p>
													&nbsp;&nbsp;${coTravellers.startLocation}
												</p>
											</div>
											<div class="col-md-3" style="text-align:center">
												<p>
													&nbsp;&nbsp;${coTravellers.phone}
												</p>
											</div>
										</g:each>
									</div>
							</div>
							</g:if>
						</div></td></tr>
					</g:each>
					</tbody></table>
				</g:if>
				<g:else>
					<script type="text/javascript">
						changeBackground('#f2f3f4');
					</script>
					<div class="row" style="margin-left: 10%;">
						<div class="col-md-12" style="width: 80%; text-align: center;">
							<img class="img-responsive" alt="" src="${resource(dir:'images',file:'no_trips.png')}"	style="margin: 0 auto; height: 320px;">
							<div style="margin: 4%;"></div>
							<h3 class="text-muted">No trips accepted</h3>
							<h4 class="text-muted">You have not accepted any trips</h4>							
						</div>
					</div>
				</g:else>
			</div>
			<div class="tab-pane" id="rejected">
				<g:if test="${rejectedTrips.rejectedTrips!=null}">
					<script type="text/javascript">
						changeBackground('#FFFFFF');
					</script>
					<table id="" class="table display">
					<thead>
					<tr>				
						<th style="display: none;"></th>				
					</tr>
					</thead>
					<tbody>		
					<g:each var="trips" in="${rejectedTrips.rejectedTrips}" status="i">
						<tr><td>
						<div class="col-md-12 shadow" style="background-color: #f2f3f4;">
							<div class="col-md-1" style="text-align: center;">
								<p>
									<img style="height: 70px; width: 65px; margin-top: 10%;"
										src="${resource(dir:'assets/images/users',file:'avatar-1.jpg')}"
										alt="user-img" class="thumb-lg img-circle img-thumbnail">
								</p>
								<p>
									${trips.firstName}
								</p>
							</div>
							<div class="col-md-6">
								<p>LOCATION</p>
								<p style="float: left;">
									<img class="img-responsive" alt=""
										src="${resource(dir:'images',file:'from_to_icon.png')}"
										style="margin: 0 auto;">
								</p>
								<p>
									&nbsp;&nbsp;${trips.startLocation}
								</p>
								<p>
									&nbsp;&nbsp;${trips.endLocation}
								</p>
							</div>
							<div class="col-md-2">
								<p>DATE</p>
								<p>
									${trips.tripDate}
								</p>
							</div>
							<div class="col-md-1">
								<p>TIME</p>
								<p>
									${trips.tripTime}
								</p>
							</div>
							<div class="col-md-2">
								<p>CAR TYPE</p>
								<p>
									${trips.vendorCarType}
								</p>
							</div>
							
						</div></td></tr>
					</g:each></tbody></table>
				</g:if>
				<g:else>
					<script type="text/javascript">
						changeBackground('#f2f3f4');
					</script>
					<div class="row" style="margin-left: 10%;">
						<div class="col-md-12" style="width: 80%; text-align: center;">
							<img class="img-responsive" alt="" src="${resource(dir:'images',file:'no_trips.png')}"
								style="margin: 0 auto; height: 320px;">
							<div style="margin: 4%;"></div>
							<h3 class="text-muted">No trips rejected</h3>
							<h4 class="text-muted">You have not rejected any trips</h4>							
						</div>
					</div>
				</g:else>
			</div>
			<div class="tab-pane" id="cancelled">
			<g:if test="${tripDetailsInstanceList!=null}">
					<script type="text/javascript">
						changeBackground('#FFFFFF');
					</script>
					<table id="" class="table display">
					<thead>
					<tr>				
						<th style="display: none;"></th>				
					</tr>
					</thead>
					<tbody>		
					<g:each var="trips" in="${tripDetailsInstanceList}" status="i">
						<tr>				
					  		 <td>
						<div class="col-md-12 shadow" style="background-color: #f2f3f4;">
							<div class="col-md-1" style="text-align: center;">
								<p>
									<img style="height: 70px; width: 65px; margin-top: 10%;" src="${resource(dir:'assets/images/users',file:'avatar-1.jpg')}"
										alt="user-img" class="thumb-lg img-circle img-thumbnail">
								</p>
								<p>
									${trips.user.firstName} ${trips.user.lastName}
								</p>
							</div>
							<div class="col-md-5">
								<p>LOCATION</p>
								<p style="float: left;">
									<img class="img-responsive" alt="" src="${resource(dir:'images',file:'from_to_icon.png')}"
										style="margin: 0 auto;">
								</p>
								<p>
									&nbsp;&nbsp;${trips?.startLocation}
								</p>
								<p>
									&nbsp;&nbsp;${trips?.endLocation}
								</p>
							</div>
							<div class="col-md-2">
								<p>DATE</p>
								<p>
									${trips?.tripDate}
								</p>
							</div>
							<div class="col-md-1">
								<p>TIME</p>
								<p>
									${trips?.tripTime}
								</p>
							</div>
							<div class="col-md-2">
								<p>CAR TYPE</p>
								<p>
									${trips?.carType}
								</p>
							</div>												
						</div></td></tr>
					</g:each>
					</tbody></table>
				</g:if>
				<g:else>
					<script type="text/javascript">
						changeBackground('#f2f3f4');
					</script>
					<div class="row" style="margin-left: 10%;">
						<div class="col-md-12" style="width: 80%; text-align: center;">
							<img class="img-responsive" alt="" src="${resource(dir:'images',file:'no_trips.png')}"	style="margin: 0 auto; height: 320px;">
							<div style="margin: 4%;"></div>
							<h3 class="text-muted">No trips canceled</h3>
							<h4 class="text-muted">You have not canceled any trips</h4>							
						</div>
					</div>
				</g:else>
			</div>
		</div>
	</div>
	<div class="col-md-0"></div>
</div>
<script src="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js')}"></script>
<script type="text/javascript">
       $(document).ready(function() {
           $('#datatable').dataTable();
       });
</script>
<script type="text/javascript">
$(document).ready(function(){
<%
session['sidebarActive']="";
session['sidebarActive']="tripRequests/requestDetails";
%>
});
</script>