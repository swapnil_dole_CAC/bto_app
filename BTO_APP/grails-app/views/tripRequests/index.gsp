<%@page import="com.bto.vendor.Driver"%>
<%@page import="com.bto.vendor.RejectionReason"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		<meta http-equiv="refresh" content="60" >
		<meta name="layout" content="vendorMaster" />
		<title>Trip requests</title>
		<link href="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css')}" rel="stylesheet" type="text/css" />
		<link href="${resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
		<link href="${resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
		<link href="${resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
		<link href="${resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css')}" rel="stylesheet" type="text/css" />
		<title>Trip requests</title>
		<style>
		table, th, td {
		    background-color: #ffffff;
		    border: none;
		}
		.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td, .table > thead > tr > th, .table-bordered {
		    border-top: medium none;
		}
		.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
		    border-top: medium none;
		    line-height: 1.42857;
		    padding: 0;
		    vertical-align: top;
		}
		.form-inline .form-control {
   		 vertical-align: middle;
   		 width: 100%;
		}
		</style>
		<style type="text/css">
		.profile-widget .bg-profile {
			height: 80px;
		}
		</style>
		
		<script type="text/javascript">
		function changeBackground(color) {
			   document.body.style.background = color;
			}
		</script>
		<%--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyAl-G44Mw9pJUOwTHmKluyzF7n_hob0D_A"></script>
		--%>
<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>

<script type="text/javascript">
function setDrivername(data){
	//document.getElementById ("driverPhone").value = data.value;
}
</script>
<script>
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Number"+charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
		document.getElementById("mob").innerHTML = "<span style='color:red;'>Only 0-9 digits allowed...!</span>"; 
	 return false;
	}
       
    else{
    document.getElementById("mob").innerHTML = "<span></span>";
	return true;
	}
}
function isNumberKey1(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Number"+charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
		document.getElementById("mob1").innerHTML = "<span style='color:red;'>Only 0-9 digits allowed...!</span>"; 
	 return false;
	}
       
    else{
    document.getElementById("mob1").innerHTML = "<span></span>";
	return true;
	}
}
function isCharacterKeyDot(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Char"+charCode);
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 46 || charCode == 40 || charCode == 41 || charCode == 32 || charCode == 47 || charCode == 45 || charCode == 8 || charCode == 13){
    	document.getElementById("fname").innerHTML = "<span></span>";    	
        return true;
    }else{
    	document.getElementById("fname").innerHTML = "<span style='color:red;'>Only a-z A-Z digits allowed...!</span>";
        return false;
    }
}
function isCharacterKeyDot1(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Char"+charCode);
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 46 || charCode == 40 || charCode == 41 || charCode == 32 || charCode == 47 || charCode == 45 || charCode == 8 || charCode == 13){
    	document.getElementById("lname").innerHTML = "<span></span>";    	
        return true;
    }else{
    	document.getElementById("lname").innerHTML = "<span style='color:red;'>Only a-z A-Z digits allowed...!</span>";
        return false;
    }
}
</script>

	</head>
<body>
	<div class="body">
		
		<div class="row">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:if test="${flash.message1}">
				<div class="message1" role="status">
					${flash.message1}
				</div>
			</g:if>
			<g:if test="${requestedTrips}">
				<script type="text/javascript">
					changeBackground('#FFFFFF');
				</script>
			<table id="datatable" class="table">
			<thead>
			<tr>				
				<th style="display: none;"></th>				
			</tr>
			</thead>
			<tbody>	
				<g:each var="trips" in="${requestedTrips.requestedTrips}" status="i">
				<tr>				
					<td>
					<div class="col-md-12" style="background-color: #f2f3f4;margin-bottom:2%;box-shadow:0 0 10px rgba(0, 0, 0, 0.2);">
						<div class="col-md-1" style="text-align: center;">
							<p>
								<img style="height: 70px; width: 65px; margin-top: 10%;" src="${resource(dir:'assets/images/users',file:'avatar-1.jpg')}" alt="user-img" class="thumb-lg img-circle img-thumbnail">
							</p>
							<p>
								${trips.name}
							</p>
						</div>
						<div class="col-md-2">
							<p>DESIGNATION</p>
							<p>
								${trips.designation}
							</p>							
						</div>
						<div class="col-md-1" style="word-break:break-all">
							<p>CAR TYPE</p>
							<p>
								${trips.vendorCarType}
							</p>
						</div>
						<div class="col-md-1">
							<p>DATE</p>
							<p>
								${trips.tripDate}
							</p>
						</div>
						<div class="col-md-1">
							<p>TIME</p>
							<p>
								${trips.tripTime}
							</p>
						</div>
						<div class="col-md-4">
							<p>LOCATION</p>
							<p style="float: left;">
								<img class="img-responsive" alt="" src="${resource(dir:'images',file:'from_to_icon.png')}" style="margin: 0 auto;">
							</p>
							<p>
								&nbsp;&nbsp;${trips.startLocation}
							</p>
							<p>
								&nbsp;&nbsp;${trips.endLocation}
							</p>
						</div>
						<div class="col-md-2" style="text-align: right;">
						<div style="margin-top: 29%;"></div>
							

							<p>
								<button class="btn btn-success waves-effect waves-light" type="submit" data-toggle="modal"	data-target="#myModalNorm1${i}" style="width: 50%;">Accept</button>
							</p>
								<!-- Modal -->
							<div class="modal fade" id="myModalNorm1${i}" tabindex="-1"	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content"
										style="width: 50%; text-align: center;">
										<div class="">
												<h4 class="modal-title" id="myModalLabel">Driver Info</h4>
										</div>
										<ul class="nav nav-tabs tabs">
								             <li class="active tab">
								                 <a href="#auto${i}" data-toggle="tab" aria-expanded="false"> 
								                     <span class="visible-xs"><i class="fa fa-home"></i></span> 
								                     <span class="hidden-xs">Auto</span> 
								                 </a> 
								             </li> 
								             <li class="tab"> 
								                 <a href="#manual${i}" data-toggle="tab" aria-expanded="false"> 
								                     <span class="visible-xs"><i class="fa fa-user"></i></span> 
								                     <span class="hidden-xs">Manual</span> 
								                 </a> 
								             </li> 
								            
								         </ul> 
								         <div class=""> 
								        <div class="tab-pane active" id="auto${i}"> 
										<g:form method="post" action="acceptRequest" controller="TripRequests" role="form" >
											<input type="hidden" name="tripID" value="${trips.tripID }"/>
											<input type="hidden" name=firstName	value="${trips.name}"/>
											<input type="hidden" name=userPhone value="${trips.phone}"/>
											<input type="hidden" name="startLocation" value="${trips.startLocation }"/>
											<input type="hidden" name="endLocation" value="${trips.endLocation }"/>
											<!-- Modal Header -->
											

											<!-- Modal Body -->
											<div class="modal-body">
												<div class="row">
													<div class="">
													
													<select name="driverName" class="form-control" onchange="" required="required" oninvalid="this.setCustomValidity('Select driver')"
												onchange="this.setCustomValidity('');">
													<option value="" selected="selected" disabled="disabled">Select</option>
													<g:each var="driver" in="${Driver.list() }">
													<option value="${driver.firstName} ${driver.lastName}">${driver.firstName} ${driver.lastName}</option>
													</g:each>
													</select>
													<br><br>
														<input type="text" name="carNo" value="" class="form-control" placeholder="Car Number" required="required" oninvalid="this.setCustomValidity('Enter car number')" onchange="this.setCustomValidity('');"/> <br><br>
														<input type="text" name="phone" maxlength="10" class="form-control" id="driverPhone" value="" placeholder="Driver Mobile" required="required" oninvalid="this.setCustomValidity('Enter mobile number')" onchange="this.setCustomValidity('');" onkeypress="return isNumberKey(event);"><p id="mob" style="color:red;"></p>
													</div>
												</div>
											</div>

											<!-- Modal Footer -->
											<div class="" style="text-align: center;">
												<button class="btn btn-success waves-effect waves-light"
													type="submit">Accept</button>
												<button class="btn btn-inverse waves-effect waves-light"
													data-dismiss="modal" type="reset">Cancel</button>
											</div>
										</g:form></div>
										<div class="tab-pane" id="manual${i}">
										<g:form method="post" action="acceptRequest" controller="TripRequests" role="form">
											<input type="hidden" name="tripID" value="${trips.tripID }"/>
											<input type="hidden" name=firstName	value="${trips.name}"/>
											<input type="hidden" name=userPhone value="${trips.phone}"/>
											<input type="hidden" name="startLocation" value="${trips.startLocation }"/>
											<input type="hidden" name="endLocation" value="${trips.endLocation }"/>
											<!-- Modal Header -->
											

											<!-- Modal Body -->
											<div class="modal-body">
												<div class="row">
													<div class="">
														<input type="text" name="driverName" value="" class="form-control" placeholder="Driver Name" required="required" onkeypress="return isCharacterKeyDot(event);" oninvalid="this.setCustomValidity('Enter driver name')" onchange="this.setCustomValidity('');"/> <p id="fname" style="color:red;"></p><br> 
														<input type="text" name="carNo" value="" class="form-control" placeholder="Car Number" required="required" oninvalid="this.setCustomValidity('Enter car number')" onchange="this.setCustomValidity('');"/> <br><br>
														<input type="text" name="phone" class="form-control" value="" placeholder="Driver Mobile" required="required" onkeypress="return isNumberKey1(event);" oninvalid="this.setCustomValidity('Enter mobile number')" onchange="this.setCustomValidity('');"><p id="mob1" style="color:red;"></p>
													</div>
												</div>
											</div>

											<!-- Modal Footer -->
											<div class="" style="text-align: center;">
												<button class="btn btn-success waves-effect waves-light"
													type="submit">Accept</button>
												<button class="btn btn-inverse waves-effect waves-light"
													data-dismiss="modal" type="reset">Cancel</button>
											</div>
										</g:form></div>
										</div>
									</div>
								</div>
							</div>
							<p>
								<button class="btn btn-inverse waves-effect waves-light"
									type="submit" data-toggle="modal"
									data-target="#myModalNorm${i}" style="width: 50%;">Reject</button>
							</p>
							<!-- Modal -->
							<div class="modal fade" id="myModalNorm${i}" tabindex="-1"
								role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">

									<div class="modal-content"
										style="width: 50%; text-align: center;">
										<g:form method="post" action="rejectRequest"
											controller="TripRequests" role="form">
											<input type="hidden" name="tripID" value="${trips.tripID }">
											<input type="hidden" name=firstName
												value="${trips.name}">
											<input type="hidden" name="phone"
												value="${trips.phone }">
											<input type="hidden" name="startLocation"
												value="${trips.startLocation }">
											<input type="hidden" name="endLocation"
												value="${trips.endLocation }">
											<!-- Modal Header -->
											<div class="">
												<h4 class="modal-title" id="myModalLabel">Select reject
													reason</h4>
											</div>

											<!-- Modal Body -->
											<div class="modal-body">
												<div class="row">
													<select class=" form-control" name="reason"
														data-style="btn-white" required="required" oninvalid="this.setCustomValidity('Select reject reason')" onchange="this.setCustomValidity('');">
														<option class="text-muted" value="" disabled selected>Select</option>
														<g:each var="reason" in="${RejectionReason.list()}">
															<option value="${reason.reasonDetails}">
																${reason.reasonDetails}
															</option>
														</g:each>
													</select>
												</div>
											</div>

											<!-- Modal Footer -->
											<div class="" style="text-align: center;">
												<button class="btn btn-success waves-effect waves-light"
													type="submit" style="float: right;">Reject</button>
												<button class="btn btn-inverse waves-effect waves-light"
													data-dismiss="modal" type="reset">Cancel</button>
											</div>
										</g:form>
									</div>
								</div>
							</div>
						</div>
					
						<g:if test="${trips.CoPasscount }">
							<div class="col-md-12">
								<p><b>CO-TRAVELLERS</b></p>	
									<div class="col-md-12 innerShadow">
										<div class="col-md-12">
											<div class="col-md-3" style="text-align:center"><b>Name</b></div>
											<div class="col-md-6" style="text-align:center"><b>Pickup Location</b></div>
											<div class="col-md-3" style="text-align:center"><b>Mobile Number</b></div>
										</div>
										<g:each var="coTravellers" in="${trips.coTravellers}" status="j">
											<div class="col-md-3" style="text-align: center;">
												<p>
													${coTravellers.name}
												</p>
											</div>
											<div class="col-md-6" style="text-align:center">
												<p>
													&nbsp;&nbsp;${coTravellers.startLocation}
												</p>
											</div>
											<div class="col-md-3" style="text-align:center">
												<p>
													&nbsp;&nbsp;${coTravellers.phone}
												</p>
											</div>
											
										</g:each>
									</div>
							</div>
						</g:if>
						<g:if test="${trips.note!=""}">
						<p style="margin-left: 9%; color: #0ebab8;">NOTE: ${trips.note}</p>
						</g:if>
						
					</div>
					</td>
				</tr>
				</g:each>
				</tbody></table>
			</g:if>
			<g:else>
				<script type="text/javascript">
					changeBackground('#f2f3f4');
				</script>
				<div class="col-md-12">
					<div style="text-align: center;">
						<div style="margin: 5%;"></div>
						<img class="img-responsive" alt="" src="${resource(dir:'images',file:'no_trip_requests.png')}" style="margin: 0 auto; height: 320px;">
						<div style="margin: 4%;"></div>
						<h3 class="text-muted">No booking requests</h3>
						<h4 class="text-muted">Relax</h4>
						<div style="margin: 7%;"></div>
					</div>
				</div>
			</g:else>
		</div>
	</div>
	<div class="container">  
  <!-- Modal -->
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="myModal" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-sm">
           <div class="modal-content" style="text-align: center;">
               <div class="modal-header" style="border-bottom: medium none;margin: -15px;">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                   
               </div>
               <div class="modal-body">
              Trip request accepted successfully.
               </div>
           </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
   </div>
</div>
<script>
$(document).ready(function(){
    // Show the Modal on load
 var d="${session["popUpData"] }";
  if(d=="show"){
    $("#myModal").modal("show");
    }
  <%
  session['popUpData']="";
  %>
});
</script>

<div class="container">  
  <!-- Modal -->
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="myModalReject" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-sm">
           <div class="modal-content" style="text-align: center;">
               <div class="modal-header" style="border-bottom: medium none;margin: -15px;">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>                   
               </div>
               <div class="modal-body">
              Trip request rejected successfully.
               </div>
           </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
   </div>  
</div>
<script>
$(document).ready(function(){
    // Show the Modal on load
 var d="${session["popUpDataReject"] }";
  if(d=="show"){
    $("#myModalReject").modal("show");
    }
  <%
  session['popUpDataReject']="";
  %>
});
</script> 
<script type="text/javascript">
$(document).ready(function(){
<%
session['sidebarActive']="";
session['sidebarActive']="tripRequests/index";
%>
});
</script>  
</body>
</html>