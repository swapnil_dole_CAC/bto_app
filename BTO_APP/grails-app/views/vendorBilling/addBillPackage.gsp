<meta name="layout" content="vendorMaster"/>
<script type="text/javascript">
	$(".package2").hide()
	function OpenDiv(id)
	{
		if(id==1)
		{
			$(".package2").hide()
		}
		else if(id==2)
		{
			$(".package2").show()
		}
	}
</script>
<div class="col-md-3"></div>
<div class="col-md-6">
	<g:if test="${flash.message}">
		<div class="message" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.message1}">
		<div class="message1" role="status">
			${flash.message1}
		</div>
	</g:if>
	<div class="card-box" style="height:390px">
		<g:form action="saveBillPackage" method="post">
			<div class="col-md-12">
				<table class="table">
					<thead>
						<tr>
							<td colspan="2" class="tableborder" style="text-align: center;border:0;"><h4>Add Package</h4></td>
						</tr>
					</thead>
					<tbody>					
						<tr>
							<td width="50%" style="border:0">Package Type</td>
							<td style="border:0">
								<select name="packageType" class="form-control" onChange="OpenDiv(this.value)" required>
									<option value="">Select Type</option>
									<option value="1">Flat Rate</option>
									<option value="2">Custom Package</option>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-12">
				<table class="table">
					<tbody>
						<tr>
							<td width="50%" style="border:0">Kilometer</td>
							<td style="border:0">
								<input type="text" class="form-control" name="kilometer" size="5" id="kilometer" value="0" pattern = "^\d+(\.\d+)*$" required="" oninvalid="this.setCustomValidity('Enter Kilometer')"	onchange="this.setCustomValidity('')"/></td>
						</tr>
						<tr class="package2">
							<td width="50%" style="border:0">Hour</td>
							<td style="border:0">
								<input type="text" class="form-control" name="hour" size="5" id="hour" value="0" pattern = "^\d+(\.\d+)*$" required="" oninvalid="this.setCustomValidity('Enter Amount per kilometer')"onchange="this.setCustomValidity('')"/>
							</td>
						</tr>
						<tr>
							<td width="50%" style="border:0">Amount</td>
							<td style="border:0">
								<input type="text" class="form-control" name="amount" size="5" id="amount" value="0" pattern = "^\d+(\.\d+)*$" required="" oninvalid="this.setCustomValidity('Enter Amount per kilometer')"onchange="this.setCustomValidity('')"/>
							</td>
						</tr>						
						<tr>
							<td colspan="2" style="border:0" align="center">
								<button class="btn btn-success waves-effect waves-light" type="submit">Submit</button>	
								&nbsp;<g:link action="index" class="btn btn-inverse waves-effect waves-light">Cancel</g:link>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</g:form>
	</div>
</div>
<div class="col-md-3"></div>