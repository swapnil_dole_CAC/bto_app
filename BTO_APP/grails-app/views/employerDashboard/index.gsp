<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<style type='text/css' media='screen'>
.message {
	padding: 6px 25px 20px 25px;
	color: green;
}

.message1 {
	padding: 6px 25px 20px 25px;
	color: red;
}

.card {
	min-height: 120px;
	max-height: 120px;
	margin-left: 2%;
	background-color: white;
	width: 250px;
	border-top: solid;
	border-radius: 7px;
	border-top-width: 3px;
	box-shadow: 3px 1px 19px 1px #888888;
}

.boldLargeText {
	font-family: sans-serif, 'Gotham Rounded medium';
	font-size: 20px;
	color: #808080;
}

.boldSmallText {
	font-family: sans-serif, 'Gotham Rounded medium';
	font-size: 10px;
	color: #808080;
}

.normalLageText {
	font-family: sans-serif, 'Gotham Rounded medium';
	font-size: 12px;
	color: #B3B3B3;
}
.aligntext{
	text-align:center;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<meta name="layout" content="main"/>
<title>Dashboard</title>
</head>
<body>
<div class="page-title">
			<div class="title">Dashboard</div>
		</div>
 	<div class="body">
 		
		<div class="col-md-12 boldLargeText" style="margin-bottom:1%">Count</div>
  		<div class="row">
			<div class="col-md-4 col-xs-4 card aligntext" style="border-top-color: orange;margin-bottom:2%">
				<div class="col-md-4 col-xs-4" style="margin-top: 12px;">
					<img src="${createLinkTo(dir:'images',file:'active_user.png')}"	alt="Users" border="0" />
				</div>
				<div class="col-md-8" style="padding-left: 12%; margin-top: 16%;">
					<div class="col-md-12 col-xs-4" style="margin-bottom: 0%;">
						<div class="boldLargeText">Users</div>
					</div>
					<div class="col-md-12">
						<div class="boldLargeText">
							${dashboardInstance.users }							
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-4 card aligntext" style="border-top-color: yellow;margin-bottom:2%">
				<div class="col-md-4 col-xs-4" style="margin-top: 12px;">
					<img src="${createLinkTo(dir:'images',file:'my_trips.png')}" alt="Trips" border="0" />
				</div>
				<div class="col-md-8 col-xs-8" style="padding-left: 5%; margin-top: 16%;">
					<div class="col-md-12 col-xs-12" style="margin-bottom: 0%;">
						<div class="boldLargeText">Trips</div>
					</div>
					<div class="col-md-12 col-xs-12">
						<div class="boldLargeText">
							${dashboardInstance.trips }	
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-5 col-xs-4 card aligntext" style="border-top-color: orange;margin-bottom:2%">
				<div class="col-md-4 col-xs-4" style="margin-top: 12px;">
					<img src="${createLinkTo(dir:'images',file:'distance-travelled.png')}" alt="Saving" border="0" />
				</div>
				<div class="col-md-8 col-xs-8" style="padding-left: 2%; margin-top: 3%;">
					<div class="col-md-12 col-xs-12" style="margin-bottom: 0%;">
						<div class="boldLargeText">Distance</div>(Travelled by <br>Co-Passengers)
					</div>
					<div class="col-md-12 col-xs-12">
						<div class="boldLargeText">
							${dashboardInstance.distance } Km.
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12 boldLargeText" style="margin-bottom:1%">Saving</div>
		<div class="row">
			<div class="col-md-4 col-xs-4 card aligntext" style="border-top-color: orange;margin-bottom:2%">
				<div class="col-md-4 col-xs-4" style="margin-top: 12px;">
					<img src="${createLinkTo(dir:'images',file:'savings.png')}" alt="Saving" border="0" />
				</div>
				<div class="col-md-8 col-xs-8" style="padding-left: 10%; margin-top: 16%;">
					<div class="col-md-12 col-xs-12" style="margin-bottom: 0%;">
						<div class="boldLargeText">Money</div>
					</div>
					<div class="col-md-12 col-xs-12">
						<div class="boldLargeText">
							<i class="fa fa-inr" aria-hidden="true"></i>&nbsp;&nbsp;${dashboardInstance.saving }
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-4 card aligntext" style="border-top-color: yellow;margin-bottom:2%">
				<div class="col-md-4 col-xs-4" style="margin-top: 12px;">
					<img src="${createLinkTo(dir:'images',file:'Fuel-saved.png')}"	alt="Users" border="0" />
				</div>
				<div class="col-md-8" style="padding-left: 12%; margin-top: 16%;">
					<div class="col-md-12 col-xs-4" style="margin-bottom: 0%;">
						<div class="boldLargeText">Fuel</div>
					</div>
					<div class="col-md-12">
						<div class="boldLargeText">
							${dashboardInstance.fuel }	&nbsp;Ltr.						
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-4 card aligntext" style="border-top-color: orange;margin-bottom:2%">
				<div class="col-md-4 col-xs-4" style="margin-top: 12px;">
					<img src="${createLinkTo(dir:'images',file:'CO2-saving.png')}" alt="Trips" border="0" />
				</div>
				<div class="col-md-8 col-xs-8" style="padding-left: 5%; margin-top: 16%;">
					<div class="col-md-12 col-xs-12" style="margin-bottom: 0%;">
						<div class="boldLargeText">CO<sub>2</sub></div>
					</div>
					<div class="col-md-12 col-xs-12">
						<div class="boldLargeText">
							${dashboardInstance.co2 }	 Kg.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>