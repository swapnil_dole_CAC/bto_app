<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<%--<meta name="layout" content="main" />--%>
<title>Login | BTO Vendor</title>
<link rel="stylesheet" href="${resource(dir:'assets/css',file:'bootstrap.min.css') }">
<link rel="stylesheet" href="${resource(dir:'assets/css',file:'components.css') }">
<link rel="stylesheet" href="${resource(dir:'assets/css',file:'core.css') }">
<link rel="stylesheet" href="${resource(dir:'assets/css',file:'icons.css') }">
<link rel="stylesheet" href="${resource(dir:'assets/css',file:'responsive.css') }">
<style>
html, body {
	background-image: url("${resource(dir:'images',file:'vendor.jpg')}");
	background-repeat: no-repeat;
	background-size: cover;
}

.inputTranspernt {
	background: transparent;
}

.card-box {
	background: rgba(255, 255, 255, 0.75) none repeat scroll 0 0;
	margin-top: 8%;
	padding: 0;
}

.message1 {
	color: green;
}

.message {
	color: red;
}
</style>
<script type="text/javascript">
$(document).ready(function() {

	  if(window.location.href.indexOf('#myModalNorm') != -1) {
	    $('#myModalNorm').modal('show');
	  }
	});
</script>
</head>
<body>
	<div class="wrapper-page">
		<div class="col-md-1"></div>
		<div class=" card-box col-md-4">
			<div class="panel-heading">
				<h3 class="">LOGIN</h3>
				
				<g:if test="${flash.message}">
					<div class="message" role="status">
						${flash.message}
					</div>
				</g:if>
				<g:if test="${flash.message1}">
					<div class="message1" role="status">
						${flash.message1}
					</div>
				</g:if>
				<div class="panel-body">
					<g:form class="form-horizontal m-t-30" action="authenticateVendor" method="post">
						<div class="form-group ">
							<div class="col-md-12">
								<label>Email address*</label> 
								<input class="form-control inputTranspernt" name="email" oninvalid="this.setCustomValidity('Enter email address')"
								onchange="this.setCustomValidity('')"  type="email" required="" placeholder="Enter email address">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<label>Password*</label> 
								<input	class="form-control inputTranspernt" name="password"
									oninvalid="this.setCustomValidity('Enter valid password')"
												onchange="this.setCustomValidity('')"
									type="password" required="" placeholder="Enter Password">
							</div>
						</div>
						<div class="form-group m-b-0">
							<div class="col-md-12">
								<a href="#" class="text-dark" data-toggle="modal"
									data-target="#myModalNorm"><i class="fa fa-lock m-r-5"></i>
									Forgot password?</a>
							</div>
						</div>
						<div class="form-group text-center m-t-30">
							<div class="col-md-12">
								<button class="btn btn-success  waves-effect waves-light"
									type="submit" style="float: right;">Login</button>
							</div>
						</div>
					</g:form>
					<%--<div class="col-md-12 text-center">
						<p>Don't have an account yet?</p>
						<h4>
							<g:link class="text-primary m-l-5" action="register">REGISTER</g:link>
						</h4>
					</div>
				--%>
				</div>

			</div>
		</div>
		<div class="col-md-7"></div>
	</div>
	<!-- Modal-Effect -->
	<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>
	<script src="${resource(dir:'assets/js',file:'bootstrap.min.js')}"></script>
	<script
		src="${resource(dir:'assets/plugins/custombox/dist',file:'custombox.min.js')}"></script>
	<script
		src="${resource(dir:'assets/plugins/custombox/dist',file:'legacy.min.js')}"></script>

	<!-- Modal -->
	<div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">

			<div class="modal-content">
				<g:form method="post" action="forgotPassword" role="form">
					<!-- Modal Header -->
					<div class="modal-header">
						
						<h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
					</div>

					<!-- Modal Body -->
					<div class="modal-body">


						<div class="alert alert-info alert-dismissable">
							<button type="button" class="close" data-dismiss="alert"
								aria-hidden="true">×</button>
							Enter your <b>Email</b> and instructions will be sent to you!
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Email address</label> <input
								type="email" class="form-control" name="emailID" required="required"
								id="exampleInputEmail1" placeholder="Enter email address" />
						</div>

					</div>

					<!-- Modal Footer -->
					<div class="modal-footer">
						<button class="btn btn-success waves-effect waves-light"
							type="submit">Submit</button>
						<button class="btn btn-inverse waves-effect waves-light"
							data-dismiss="modal" type="reset">Cancel</button>
					</div>
				</g:form>
			</div>
		</div>
	</div>


</body>
</html>