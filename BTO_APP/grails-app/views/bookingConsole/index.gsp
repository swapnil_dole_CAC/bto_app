<%@page import="com.bto.trip.TravelPurpose"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.lang.String"%>
<%@page import="com.bto.customer.VendorDetails"%>
<%@page import="com.bto.customer.CostCenter"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<script type="text/javascript">
function start() {
// put all your onload functions here
check();
getLocation();
}
window.onload = start;
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="layout" content="main" />
<title>Booking Console | BTO</title>
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/timepicker',file:'bootstrap-timepicker.min.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/mjolnic-bootstrap-colorpicker/dist/css',file:'bootstrap-colorpicker.min.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/bootstrap-datepicker/dist/css',file:'bootstrap-datepicker.min.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/clockpicker/dist',file:'jquery-clockpicker.min.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/plugins/bootstrap-daterangepicker',file:'daterangepicker.css') }">

<%--code for getting locations from google --%>
<%--<script
	src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
--%>
<script	src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyBI9iSbSEo3h0LdqlqRFwnayYApQfmNXuE&region=IN"></script>

<script>
	function initialize() {

		var input = document.getElementById('searchTextField');
		var autocomplete = new google.maps.places.Autocomplete(input);
		var input1 = document.getElementById('searchTextField1');
		var autocomplete1 = new google.maps.places.Autocomplete(input1);
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<%--code for getting locations from google END HERE --%>

<%--code for other reason of purpose select --%>
<script>
	function check() {
		var currentSelection = $("#checkSelection").val();
		if (currentSelection == "") {
			$("#current").css("display", "");
			$("#other").css("display", "none");
		} else if (currentSelection == "other") {
			$("#other").css("display", "");
			$("#current").css("display", "none");
		} else {
			$("#current").css("display", "");
			$("#other").css("display", "none");
		}

	}
</script>
<script type="text/javascript">
function checkTime() {
	var d=document.getElementsByName("tripDate")[0].value
	var t=document.getElementsByName("tripTime")[0].value
	<%
	Date date = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	String strDate = sdf.format(date);	
	%>
	var dt = new Date();
    var h =  dt.getHours(), m = dt.getMinutes();
    var _time = (h > 12) ? (h-12 + ':' + m +' PM') : (h + ':' + m +' AM');
    var cd="<%=strDate.toString()%>";

     //if(d==cd){
     if(false){
        if(t<"0"+_time){
        	//alert("invalid time")
        	document.getElementById("ptime").innerHTML = "<span style='color:red;'>Invalid Time...!</span>"; 
        	return false;
            }else{
            	document.getElementById("ptime").innerHTML = "<span></span>";
            	return true;
            }   
        }
}
function hideError(){
	document.getElementById("ptime").innerHTML = "<span></span>";
}
</script>

<style type="text/css">
html {
	background-color: transparent;
}

body {
	background-color: white;
}

.card-box { //
	box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0
		rgba(0, 0, 0, 0.19);
	box-shadow: 1px 1px 5px #aeaeae;
	background-color: transparent;
}

:-moz-ui-invalid:not(output) {
    box-shadow: 0 0 1.5px 1px red;
}

.bootstrap-timepicker-widget table td {
    padding: 0px;
    }
</style>
</head>
<body>

<script>
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(GetAddress); 

    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

</script>
    <script type="text/javascript">
        function GetAddress(position) {
            var lat = parseFloat(position.coords.latitude);
            var lng = parseFloat(position.coords.longitude);
            var latlng = new google.maps.LatLng(lat, lng);
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                       // alert("Location: " + results[1].formatted_address);
						document.getElementById("searchTextField").value = ""+results[1].formatted_address;
                    }
                }
            });
        }
    </script>

	<div class="body">
					<div class="col-md-3"></div>
					<div class="">
						<div class="col-md-6">
							<div class="card-box">
								<h3 class="m-b-20" style="text-align: center; "> 
									Create Trip
								</h3>
								<g:if test="${flash.message}">
									<div class="message" role="status">
										${flash.message}
									</div>
								</g:if>
								<g:if test="${flash.message1}">
									<div class="message1" role="status">
										${flash.message1}
									</div>
								</g:if>
								
								<g:form controller="BookingConsole" action="bookTrip" method="post">
								<input type="hidden" name="userID" value="${session.userID}"/>
								<%--<input type="text" name="vendorID" value="1"/>
								<input type="text" name="carID" value="1"/>
									--%><div class="col-md-12">
										<div class="form-group col-md-6">
											<label>From</label><p> <input type="text" name="startLocation"
												oninvalid="this.setCustomValidity('Enter start Location')"
												onchange="this.setCustomValidity('')"
												parsley-trigger="change" required id="searchTextField"
												placeholder="Enter start location" class="form-control"></p>
										</div>
										<div class="form-group col-md-6">
											<label>To</label><p> <input type="text" parsley-trigger="change"
												name="endLocation"
												oninvalid="this.setCustomValidity('Enter end location')"
												onchange="this.setCustomValidity('')" id="searchTextField1"
												required placeholder="Enter end location"
												class="form-control"></p>
										</div>
									</div>
									<div class="form-group col-md-12">
										<label style="max-width: 96%; margin-left: 2%;">Travel purpose</label> 
										<select class=" form-control" style="max-width: 96%; margin-left: 2%;" name="purpose" data-style="btn-white" oninvalid="this.setCustomValidity('Select travel purpose')"
												onchange="check(); this.setCustomValidity('');" id="checkSelection" required="required">
											<option class="text-muted" value="" disabled selected>Select</option>
											<g:each var="purpose" in="${TravelPurpose.list()}">
												<option value="${purpose.travelPurpose}">${purpose.travelPurpose}</option>
											</g:each>
											<option value="other">Other</option>
										</select>
									</div>
									<div class="form-group col-md-12" id="other"
										style="display: none">

										<input type="text" parsley-trigger="change"	name="otherPurpose" placeholder="Enter here" class="form-control" style="max-width: 96%; margin-left: 2%;">
									</div>
									<div class="col-md-12">
										<div class="form-group col-md-6">
											<label>Date</label>
											<div class="input-group">
												<input type="text" class="form-control" required="required" data-date-format='dd/mm/yyyy' name="tripDate"
													placeholder="dd/mm/yyyy" id="datepicker-autoclose">
												<span class="input-group-addon bg-custom b-0 text-white">
													<i class="icon-calender"></i>
												</span>
											</div>
										</div>
										
										<div class="form-group col-md-6">
											<label>Time</label>
											<div class="input-group m-b-15">
												<div class="bootstrap-timepicker">
													<input id="timepicker" type="text" name="tripTime" class="form-control" required="required" value="" onblur="hideError();">
												</div>
												<span class="input-group-addon bg-custom b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>												
											</div>
											<p id="ptime" style="color:red;"></p>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group col-md-6">
											<label>Vendor</label>								
											<select name="vendorID" id="vendorID" class=" form-control" data-style="btn-white" required="required" oninvalid="this.setCustomValidity('Select vendor')"
												onchange="this.setCustomValidity('');" id="checkSelection">
												<option class="text-muted" value="" disabled selected>Select vendor</option>
											</select>
										</div>
										<div class="form-group col-md-6">
											<label>Car type</label>
											<select name="carType" id="carType" class="form-control" data-style="btn-white" required="required" oninvalid="this.setCustomValidity('Select car type')"
												onchange="this.setCustomValidity('');">
												<option class="text-muted" value="" disabled selected>Select car type</option>
											</select>
										</div>
									</div>
									<div class="form-group col-md-6">
										<label style="max-width: 96%; margin-left: 2%;">Cost center</label>
										<input type="text" parsley-trigger="change" value="${session['costCenter'] }" required="required"
											name="costCenter" placeholder="Enter cost center"
											class="form-control" style="max-width: 96%; margin-left: 2%;">
									</div>
									<div class="form-group col-md-6">
										<label style="max-width: 96%; margin-left: 2%;">Note</label>
										<textarea rows="1" name="note" cols="21" placeholder="Enter note" class=""></textarea>
										<!-- <input type="text" parsley-trigger="change"
											name="note" placeholder="Enter note" class="form-control" style="max-width: 96%; margin-left: 2%;"> -->
									</div>
									<div class="form-group m-b-0" style="text-align: center;">
										<button class="btn btn-success waves-effect waves-light" style="width: 40%;" type="submit">Submit</button>										
									</div>									
								</g:form>
							</div>
						</div>
					</div>
					<div class="col-md-0"></div>
					<%--<div class="col-md-5">
						<div class="col-md-12">
						<div class="card-box" id="map" style="width: 490px; height: 510px;"></div>
						</div>
					</div>
					--%>
					<div class="col-md-3"></div>
				</div>
			
			
	<%--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> --%>
	
	
	<%--
	<script type="text/javascript">

// this will load map with default locations
	window.onload = function() {
		getDirections();
		};

	//get directions using ggogle api
		function getDirections(){
		
		var a= document.getElementById("searchTextField").value;
		var b= document.getElementById("searchTextField1").value;
		var origin='Pune India'
		var	destination='Pune India';
		if(!a==''){
			origin=a
			}
		if( !b ==''){
			destination=b
				
			}
		//alert(origin)
		//alert(destination)
		var directionsService = new google.maps.DirectionsService();
		var directionsDisplay = new google.maps.DirectionsRenderer();
		
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom : 7,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		});

		directionsDisplay.setMap(map);

		var request = {
			origin : origin,
			destination : destination,
			travelMode : google.maps.DirectionsTravelMode.DRIVING
		};

		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
			}
		});
	}
	</script>

--%>

<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>
<script type="text/javascript">

var myJson="";
var json='${jsonObject}';
myJson=JSON.parse(json.split("&quot;").join('"'));
//alert(myJson);

$.each(myJson.vendorAndCars, function (index, value) {
    $("#vendorID").append('<option value="'+value.id+'">'+value.vendorName+'</option>');
});

$('#vendorID').on('change', function(){
    console.log($(this).val());
    for(var i = 0; i < myJson.vendorAndCars.length; i++)
    {
      if(myJson.vendorAndCars[i].id == $(this).val())
      {
         $('#carType').html('<option value="" disabled>Select</option>');
         $.each(myJson.vendorAndCars[i].carTypes, function (index, value) {
            $("#carType").append('<option value="'+value.type+'">'+value.type+'</option>');
        });
      }
     
    }
});

</script>
<script type="text/javascript">
$(document).ready(function(){
<%
session['sidebarActive']="";
session['sidebarActive']="bookingConsole/index";
%>
});
</script>
</body>
</html>