<meta name="layout" content="vendorMaster"/>
 <link href="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css')}" rel="stylesheet" type="text/css" />
<div class="page-title">
	<div class="title">Car Types</div>
</div>
<div class="col-md-12" style="text-align:right">
	<g:link controller="VendorCars" action="addVendorCar" class="btn btn-success waves-effect waves-light" style="float:right;"> Add Car Types</g:link>
<br><br>
</div>
<div class="row" style="margin-top: 5%;">
	<g:if test="${flash.message}">
		<div class="message" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.message1}">
		<div class="message1" role="status">
			${flash.message1}
		</div>
	</g:if>
	<div class="col-md-1"></div>
<div class="col-md-10">
	<table id="datatable" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Sr. No.</th>
				<th>Car Types</th>
			</tr>
			</thead>
			<tbody>
				<g:set var="j" value="${1 }"/>
				<g:each in="${carsInstance}" status="i" var="vendorDetailsInstance">
					<tr>
						<td align="center" style="border:0">${j++}</td>
						<td>${vendorDetailsInstance}</td>
					</tr>
				</g:each>			
		</tbody>
	</table>
</div>
<div class="col-md-1"></div>
</div>
<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js')}"></script>
<script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').dataTable();
            });
          </script>
<script type="text/javascript">
$(document).ready(function(){
<%
session['sidebarActive']="";
session['sidebarActive']="vendorCars/index";
%>
});
</script>