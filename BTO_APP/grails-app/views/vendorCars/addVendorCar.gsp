<%@page import="com.bto.customer.CarType"%>
<meta name="layout" content="vendorMaster" />
<link rel="stylesheet" href="${resource(dir:'assets/plugins/bootstrap-tagsinput/dist',file:'bootstrap-tagsinput.css') }">
<link rel="stylesheet" href="${resource(dir:'assets/plugins/switchery/dist',file:'switchery.min.css') }">
<link rel="stylesheet" type="text/css" href="${resource(dir:'assets/plugins/multiselect/css',file:'multi-select.css') }">
<link rel="stylesheet" type="text/css" href="${resource(dir:'assets/plugins/select2',file:'select2.css') }">
<link rel="stylesheet" href="${resource(dir:'assets/plugins/bootstrap-select/dist/css',file:'bootstrap-select.min.css') }">
<link rel="stylesheet" href="${resource(dir:'assets/plugins/bootstrap-touchspin/dist',file:'jquery.bootstrap-touchspin.min.css') }">
	    
<div class="row">
	<div class="col-sm-12">
		<div class="">
			<h4 class="m-t-0 header-title">
				<b>New Car Type</b>
			</h4>
			<div class="col-md-2"></div>
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:if test="${flash.message1}">
				<div class="message1" role="status">
					${flash.message1}
				</div>
			</g:if>
			<div class="col-md-8">
				<g:form action="saveVendorCar" method="post">
					<div>
					
					<select name="carTypes" class="multi-select" multiple=""
							id="my_multi_select3">							
							<g:each var="carType" in="${CarType.list()}">
						    <option value="${carType.type}">${carType.type}</option>					    				  
							</g:each>
							
						</select>

						<div class="form-group clearfix">
							<div class="col-lg-3" style="float: right;">
								<button class="btn btn-success waves-effect waves-light"
									type="submit">Submit</button>
									&nbsp;<g:link action="index" class="btn btn-inverse waves-effect waves-light">Cancel</g:link>
							</div>
						</div>


					</div>
				</g:form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</div>
<!-- End row -->

	