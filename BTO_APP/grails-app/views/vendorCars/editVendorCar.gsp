<meta name="layout" content="vendorMaster" />
<div class="row">
	<div class="col-sm-12">
		<div class="">
			<h4 class="m-t-0 header-title">
				<b>Update Car Type</b>
			</h4>
			<div class="col-md-2"></div>
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:if test="${flash.message1}">
				<div class="message1" role="status">
					${flash.message1}
				</div>
			</g:if>
			<div class="col-md-8">
				<g:form action="updateVendorCar" controller="VendorCars" method="post">
					<div>
						<g:hiddenField name="id" value="${vendorDetailsInstance?.id}" />
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label " for="userName2">Car Type *</label>
							<div class="col-lg-9">
								<input class="form-control" id="userName2" name="carTypes" type="text" value="${vendorDetailsInstance?.carTypes}" required="required">
							</div>
						</div>
						
						<div class="form-group clearfix">
							<div class="col-lg-3" style="float: right;">
								<button class="btn btn-success waves-effect waves-light"
									type="submit">Update</button>
									&nbsp;<g:link action="index" class="btn btn-inverse waves-effect waves-light">Cancel</g:link>
							</div>
						</div>


					</div>
				</g:form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</div>
<!-- End row -->