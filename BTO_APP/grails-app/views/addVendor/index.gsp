<meta name="layout" content="main"/>
 <link href="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css')}" rel="stylesheet" type="text/css" />
<h4 class="m-t-0 header-title">
		<b>Vendor List</b>
	</h4>
	
<div class="col-md-12" style="text-align:right">
	<g:link controller="AddVendor" action="addVendor" class="btn btn-success waves-effect waves-light" style="float:right;"> New Vendor</g:link>
<br>
</div>
<div class="row" style="margin-top: 5%;">
	<g:if test="${flash.message}">
		<div class="message" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.message1}">
		<div class="message1" role="status">
			${flash.message1}
		</div>
	</g:if>
	<div class="col-md-1"></div>
<div class="col-md-10">
	<table id="datatable" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Company Name</th>
				<th>Email Address</th>
				<th>Mobile</th>
				<th>Address</th>
				<th>Car Types</th>
				<th>Actions</th>
			</tr>
			</thead>
			<tbody>
			<g:each in="${vendorDetailsInstanceList}" status="i" var="vendorDetailsInstance">
				<tr>
				<td>${vendorDetailsInstance?.companyName}</td>
				<td>${vendorDetailsInstance?.email}</td>
				<td>${vendorDetailsInstance?.mobile}</td>
				<td>${vendorDetailsInstance?.address}</td>
				<td>${vendorDetailsInstance?.carTypes}</td>
				<td>
				<g:link action="editVendor" id="${vendorDetailsInstance?.id}" title="Edit"><i class="md  md-edit"></i></g:link>
				<%--
				 | 
				<g:link action="deleteVendorDetails" id="${vendorDetailsInstance?.id}" title="Delete" onclick="return confirm(' You want to delete?');"><i class="md  md-delete"></i></g:link>
				
				--%></td>
				</tr>
			</g:each>			
		</tbody>
	</table>
</div>
<div class="col-md-1"></div>
</div>
<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js')}"></script>
<script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').dataTable();
            });
          </script>