<meta name="layout" content="main" />
<script>
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Number"+charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
		document.getElementById("mob").innerHTML = "<span style='color:red;'>Only 0-9 digits allowed...!</span>"; 
	 return false;
	}
       
    else{
    document.getElementById("mob").innerHTML = "<span></span>";
	return true;
	}
}
function isNumberKey1(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Number"+charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
		document.getElementById("pmob").innerHTML = "<span style='color:red;'>Only 0-9 digits allowed...!</span>"; 
	 return false;
	}
       
    else{
    document.getElementById("pmob").innerHTML = "<span></span>";
	return true;
	}
}
function isCharacterKeyDot(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Char"+charCode);
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 46 || charCode == 40 || charCode == 41 || charCode == 32 || charCode == 47 || charCode == 45 || charCode == 8 || charCode == 13){
    	document.getElementById("pname").innerHTML = "<span></span>";    	
        return true;
    }else{
    	document.getElementById("pname").innerHTML = "<span style='color:red;'>Only a-z A-Z digits allowed...!</span>";
        return false;
    }
}
function isCharacterKeyDot1(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Char"+charCode);
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 48 && charCode <= 57) || (charCode >= 97 && charCode <= 122) || charCode == 46 || charCode == 40 || charCode == 41 || charCode == 32 || charCode == 47 || charCode == 45 || charCode == 8 || charCode == 13){
    	document.getElementById("cname").innerHTML = "<span></span>";    	
        return true;
    }else{
    	document.getElementById("cname").innerHTML = "<span style='color:red;'>Only 0-9 a-z A-Z digits allowed...!</span>";
        return false;
    }
}
</script>
<div class="row">
	<div class="col-sm-12">
		<div class="">
			<h4 class="m-t-0 header-title">
				<b>New Vendor</b>
			</h4>
			<div class="col-md-2"></div>
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:if test="${flash.message1}">
				<div class="message1" role="status">
					${flash.message1}
				</div>
			</g:if>
			<div class="col-md-8">
				<g:form controller="AddVendor" action="saveVendor" method="post">
					<div>

						<div class="form-group clearfix">
							<label class="col-lg-3 control-label " for="userName2">Enter email address*</label>
							<div class="col-lg-9">
								<input class="form-control" id="userName2" name="email"	type="text" required="required" oninvalid="this.setCustomValidity('Enter email address')"
											onchange="this.setCustomValidity('')">
							</div>
						</div>

						<div class="form-group clearfix">
							<label class="col-lg-3 control-label" for="name2">Company name *</label>
							<div class="col-lg-9">
								<input id="name2" name="companyName" type="text" class="form-control" required="required" oninvalid="this.setCustomValidity('Enter company name')"
											onchange="this.setCustomValidity('')" onkeypress="return isCharacterKeyDot1(event);">
											<div id="cname" style="color:red;"></div>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label " for="surname2">Enter  mobile number *</label>
							<div class="col-lg-9">
								<input id="surname2" name="mobile" type="text" required="required" maxlength="10" min="10" oninvalid="this.setCustomValidity('Enter mobile number')"
											onchange="this.setCustomValidity('')" onkeypress="return isNumberKey(event);"
									class=" form-control">
									<div id="mob" style="color:red;"></div>

							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label " for="address2">Address</label>
							<div class="col-lg-9">
								<input id="address2" name="address" type="text" class="form-control">
							</div>
						</div>


						<div class="form-group clearfix">
							<label class="col-lg-3 control-label " for="email2">Enter POC name *</label>
							<div class="col-lg-9">
								<input id="email2" name="POCName" type="text" required="required" class=" form-control" oninvalid="this.setCustomValidity('Enter POC name')"
											onchange="this.setCustomValidity('')" onkeypress="return isCharacterKeyDot(event);">
											<div id="pname" style="color:red;"></div>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label " for="userName2">Enter POC email address *</label>
							<div class="col-lg-9">
								<input class="form-control" id="userName2" name="POCEmail" required="required" type="email" oninvalid="this.setCustomValidity('Enter poc email address')"
											onchange="this.setCustomValidity('')">
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label " for="POCMobile">Enter POC mobile number *</label>
							<div class="col-lg-9">
								<input class="form-control" id="POCMobile" name="POCMobile" maxlength="10" min="10" required="required" type="text" oninvalid="this.setCustomValidity('Enter POC mobile number')"
											onchange="this.setCustomValidity('')" onkeypress="return isNumberKey1(event);">
											<div id="pmob" style="color:red;"></div>
							</div>
						</div>

						<div class="form-group clearfix">
							<label class="col-lg-3 control-label " for="POCMobile">Profile Pictue</label>
							<div class="col-lg-9">
								<input class="" id="image" name="image" type="file">
							</div>
						</div>
						<div class="form-group clearfix">
							<div class="col-lg-4" style="float: right;">
								<button class="btn btn-success waves-effect waves-light"
									type="submit">Submit</button>
								&nbsp;&nbsp;
								<g:link action="index"
									class="btn btn-inverse waves-effect waves-light">Cancel</g:link>
							</div>
						</div>


					</div>
				</g:form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</div>
<!-- End row -->