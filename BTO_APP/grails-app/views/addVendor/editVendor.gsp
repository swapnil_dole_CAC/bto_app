<meta name="layout" content="main" />
<div class="row">
	<div class="col-sm-12">
		<div class="">
			<h4 class="m-t-0 header-title">
				<b>Update Vendor</b>
			</h4>
			<div class="col-md-2"></div>
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:if test="${flash.message1}">
				<div class="message1" role="status">
					${flash.message1}
				</div>
			</g:if>
			<div class="col-md-8">
				<g:form action="updateVendor" controller="AddVendor" method="post">
					<div>
						<g:hiddenField name="id" value="${vendorDetailsInstance?.id}" />
						<div class="form-group clearfix">
							<label class="col-lg-2 control-label " for="userName2">Email Address *</label>
							<div class="col-lg-10">
								<input class="form-control" id="userName2" name="email" type="text" value="${vendorDetailsInstance?.email}" required="required">
							</div>
						</div>

						<div class="form-group clearfix">
							<label class="col-lg-2 control-label" for="name2">Company name *</label>
							<div class="col-lg-10">
								<input id="name2" name="companyName" type="text" class=" form-control" value="${vendorDetailsInstance?.companyName}" required="required">
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-lg-2 control-label " for="surname2"> Mobile *</label>
							<div class="col-lg-10">
								<input id="surname2" name="mobile" type="text" value="${vendorDetailsInstance?.mobile}" maxlength="10" min="10"	class=" form-control" required="required">

							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-lg-2 control-label " for="address2">Address</label>
							<div class="col-lg-10">
								<input id="address2" name="address" type="text" value="${vendorDetailsInstance?.address}" class="form-control">
							</div>
						</div>
						

						<div class="form-group clearfix">
							<label class="col-lg-2 control-label " for="email2">POC Name *</label>
							<div class="col-lg-10">
								<input id="email2" name="POCName" type="text" value="${vendorDetailsInstance?.POCName}" class=" form-control" required="required">
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-lg-2 control-label " for="userName2">POC Email Address</label>
							<div class="col-lg-10">
								<input class="form-control" id="userName2" name="POCEmail" value="${vendorDetailsInstance?.POCEmail}" type="email">
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-lg-2 control-label " for="POCMobile">POC Mobile</label>
							<div class="col-lg-10">
								<input class="form-control" id="POCMobile" name="POCMobile" value="${vendorDetailsInstance?.POCMobile}" type="text">
							</div>
						</div>

						<%--<div class="form-group clearfix">
							<label class="col-lg-2 control-label " for="POCMobile">Car
								Types</label>
							<div class="col-lg-10">
								<input class="form-control" id="POCMobile" name="carTypes" value="${vendorDetailsInstance?.carTypes}"
									type="text">
							</div>
						</div>	--%>
						<div class="form-group clearfix">
							<label class="col-lg-2 control-label " for="POCMobile">Image</label>
							<div class="col-lg-10">
								<input class="" id="image" name="image" type="file">
							</div>
						</div>
						<div class="form-group clearfix">
							<div class="col-lg-3" style="float: right;">
								<button class="btn btn-success waves-effect waves-light" type="submit">Update</button>
									&nbsp;<g:link controller="AddVendor" action="index" class="btn btn-inverse waves-effect waves-light">Cancel</g:link>
							</div>
						</div>


					</div>
				</g:form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</div>
<!-- End row -->