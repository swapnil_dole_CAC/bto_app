<%@page import="com.bto.vendor.Driver"%>
<%@page import="com.bto.vendor.RejectionReason"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<meta http-equiv="refresh" content="100" >
	<meta name="layout" content="main" />
	<link href="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css')}" rel="stylesheet" type="text/css" />
	<link href="${resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
	<link href="${resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
	<link href="${resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
	<link href="${resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css')}" rel="stylesheet" type="text/css" />
	<title>Trip requests</title>
	<style>
	table, th, td {
	    background-color: #ffffff;
	    border: none;
	}
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td, .table > thead > tr > th, .table-bordered {
	    border-top: medium none;
	}
	.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
	    border-top: medium none;
	    line-height: 1.42857;
	    padding: 0;
	    vertical-align: top;
	}
	</style>
	<style type="text/css">
		.profile-widget .bg-profile {
			height: 80px;
		}
	</style>

	<script type="text/javascript">
		function changeBackground(color) {
		   document.body.style.background = color;
		}
	</script>
	<%--<script	src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyAl-G44Mw9pJUOwTHmKluyzF7n_hob0D_A"></script> --%>
</head>
<body>
	<div class="body">
		<div class="row">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:if test="${flash.message1}">
				<div class="message1" role="status">
					${flash.message1}
				</div>
			</g:if>
		
			<g:if test="${jsonObject.alerts}">
				<script type="text/javascript">
					changeBackground('#FFFFFF');
				</script>
			<table id="datatable" class="table">
			<thead>
			<tr>				
				<th style="display: none;"></th>				
			</tr>
			</thead>
			<tbody>			
				
				<g:each var="trips" in="${jsonObject.alerts.data}" status="i">
					
					<g:if test="${trips.alertType.equals("RequestArrives") }">
					<tr>				
					<td>
						<g:if test="${trips.status.equals("Unread") }">
							<div class="col-md-12" style="background-color: #f2f3f4;margin-bottom:2%;box-shadow:0 0 10px rgba(0, 0, 0, 0.2);border-left: 20px solid #0EBAB8; ">
								<div class="col-md-1" style="text-align: center;">
									<p>
										<img style="height: 70px; width: 65px; margin-top: 10%;" src="${resource(dir:'assets/images/users',file:'avatar-1.jpg')}" alt="user-img" class="thumb-lg img-circle img-thumbnail">
									</p>
									<p>
										${trips.sender_name}
									</p>
								</div>
								<div class="col-md-2">
									<p>DESIGNATION</p>
									<p>
										${trips.designation}
									</p>
								</div>
								<div class="col-md-1" style="word-break:break-all">
									<p>CAR TYPE</p>
									<p>
										${trips.carType}
									</p>
								</div>
								<div class="col-md-1">
									<p>DATE</p>
									<p>
										${trips.date}
									</p>
								</div>
								<div class="col-md-1">
									<p>TIME</p>
									<p>
										${trips.time}
									</p>
								</div>
								<div class="col-md-4">
									<p>LOCATION</p>
									<p style="float: left;">
										<img class="img-responsive" alt="" src="${resource(dir:'images',file:'from_to_icon.png')}" style="margin: 0 auto;">
									</p>
									<p>
										&nbsp;&nbsp;${trips.source_location}
									</p>
									<p>
										&nbsp;&nbsp;${trips.destination_location}
									</p>
								</div>
								<div class="col-md-2">
									<div style="margin-top: 10%;"></div>
									<p>
										<g:form method="post" action="performInvitationAction" controller="Notifications" role="form">
											<input type="hidden" name="invitationId" value="${trips.invitationId}">
											<input type="hidden" name="statusAction" value="1">
											<input type="hidden" name="alert_id" value="${trips.alert_id}"/>
											<button class="btn btn-success waves-effect waves-light" type="submit" style="width: 80%;" onclick="return confirm(' Do you want to accept?');">Accept</button>
										</g:form>
									</p>
									<p>
										<g:form method="post" action="performInvitationAction" controller="Notifications" role="form">
											<input type="hidden" name="invitationId" value="${trips.invitationId}">
											<input type="hidden" name="statusAction" value="0">
											<input type="hidden" name="alert_id" value="${trips.alert_id}"/>
											<button class="btn btn-inverse waves-effect waves-light" type="submit" style="width: 80%;" onclick="return confirm('Do you want to reject?');">Reject</button>
										</g:form>
									</p>
									
								</div>
							</div>
						</g:if>
					</td>
				</tr>
					</g:if>
					
					<g:if test="${trips.alertType.equals("RejectedByVendor") }">
					<tr>				
						<td>
						<div class="col-md-12" style="background-color: #f2f3f4;margin-bottom:2%;box-shadow:0 0 10px rgba(0, 0, 0, 0.2);border-left: 20px solid #0EBAB8;">
							<div class="col-md-1" style="text-align: center;">
								<p>
									<img style="height: 70px; width: 65px; margin-top: 10%;" src="${resource(dir:'assets/images/users',file:'avatar-1.jpg')}" alt="user-img" class="thumb-lg img-circle img-thumbnail">
								</p>
								<p>
									${trips.sender_name}
								</p>
							</div>
							<div class="col-md-9" style="text-align: left;margin-top:1%">
								
								<p>
									${trips.message}
								</p>
								<p>
									Trip Date: <b>${trips.tripDate}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Trip Time: <b>${trips.tripTime}</b>
								</p>
								<p>
									Start location: <b>${trips.startLocation}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; End location: <b>${trips.endLocation}</b>
								</p>
							</div>
							<div class="col-md-2" style="text-align: left">
								<g:if test="${trips.status=="Unread" }">
									<div style="margin-top: 10%;">
										<p>
											<g:form method="post" action="edit" controller="BookingConsole" role="form">
												<input type="hidden" name="alert_id" value="${trips.alert_id}"/>
												<input type="hidden" name="tripID" value="${trips.tripId }"/>
												<button class="btn btn-success waves-effect waves-light" type="submit" style="width: 80%;">Change Vendor</button>
											</g:form>
										</p>
										<p >
											<g:form method="post" action="cancelBookedTrip" controller="BookedTripHistory" role="form">
												<input type="hidden" name="tripID" value="${trips.tripId}">
												<button class="btn btn-inverse waves-effect waves-light" type="submit" style="width: 80%;" onclick="return confirm('Do you want to cancel?');">Cancel</button>
											</g:form>
										</p>
									</div>
								</g:if>
							</div>
						</div>
						</td>				
					</tr>
					</g:if>
					<g:if test="${trips.alertType.equals("RejectedReqest") }">
					<tr>				
						<td>	
						<div class="col-md-12" style="background-color: #f2f3f4;margin-bottom:2%;box-shadow:0 0 10px rgba(0, 0, 0, 0.2);border-left: 20px solid #0EBAB8;">
							<div class="col-md-1" style="text-align: center;">
								<p>
									<img style="height: 70px; width: 65px; margin-top: 10%;" src="${resource(dir:'assets/images/users',file:'avatar-1.jpg')}" alt="user-img" class="thumb-lg img-circle img-thumbnail">
								</p>
								<p>
									${trips.sender_name}
								</p>
							</div>
							<div class="col-md-9" style="text-align: left;margin-top:1%">
								<p>
									${trips.message}
								</p>
								<p>
									Trip Date: <b>${trips.tripDate}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Trip Time: <b>${trips.tripTime}</b>
								</p>
								<p>
									Start location: <b>${trips.startLocation}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; End location: <b>${trips.endLocation}</b>
								</p>
							</div>
							<div class="col-md-2" style="text-align: left">
								<g:if test="${trips.status=="Unread" }">
									<div style="margin-top: 10%">
										<p>
											<g:form method="post" action="findMatchingUser" controller="BookingConsole" role="form">
												<input type="hidden" name="alert_id" value="${trips.alert_id}"/>
												<input type="hidden" name="tripID" value="${trips.tripId }"/>
												<button class="btn btn-success waves-effect waves-light" type="submit" style="width: 80%;font-size:12px;">Find Matching User</button>
											</g:form>
										</p>
										<p>
											<g:form method="post" action="cancelBookedTrip" controller="BookedTripHistory" role="form">
												<input type="hidden" name="invitationId" value="${trips.invitationId}">
												<input type="hidden" name="tripID" value="${trips.tripId }"/>
												<input type="hidden" name="alert_id" value="${trips.alert_id}"/>
												<button class="btn btn-inverse waves-effect waves-light" type="submit" style="width: 80%;" onclick="return confirm('Do you want to cancel?');">Cancel</button>
											</g:form>
										</p>
									</div>
								</g:if>
							</div>
						</div>
						</td>
						</tr>
					</g:if>
					<g:if test="${trips.alertType.equals("RequestAccepted") }">
					<tr>				
						<td>
						<div class="col-md-12" style="background-color: #f2f3f4;margin-bottom:2%;box-shadow:0 0 10px rgba(0, 0, 0, 0.2);border-left: 20px solid #0EBAB8;">
							<div class="col-md-1" style="text-align: center;">
								<p>
									<img style="height: 70px; width: 65px; margin-top: 10%;" src="${resource(dir:'assets/images/users',file:'avatar-1.jpg')}" alt="user-img" class="thumb-lg img-circle img-thumbnail">
								</p>
								<p>
									${trips.sender_name}
								</p>
							</div>
							<div class="col-md-10" style="text-align: left;margin-top:1%">
								<p>
									${trips.message}
								</p>
								<p>
									Trip Date: <b>${trips.tripDate}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Trip Time: <b>${trips.tripTime}</b>
								</p>
								<p>
									Start location: <b>${trips.startLocation}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; End location: <b>${trips.endLocation}</b>
								</p>
							</div>
						</div>
						</td>
					</tr>
					</g:if>
					<g:if test="${trips.alertType.equals("AcceptedByVendor") }">
					<tr>				
					   <td>
						<div class="col-md-12" style="background-color: #f2f3f4;margin-bottom:2%;box-shadow:0 0 10px rgba(0, 0, 0, 0.2);border-left: 20px solid #0EBAB8;">
							<div class="col-md-1" style="text-align: center;">
								<p>
									<img style="height: 70px; width: 65px; margin-top: 10%;" src="${resource(dir:'assets/images/users',file:'avatar-1.jpg')}" alt="user-img" class="thumb-lg img-circle img-thumbnail">
								</p>
								<p>
									${trips.sender_name}
								</p>
							</div>
							<div class="col-md-10" style="text-align: left;margin-top:1%">
								
								<p>
									${trips.message}
								</p>
								<p>
									Trip Date: <b>${trips.tripDate}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Trip Time: <b>${trips.tripTime}</b>
								</p>
								<p>
									Start location: <b>${trips.startLocation}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; End location: <b>${trips.endLocation}</b>
								</p>
							</div>
						</div>
						</td>
						</tr>
					</g:if>
					<g:if test="${trips.alertType.equals("EventCreated") }">
					<tr>				
						<td>
						<div class="col-md-12" style="background-color: #f2f3f4;margin-bottom:2%;box-shadow:0 0 10px rgba(0, 0, 0, 0.2);border-left: 20px solid #0EBAB8;">
							<div class="col-md-1" style="text-align: center;">
								<p>
									<img style="height: 70px; width: 65px; margin-top: 10%;" src="${resource(dir:'assets/images/users',file:'avatar-1.jpg')}" alt="user-img" class="thumb-lg img-circle img-thumbnail">
								</p>
								<p>
									${trips.sender_name}
								</p>
							</div>
							<div class="col-md-10" style="text-align: left;margin-top:1%">
								
								<p>
									${trips.sender_name} ${trips.message}
								</p>
								<p>
									Event Name: <b>${trips.eventName}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Event Venue: <b>${trips.eventVenue}</b>
								</p>
								<p>
									Event Date: <b>${trips.eventDate}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Event Time: <b>${trips.eventTime}</b>
								</p>
								<p>
								<g:form method="post" action="bookThisEvent" controller="Events" role="form">
								<input type="hidden" name="eventName" value="${trips.eventName}">
								<input type="hidden" name="eventVenue" value="${trips.eventVenue}">
								<input type="hidden" name="eventDate" value="${trips.eventDate}">
								<input type="hidden" name="eventCode" value="${trips.eventCode}">
								<input type="hidden" name="costCenter" value="${trips.costCenter}">
								<button class="btn btn-success waves-effect waves-light" type="submit" style="margin-left: 93%;">Book trip for event</button>
								</g:form>
								</p>
							</div>
						</div>
						</td>
						</tr>
					</g:if>
				</g:each>
					
				</tbody>
		</table>			
			</g:if>
			<g:else>
				<script type="text/javascript">
					changeBackground('#f2f3f4');
				</script>
				<div class="col-md-12">
					<div style="text-align: center;">
						<div style="margin: 5%;"></div>
						<img class="img-responsive" alt="" src="${resource(dir:'images',file:'no_notofications.png')}" style="margin: 0 auto; height: 320px;">
						<div style="margin: 4%;"></div>
						<h3 class="text-muted">Oops</h3>
						<h4 class="text-muted">You don't have any notifications</h4>
						<div style="margin: 7%;"></div>
					</div>
				</div>
			</g:else>
		</div>
	</div>
	
<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>      
<div class="container">  
  <!-- Modal -->
  <div class="modal fade" id="myModalAccept" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width:370px; text-align:center;">
      
        <div class="modal-body">
          <p>Trip request accepted successfully.</p>
        </div>
        <div class="">
          <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
    // Show the Modal on load
 var d="${session["popUpDataAccept"] }";
  if(d=="show"){
    $("#myModalAccept").modal("show");
    }
  <%
  session['popUpDataAccept']="";
  %>
});
</script>

<div class="container">  
  <!-- Modal -->
  <div class="modal fade" id="myModalReject" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width:370px; text-align:center;">
      
        <div class="modal-body">
          <p>Trip request rejected successfully.</p>
        </div>
        <div class="">
          <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
    // Show the Modal on load
 var d="${session["popUpDataReject"] }";
  if(d=="show"){
    $("#myModalReject").modal("show");
    }
  <%
  session['popUpDataReject']="";
  %>
});
</script>  
<script type="text/javascript">
$(document).ready(function(){
<%
session['sidebarActive']="";
session['sidebarActive']="notifications/index";
%>
});
</script>
</body>
</html>