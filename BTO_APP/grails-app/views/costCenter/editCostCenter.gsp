<meta name="layout" content="main" />
<div class="row">
	<div class="col-sm-12">
		<div class="">
			<h4 class="m-t-0 header-title">
				<b>Update Cost Center</b>
			</h4>
			<div class="col-md-2"></div>
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:if test="${flash.message1}">
				<div class="message1" role="status">
					${flash.message1}
				</div>
			</g:if>

			<g:form action="updateCostCenter" method="post">
				<div class="col-md-8">
					<div>
						<g:hiddenField name="id" value="${costCenterInstance?.id}" />
						<div class="form-group clearfix">
							<label class="col-lg-4 control-label" for="name2">Cost Center Code*</label>
							<div class="col-lg-8">
								<input id="name2" required="required" name="costCenterName" value="${costCenterInstance?."costCenterName"}" type="text" class=" form-control">
							</div>
						</div>
						<div class="col-lg-3" style="float: right;">
								<button class="btn btn-success waves-effect waves-light"
									type="submit">Update</button>
									&nbsp;<g:link action="index" class="btn btn-inverse waves-effect waves-light">Cancel</g:link>
							</div>
						</div>


					</div>
				</g:form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</div>
<!-- End row -->