<meta name="layout" content="main"/>
 <link href="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css')}" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function changeBackground(color) {
	   document.body.style.background = color;
	}
</script>
<div class="row" style="margin-top: 5%;">
	<g:if test="${flash.message}">
		<div class="message" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.message1}">
		<div class="message1" role="status">
			${flash.message1}
		</div>
	</g:if>
	<g:if test="${tripDetailsInstanceList}">
	<script type="text/javascript">
			changeBackground('#FFFFFF');
	</script>
	<h4 class="m-t-0 header-title">
		<b>All Trips History</b>
	</h4>
	<div class="col-md-0"></div>
<div class="col-md-12">

	<table id="datatable" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Sr. No.</th>
				<th>Vendor Name</th>
				<th>Car Type</th>
				<th>Trip Time</th>
				<th>Start Location</th>
				<th>End Location</th>
			</tr>
			</thead>
			<tbody>			
				
				<g:each var="trips" in="${tripDetailsInstanceList}" status="i">
				<tr>
				<td>${i+1}</td>
				<td>${trips.vendor.companyName}</td>
				<td>${trips.carType}</td>
				<td>${trips.tripTime}</td>
				<td>${trips.startLocation}</td>
				<td>${trips.endLocation}</td>				
				</tr>
				</g:each>
				
		</tbody>
	</table>
	
</div>
</g:if>
	<g:else>
	<script type="text/javascript">
			changeBackground('#f2f3f4');
			</script>
		<div class="row" style="margin-left: 10%;">
					<div class="col-md-12" style="width:80%; text-align: center; ">
					<img class="img-responsive" alt="" src="${resource(dir:'images',file:'no_history.png')}" style="margin: 0 auto; height: 320px;">
					<div style="margin: 4%;"></div>
					<h3 class="text-muted">No previous trip history</h3>
					<h4 class="text-muted">Let's plan a trip</h4>
					<g:link controller="BookingConsole" action="index" class="btn btn-success waves-effect waves-light"> Create One</g:link>
					</div>
		</div>
	</g:else>
</div>
<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js')}"></script>
<script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').dataTable();
            });
</script>
<script type="text/javascript">
$(document).ready(function(){
<%
session['sidebarActive']="";
session['sidebarActive']="previousTripDetails/index";
%>
});
</script>