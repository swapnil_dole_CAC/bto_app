<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<meta name="layout" content="main"/>
<title>404 Error</title>
</head>
<body>
  <div class="body">
  <h2>Oops something went wrong <br><g:link controller="User" action="index"><i class="ti-power-off m-r-5"></i> Click here to go Home</g:link></h2>
  </div>
</body>
</html>