<meta name="layout" content="main"/>
<%@page import="com.bto.customer.VendorDetails"%>
<%@page import="com.bto.customer.BillingPlan"%>
<script type="text/javascript">
	$(".package2").hide()
	function OpenDiv(id)
	{
		if(id==1)
		{
			$(".package2").hide()
		}
		else if(id==2)
		{
			$(".package2").show()
		}
	}
</script>
<div class="col-md-2"></div>
<div class="col-md-8">
	<g:if test="${flash.message}">
		<div class="message" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.message1}">
		<div class="message1" role="status">
			${flash.message1}
		</div>
	</g:if>
	<div class="card-box" style="height:390px">
		<g:form action="setPlan" method="post">
			<div class="col-md-12">
				<table class="table">
					<thead>
						<tr>
							<td colspan="2" class="tableborder" style="text-align: center;border:0;"><h4>Add Package</h4></td>
						</tr>
					</thead>
					<tbody>					
						<tr>
							<td width="20%" style="border:0">Vendor</td>
							<td style="border:0">
								<select name="vendor" class="form-control" required>
									<option value="">Select Vendor</option>
									<g:each in="${VendorDetails.list()}" status="i" var="vendorInstance">
										<option value="${vendorInstance.id }">${vendorInstance.companyName }</option>
									</g:each>
								</select>
							</td>
							<td style="border:0">
								<button name="submit" value="Get Packages" class="btn btn-success waves-effect waves-light">Get Packages</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</g:form>
		<div class="col-md-12">
			<ul class="nav nav-tabs tabs">
             <li class="active tab">
                 <a href="#flatRate" data-toggle="tab" aria-expanded="false"> 
                     <span class="visible-xs"><i class="fa fa-home"></i></span> 
                     <span class="hidden-xs">Flat Rate Plans</span> 
                 </a> 
             </li> 
             <li class="tab"> 
                 <a href="#customRate" data-toggle="tab" aria-expanded="false"> 
                     <span class="visible-xs"><i class="fa fa-user"></i></span> 
                     <span class="hidden-xs">Custom Rate Plans</span> 
                 </a> 
             </li>             
         </ul> 
			<div class="tab-content"> 
				<g:form action="savePlan" method="post">
					<g:hiddenField name="packageType" value="1"/>
					<g:hiddenField name="vendor" value="${session["vendor"] }"/>
		        	<div class="tab-pane active" id="flatRate">
		        		<table class="table">
							<thead>
								<tr>
									<th style="text-align:center">Sr. No.</th>
									<th style="text-align:center">Amount</th>
									<th style="text-align:center">Kilometer</th>
								</tr>
							</thead>
							<tbody>
								<g:set var="j" value="${1 }"/>
								<g:each in="${packagesInstance}" status="i" var="billInstance">
									<g:if test="${billInstance.packageType==1 }">
										<tr>
											<td align="center" style="border: 0">
												${j++}
											</td>
											<td align="center" style="border: 0">
												${billInstance.amount }
											</td>
											<td align="center" style="border: 0">
												${billInstance.kilometer }
											</td>
											<td>
												<g:radio name="flatRate" value="${billInstance.id }"/>
											</td>
										</tr>
									</g:if>
								</g:each>
										<tr>
											<td colspan="4" align="center">
												<button class="btn btn-success waves-effect waves-light">Save</button> 
											</td>
										</tr>       					
							</tbody>
		            	</table>
		            </div>
		        </g:form>
		        <g:form action="savePlan" method="post">
					<g:hiddenField name="packageType" value="2"/>
					<g:hiddenField name="vendor" value="${session["vendor"] }"/>
			            <div class="tab-pane" id="customRate">
							<table class="table">
								<thead>
									<tr>
										<th style="text-align: center">Sr. No.</th>
										<th style="text-align: center">Amount</th>
										<th style="text-align: center">Kilometer</th>
										<th style="text-align: center">Hour</th>
									</tr>
								</thead>
								<tbody>
									<g:set var="j" value="${1 }" />
									<g:each in="${packagesInstance}" status="k" var="billInstance1">
										<g:if test="${billInstance1.packageType==2 }">
											<tr>
												<td align="center" style="border: 0">
													${j++}
												</td>
												<td align="center" style="border: 0">
													${billInstance1.amount }
												</td>
												<td align="center" style="border: 0">
													${billInstance1.kilometer }
												</td>
												<td align="center" style="border: 0">
													${billInstance1.hour}
												</td>
												<td>
													<g:checkBox name="customRatePlan" value="${billInstance1.id }" checked="false"/>
												</td>
											</tr>
										</g:if>
									</g:each>
											<tr>
												<td colspan="5" align="center">
													<button class="btn btn-success waves-effect waves-light">Save</button> 
												</td>
											</tr>
								</tbody>
							</table>
						</div>
					</g:form>
	        </div>
		</div>
	</div>
</div>
<div class="col-md-2"></div>