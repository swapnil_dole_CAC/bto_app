<meta name="layout" content="main"/>
 <link href="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css')}" rel="stylesheet" type="text/css" />
<div class="col-md-12" style="text-align:right">

</div>
<div class="col-md-12" style="text-align:right">
	<g:link controller="BillingPlan" action="setPlan" class="btn btn-success waves-effect waves-light" style="float:right;">Set Package</g:link>
<br>
</div>
<div class="row" style="margin-top: 5%;">
	<g:if test="${flash.message}">
		<div class="message" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.message1}">
		<div class="message1" role="status">
			${flash.message1}
		</div>
	</g:if>
	
	<div class="col-md-1"></div>
	<div class="col-md-10">
		<div class="col-md-12" style="text-align:center">
			<h4 class="m-t-0 header-title">
				<b>Selected Package</b>
			</h4>
		</div>
		<table id="datatable" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Sr. No.</th>
					<th>Vendor Name</th>
					<th>Package Type</th>
					<th>Amount</th>
					<th>Kilometer</th>	
					<th>Hour</th>	
				</tr>
			</thead>
			<tbody>
				<g:set var="j" value="${1 }"/>
				<g:each in="${billingPlanInstanceList}" status="i" var="billInstance">
					<tr>
						<td align="center" style="border:0">${j++}</td>
						<td align="center" style="border:0">${billInstance.vendor.companyName}</td>
						<td>
							<g:if test="${billInstance.packageType==1 }">
								Flat Rate
							</g:if>
							<g:else>
								Other Package
							</g:else>
						</td>
						<td>${billInstance.amount }</td>
						<td>${billInstance.kilometer }</td>
						<td>${billInstance.hour }</td>	
					</tr>
				</g:each>			
			</tbody>
		</table>
	</div>
</div>
<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js')}"></script>
<script type="text/javascript">
	$(document).ready(function() {
    	$('#datatable').dataTable();
	});
</script>