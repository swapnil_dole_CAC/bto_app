<meta name="layout" content="main"/>
<div class="main-content">
	<div class="page-title">
    	<div class="title">Edit Car type</div>
    </div>
	<div class="col-md-2"></div>
	<div class="col-md-5 shadow">
		<div class="card-box">
			<g:form action="update">
				<g:hiddenField name="id" value="${carTypeInstance?.id}" />
				<table class="table">
						<tbody>
							<tr>
								<td width="25%" style="border:0">Car Type</td>
								<td style="border:0"><input type="text" class="form-control" name="type_name" id="type_name" value="${carTypeInstance?.type}" /></td>
							</tr>
							<tr>
								<td width="50%" style="border:0">Passenger capacity</td>
								<td style="border:0"><input type="number" class="form-control" name="capacity" size="5" required="required" value="${carTypeInstance?.capacity}"/></td>
							</tr>
							<tr>
								<td width="30%" style="border:0">Petrol Average</td>
								<td style="border:0"><input type="text" class="form-control" name="petrol"  pattern = "^\d+(\.\d+)*$" size="5" id="petrol" value="${carTypeInstance?.petrolMileage}" required=""/></td>
							</tr>
							<tr>
								<td width="30%" style="border:0">Diesel Average</td>
								<td style="border:0"><input type="text" class="form-control" name="diesel"  pattern = "^\d+(\.\d+)*$" size="5" id="diesel" value="${carTypeInstance?.dieselMileage}" required=""/></td>
							</tr>
							<tr>
								<td width="20%" style="border:0">CNG Average</td>
								<td style="border:0"><input type="text" name="cng"  class="form-control" pattern = "^\d+(\.\d+)*$" size="5" id="cng" value="${carTypeInstance?.cngMileage}" required=""/></td>
							</tr>
							<tr>
								<td colspan="2" style="border:0" align="center"><input type="submit" value="Update" class="btn btn-success shadow submit"/>
								&nbsp;<g:link action="index" class="btn btn-inverse waves-effect waves-light">Cancel</g:link>
								</td>
							</tr>
						</tbody>
				</table>
			</g:form>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>