<meta name="layout" content="main"/>
<div class="col-md-4"></div>
<div class="col-md-4">
	<g:if test="${flash.message}">
		<div class="message" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.message1}">
		<div class="message1" role="status">
			${flash.message1}
		</div>
	</g:if>
	<div class="card-box">
		<g:form action="saveCarType">
			<table class="table">
				<thead>
					<tr>
						<td colspan="2" class="tableborder" style="text-align: center;border:0;"><h4>Add Type</h4></td>
					</tr>
				</thead>
				<tbody>
					
					<tr>
						<td width="50%" style="border:0">Car Type</td>
						<td style="border:0"><input type="text" class="form-control" name="type_name" id="type_name" required="" oninvalid="this.setCustomValidity('Enter car type')"
											onchange="this.setCustomValidity('')"/></td>
					</tr>
					<tr>
						<td width="50%" style="border:0">Passenger capacity</td>
						<td style="border:0"><input type="number" class="form-control" name="capacity" size="5" value="4" required="" oninvalid="this.setCustomValidity('Enter passenger capacity')"
											onchange="this.setCustomValidity('')"/></td>
					</tr>
					<tr>
						<td width="50%" style="border:0">Petrol Average</td>
						<td style="border:0"><input type="text" class="form-control" name="petrol" size="5" id="petrol" value="17" pattern = "^\d+(\.\d+)*$" required="" oninvalid="this.setCustomValidity('Enter petrol average')"
											onchange="this.setCustomValidity('')"/></td>
					</tr>
					<tr>
						<td width="50%" style="border:0">Diesel Average</td>
						<td style="border:0"><input type="text" class="form-control" name="diesel" size="5" id="diesel" value="15" pattern = "^\d+(\.\d+)*$" required="" oninvalid="this.setCustomValidity('Enter deasel average')"
											onchange="this.setCustomValidity('')"/></td>
					</tr>
					<tr>
						<td width="50%" style="border:0">CNG Average</td>
						<td style="border:0"><input type="text" class="form-control" name="cng" size="5" id="cng" value="18" pattern = "^\d+(\.\d+)*$" required="" oninvalid="this.setCustomValidity('Enter CNG average')"
											onchange="this.setCustomValidity('')"/></td>
					</tr>
					<tr>
						<td colspan="2" style="border:0" align="center">
							<button class="btn btn-success waves-effect waves-light" type="submit">Submit</button>	
							&nbsp;<g:link action="index" class="btn btn-inverse waves-effect waves-light">Cancel</g:link>
						</td>
					</tr>
				</tbody>
			</table>
		</g:form>
	</div>
</div>
<div class="col-md-2"></div>