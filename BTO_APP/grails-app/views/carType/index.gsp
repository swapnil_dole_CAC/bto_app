<meta name="layout" content="main"/>
<h4 class="m-t-0 header-title">
		<b>Car Type</b>
	</h4>
<div class="col-md-2"></div>
<div class="col-md-8">
	<g:if test="${flash.message}">
		<div class="message" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.message1}">
		<div class="message1" role="status">
			${flash.message1}
		</div>
	</g:if>
	
	<div class="col-md-12" style="text-align:right">
		<g:link controller="CarType" action="add_type">
			<button class="btn btn-success waves-effect waves-light" type="button">Add Type</button>
		</g:link>
	</div>
	<table style="max-height: 600px" class="table table-striped table-bordered" id="datatable">
		<thead>	
			<tr>
				<td rowspan="2" class="tableborder" align="center">Id</td>
				<td rowspan="2" class="tableborder" align="center">Capacity</td>
				<td rowspan="2" class="tableborder" align="center">Car Type</td>
				<td colspan="3" align="center" class="tableborder">Mileage</td>
				<td rowspan="2" align="center" width="5%" class="tableborder">Edit</td>
				<td rowspan="2" align="center" width="5%" class="tableborder">Delete</td>
			</tr>
			<tr>
				<td align="center" class="tableborder">Petrol</td>
				<td align="center" class="tableborder">Diesel</td>
				<td align="center" class="tableborder">CNG</td>				
			</tr>
		</thead>
		<tbody>
			<g:set var="j" value="${1 }"/>
			<g:each in="${carTypeInstanceList}" status="i" var="carTypeInstance">
				<tr>
					<td align="center" style="border:0">${j++}</td>
					<td align="center" style="border:0">${carTypeInstance?.capacity}</td>
					<td align="center" style="border:0">${carTypeInstance?.type}</td>
					<td align="center" style="border:0">${carTypeInstance?.petrolMileage}</td>
					<td align="center" style="border:0">${carTypeInstance?.dieselMileage}</td>
					<td align="center" style="border:0">${carTypeInstance?.cngMileage}</td>
					<td style="border:0" align="center"><g:link action="edit" id="${carTypeInstance?.id}" title="Edit"><i class="md  md-edit"></i></g:link></td>				
					<td style="border:0" align="center"><g:link action="deleteCarType" id="${carTypeInstance?.id}" title="Delete" onclick="return confirm(' You want to delete?');" ><i class="md  md-delete"></i></g:link></td>
				</tr>
			</g:each>
		</tbody>
	</table>
</div>
<div class="col-md-2"></div>