<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<%--<meta name="layout" content="main" />--%>
<title>Reset Password | BTO </title>
<link rel="stylesheet"	href="${resource(dir:'assets/css',file:'bootstrap.min.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/css',file:'components.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/css',file:'core.css') }">
<link rel="stylesheet"	href="${resource(dir:'assets/css',file:'icons.css') }">
<%--<link rel="stylesheet" href="${resource(dir:'assets/css',file:'pages.css') }">
		--%>
<link rel="stylesheet"	href="${resource(dir:'assets/css',file:'responsive.css') }">
<style>
html, body {
	background-image: url("${resource(dir:'images',file:'verifiction-bg.png')}");
	background-repeat: no-repeat;
	background-size: cover;
}

.inputTranspernt {
	background: transparent;
}

.card-box {
	background: rgba(255, 255, 255, 0.75) none repeat scroll 0 0;
	margin-top: 8%;
	padding: 0;
}

.message1 {
	color: green;
}

.message {
	color: red;
}
</style>
<script type="text/javascript">
$(document).ready(function() {

	  if(window.location.href.indexOf('#myModalNorm') != -1) {
	    $('#myModalNorm').modal('show');
	  }

	});
</script>
<script>
    function validatePassword() {
        var ok=false;
        var newPassword = document.frmChange.newPassword.value;
        var confirmPassword = document.frmChange.confirmPassword.value;
        if (newPassword != confirmPassword) {
            ok=false
            //alert("Passwords Do not match");
            document.getElementById("newPassword").style.borderColor = "#E34234";
            document.getElementById("confirmPassword").style.borderColor = "#E34234";
            document.getElementById("confirmPassword").innerHTML = "<span style=\"color:red\">New and confirm password does not match</span>";
        	
        }
        else {
            ok=true
          //  alert("Passwords Match!!!");
        }

        return ok;
    }
</script>

</head>
<body>
	<%--
	background="${resource(dir:'images',file:'login-bk.jpg')}"
	style="background-image: url(${resource(dir:'images',file:'login-bk.jpg')});"
	--%>
	<%--<div class="account-pages"></div>
	<div class="clearfix"></div>		
	--%>
	<div class="wrapper-page">
		<%--<h1 class="" style="color: white;margin-left: 20%;">Welcome to BTO</h1>
		<h1>Welcome to BTO</h1>
		--%>
		<h1></h1>
		<div class="col-md-1"></div>
		<div class=" card-box col-md-4">
			<div class="panel-heading">
				<h3 class="">Reset Password</h3>
				
				<g:if test="${flash.message}">
					<div class="message" role="status">
						${flash.message}
					</div>
				</g:if>
				<g:if test="${flash.message1}">
					<div class="message1" role="status">
						${flash.message1}
					</div>
				</g:if>
				<div class="panel-body">
					<g:form name="frmChange" class="form-horizontal m-t-30" controller="Employer"  action="setPassword" method="post">
						<input type="hidden" name="emailID" value="${emailID}">
						<div class="form-group">
							<div class="col-md-12">
								<label>New Password*</label> <input class="form-control" name="newPassword"
								oninvalid="this.setCustomValidity('Enter new password')"
												onchange="this.setCustomValidity('')"
									type="password" required="" placeholder="Enter new password">
									<span id="newPassword"  class="required"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<label>Confirm Password*</label> <input class="form-control" name="confirmPassword"
								oninvalid="this.setCustomValidity('Confirm new password')"
								onchange="this.setCustomValidity('')"	type="password" required="" placeholder="Confirm new password">
									<span id="confirmPassword"  class="required"></span>
							</div>
						</div>
						
						
						<div class="form-group text-center m-t-30">
							<div class="col-md-12">
								<button class="btn btn-success  waves-effect waves-light" onclick="return validatePassword();"
									type="submit" style="float: right;">Reset password</button>
							</div>
						</div>
					</g:form>
					<%--<div class="col-md-12 text-center">
						<p>Don't have an account yet?</p>
						<h4>
							<g:link class="text-primary m-l-5" action="register">REGISTER</g:link>
						</h4>
					</div>
				--%>
				</div>

			</div>
		</div>
		<div class="col-md-7"></div>
	</div>
	<!-- Modal-Effect -->
	<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>
	<script src="${resource(dir:'assets/js',file:'bootstrap.min.js')}"></script>
	<script
		src="${resource(dir:'assets/plugins/custombox/dist',file:'custombox.min.js')}"></script>
	<script
		src="${resource(dir:'assets/plugins/custombox/dist',file:'legacy.min.js')}"></script>
</body>
</html>