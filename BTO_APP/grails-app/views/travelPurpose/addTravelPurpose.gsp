<meta name="layout" content="main" />
<script>
function isCharacterKeyDot(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // alert("Char"+charCode);
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 46 || charCode == 40 || charCode == 41 || charCode == 32 || charCode == 47 || charCode == 45 || charCode == 8 || charCode == 13){
    	document.getElementById("tpurpose").innerHTML = "<span></span>";    	
        return true;
    }else{
    	document.getElementById("tpurpose").innerHTML = "<span style='color:red;'>Only a-z A-Z digits allowed...!</span>";
        return false;
    }
}

</script>
<div class="row">
	<div class="col-sm-12">
		<div class="">
			<h4 class="m-t-0 header-title">
				<b>New Travel Purpose</b>
			</h4>
			<div class="col-md-2"></div>
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:if test="${flash.message1}">
				<div class="message1" role="status">
					${flash.message1}
				</div>
			</g:if>
			<div class="col-md-8">
				<g:form controller="TravelPurpose" action="saveTravelPurpose" method="post">
					<div>

						
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label" for="name2">Travel purpose *</label>
							<div class="col-lg-9">
								<input id="name2" name="travelPurpose" type="text" class="form-control" required="required" oninvalid="this.setCustomValidity('Enter travel purpose')"
											onchange="this.setCustomValidity('')" onkeypress="return isCharacterKeyDot(event);">
											<div id="tpurpose" style="color:red;"></div>
							</div>
						</div>
						
						<div class="form-group clearfix">
							<div class="col-lg-3" style="float: right;">
								<button class="btn btn-success waves-effect waves-light"
									type="submit">Submit</button>
								&nbsp;
								<g:link action="index"
									class="btn btn-inverse waves-effect waves-light">Cancel</g:link>
							</div>
						</div>


					</div>
				</g:form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</div>
<!-- End row -->