<meta name="layout" content="main" />
<div class="row">
	<div class="col-sm-12">
		<div class="">
			<h4 class="m-t-0 header-title">
				<b>Update Travel Purpose</b>
			</h4>
			<div class="col-md-2"></div>
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:if test="${flash.message1}">
				<div class="message1" role="status">
					${flash.message1}
				</div>
			</g:if>
			<div class="col-md-8">
				<g:form action="updateTravelPurpose" controller="TravelPurpose" method="post">
					<div>
						<g:hiddenField name="id" value="${travelPurposeInstance?.id}" />
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label" for="name2">Travel purpose *</label>
							<div class="col-lg-9">
								<input id="name2" name="travelPurpose" type="text" class="form-control" required="required" oninvalid="this.setCustomValidity('Enter travel purpose')"
											onchange="this.setCustomValidity('')" onkeypress="return isCharacterKeyDot(event);" value="${travelPurposeInstance?.travelPurpose}">
											<div id="tpurpose" style="color:red;"></div>
							</div>
						</div>
						<div class="form-group clearfix">
							<div class="col-lg-3" style="float: right;">
								<button class="btn btn-success waves-effect waves-light" type="submit">Update</button>
									&nbsp;<g:link controller="AddVendor" action="index" class="btn btn-inverse waves-effect waves-light">Cancel</g:link>
							</div>
						</div>


					</div>
				</g:form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</div>
<!-- End row -->