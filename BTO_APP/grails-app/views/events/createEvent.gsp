<%@page import="com.bto.customer.EventType"%>
<%@page import="com.bto.customer.CostCenter"%>
<%@page import="java.lang.String"%>
<%@page import="com.bto.customer.VendorDetails"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="layout" content="main" />
<title>Create Event | BTO</title>
<link rel="stylesheet"
	href="${resource(dir:'assets/plugins/timepicker',file:'bootstrap-timepicker.min.css') }">
<link rel="stylesheet"
	href="${resource(dir:'assets/plugins/mjolnic-bootstrap-colorpicker/dist/css',file:'bootstrap-colorpicker.min.css') }">
<link rel="stylesheet"
	href="${resource(dir:'assets/plugins/bootstrap-datepicker/dist/css',file:'bootstrap-datepicker.min.css') }">
<link rel="stylesheet"
	href="${resource(dir:'assets/plugins/clockpicker/dist',file:'jquery-clockpicker.min.css') }">
<link rel="stylesheet"
	href="${resource(dir:'assets/plugins/bootstrap-daterangepicker',file:'daterangepicker.css') }">

<%--code for getting locations from google --%>
<%--<script
	src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
--%>
<script	src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyAl-G44Mw9pJUOwTHmKluyzF7n_hob0D_A&region=IN"></script>
<script>
	function initialize() {

		var input = document.getElementById('searchTextField');
		var autocomplete = new google.maps.places.Autocomplete(input);
		
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<%--code for getting locations from google END HERE --%>

<style type="text/css">
html {
	background-color: transparent;
}

body {
	background-color: white;
}

.card-box { //
	box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0
		rgba(0, 0, 0, 0.19);
	box-shadow: 1px 1px 5px #aeaeae;
	background-color: transparent;
}
</style>
</head>
<body>
	<div class="body">
		
					<div class="col-md-0"></div>
					<div class="col-md-3"></div>
					<div class="">
						<div class="col-md-6">
							<div class="card-box">
								<h4 class="m-t-0 header-title">
									<b>Create Event</b>
								</h4>
								<g:if test="${flash.message}">
									<div class="message" role="status">
										${flash.message}
									</div>
								</g:if>
								<g:if test="${flash.message1}">
									<div class="message1" role="status">
										${flash.message1}
									</div>
								</g:if>
								
								<g:form controller="Events" action="addEvent" method="post">
								<input type="hidden" name="userID" value="${session.userID}"/>
								<div class="form-group col-md-12">
										<label>Event Name</label>
										<p><input type="text" name="eventName"	oninvalid="this.setCustomValidity('Please enter event name')"
									onchange="this.setCustomValidity('')" parsley-trigger="change"	placeholder="Please enter event name" class="form-control" required></p>
								</div>
								<div class="col-md-12">
										
										<div class="form-group col-md-6">
											<label>Event Type</label>								
											<select name="eventType"	class=" form-control" data-style="btn-white" required="required" oninvalid="this.setCustomValidity('Select event type')"
												onchange="this.setCustomValidity('');">
												<option class="text-muted" value="" disabled selected>Select event type</option>
												<g:each var="type" in="${EventType.list()}">
													<option value="${type.eventType}">${type.eventType}</option>
												</g:each>
											</select>
										</div>
										<div class="form-group col-md-6">
											<label>Venue</label><p> <input type="text" parsley-trigger="change"
												name="eventVenue"
												oninvalid="this.setCustomValidity('Please enter event Venue')"
												onchange="this.setCustomValidity('')" id="searchTextField" onblur="getDirections();" onchange="getDirections();"
												required placeholder="Please enter event Venue"
												class="form-control"></p>
										</div>
									</div>
									
									<div class="col-md-12">
										<div class="form-group col-md-6">
											<label>Date</label>
											<div class="input-group">
												<input type="text" class="form-control" required="required"
													data-date-format='dd/mm/yyyy' name="eventDate"
													placeholder="dd/mm/yyyy" id="datepicker-autoclose">
												<span class="input-group-addon bg-custom b-0 text-white"><i
													class="icon-calender"></i></span>
											</div>

										</div>
										<div class="form-group col-md-6">
											<label>Time</label>
											<div class="input-group m-b-15">

												<div class="bootstrap-timepicker">
													<input id="timepicker" type="text" name="eventTime"
														class="form-control" required="required" >
												</div>
												<span class="input-group-addon bg-custom b-0 text-white"><i
													class="glyphicon glyphicon-time"></i></span>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group col-md-6">
											<label>Event Code</label>
								<p> <input type="text" name="eventCode"	oninvalid="this.setCustomValidity('Please enter event code')"
									onchange="this.setCustomValidity('')" parsley-trigger="change"	placeholder="Please enter event code" class="form-control" required=""></p>
													
										</div>
										<div class="form-group col-md-6">
										<label>Cost Center</label>
										<select name="costCenter" class=" form-control"
										data-style="btn-white" required="required" oninvalid="this.setCustomValidity('Select cost center')"
												onchange="this.setCustomValidity('');">
										<option class="text-muted" value="" disabled selected>Select
											Cost Center Code</option>
										<option value="NONE">NONE</option>
										<g:each var="costCenter" in="${CostCenter.list()}">
											<option value="${costCenter.costCenterName}">
												${costCenter.costCenterName}
											</option>
										</g:each>
									</select>
										</div>
									</div>
									<div class="form-group col-md-12">
										<label>Special Note / Event Description</label>
										<p><input type="text" name="description" oninvalid="this.setCustomValidity('Please enter event description')"
									onchange="this.setCustomValidity('')" parsley-trigger="change"	placeholder="Please enter event description" class="form-control"></p>
								</div>

									<div class="form-group text-right m-b-0">
										<button class="btn btn-success waves-effect waves-light"
											type="submit">Submit</button>
										<button type="reset"
											class="btn btn-inverse waves-effect waves-light m-l-5">
											Cancel</button>
									</div>									
								</g:form>
							</div>
						</div>

					</div>
					<div class="col-md-0"></div>
					<%--<div class="col-md-5">
						<div class="col-md-12">
						<div class="card-box" id="map" style="width: 490px; height: 545px;"></div>
						</div>
					</div>
					--%>
					<div class="col-md-3"></div>
				</div>
			
	<%--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> --%>
	<script type="text/javascript">

// this will load map with default locations
	window.onload = function() {
		getDirections();
		};

	//get directions using ggogle api
		function getDirections(){
		
		var a= document.getElementById("searchTextField").value;
		var origin='Pune India'
		var	destination='Pune India';
		if(!a==''){
			origin=a
			}
		if( !a ==''){
			destination=a				
			}
		//alert(origin)
		//alert(destination)
		var directionsService = new google.maps.DirectionsService();
		var directionsDisplay = new google.maps.DirectionsRenderer();
		
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom : 3,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		});

		directionsDisplay.setMap(map);

		var request = {
			origin : origin,
			destination : destination,
			travelMode : google.maps.DirectionsTravelMode.DRIVING
		};

		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
			}
		});
	}
	</script>



</body>
</html>