<%@page import="java.lang.System"%>
<%@page import="com.bto.trip.EventDetails"%>
<meta name="layout" content="main"/>
<link href="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css')}" rel="stylesheet" type="text/css" />
<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>
<style>
	table, th, td {
	    background-color: #ffffff;
	    border: none;
	}
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td, .table > thead > tr > th, .table-bordered {
	    border-top: medium none;
	}
	.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
	    border-top: medium none;
	    line-height: 1.42857;
	    padding: 0;
	    vertical-align: top;
	}
</style>
<script type="text/javascript">
$(document).ready(function() {
    $('table.display').DataTable();
} );
</script>
<script type="text/javascript">
function changeBackground(color) {
	   document.body.style.background = color;
	}
</script>
<g:if test="${eventDetailsInstanceList!=null}">
<div class="col-md-12" style="text-align:right">
	<g:link controller="Events" action="createEvent" class="btn btn-success waves-effect waves-light" style="float:right;"> Add Event</g:link>
<br>
</div>
</g:if>
<div class="row" style="">
	<g:if test="${flash.message}">
		<div class="message" role="status">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.message1}">
		<div class="message1" role="status">
			${flash.message1}
		</div>
	</g:if>	
	<g:if test="${eventDetailsInstanceList}">
	<script type="text/javascript">
			changeBackground('#FFFFFF');
	</script>
	
	<div style="margin: 4%;"></div>
	<div class="col-lg-12">
	<table id="" class="table display">
	<thead>
	<tr>				
		<th style="display: none;"></th>				
	</tr>
	</thead>
	<tbody>	
	 <g:each var="event" in="${eventDetailsInstanceList}" status="i">
	  <tr>				
		<td>
	 		<div class="shadow">
	 		<div class="row">
             <div class="col-md-12" style="background-color: #f2f3f4;">            
             <div class="col-md-2">
             <p>EVENT NAME</p>
             <p><b>${event.eventName}</b></p>
             </div>
             <div class="col-md-2">
             <p>TYPE</p>
             <p><b>${event.eventType}</b></p>
             </div>
             <div class="col-md-2">
             <p>DATE</p>
             <p><b>${event.eventDate}</b></p>
             </div>
             <div class="col-md-2">
             <p>TIME</p>
             <p><b>${event.eventTime}</b></p>
              
             </div>
              <div class="col-md-4"> 
             <p>VENUE</p>
              <p><b>${event.eventVenue}</b></p>
             </div>
             </div>
             </div>
             <div class="row">
             <div class="col-md-12" style="background-color: #f2f3f4;">
            
             <div class="col-md-2">
             <p>EVENT CODE</p>
             <p><b>${event.eventCode}</b></p>
             </div>
             <div class="col-md-2">
             <p>COST CENTER</p>
             <p><b>${event.costCenter}</b></p>
             </div>
             <%
			 def id=event.user.id;
			  %>
             <g:if test="${id.toString()==session['userID'].toString()}">
				<div class="col-md-4"></div>
             <div class="col-md-4">
             <div style="margin: 2%;"></div>
            	<g:form method="post" action="bookThisEvent" controller="Events" role="form">
				<input type="hidden" name="eventName" value="${event.eventName}">
				<input type="hidden" name="eventVenue" value="${event.eventVenue}">
				<input type="hidden" name="eventDate" value="${event.eventDate}">
				<input type="hidden" name="eventCode" value="${event.eventCode}">
				<input type="hidden" name="costCenter" value="${event.costCenter}">
				<button class="btn btn-success waves-effect waves-light" type="submit" style="float: left; width: 45%;">Book trip for event</button>
				</g:form>
				<g:form method="post" action="cancelEvent" controller="Events" role="form">
				<input type="hidden" name="eventId" value="${event.id}">
				<button class="btn btn-inverse waves-effect waves-light" type="submit" style="float: right;width: 45%;">Cancel</button>
            	</g:form>
             </div>
			 </g:if>
             <g:else>
             <div class="col-md-4"></div>
             <div class="col-md-4">
             <div style="margin: 2%;"></div>
            	<g:form method="post" action="bookThisEvent" controller="Events" role="form">
				<input type="hidden" name="eventName" value="${event.eventName}">
				<input type="hidden" name="eventVenue" value="${event.eventVenue}">
				<input type="hidden" name="eventDate" value="${event.eventDate}">
				<input type="hidden" name="eventCode" value="${event.eventCode}">
				<input type="hidden" name="costCenter" value="${event.costCenter}">
				<button class="btn btn-success waves-effect waves-light" type="submit" style="float: right; width: 45%;">Book trip for event</button>
				</g:form>				
             </div>
             </g:else>
             
             </div>
             </div>
             </div>
             <div style="margin-bottom:2%;"></div> 
             </td></tr>            
             </g:each>    </tbody></table>         
	</div>	
	</g:if>
	<g:else>
	<script type="text/javascript">
			changeBackground('#f2f3f4');
			</script>
		<div class="row" style="margin-left: 10%;">
					<div class="col-md-12" style="width:80%; text-align: center; ">
					<img class="img-responsive" alt="" src="${resource(dir:'images',file:'no_events.png')}" style="margin: 0 auto; height: 280px;">
					<div style="margin: 4%;"></div>
					<h3 class="text-muted">No events available</h3>
					<h4 class="text-muted">You have no upcoming events. </h4>
					<h4 class="text-muted">Let's create one!</h4>					
					<div class="col-md-12" style="width=30%;">
					<g:link controller="Events" action="createEvent" class="btn btn-success waves-effect waves-light"> Create Event</g:link>
					</div>
					</div>
		</div>
	</g:else>
</div>
<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js')}"></script>
<script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').dataTable();
            });
          </script>
<div class="container">  
  <!-- Modal -->
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="myModal" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-sm">
           <div class="modal-content" style="text-align: center;">
               <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                   <h4 class="modal-title" id="mySmallModalLabel">Congratulations...!</h4>
               </div>
               <div class="modal-body">
              Event created successfully.
               </div>
           </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
   </div>
</div>
<script>
$(document).ready(function(){
    // Show the Modal on load
 var d="${session["popUpData"] }";
  if(d=="show"){
    $("#myModal").modal("show");
    }
  <%
  session['popUpData']="";
  %>
});
</script>
<script type="text/javascript">
$(document).ready(function(){
<%
session['sidebarActive']="";
session['sidebarActive']="events/index";
%>
});
</script>     