<meta name="layout" content="vendorMaster"/>
 <link href="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'fixedHeader.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'responsive.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'scroller.bootstrap.min.css')}" rel="stylesheet" type="text/css" />
<link href="${resource(dir:'assets/plugins/datatables',file:'dataTables.colVis.css')}" rel="stylesheet" type="text/css" />
<script src="${resource(dir:'assets/js',file:'jquery.min.js')}"></script>
<div class="page-title">
	<div class="title">Rejection reasons</div>
</div>
<div class="col-md-12" style="text-align:right">
	<g:link controller="RejectionReason" action="addRejectionReason" class="btn btn-success waves-effect waves-light" style="float:right;"> New Reason</g:link>
	<br>
<br>
</div>
<div class="row" style="margin-top: 5%;">
<g:if test="${flash.message}">
		<div class="message" role="status">
				
		<div class="container">  
		  <!-- Modal -->
		  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="myModalCancel" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
		      <div class="modal-dialog modal-sm">
		           <div class="modal-content" style="text-align: center;">
		               <div class="modal-header" style="border-bottom: medium none;margin: -15px;">
		                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>                 
		               </div>
		               <div class="modal-body">
		             ${flash.message}
		               </div>
		           </div><!-- /.modal-content -->
		       </div><!-- /.modal-dialog -->
		   </div>  
		</div>
		<script>
		$(document).ready(function(){
		    // Show the Modal on load
		 var d="${session["popUpDataCancel"] }";
		
		  if(d=="show"){
		    $("#myModalCancel").modal("show");
		    }
		  <%
		  session['popUpDataCancel']="";
		  %>
		});
		</script>		
		</div>
	</g:if>
	<g:if test="${flash.message1}">
		<div class="message1" role="status">
		<div class="container">  
		  <!-- Modal -->
		  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="myModal" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
		      <div class="modal-dialog modal-sm">
		           <div class="modal-content" style="text-align: center;">
		               <div class="modal-header" style="border-bottom: medium none;margin: -15px;">
		                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		                 
		               </div>
		               <div class="modal-body">
		              ${flash.message1}
		               </div>
		           </div><!-- /.modal-content -->
		       </div><!-- /.modal-dialog -->
		   </div>
		</div>
		<script>
		$(document).ready(function(){
		    // Show the Modal on load
		 var d="${session["popUpData"] }"; 
		  if(d=="show"){
		    $("#myModal").modal("show");
		    }
		  <%
		  session['popUpData']="";
		  %>
		});
		</script>
		</div>
	</g:if>
<div class="col-md-1"></div>
<div class="col-md-10">
	<table id="datatable" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Reason details</th>
				<th>Actions</th>
			</tr>
			</thead>
			<tbody>
			<g:each in="${rejectionReasonInstanceList}" status="i" var="rejectionReasonInstance">
				<tr>
					<td>${rejectionReasonInstance?.reasonDetails}</td>
				<td>
				<g:link action="editRejectionReason" id="${rejectionReasonInstance?.id}" title="Edit"><i class="md  md-edit"></i></g:link> | 
				<g:link action="deleteRejectionReason" id="${rejectionReasonInstance?.id}" title="Delete" onclick="return confirm(' You want to delete?');"><i class="md  md-delete"></i></g:link>
				
				</td>
				</tr>
			</g:each>			
		</tbody>
	</table>
</div>
<div class="col-md-1"></div>
</div>
<script src="${resource(dir:'assets/plugins/datatables',file:'jquery.dataTables.min.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.bootstrap.js')}"></script>
<script src="${resource(dir:'assets/plugins/datatables',file:'dataTables.scroller.min.js')}"></script>
<script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').dataTable();
            });
          </script>

<script type="text/javascript">
$(document).ready(function(){
<%
session['sidebarActive']="";
session['sidebarActive']="rejectionReason/index";
%>
});
</script>