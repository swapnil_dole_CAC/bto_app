<meta name="layout" content="vendorMaster" />
<div class="row">
	<div class="col-sm-12">
		<div class="">
			<h4 class="m-t-0 header-title">
				<b>Update reason</b>
			</h4>
			<div class="col-md-2"></div>
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			
			<g:form action="updateRejectionReason" method="post">
				<div class="col-md-8">
					<div>
						<g:hiddenField name="id" value="${rejectionReasonInstance?.id}" />
						<div class="form-group clearfix">
							<label class="col-lg-3 control-label" for="name2">Reason Details*</label>
							<div class="col-lg-9">
								<input id="name2" name="reasonDetails" value="${rejectionReasonInstance?.reasonDetails}" type="text" class=" form-control" required="required" >
							</div>
						</div>
						<div class="col-lg-3" style="float: right;">
								<button class="btn btn-success waves-effect waves-light"
									type="submit">Update</button>
								&nbsp;<g:link action="index" class="btn btn-inverse waves-effect waves-light">Cancel</g:link>
							</div>
						</div>


					</div>
				</g:form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</div>
<!-- End row -->