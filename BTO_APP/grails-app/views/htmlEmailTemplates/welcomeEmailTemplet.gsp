<%@ page contentType="text/html" %>
<HTML><HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/></HEAD>
<BODY>
<DIV style="FONT-SIZE: 12px">
<TABLE 
style="BORDER-BOTTOM: lightblue 1px solid; BORDER-LEFT: lightblue 1px solid; FONT-FAMILY: Arial,Helvetica,sans-serif; BORDER-TOP: lightblue 1px solid; BORDER-RIGHT: lightblue 1px solid" 
width=800>
  <TBODY>
  <TR>
    <TD>
      <TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
        <TBODY>
        <TR>
          <TD 
          style="BORDER-BOTTOM: lightblue 2px solid; TEXT-ALIGN: left; PADDING-BOTTOM: 10px; PADDING-LEFT: 20px"><IMG 
            align=middle src="http://cloudacar.org/images/img/cac_logo.png" 
        width=300></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD 
    style="TEXT-ALIGN: left; PADDING-BOTTOM: 0px; PADDING-LEFT: 20px; PADDING-RIGHT: 20px; COLOR: #333; PADDING-TOP: 20px">Dear 
      <SPAN style="FONT-WEIGHT: bold">Reader</SPAN>, </TD></TR>
  <TR>
    <TD 
    style="TEXT-ALIGN: justify; PADDING-BOTTOM: 0px; LINE-HEIGHT: 20px; PADDING-LEFT: 20px; PADDING-RIGHT: 20px; MARGIN-BOTTOM: 15px; VERTICAL-ALIGN: top; PADDING-TOP: 0px">
      <H2 style="PADDING-BOTTOM: 0px; MARGIN-BOTTOM: 0px">Welcome to the all new 
      world of commuting !</H2>With great pleasure we introduce you to your new 
      social commute platform : <A href="http://cloudacar.org//" 
      target=_blank>cloudacar.org</A> 
      <H3 style="COLOR: #039dd0">What is <A href="http://cloudacar.org/" 
      target=_blank>cloudacar.org</A> ?</H3>
      <P>In Today’s world “Cloud” is synonymous with the power of shared 
      resources to create shared value. Cloudacar (CAC) derives the same synergy 
      to build a new social commute platform for ride sharing. CAC combines 
      convenience of Mobile App &amp; Website with features like Pre-verified 
      Users &amp; Customized Trip Preferences for delivering Secured, Convenient 
      &amp; Reliable social commute. It is an effort to build a community of 
      smart and responsible commuters who share their ride in an effective way 
      by using CAC and derive benefits from it for self and the society for 
      better tomorrow.We solicit your initiative to join the community and 
      invite your friends to build this community for better tomorrow. </P>
      <HR>

      <H3 style="COLOR: #039dd0">Why <A href="http://cloudacar.org/" 
      target=_blank>cloudacar.org</A> ?</H3>
      <P>
      <UL>
        <LI>450 million vehicles on indian roads by 2020 !!! 
        <LI>78% cars travel with only 1 person inside. 
        <LI>Massive Traffic Congestion in every major city of India. 
        <LI>6 -25 kmph : Average driving speed in Indian cities. 
        <LI>100 Cr Man hours lost in major Indian cities, in commuting from 
        office to home every month. 
        <LI>$80 Bn per year is the cost of pollution in India. 
        <LI>50% rise in fuel prices in last 3 years. 
        <LI>No. 1 : India’s ranking in road accidents. 
        <LI>620,000 premature deaths per year in India due to Air Pollution. 
        <LI>Frustrated Commuters!! Frustrated Employers!! </LI></UL>
      <P></P>
      <HR>

      <H3 style="COLOR: #039dd0">Why cloudacar Mobile App ?</H3>
      <P>Our mobile app makes your ride sharing experience very comfortable and 
      covinient. Our ease-of-use features help you in finding your prospective 
      ride matches on your finger tips. Following innovative features of CAC 
      Mobile App makes your ridesharing effortless. 
      <H4 
      style="PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; COLOR: #000; PADDING-TOP: 0px">Cloudacar 
      mobile app features</H4>
      <UL>
        <LI>Perform all steps from Registration to Ride Search &amp; Trip 
        Ratings using CAC Mobile App 
        <LI>See Matching Users &amp; Live Trips at your fingertips 
        <LI>Track Location of members booked on same trip | Never Lose Each 
        Other 
        <LI>Effective User Menu for features and functionalities </LI></UL>
      <P></P>
      <HR>

      <H3 style="COLOR: #039dd0">Benefits of using <A 
      href="http://cloudacar.org///" target=_blank>cloudacar.org</A></H3>
      <P>
      <UL>
        <LI><STRONG>Protect The Planet For Generation Next</STRONG> : Using CAC 
        reduces harmful carbon emissions, brings down global warming. It saves 
        natural resources leaving greener planet for next generation. 
        <LI><STRONG>Enrich Your Travel Experience Collectively</STRONG> : 
        Upgrade from tiresome public transport to comfort of a car without 
        spending extra. Using CAC collectively reduces traffic, driving efforts 
        and enhances traveling experience of community 
        <LI><STRONG>Cloud Your Car &amp; Save Money</STRONG> : Save average 50 K 
        Annually. Regular Car Sharing not only makes your home-office commuting 
        almost free but also earns you more. 
        <LI><STRONG>Power Of Network </STRONG>: CAC brings in likeminded people 
        from various professional background together to benefit from the 
        networking. </LI></UL>
      <P></P>
      <HR>

      <P>Visit <A href="http://cloudacar.org//" 
      target=_blank>cloudacar.org</A> to know more about us. <STRONG 
      style="COLOR: #039dd0">|</STRONG> You can also check out <A 
      href="http://cloudacar.org/images/CAC_Brochure.pdf" target=_blank>Our 
      Brochure</A>.</P>
      <P>
      <HR>

      <H1 
      style="PADDING-BOTTOM: 5px; PADDING-LEFT: 5px; PADDING-RIGHT: 5px; BACKGROUND: #ffff99; PADDING-TOP: 5px"><STRONG 
      style="COLOR: red; TEXT-DECORATION: blink">Hurry !!</STRONG> <STRONG 
      style="FONT-SIZE: 18px">Limited Offer For Free Registration!</STRONG></H1>
      <HR>

      <P></P></TD></TR>
  <TR>
    <TD>
      <TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
        <TBODY>
        <TR>
          <TD 
          style="BORDER-BOTTOM: lightblue 2px solid; TEXT-ALIGN: left; PADDING-BOTTOM: 10px; PADDING-LEFT: 20px; VERTICAL-ALIGN: bottom">Best 
            Regards,<BR><SPAN 
            style="FONT-SIZE: 14px; FONT-WEIGHT: bold">Cloudacar Team</SPAN> </TD>
         </TR></TBODY></TABLE></TD></TR></TBODY></TABLE></DIV></BODY></HTML>
