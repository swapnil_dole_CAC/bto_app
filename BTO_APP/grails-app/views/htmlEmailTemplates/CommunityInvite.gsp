<%@ page contentType="text/html" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<style type="text/css">

	body { font-size: 14px;
	line-height: 1.6em;}

	.outerBox{
	border: 1px solid #A0A0A0 ;
	padding: 10px;
	margin-left: 5%;
	margin-right:5%
		}

.innerBox{
	background-color: #F0F0F0 ;
	padding: 5px;
	font-family: "Arial", Sans-serif;
	color: #787878
	}

	p.title{
	color : #33A1DE	;
	font-size: x-large;
	font-weight: bold;
	}

	#code{
	color :#33A1DE
	}

	#benefites{
	font-weight: bold;
	}

	#appName{
	color : #33A1DE;
	}
	a {
    color :#33A1DE;
	}


</style>
</head>
<body>
<div class="outerBox">

<img src="http://cloudacar.org/images/img/cac_logo.png" width="300" /><br>
<div class="innerBox">
<p class="title">cloudacar Community Invitation</p>
<p class="verifyText">Dear ${userData.email} ,</br></br></p>


<p class="verifyText">${userData.name} has invited you to Join cloudacar ! A new carpooling app and take a step towards responsible commute to reduce driving stress, traffic, pollution, accidents and fuel expenses.
</p>

<p id="code">Click below link to download the <b>cloudACar, Car Pooling Application.</b></p>
<p><a href=${userData.androidLink}>Android Application</a></p>
<p><a href=${userData.iosLink}>IOS Application</a></p>


</br></br>
Thanks & Regards,
<p><span id="appName">cloudacar.org</span> Team</p>
</div>

</div>
</body>
</html>