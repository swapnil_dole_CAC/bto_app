<%@ page contentType="text/html" %>
<HTML><HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/></HEAD>
<BODY>
<DIV style="FONT-SIZE: 12px">
<TABLE 
style="BORDER-BOTTOM: lightblue 1px solid; BORDER-LEFT: lightblue 1px solid; FONT-FAMILY: Arial,Helvetica,sans-serif; BORDER-TOP: lightblue 1px solid; BORDER-RIGHT: lightblue 1px solid" 
width=800>
  <TBODY>
  <TR>
    <TD>
      <TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
        <TBODY>
        <TR>
          <TD 
          style="BORDER-BOTTOM: lightblue 2px solid; TEXT-ALIGN: left; PADDING-BOTTOM: 10px; PADDING-LEFT: 20px"><IMG 
            align=middle src="http://cloudacar.org/images/img/cac_logo.png" 
        width=300></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD 
    style="TEXT-ALIGN: left; PADDING-BOTTOM: 0px; PADDING-LEFT: 20px; PADDING-RIGHT: 20px; COLOR: #333; PADDING-TOP: 20px">Dear 
      <SPAN style="FONT-WEIGHT: bold">User</SPAN>, </TD></TR>
  <TR>
    <TD 
    style="TEXT-ALIGN: justify; PADDING-BOTTOM: 0px; LINE-HEIGHT: 20px; PADDING-LEFT: 20px; PADDING-RIGHT: 20px; MARGIN-BOTTOM: 15px; VERTICAL-ALIGN: top; PADDING-TOP: 0px">
      Welcome to cloudacar.<br/> </br>
     We highly appreciate your first step towards deciding to carpool, a way to commute responsibly and socially for brighter tomorrow.<br/><br/>
      Next Steps <br/></br>
	  <ul>
		<li>Download CLOUDACAR APP On Android and IOS </br><a href="https://itunes.apple.com/us/app/cloudacar/id1054338809?mt=8" target="_blank"><img class="img-rounded" src="http://52.25.10.187:8080/images/apple-store.png" alt="Apple Store" width="150" border="0"/></a><a href="https://play.google.com/store/apps/details?id=com.scispl.cloudacar" target="_blank"><img class="img-rounded" src="http://52.25.10.187:8080/images/google-play.png" alt="Google Play Store" width="150" border="0" /></a></li>
		<li>Complete your registration & first trip </li>
		<li>Build your community & start carpooling </li>
		</ul>
	We also seek your support to invite your friends and spread the mission of social commute as a part of our responsibility towards building better environment.
      <HR>

     </tr> 
  <TR>
    <TD>
      <TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
        <TBODY>
        <TR>
          <TD 
          style="BORDER-BOTTOM: lightblue 2px solid; TEXT-ALIGN: left; PADDING-BOTTOM: 10px; PADDING-LEFT: 20px; VERTICAL-ALIGN: bottom">Best 
            Regards,<BR><SPAN 
            style="FONT-SIZE: 14px; FONT-WEIGHT: bold">Cloudacar Team</SPAN> 
			<br/><a href="http://www.cloudacar.org" target="_blank">cloudacar.org</a>
			</br><a href="mailto:info@cloudacar.org"> info@cloudacar.org</a>
			</TD>
         </TR>
		
		 </TBODY></TABLE></TD></TR></TBODY></TABLE></DIV></BODY></HTML>
