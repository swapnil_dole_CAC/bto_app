<%@ page contentType="text/html" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<style type="text/css">

	body { 
	font-size: 14px;
	line-height: 1.6em;
	
	background-size:100%;
	}
	p{
	color:gray;
	font-size:16px;
	}
</style>
</head>
<body>
<div style="width:70%; text-align:center;margin-left:30%;">

<div style="width:60%; text-align:center;background-image:url(http://52.35.38.84/images/bg_graphics.png);background-repeat:no-repeat;">
	<div style="margin-left:-62%;">
		<img src="http://52.35.38.84/images/logo.png" style="margin-top:5%;">
	</div>
	<div style="margin-top:-10%;margin-left:26%;">
		<font size="4%" color="#00BEFF" face="Verdana">Thank you for registering with Cloudacar</font>
	</div>
	<div style="text-align:left;margin-top:10%;margin-left:9%;">
		<font face="Verdana">
			<p>cloudacar Welcomes <b>${userData.name}</b> on-board.</p>
			<p>You are successfully registered with cloudacar.</p>
			<p>Please login using given User id</p>
			<p>Email Address : <b>${userData.email}</b></p> 
			<p>Please set your password using below link</p>
			<p><span id="benefites"><a href="${userData.link}" target="_blank">Set Password</a></span></p>
			<br>
			<p>Thanks,</p>
			<p>Team <span id="appName">cloudacar</span> </p>
			<p>Visit us at : <a href="http://www.cloudacar.org" target="_blank">http://www.cloudacar.org</a> for more information. </p>
		</font>
	</div>
	

</div>
</div>
</div>
</body>
</html>