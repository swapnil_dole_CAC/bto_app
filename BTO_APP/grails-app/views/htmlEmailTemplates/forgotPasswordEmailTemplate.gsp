<%@ page contentType="text/html" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<style type="text/css">

	body { font-size: 14px;
	line-height: 1.6em;}
	
	.outerBox{
	border: 1px solid #A0A0A0 ;
	padding: 10px;
	margin-left: 5%;
	margin-right:5% 
		}

.innerBox{
	background-color: #F0F0F0 ;
	padding: 5px;
	font-family: "Arial", Sans-serif;
	color: #787878 
	}
	
	p.title{
	color : #33A1DE	;
	font-size: x-large;
	font-weight: bold;
	}

	#code{
	color :#33A1DE
	}
	
	#appName{
	color : #33A1DE;
	}


</style>
</head>
<body>
<div class="outerBox">

<img src="http://cloudacar.org/imagesW/CAC_logo_white.png" width="300" /><br>
<div class="innerBox">
<p class="title">BTO account new password</p></br></p> 
<p class="verifyText">Dear ${userData.name} ,</br></br></p>


<p class="verifyText">This is your new password for your BTO account.<br>
</p>

<p><span id="code">Password :</span> ${userData.password} </br><span id="code"> UserName(email-address) :</span> ${userData.email}  </p> 

</br>
Thanks & Regards,
<p><span id="appName">cloudacar.org</span> Team</p>
</div>

</div>
</body>
</html>