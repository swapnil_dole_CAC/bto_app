<%@ page contentType="text/html" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<style type="text/css">

	body { font-size: 14px;
	line-height: 1.6em;}
	
	.outerBox{
	border: 1px solid #A0A0A0 ;
	padding: 10px;
	margin-left: 5%;
	margin-right:5% 
		}

.innerBox{
	background-color: #F0F0F0 ;
	padding: 5px;
	font-family: "Arial", Sans-serif;
	color: #787878 
	}
	
	p.title{
	color : #33A1DE	;
	font-size: x-large;
	font-weight: bold;
	}

	#code{
	color :#33A1DE
	}
	
	#benefites{
	font-weight: bold;
	}
	
	#appName{
	color : #33A1DE;
	}


</style>
</head>
<body>
<div class="outerBox">

<img src="http://cloudacar.org/images/cac_logo.png" width="150" height="100"/><br>
<div class="innerBox">
<p class="title" style="color:#33A1DE;font-style:bold"><center><font size="5" color="#33A1DE"><b>cloudacar - Your New Social Commute Platform</b></font></center></p>
<p><h3>Hi There</h3></p></p>
<p class="verifyText">Thank you for taking the pledge!</br></p>


<p class="verifyText">
cloudacar covers all the challenges and needs of car pooling mainly for commute from home to office to home for millions of people everyday.
</p>

<p id="code">Book your trip today with cloudacar @  </p> 

<p id="code">iOS: <a href="${userData.androidURL}" target="_blank">${userData.androidURL}</a></p>
<p id="code">Android: <a href="${userData.iosURL}" target="_blank">${userData.androidURL}</a></p>

</br>
Thanks & Regards,
<p><span id="appName">cloudacar.org</span> Team</p>
</div>

</div>
</body>
</html>