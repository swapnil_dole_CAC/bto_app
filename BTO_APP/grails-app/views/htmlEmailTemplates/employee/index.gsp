<!DOCTYPE html>
<html lang="en">
<head>
  <title>Employee Emailer</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  
</head>
<body style="color:#302c2a;font-weight:700">

<div class="container">
	<div class="col-md-2"></div>
		<div class="col-md-8 col-xs-12" style="background:url(http://cloudacar.org/images/subscription_images/emailer_Top.png) no-repeat;min-height:320px;background-size:100%">
		</div>
	<div class="col-md-2"></div>
	<div class="col-md-12">
		<div class="col-md-2"></div>
		<div class="col-md-8 col-xs-12">
		<p>
		Dear Reader, </br></br>With pollution at hazardous levels in major Indian cities, massive traffic congestions & driving<br>stress
		 impacting our health and productivity adversely there is a need to take some urgent </br> corrective measures by corporate and 
		 individuals for brighter tomorrow.
		</p
		<p>We invite you to join <a href="http://cloudacar.org/" target="blank">www.cloudacar.org</a> a free social commute
		platform for your office to home commute and spread movement to your friends, colleagues and family to build a network of socially responsible commuters.
		</p>
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="col-md-4"></div>
	<div class="col-md-12">
		<div class="col-md-2"></div>
			<div class="col-md-8 col-xs-12">
				<ul style="line-height:2;">
					<li><i>Its Simple & Free.</i></li>
					<li><i>Download the App.</i></li>
					<li><i>Register & Build your Community.</i></li>
					<li><i>Create Trip & Start Carpooling/Social Commute.</i></li>
				</ul>
				As responsible citizen a small step taken by you today in promoting ride sharing and social commuting within your community will help the entire society in a big way.
			</div>
		<div class="col-md-2"></div>
	</div>
	<div class="col-md-4"></div>
		<div class="col-md-4 col-xs-12" style="text-align:center;"></br>
			<h4 style="font-weight:700;color:#075379">You know It Makes Sense !!</h4>
		</div>
	<div class="col-md-4"></div>
	<div class="col-md-12">
		<div class="col-md-2"></div>
			<div class="col-md-8 col-xs-12">
					<table class="table">
					  <tr>
						<th style="border:0"><a target="blank"><img src="http://cloudacar.org/images/subscription_images/clickhere_left.png"></a></th>
						<th style="border:0"><a href="https://itunes.apple.com/us/app/cloudacar/id1054338809?ls=1&mt=8" target="_blank"><img src="http://cloudacar.org/images/subscription_images/appStore.png"></a></th>
						<th style="border:0"><a href="http://cloudacar.org#service" target="blank"><img src="http://cloudacar.org/images/subscription_images/joinnow.png"></a></th>
						<th style="border:0"><a href="https://play.google.com/store/apps/details?id=com.scispl.cloudacar&hl=en" target="_blank"><img src="http://cloudacar.org/images/subscription_images/playStore.png"></a></th>
						<th style="border:0"><a target="blank"><img src="http://cloudacar.org/images/subscription_images/clickhere_right.png"></a></th>
					  </tr>
				</table>
			</div>
		<div class="col-md-2"></div>
	</div>
	
	
	<div class="col-md-2">
	</div>
	<div class="col-md-12"></div>
	<div class="col-md-2"></div>
		<div class="col-md-8 col-xs-12" style="background:url(http://cloudacar.org/images/subscription_images/emailer_Bottom.png) no-repeat;min-height:260px;background-size:100%">
		</div>
		<div class="col-md-12"></div>
	<div class="col-md-2"></div>
		<div class="col-md-8">
			<div> 
			<span><a href="http://cloudacar.org/" target="_blank">www.cloudacar.org</a></span>
			<span style="text-align:right;padding-left:400px"><a href="http://cloudacar.org/images/CAC_Brochure.pdf" target="_blank">Download Brochure</span>
			</div>
		</div>
	<div class="col-md-2"></div>
</div>

</body>
</html>