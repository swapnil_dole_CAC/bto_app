<%@ page contentType="text/html" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<style type="text/css">

	body { font-size: 14px;
	line-height: 1.6em;}
	
	.outerBox{
	border: 1px solid #A0A0A0 ;
	padding: 10px;
	margin-left: 5%;
	margin-right:5% 
		}

.innerBox{
	background-color: #F0F0F0 ;
	padding: 5px;
	font-family: "Arial", Sans-serif;
	color: #787878 
	}
	
	p.title{
	color : #33A1DE	;
	font-size: x-large;
	font-weight: bold;
	}

	#code{
	color :#33A1DE
	}
	
	#appName{
	color : #33A1DE;
	}


</style>
</head>
<body>
<div class="outerBox">
<img src="http://cloudacar.org/images/img/cac_logo.png" width="300" /><br>
<div class="innerBox">
SECURITY ALERT: Failed Login Attempt to your account: <b>mandar.lande@cloudacar.org</b> <br/><br/><br/>

A failed login attempt has occurred on May 11, 2016 at 17:46:00. Someone used the <b>Username</b> :<b>mandarlande@cloudacar.org</b> to attempt to login .<br/><br/>

If you did not attempt to access your account, please contact <b>cloudacar</b> Team immediately.<br/><br/>

This email is being sent to you by cloudacar Team because your email address used for the failed login. Our system administrator has enabled this security alert.<br/><br/>

</br>
Thanks & Regards,
<p><span id="appName">cloudacar.org</span> Team</p>
</div>

</div>
</body>
</html>