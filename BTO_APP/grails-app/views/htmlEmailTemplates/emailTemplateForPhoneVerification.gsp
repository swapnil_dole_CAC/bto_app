<%@ page contentType="text/html" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<style type="text/css">

	body { font-size: 14px;
	line-height: 1.6em;}
	
	.outerBox{
	border: 1px solid #A0A0A0 ;
	padding: 10px;
	margin-left: 5%;
	margin-right:5% 
		}

.innerBox{
	background-color: #F0F0F0 ;
	padding: 5px;
	font-family: "Arial", Sans-serif;
	color: #787878 
	}
	
	p.title{
	color : #33A1DE	;
	font-size: x-large;
	font-weight: bold;
	}

	#code{
	color :#33A1DE
	}
	
	#benefites{
	font-weight: bold;
	}
	
	#appName{
	color : #33A1DE;
	}


</style>
</head>
<body>
<div class="outerBox">

<img src="http://cloudacar.org/images/img/cac_logo.png" width="300" /><br>
<div class="innerBox">
<p class="title">Verify your mobile number</p></br></p>
<p class="verifyText">Dear ${userData.name} ,</br></br></p>


<p class="verifyText">To complete your registration,please verify your mobile number by entering</br>
following Verification code in your mobile.
</p>

<p id="code">Your mobile number  verification code is : ${userData.code}  </p> 

</br><p><span id="benefites">Benefits of Verifying your email address:</span></br> Other Users are more likely to contact & request for trip </br>to users
with verified contact details.</p>

</br>
Thanks & Regards,
<p><span id="appName">cloudacar.org</span> Team</p>
</div>

</div>
</body>
</html>