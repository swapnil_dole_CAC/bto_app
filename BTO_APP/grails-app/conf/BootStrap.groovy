import com.bto.customer.Employer
import com.bto.user.AlertTypes;

import groovy.sql.Sql

import org.codehaus.groovy.grails.commons.GrailsApplication

class BootStrap {
	GrailsApplication grailsApplication
	def dataSource
	def mailService
	
	private boolean isEmptyTable(Sql sqlInstance, String tableName){
		def returnVal = false
		sqlInstance.eachRow("SELECT count(*) as count from " + tableName){ row ->
			println("Table: " + tableName + " RowCount: " + row.count)
			if(row.count < 1){
				returnVal = true
			}
		}
		return returnVal
	}
	
	private boolean isdataExists(Sql sqlInstance, String tableName,String whereclause){
		def returnVal = false
		sqlInstance.eachRow("SELECT * from " + tableName+ whereclause){ row ->
			if(row){
				returnVal = true
			}
		}
		return returnVal
	}
	
	private void runScript(Sql sqlInstance, String path){
		try{
			println("Running script from path: " + path)
			String sqlFilePath = grailsApplication.parentContext.servletContext.getRealPath(path)
			String sqlString = new File(sqlFilePath).eachLine {
				sqlInstance.execute(it)
			}
		}catch(Exception e){
			e.printStackTrace()
			throw e
		}
	}
	
	private void setupInitialData(){
		// set default users roles etc.
		
		//create admin role
		def adminUser = Employer.findByEmailID('mandar.lande@cloudacar.org') ?: new Employer(
			password: "",
			firstName: "Mandar",
			lastName: "Lande",
			emailID: "mandar.lande@cloudacar.org",
			gender: "Male",
			city: "Pune",
			phone: "1234567890",
			role:"employerAdmin",
			logo:"",
			enabled: true,
			accountExpired: false,
			accountLocked: false,
			passwordExpired: false,
			createdOn: new Date(),
			lastUpdatedOn: new Date(),
			 
		).save(flush: true, failOnError: true)
		
		
		//Run start up SQL script to load static data in Tabless
		if(AlertTypes.count()== 0)
		{
			//Loading messages into database
			String sqlFilePath = grailsApplication.parentContext.servletContext.getRealPath("/")+"images/startUpSqlScript.sql"
			def sql = Sql.newInstance(dataSource)
			String sqlString = new File(sqlFilePath).eachLine {
				sql.execute(it)
			}
		}
		
		//Load smsGetwayAPI URL from properties file and set to config Variable, that variable is application context level variable
		//and can be accessed(Read/Write) from all artifacts of grails application.
//		def props = new Properties()
//		new File( grailsApplication.parentContext.servletContext.getRealPath("/")+"/images/propertyFiles/appplicationData.properties").withInputStream {
//		  stream -> props.load(stream)
//		}
//		grailsApplication.config.smsAPI=props["smsGetwayAPI"]
		
	}
	
    def init = { servletContext ->
		setupInitialData()
    }
    def destroy = {
    }
}
