package com.bto.booking

import com.bto.utilities.DateTimeHelperService;
import com.bto.utilities.SmsService;
import com.bto.customer.VendorDetails;
import com.bto.trip.InviteDetails;
import com.bto.trip.TripDetails;
import com.bto.user.AlertTypes;
import com.bto.user.User;
import com.bto.user.UserAlerts

import grails.converters.JSON
import grails.transaction.Transactional

import java.text.DateFormat
import java.text.SimpleDateFormat

import org.springframework.web.context.request.RequestContextHolder;

@Transactional
class TripDetailsService {

	def smsService
	def dateTimeHelperService
	def notificationService
	def grailsApplication
	def serviceMethod() {
	}

	def getAllRequests(String vendorID){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def result, subResult
		def coPassengersList
		HashMap coPassengerMap
		List<HashMap<String, String>> tripList
		def hasTrips=false
		try {
			if(vendorID)
			{
				def vId=VendorDetails.get(Long.valueOf(vendorID).longValue())
				def criteria = TripDetails.createCriteria()
				result = criteria.list {
					like("tripStatus", "requested")
					and { eq("vendor", vId)	}
					order("lastUpdatedOn", "desc")
					projections {
						property 'id'
						property 'startLocation'
						property 'endLocation'
						property 'purpose'
						property 'tripDistance'
						property 'tripStatus'
						property 'tripDate'
						property 'tripTime'
					}
				}
				HashMap<String ,String> map
				if(result.size()== 0)
				{
					hasTrips=false
					status = false
					responseData = "failed"
					errorList.add("Booked trip history not available")
				}
				else
				{
					tripList=new ArrayList()
					coPassengersList=new ArrayList()
					for(int i=0 ; i<result.size(); i++)
					{						
						hasTrips=true
						def trip=result[i]
						
						def tripData=TripDetails.get(trip[0])
						def vendorData=VendorDetails.get(tripData.vendor.id)
						def userData=User.get(tripData.user.id)
						def isAsCoTravellerExixts=InviteDetails.findByInvitedByTripIDAndStatus(tripData,"invite")
						
						if(isAsCoTravellerExixts && isAsCoTravellerExixts.vendor==vId)
						{
							map =new HashMap<String,String>()
							map.put("tripID", trip[0])
							map.put("startLocation", trip[1])
							map.put('endLocation', trip[2])
							map.put('purpose', trip[3])
							map.put('tripDistance', trip[4])
							map.put('tripStatus', trip[5])
							map.put('tripDate', trip[6])						
							map.put('tripTime', convertTimeFromTwentyFourHoursToTwelveHours(trip[7]))
							map.put('vendorName',vendorData.companyName)
							map.put('vendorCarType', tripData.carType)
							map.put("designation",userData.designation)
							map.put("department",userData.department)
							map.put("name",userData.firstName+" "+userData.lastName)
							map.put("phone",userData.phone)
							map.put("note",tripData.note)
							
							def invitedTripId=TripDetails.get(tripData.id)
							def inviteTrips=InviteDetails.findByInvitedToTripID(invitedTripId)
							if(inviteTrips)
							{	
								def subCriteria=InviteDetails.createCriteria()
								subResult=subCriteria.list {
									like("status", "accept")
									and { eq("invitedToTripID",  invitedTripId) }
									projections {
										property 'id'
										property 'invitedByTripID'
										property 'invitedToTripID'
										property 'invitedByUserID'
										property 'invitedToUserID'
										property 'status'
									}
								}							
									for(int j=0 ; j<subResult.size(); j++)
									{
										coPassengerMap=new HashMap()
										def travellers=subResult[j]	
										def travellersData=InviteDetails.get(travellers[0])
										if(travellersData)
										{	
											coPassengerMap.put("name", travellersData.invitedByUserID.firstName+" "+travellersData.invitedByUserID.lastName)
											coPassengerMap.put("startLocation", travellersData.invitedByTripID.startLocation)
											coPassengerMap.put("id", travellersData.invitedByUserID.id)
											coPassengerMap.put("tripId", travellersData.invitedByTripID.id)
											coPassengerMap.put("phone", travellersData.invitedByUserID.phone)
											coPassengersList.add(coPassengerMap)
										}
									}
									map.put("coTravellers",coPassengersList)
									map.put("CoPasscount", coPassengersList.size())
							}		
							//map.put("CoPasscount", coPassengersList.size())
							tripList.add(map)
						}
					}
					jsonData["requestedTrips"]=tripList
					status=true
					responseData="success"
				}
			}
			else
			{
				status=false
				responseData="failed"
				errorList.add("Vendor Id Not Found")	
			}
		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}
	
	def getPreviousTripDetails(String userID){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def result,subResult
		List<HashMap<String, String>> tripList
		def hasTrips=false
		try {
			// working code to get all records

			/*def user=User.get(userID)
			 result=TripDetails.findByUser(user)*/

			//another way
			//result=TripDetails.executeQuery("select startLocation,startLat,startLog,endLocation,endLat,endLog,purpose,tripDistance,tripStatus,tripDate,tripTime,createdOn,lastUpdatedOn from TripDetails as t where t.user="+userID)
			def uId=User.get(Long.parseLong(userID).longValue())
			def criteria = TripDetails.createCriteria()
			result = criteria.list {
				like("tripStatus", "completed")
				and { eq("user", uId) }
				projections {
					property 'id'
					property 'startLocation'
					property 'endLocation'
					property 'purpose'
					property 'tripDistance'
					property 'tripStatus'
					property 'tripDate'
					property 'tripTime'
					property 'createdOn'
					property 'lastUpdatedOn'
				}
			}



			HashMap<String ,String> map

			if(result.size()== 0)
			{
				hasTrips=false
				status = false
				responseData = "failed"
				errorList.add("Booking history not available")
			}
			else
			{
				tripList=new ArrayList()
				for(int i=0 ; i<result.size(); i++)
				{
					hasTrips=true
					def trip=result[i]

					def tripData=TripDetails.get(trip[0])
					def vendorData=VendorDetails.get(tripData.vendor.id)
					def userData=User.get(tripData.user.id)

					map =new HashMap<String,String>()
					map.put("tripID", trip[0])
					map.put("startLocation", trip[1])
					map.put('endLocation', trip[2])
					map.put('purpose', trip[3])
					map.put('tripDistance', trip[4])
					map.put('tripStatus', trip[5])
					map.put('tripDate', trip[6])
					map.put('tripTime', convertTimeFromTwentyFourHoursToTwelveHours(trip[7]))
					map.put('createdOn', trip[8])
					map.put('lastUpdatedOn', trip[9])
					map.put('vendorName',vendorData.companyName)
					map.put('vendorCarType', tripData.carType)
					map.put("department",userData.department)

					def invitedTripId=TripDetails.get(tripData.id)
					def inviteTrips=InviteDetails.findByInvitedToTripID(invitedTripId)

					if(inviteTrips){

						def subCriteria=InviteDetails.createCriteria()
						subResult=subCriteria.list {
							like("status", "accept")
							and { eq("invitedToTripID",  invitedTripId) }
							projections {
								property 'id'
								property 'invitedByTripID'
								property 'invitedToTripID'
								property 'invitedByUserID'
								property 'invitedToUserID'
								property 'status'
							}
						}

						String users=""
						if(subResult.size()== 0)
						{

						}else
						{
							for(int j=0 ; j<subResult.size(); j++)
							{
								def travellers=subResult[j]

								def travellersData=InviteDetails.get(travellers[0])
								def coUsersData=User.get(travellersData.invitedByUserID.id)

								users+=coUsersData.firstName+" "+coUsersData.lastName+","

							}
							users =users.substring(0, users.length()-1)
							map.put("coTravellerName",users)
						}
					}else{
						map.put("coTravellerName","")
					}


					tripList.add(map)
				}

				jsonData["tripHistory"]=tripList
				status=true
				responseData="success"
			}


		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}

	def getBookedTripHistory(String userID){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def result
		List<HashMap<String, String>> tripList
		def hasTrips=false
		try {
			if(userID)
			{
				def uId=User.get(Long.parseLong(userID.trim()).longValue())
				def criteria = TripDetails.createCriteria()
				result = criteria.list {
	//				like("tripStatus", "booked")
					
					and { eq("user", uId) }
					order("lastUpdatedOn", "desc")
					projections {
						property 'id'
						property 'startLocation'
						property 'endLocation'
						property 'purpose'
						property 'tripDistance'
						property 'tripStatus'
						property 'tripDate'
						property 'tripTime'
						property 'createdOn'
						property 'lastUpdatedOn'
						property 'carType'
					}
				}
	
				HashMap<String ,String> map
	
				if(result.size()== 0)
				{
					hasTrips=false
					status = false
					responseData = "failed"
					errorList.add("Booked trip history not available")
				}
				else
				{
					tripList=new ArrayList()
					for(int i=0 ; i<result.size(); i++)
					{
						hasTrips=true
						def trip=result[i]
	
						def tripData=TripDetails.get(trip[0])
						def vendorData=VendorDetails.get(tripData.vendor.id)
						def userData=User.get(tripData.user.id)
	
						map =new HashMap<String,String>()
						map.put("tripID", trip[0])
						map.put("startLocation", trip[1])
						map.put('endLocation', trip[2])
						map.put('purpose', trip[3])
						map.put('tripDistance', trip[4])
						map.put('tripStatus', trip[5])
						map.put('tripDate', trip[6])
						map.put('tripTime', convertTimeFromTwentyFourHoursToTwelveHours(trip[7]))
						map.put('createdOn', trip[8])
						map.put('lastUpdatedOn', trip[9])
						map.put('carType', trip[10])
						map.put('vendorName',vendorData.companyName)
						map.put('vendorCarType', tripData.carType)
						map.put("department",userData.department)
						tripList.add(map)
					}
	
					jsonData["bookedTripHistory"]=tripList
					status=true
					responseData="success"
				}
			}
			else
			{
				errorList.add("UserId not found")
				status=false
				responseData = "failed"
			}
		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}

	def getInviteDetails(String userID){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def result,subResult
		List<HashMap<String, String>> tripList
		def hasTrips=false
		try {
			def uId=User.get(Long.parseLong(userID.trim()).longValue())
			def criteria = InviteDetails.createCriteria()
			result = criteria.list {
				like("status", "invite")
				and { eq("invitedToUserID", uId) }
				projections {
					property 'id'
					property 'invitedByTripID'
					property 'invitedToTripID'
					property 'invitedByUserID'
					property 'invitedToUserID'
					property 'status'
				}
			}
			HashMap<String ,String> map

			if(result.size()== 0)
			{
				hasTrips=false
				status = false
				responseData = "failed"
				errorList.add("invitations not available")
			}
			else
			{
				tripList=new ArrayList()
				for(int i=0 ; i<result.size(); i++)
				{
					hasTrips=true
					def trip=result[i]
					def inviteBy=InviteDetails.findByInvitedByTripID(trip[1])
					def inviteTo=InviteDetails.findByInvitedToTripID(trip[2])
					def tripDataBy=TripDetails.get(inviteBy.invitedByTripID.id)
					def tripDataTo=TripDetails.get(inviteTo.invitedToTripID.id)
					def vendorDataBy=VendorDetails.get(tripDataBy.vendor.id)
					def vendorDataTo=VendorDetails.get(tripDataTo.vendor.id)
					def userDataBy=User.get(tripDataBy.user.id)
					def userDataTo=User.get(tripDataTo.user.id)

					map =new HashMap<String,String>()
					map.put("inviteID", trip[0])
					map.put("invitedByTripID", inviteBy.invitedByTripID.id)
					map.put("startLocation", tripDataBy.startLocation)
					map.put('endLocation', tripDataBy.endLocation)
					map.put('purpose',tripDataBy.purpose)
					map.put('tripDistance', tripDataBy.tripDistance)
					map.put('tripStatus',tripDataBy.tripStatus)
					map.put('tripDate', tripDataBy.tripStatus)
					map.put('tripTime', convertTimeFromTwentyFourHoursToTwelveHours(tripDataBy.tripTime))
					map.put('vendorName',vendorDataBy.companyName)
					map.put('carType', tripDataBy.carType)
					map.put("firstName",userDataBy.firstName)
					map.put("lastName",userDataBy.lastName)
					map.put("department",userDataBy.department)
					map.put("designation",userDataBy.designation)
					tripList.add(map)
				}
				jsonData["inviteDetails"]=tripList
				status=true
				responseData="success"
			}
		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}
	
	def scheduleTripWithReason(String userID,String tripId,String reason){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		try {
			if(tripId)
			{
				def trip=TripDetails.get(Long.parseLong(tripId.trim()).longValue())
				if(trip){
					trip.reason=reason
					trip.tripStatus="requested"
					trip.lastUpdatedOn=new Date()
	
					if(trip.save(flush:true,failOnError:true)){
						if(reason!="WithMatchingUser")
						{
							def invite=new InviteDetails()
							invite.vendor=trip.vendor
							invite.invitedByTripID=trip
							invite.invitedByUserID=trip.user
							invite.createdOn=new Date()
							invite.lastUpdatedOn=new Date()
							invite.status="invite"
							if(invite.save(flush:true,failOnError:true))
							{
								status=true
								responseData="success"
							}
						}
						status=true
						responseData="success"
						jsonData['tripId']=trip.id
	
					}else{
						status=false
						responseData="failed"
						errorList.add("Error in change trip status")
					}
				}else{
					status=false
					responseData="failed"
					errorList.add("Trip not found")
				}
			}

		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}

	def inviteUser(String invitedByUserID,String invitedToUserID,String invitedByTripID,String invitedToTripID){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		try {
			def invitedByTrip=TripDetails.get(invitedByTripID)
			def invitedToTrip=TripDetails.get(invitedToTripID)
			def invitedByUser=User.get(invitedByUserID)
			def invitedToUser=User.get(invitedToUserID)
			def invite=new InviteDetails()

			if(invitedToTrip){
				if(invitedByTrip){
					invite.invitedByUserID=invitedByUser
					invite.invitedToUserID=invitedToUser
					invite.invitedByTripID=invitedByTrip
					invite.invitedToTripID=invitedToTrip
					invite.status="invite"
					invite.createdOn=new Date()
					invite.lastUpdatedOn=new Date()

					if(invite.save(flush:true,failOnError:true)){
						status=true
						responseData="success"
						jsonData['invitationId']=invite.id
						jsonData['tripId']=invitedByTripID
						//Notification
						def userAlert=new UserAlerts()
						userAlert.sender=invitedByUser
						userAlert.receiver=invitedToUser
						userAlert.type="TripAlert"
						userAlert.status="Unread"
						userAlert.invite=invite
						def alertId= AlertTypes.findByAlertType("RequestArrives")
						if(alertId)
							userAlert.alertType=alertId
							userAlert.createdOn=new Date()
							userAlert.lastUpdatedOn=new Date()
							userAlert.trip= invitedByTrip
							if(userAlert.save(flush:true,failOnError:true))
							{
								Map userData=new HashMap<String,String>()
								userData.put("alertType", userAlert.alertType.alertType)
								userData.put("tripId", userAlert.trip.id)
								userData.put("invitationId", invite.id)
								userData.put("sender_name", userAlert.sender.firstName+" "+userAlert.sender.lastName)
								userData.put("source_location", userAlert.trip.startLocation)
								userData.put("destination_location", userAlert.trip.endLocation)
								userData.put("department", userAlert.sender.department)
								userData.put("department", userAlert.sender.designation)
								if(userAlert.sender.profilePicture)
									userData.put("user_profile_pic", grailsApplication.config.grails.webSite+"images/userProfilePictures/"+userAlert.sender)
								else
									userData.put("user_profile_pic", "")
								userData.put("date", userAlert.trip.tripDate)
								userData.put("time", convertTimeFromTwentyFourHoursToTwelveHours(userAlert.trip.tripTime))
								userData.put("alert_id", userAlert.alertType.id)
								userData.put("badge", UserAlerts.countByReceiver(invitedToUser))
								def message= alertId.message
								message=invitedByUser.firstName+" "+invitedByUser.lastName+" "+message.substring(0, message.indexOf("from"))
								notificationService.sendPushNotification(message, "invitation_trip", invitedToUser,userData)
								//smsService.sendSMS("Mr/Ms. "+invitedToUser.firstName+", \n  "+invitedByUser.firstName+" wants join your Trip.\n\n Mobile: "+invitedByUser.phone+" \n\n Thank you!\n cloudacar.org Team.",invitedToUser.phone,"91")
							}

					}else{
						status=false
						responseData="failed"
						errorList.add("Error in invite user")
					}
				}else{
					status=false
					responseData="failed"
					errorList.add("invitedByTrip not found")
				}
			}else{
				status=false
				responseData="failed"
				errorList.add("invitedToTrip not found")
			}

		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}

	def cancelBookedTrip(String tripID){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		try {
			def trip=TripDetails.get(tripID)
			if(trip)
			{
				def tripAlerts=UserAlerts.findAllByTrip(trip)
				for(UserAlerts alert:tripAlerts)
				{
					alert.delete(flush:true,failOnError:true)
				}
				def inviteByTrips=InviteDetails.findAllByInvitedByTripID(trip)
				for(InviteDetails invite:inviteByTrips)
				{
					def tripAlerts1=UserAlerts.findAllByInvite(invite)
					for(UserAlerts alert:tripAlerts1)
					{
						alert.delete(flush:true,failOnError:true)
					}
					invite.delete(flush:true,failOnError:true)
				}
				def inviteToTrip=InviteDetails.findAllByInvitedToTripID(trip)
				for(InviteDetails invite:inviteToTrip)
				{
					def tripAlerts2=UserAlerts.findAllByInvite(invite)
					for(UserAlerts alert:tripAlerts2)
					{
						alert.delete(flush:true,failOnError:true)
					}
					invite.delete(flush:true,failOnError:true)
				}				
				trip.delete(flush:true,failOnError:true)
					status=true
					responseData="success"
					/*
					try {						
					def request = RequestContextHolder.currentRequestAttributes().request					
					File file=new File(grailsApplication.parentContext.servletContext.getRealPath("/")+"images/OtherFiles/routeFile/"+trip+".kml")
					//File file=new File(request.getSession().getServletContext().getRealPath("/")+"images/OtherFiles/routeFile/"+ trip+".kml")
					if(file.exists())
					{
						file.delete()
					}
					} catch (Exception e) {
						e.printStackTrace()
					}*/
					
			}
			else
			{
				status=false
				responseData="failed"
				errorList.add("Trip Not Found")
			}
		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}

	def performInvitationAction(String invitationId,String statusAction){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		try {
			def invite=InviteDetails.get(invitationId)
			if(invite){
				if(statusAction=="0"){
					invite.status="reject"
				}else if(statusAction=="1"){
					invite.status="accept"
				}
				invite.lastUpdatedOn=new Date()
				if(invite.save(flush:true,failOnError:true))
				{
					invite.invitedToTripID.lastUpdatedOn=new Date()
					invite.save(flush:true,failOnError:true)
					if(invite.invitedToTripID.tripStatus=="scheduled")
					{
						def InvitedByTrip=TripDetails.get(invite.invitedByTripID.id)
						if(InvitedByTrip){
							InvitedByTrip.tripStatus="scheduled"
							InvitedByTrip.lastUpdatedOn=new Date()
							InvitedByTrip.save(flush:true,failOnError:true)
						}
					}
					//Save Alert
					def userAlert=new UserAlerts()
					userAlert.sender=invite.invitedToUserID
					userAlert.receiver=invite.invitedByUserID
					userAlert.type="TripAlert"
					userAlert.status="Unread"
					def alertId
					if(statusAction=="0")
						alertId= AlertTypes.findByAlertType("RejectedReqest")
					else if(statusAction=="1")
						alertId= AlertTypes.findByAlertType("RequestAccepted")
					if(alertId)
						userAlert.alertType=alertId
						userAlert.createdOn=new Date()
						userAlert.lastUpdatedOn=new Date()
						userAlert.trip= invite.invitedByTripID
						if(userAlert.save(flush:true,failOnError:true))
						{
							Map userData=new HashMap<String,String>()
							userData.put("tripId", userAlert.trip.id)
							userData.put("alert_id", userAlert.alertType.id)
							userData.put("alertType", userAlert.alertType.alertType)
							userData.put("badge", UserAlerts.countByReceiver(invite.invitedByUserID))
							
							def message= alertId.message
							if(statusAction=="1")
								message=invite.invitedToUserID.firstName+" "+invite.invitedToUserID+" "+alertId.message
							else
								message=invite.invitedToUserID.firstName+" "+invite.invitedToUserID.lastName+" "+message
							notificationService.sendPushNotification(message, "notification_trip_rejected_request", invite.invitedByUserID,userData)
						}
					status=true
					responseData="success"

				}else{
					status=false
					responseData="failed"
					errorList.add("Error in invite user")
				}
			}
			else{
				status=false
				responseData="failed"
				errorList.add("Invitation details not found")
			}

		} 
		catch (Exception e) {
			e.printStackTrace()
		}
		finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}

	//To get Time in trip owner's timeZone
	String getTripTime(TripDetails matchedTrip)
	{
		def user=User.get(matchedTrip.user.id)

		//get Trip Time from database and Convert it to 12 Hour Clock and then Return
		HashMap<String, Object> tripTimeMap=dateTimeHelperService.convertTimeFromTwentyFourHoursToTwelveHours(matchedTrip.tripTime)
		String minutes
		if(tripTimeMap.get("minute")>=0 && tripTimeMap.get("minute")<=9)
		{
			minutes="0"+tripTimeMap.get("minute")
		}
		else
		{
			minutes=tripTimeMap.get("minute")
		}
		String hours= tripTimeMap.get("hour")==0? "12" :  tripTimeMap.get("hour")+""
		return hours+":"+minutes+" "+tripTimeMap.get("AM_PM")


	}
	
	def getAcceptedTrips(String vendorID){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def result, subResult
		def coPassengersList
		HashMap coPassengerMap
		List<HashMap<String, String>> tripList
		def hasTrips=false
		try {
			if(vendorID)
			{
				def vId=VendorDetails.get(Long.parseLong(vendorID.trim()).longValue())
				def criteria = TripDetails.createCriteria()
				result = criteria.list {
					like("tripStatus", "scheduled")
					and { eq("vendor", vId) }
					order("lastUpdatedOn", "desc")
					projections {
						property 'id'
						property 'startLocation'
						property 'endLocation'
						property 'purpose'
						property 'tripDistance'
						property 'tripStatus'
						property 'tripDate'
						property 'tripTime'
					}
				}
				HashMap<String ,String> map
	
				if(result.size()== 0)
				{
					hasTrips=false
					status = false
					responseData = "failed"
					errorList.add("Booked trip history not available")
				}
				else
				{
					tripList=new ArrayList()
					coPassengersList=new ArrayList()
					for(int i=0 ; i<result.size(); i++)
					{						
						hasTrips=true
						def trip=result[i]
						
						def tripData=TripDetails.get(trip[0])
						def vendorData=VendorDetails.get(tripData.vendor.id)
						def userData=User.get(tripData.user.id)
						def isAsCoTravellerExixts=InviteDetails.findByInvitedByUserIDAndStatus(userData,"accept")
						if(isAsCoTravellerExixts)
							continue
						map =new HashMap<String,String>()
						map.put("tripID", trip[0])
						map.put("startLocation", trip[1])
						map.put('endLocation', trip[2])
						map.put('purpose', trip[3])
						map.put('tripDistance', trip[4])
						map.put('tripStatus', trip[5])
						map.put('tripDate', trip[6])
						
						map.put('tripTime', convertTimeFromTwentyFourHoursToTwelveHours(trip[7]))
						map.put('vendorName',vendorData.companyName)
						map.put('vendorCarType', tripData.carType)
						map.put("department",userData.department)
						map.put("firstName",userData.firstName)
						map.put("lastName",userData.lastName)
						map.put("phone",userData.phone)
						def invitedTripId=TripDetails.get(tripData.id)
						def inviteTrips=InviteDetails.findByInvitedToTripID(invitedTripId)
						if(inviteTrips)
						{	
							def subCriteria=InviteDetails.createCriteria()
							subResult=subCriteria.list {
								like("status", "accept")
								and { eq("invitedToTripID",  invitedTripId) }
								projections {
									property 'id'
									property 'invitedByTripID'
									property 'invitedToTripID'
									property 'invitedByUserID'
									property 'invitedToUserID'
									property 'status'
								}
							}	
							def CoPasscount=0						
								for(int j=0 ; j<subResult.size(); j++)
								{
									coPassengerMap=new HashMap()
									def travellers=subResult[j]	
									def travellersData=InviteDetails.get(travellers[0])
									if(travellersData)
									{	
										CoPasscount++
										coPassengerMap.put("name", travellersData.invitedByUserID.firstName+" "+travellersData.invitedByUserID.lastName)
										coPassengerMap.put("startLocation", travellersData.invitedByTripID.startLocation)
										coPassengerMap.put("id", travellersData.invitedByUserID.id)
										coPassengerMap.put("tripId", travellersData.invitedByTripID.id)
										coPassengerMap.put("phone", travellersData.invitedByUserID.phone)
										coPassengersList.add(coPassengerMap)
									}
								}
								map.put("CoPasscount", CoPasscount)
								map.put("coTravellers",coPassengersList)
						}						
						tripList.add(map)
					}
					jsonData["acceptedTrips"]=tripList
					status=true
					responseData="success"
				}
			}
			else
			{
				status=false
				responseData="failed"
				errorList.add("Vendor Id Not Found")	
			}
		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}
	
	def getRejectedTrips(String vendorID){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def result
		List<HashMap<String, String>> tripList
		def hasTrips=false
		try {
			def vId=VendorDetails.get(vendorID)
			def criteria = TripDetails.createCriteria()
			result = criteria.list {
				like("tripStatus", "Rejected")
				and { eq("vendor", vId) }
				projections {
					property 'id'
					property 'startLocation'
					property 'endLocation'
					property 'purpose'
					property 'tripDistance'
					property 'tripStatus'
					property 'tripDate'
					property 'tripTime'
				}
			}


			HashMap<String ,String> map

			if(result.size()== 0)
			{
				hasTrips=false
				status = false
				responseData = "failed"
				errorList.add("Booked trip history not available")
			}
			else
			{
				tripList=new ArrayList()
				for(int i=0 ; i<result.size(); i++)
				{
					hasTrips=true
					def trip=result[i]

					def tripData=TripDetails.get(trip[0])
					def vendorData=VendorDetails.get(tripData.vendor.id)
					def userData=User.get(tripData.user.id)

					map =new HashMap<String,String>()
					map.put("tripID", trip[0])
					map.put("startLocation", trip[1])
					map.put('endLocation', trip[2])
					map.put('purpose', trip[3])
					map.put('tripDistance', trip[4])
					map.put('tripStatus', trip[5])
					map.put('tripDate', trip[6])
					map.put('tripTime', convertTimeFromTwentyFourHoursToTwelveHours(trip[7]))
					map.put('vendorName',vendorData.companyName)
					map.put('vendorCarType', tripData.carType)
					map.put("department",userData.department)
					map.put("firstName",userData.firstName)
					tripList.add(map)
				}

				jsonData["rejectedTrips"]=tripList
				status=true
				responseData="success"
			}

		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}

	static String convertTimeFromTwentyFourHoursToTwelveHours(String tripStartTime)
	{
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
			Date _24HourDt = _24HourSDF.parse(tripStartTime);
			SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
			return _12HourSDF.format(_24HourDt)
	}
	
}


