package com.bto.booking

import org.codehaus.groovy.grails.web.json.JSONObject;

import com.bto.customer.CarType;
import com.bto.customer.CostCenter;
import com.bto.customer.MatchingUserSkipReason;
import com.bto.customer.VendorDetails;
import com.google.gson.JsonNull;

import grails.converters.JSON
import grails.transaction.Transactional

@Transactional
class GetVendorAndCarDetailsService {

    def serviceMethod() {

    }
	
	def getDropDownList(){
		def jsonData=[:]
		def responseData
		def status=true
		def errorList=[]
		def vendorList=[]
		def costCenterList=[]
		def reasonsList=[]
		def result
		try {
			//Vendor List with Car Type
			def vendList=VendorDetails.list()
			for(VendorDetails vendor:vendList)
			{
				Map vendorMap=new HashMap<String,String>()
				vendorMap.put("vendorName", vendor.companyName)
				vendorMap.put("mobile", vendor.mobile)
				vendorMap.put("address", vendor.address)
				vendorMap.put("id", vendor.id)
				Map vendorCarMap
				def vendorCarList=[]
				def cartypes=vendor.carTypes.split(",")
				if(cartypes.size()>0)
				{
					for(def i=0;i<cartypes.size();i++)
					{
						def isCarExists=CarType.findByType(cartypes[i].trim())
						if(isCarExists)
						{
							vendorCarMap=new HashMap<String,String>()
							vendorCarMap.put("type", cartypes[i])
							vendorCarMap.put("id", isCarExists.id)
							vendorCarList.add(vendorCarMap)
						}
					}
				}
				vendorMap.put("carTypes", vendorCarList)
				vendorList.add(vendorMap)
				
				//Cost Center List
				def centerList=CostCenter.list()
				for(CostCenter center:centerList)
				{
					Map costCenter=new HashMap<String,String>()
					costCenter.put("id", center.id)
					costCenter.put("centerName", center.costCenterName)
					costCenterList.add(costCenter)
				}
				
				//reasons
				def resons=MatchingUserSkipReason.list()
				for(MatchingUserSkipReason reson:resons)
				{
					Map reasonMap=new HashMap<String,String>()
					reasonMap.put("id", reson.id)
					reasonMap.put("reason", reson.reasonDetails)
					reasonsList.add(reasonMap)
				}
			}	
			jsonData["vendorAndCars"]=vendorList
			jsonData["reasons"]=reasonsList
			jsonData["costCenters"]=costCenterList
			status=true
			responseData="success"

		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}
}
