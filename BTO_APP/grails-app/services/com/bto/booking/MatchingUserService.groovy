package com.bto.booking
import java.net.URLEncoder;
import java.net.HttpURLConnection;
import java.net.URL;

import com.bto.trip.InviteDetails;
import com.bto.trip.TripDetails
import com.sun.org.apache.xerces.internal.impl.dv.xs.PrecisionDecimalDV;

import grails.transaction.Transactional

import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression
import javax.xml.xpath.XPathFactory;

import org.codehaus.groovy.grails.commons.GrailsApplication;
import org.hibernate.SessionFactory
import org.springframework.web.context.request.RequestContextHolder;
import org.w3c.dom.Document;

@Transactional
class MatchingUserService {

	def grailsApplication
	def dateTimeHelperService
	SessionFactory sessionFactory
	private static final totalMatchingItems=8;
	
    def serviceMethod() {

    }
	
	def getMatchingUsers(TripDetails requestedTrip)
	{
		try{
			def session1=sessionFactory.currentSession
			def ArrayList<TripDetails> matchingUsers=new ArrayList<TripDetails>()
			ArrayList<TripDetails> allTrips=new ArrayList<TripDetails>()
			def originalTrip=requestedTrip
			def distanceLimit=java.lang.Double.parseDouble(grailsApplication.config.grails.distanceLimit.toString())
			
			String query="select t.* from trip_details as t left join user as c ON t.user_id=c.id where t.user_id <>"+requestedTrip.user.id;
			allTrips=session1.createSQLQuery(query).addEntity(TripDetails.class).list()
			for(TripDetails currentTrip: allTrips)
			{
				def isMainTrip=InviteDetails.findByInvitedByTripID(currentTrip)
				if(isMainTrip)
					if(!isMainTrip.vendor)
					continue;
				if(checkDate(currentTrip,requestedTrip))
				{
					if(originalTrip.user.id==currentTrip.user.id)
						continue;
					else
					{
						if(checkTripTime(requestedTrip, currentTrip)==false) {
							continue;
						}
					}
					def request = RequestContextHolder.currentRequestAttributes().request
					
					//File file=new File(grailsApplication.parentContext.servletContext.getRealPath("/")+"KMLFiles/"+ requestedTrip.routeName)
					File file=new File(request.getSession().getServletContext().getRealPath("/")+"images/OtherFiles/routeFile/"+ currentTrip.routeName)
					if(!file.exists())
					continue
					def xml = new XmlSlurper().parse(file)
					def cars = xml.depthFirst().findAll { it.name() == 'coordinates' }
					def temp=cars.toString().replaceAll("\n","#")
					
					def latlong_all =temp.tokenize('#')
					def i
					def currtrip_start_lon=java.lang.Double.parseDouble(originalTrip.startLog)
					def currtrip_start_lat=java.lang.Double.parseDouble(originalTrip.startLat)
					
					def currtrip_end_lon=java.lang.Double.parseDouble(originalTrip.endLog)
					def currtrip_end_lat=java.lang.Double.parseDouble(originalTrip.endLat)
					
					def dist,dist1
					def flg=false
					for(i=0;i<latlong_all.size();i++)
					{
						def usertrip_lon
						def usertrip_lat
						def latlong=latlong_all[i].split(',')
						
						if(latlong.size()==2)
						{
							if((latlong[0]).contains('['))
								usertrip_lon=java.lang.Double.parseDouble(latlong[1].substring(1,latlong[1].length()))
							else if((latlong[0]).contains(']'))
								usertrip_lon=java.lang.Double.parseDouble(latlong[1].substring(0,latlong[1].length()-1))
							else
								usertrip_lon=java.lang.Double.parseDouble(latlong[1])

							if((latlong[1]).contains('['))
								usertrip_lat=java.lang.Double.parseDouble(latlong[0].substring(1,latlong[0].length()))
							else if((latlong[1]).contains(']'))
								usertrip_lat=java.lang.Double.parseDouble(latlong[0].substring(0,latlong[0].length()-1))
							else
								usertrip_lat=java.lang.Double.parseDouble(latlong[0])

							def unit="K"
							if(flg==false)
							{
								double theta = currtrip_start_lon - usertrip_lon
								dist = Math.sin(deg2rad(currtrip_start_lat)) * Math.sin(deg2rad(usertrip_lat)) + Math.cos(deg2rad(currtrip_start_lat)) * Math.cos(deg2rad(usertrip_lat)) * Math.cos(deg2rad(theta));
								dist = Math.acos(dist);
								dist = rad2deg(dist);
								dist = dist * 60 * 1.1515;
								if (unit == 'K') {
									dist = dist * 1.609344;
								}
								dist=Math.round(dist)
								if(dist>=0 && dist<=2.2)
									flg=true
							}
							if(flg==true)
							{
								double theta = usertrip_lon- currtrip_end_lon ;
								dist1 = Math.sin(deg2rad(usertrip_lat)) * Math.sin(deg2rad(currtrip_end_lat)) + Math.cos(deg2rad(usertrip_lat)) * Math.cos(deg2rad(currtrip_end_lat)) * Math.cos(deg2rad(theta));
								dist1 = Math.acos(dist1);
								dist1 = rad2deg(dist1);
								dist1 = dist1 * 60 * 1.1515;
								if (unit == 'K') {
									dist1 = dist1 * 1.609344;
								}

								dist1=Math.round(dist1)
								
								if(dist1>=0 && dist1<=2)
								{
									//break;
								}
							}
						}
							if((dist>=0 && dist<=2) && (dist1>=0 && dist1<=2))
							{
								matchingUsers.add(currentTrip)
								break
							}
					}					
				}
			}
			HashMap<String, Object> tempMap=new HashMap<String, Object>()
			tempMap.put("trips", matchingUsers)
			return tempMap
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
	}
		
	def double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}
	  
	def double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}
	  
	def checkETA(String requestedTripStartLoc,String userTripStartLoc){
		def minutes=0
		try{
			def responseCode=0
			String api="https://maps.googleapis.com/maps/api/distancematrix/xml?origins="+URLEncoder.encode(requestedTripStartLoc, "UTF-8")+"&destinations="+URLEncoder.encode(userTripStartLoc, "UTF-8")+"&mode=driving"
			URL url = new URL(api);
			HttpURLConnection httpConnection = (HttpURLConnection)url.openConnection();
			httpConnection.connect();
			responseCode = httpConnection.getResponseCode();
			if(responseCode == 200)
			{
				DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();;
				Document document = builder.parse(httpConnection.getInputStream());
				XPathFactory xPathfactory = XPathFactory.newInstance();
				XPath xpath = xPathfactory.newXPath();
				XPathExpression expr = xpath.compile("//element/duration/value");
				def seconds=expr.evaluate(document, XPathConstants.STRING);
				minutes=TimeUnit.SECONDS.toMinutes(Long.parseLong(seconds.toString()))
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
		finally{
			return minutes
		}
	}
		  
	def checkTripTime(TripDetails userTrip,TripDetails currentTrip)
	{
		if(checkTripStartTime(userTrip,currentTrip))
		{			
			return true
		}
		else
		{			
			return false
		}
	}
	
	boolean checkTripStartTime(TripDetails userTrip,TripDetails currentTrip )
	{
		DateFormat formatter = new SimpleDateFormat("hh:mm");
		Date requestedDate = formatter.parse(userTrip.tripTime.toLowerCase());
		Date currentTripDate = formatter.parse(currentTrip.tripTime.toLowerCase());
		def minutes=Math.abs(((requestedDate.getTime()-currentTripDate.getTime())/1000)/60)
		Date currentDate
		//if((currCalendar.after(loweLimit)|| currCalendar.equals(loweLimit)) && (currCalendar.before(upperLimit)|| currCalendar.equals(upperLimit)))
		def timeLimit=grailsApplication.config.grails.startTripTimeSlotLimit+15
		if(minutes<=timeLimit)
			return true
		else
			return false
	}

	boolean checkDate(TripDetails currentTrip,TripDetails requestedTrip)
	{
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date currentTripDate = df.parse(currentTrip.tripDate.trim());
		Date requestedTripDate = df.parse(requestedTrip.tripDate.trim());
		currentTripDate.compareTo(requestedTripDate)
		try{
			if(currentTripDate.compareTo(requestedTripDate)==0)
				return true
			else
				return false
		}
		catch(Exception e){
			e.printStackTrace()
		}
	}	
}
