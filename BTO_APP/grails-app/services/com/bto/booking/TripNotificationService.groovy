package com.bto.booking

import com.bto.trip.TripDetails;
import com.bto.user.AlertTypes;
import com.bto.user.User;
import com.bto.user.UserAlertService;
import com.bto.user.UserAlerts;
import com.bto.user.UserDeviceToken;

import grails.gsp.PageRenderer;
import grails.transaction.Transactional
import java.text.SimpleDateFormat

@Transactional
class TripNotificationService {

	PageRenderer groovyPageRenderer
	def userAlertService
	def tripDetailsService
	def grailsApplication
	def dateTimeHelperService
	def serviceMethod() {
	}

	def getNotification(String userID){
		def jsonData = [:]
		def requestData = [:]
		def responseData
		def status = true
		def errorList = []
		def user
		ArrayList<HashMap<String, Object>> alertList
		def hasAlerts=false,tempUser,tempAlert
		HashMap<String, Object> outerMap
		HashMap<String, Object> innerMap

		try{
			user=User.get(Long.parseLong(userID.trim()))
			if(!user) {
				status = false
				responseData="failed"
				errorList.add("User not found,Invalid User Id")
			}
			if(status) {
				alertList=new ArrayList()
				def criteria = UserAlerts.createCriteria()
				def result = criteria.list {
					eq("receiver",user)
					ne("status","Read")
					order("lastUpdatedOn", "desc")
				}
				def alerts=UserAlerts.findAllByReceiver(user)
				for(def i=0;i<result.size();i++) {
					def alert=UserAlerts.get(result[i].getAt("id"))
					if(alert.status=="Canceled")
						continue;
					outerMap=new HashMap<String, Object>()
					innerMap=new HashMap<String, Object>()
					if(alert.alertType.alertType.equals("RejectedReqest")) {
						outerMap.put("type", "notification_trip_rejected_request")
						innerMap.put("alertType", alert.alertType.alertType)
						innerMap.put("tripId", alert.trip.id)
						innerMap.put("alert_id", alert.id)
						innerMap.put("status", alert.status)
						innerMap.put("alertDateTime", alert.lastUpdatedOn)
						if(alert.invite)
							innerMap.put("invitationId",alert.invite.id)
						else
							innerMap.put("invitationId","")
						def message= alert.alertType.message
						innerMap.put("message", alert.sender.firstName+" "+alert.sender.lastName+" "+message)
						innerMap.put("tripTime",convertTimeFromTwentyFourHoursToTwelveHours(alert.trip.tripTime))
						innerMap.put("tripDate",alert.trip.tripDate)
						innerMap.put("startLocation",alert.trip.startLocation)
						innerMap.put("endLocation",alert.trip.endLocation)
						outerMap.put("data", innerMap)
					}
					else if(alert.alertType.alertType.equals("RequestArrives")) {
						outerMap.put("type", "notification_trip_invite_request")
						innerMap.put("alertType", alert.alertType.alertType)
						innerMap.put("tripId", alert.trip.id)
						innerMap.put("sender_name", alert.sender.firstName+" "+alert.sender.lastName)
						innerMap.put("source_location", alert.trip.startLocation)
						innerMap.put("destination_location", alert.trip.endLocation)
						innerMap.put("carType",alert.trip.carType)
						innerMap.put("invitationId",alert.invite.id)
						innerMap.put("designation",alert.trip.user.designation)
						innerMap.put("department",alert.trip.user.department)
						innerMap.put("status", alert.status)
						innerMap.put("alertDateTime", alert.lastUpdatedOn)
						if(alert.sender.profilePicture){
							File file=new File("../images/userProfilePictures/"+alert.sender.profilePicture)
							if(file.exists())
								innerMap.put("user_profile_pic", grailsApplication.config.grails.webSite+"images/userProfilePictures/"+alert.sender.profilePicture)
							else
								innerMap.put("user_profile_pic", "")
						}
						else
							innerMap.put("user_profile_pic", "")
						innerMap.put("date", alert.trip.tripDate)
						innerMap.put("time", convertTimeFromTwentyFourHoursToTwelveHours(alert.trip.tripTime))
						innerMap.put("alert_id", alert.id)
						def message= alert.alertType.message
						message= message.toString().replace("start",alert.trip.startLocation)
						message=message.toString().replace("end",alert.trip.endLocation)
						message=message.replace("time", tripDetailsService.getTripTime(alert.trip))
						innerMap.put("message", message)
						outerMap.put("data", innerMap)
					}
					else if(alert.alertType.alertType.equals("RequestAccepted")) {
						outerMap.put("type", "notification_trip_accepted_request")
						innerMap.put("tripId", alert.trip.id)
						innerMap.put("alertType", alert.alertType.alertType)
						innerMap.put("alert_id", alert.id)
						innerMap.put("status", alert.status)
						innerMap.put("alertDateTime", alert.lastUpdatedOn)
						if(alert.invite)
							innerMap.put("invitationId",alert.invite.id)
						else
							innerMap.put("invitationId","")
						def message= alert.alertType.message
						innerMap.put("message", alert.sender.firstName+" "+alert.sender.lastName+" "+message)
						innerMap.put("tripTime",convertTimeFromTwentyFourHoursToTwelveHours(alert.trip.tripTime))
						innerMap.put("tripDate",alert.trip.tripDate)
						innerMap.put("startLocation",alert.trip.startLocation)
						innerMap.put("endLocation",alert.trip.endLocation)
						outerMap.put("data", innerMap)
					}
					else if(alert.alertType.alertType.equals("RejectedByVendor")) {
						outerMap.put("type", "notification_trip_rejectedByVendor")
						innerMap.put("alertType", alert.alertType.alertType)
						innerMap.put("tripId", alert.trip.id)
						innerMap.put("alert_id", alert.id)
						innerMap.put("status", alert.status)
						innerMap.put("alertDateTime", alert.lastUpdatedOn)
						//	innerMap.put("invitationId",alert.invite.id)
						def message= alert.alertType.message
						innerMap.put("message", alert.vendor.companyName+" "+message)
						innerMap.put("tripTime",convertTimeFromTwentyFourHoursToTwelveHours(alert.trip.tripTime))
						innerMap.put("tripDate",alert.trip.tripDate)
						innerMap.put("startLocation",alert.trip.startLocation)
						innerMap.put("endLocation",alert.trip.endLocation)
						outerMap.put("data", innerMap)
					}
					else if(alert.alertType.alertType.equals("AcceptedByVendor")) {
						outerMap.put("type", "notification_trip_acceptedByVendor")
						innerMap.put("alertType", alert.alertType.alertType)
						innerMap.put("tripId", alert.trip.id)
						innerMap.put("alert_id", alert.id)
						innerMap.put("status", alert.status)
						innerMap.put("alertDateTime", alert.lastUpdatedOn)
						//	innerMap.put("invitationId",alert.invite.id)
						def message= alert.alertType.message
						innerMap.put("message", alert.smsText)
						innerMap.put("tripTime",convertTimeFromTwentyFourHoursToTwelveHours(alert.trip.tripTime))
						innerMap.put("tripDate",alert.trip.tripDate)
						innerMap.put("startLocation",alert.trip.startLocation)
						innerMap.put("endLocation",alert.trip.endLocation)
						outerMap.put("data", innerMap)
					}
					else if(alert.alertType.alertType.equals("EventCreated")) {
						innerMap.put("sender_name", alert.sender.firstName+" "+alert.sender.lastName)
						outerMap.put("type", "notification_event_created")
						innerMap.put("alertType", alert.alertType.alertType)
						innerMap.put("alert_id", alert.id)
						innerMap.put("status", alert.status)
						innerMap.put("alertDateTime", alert.lastUpdatedOn)
						def message= alert.alertType.message
						innerMap.put("message", message)
						innerMap.put("eventTime",convertTimeFromTwentyFourHoursToTwelveHours(alert.event.eventTime))
						innerMap.put("eventDate",alert.event.eventDate)
						innerMap.put("eventName",alert.event.eventName)
						innerMap.put("eventVenue",alert.event.eventVenue)
						innerMap.put("eventCode",alert.event.eventCode)
						innerMap.put("costCenter",alert.event.costCenter)
						outerMap.put("data", innerMap)
					}
					alertList.add(outerMap)
				}
				responseData="success"
			}
		}
		catch(Exception e) {
			e.printStackTrace()
		}
		finally {
			jsonData['response'] = responseData
			jsonData['status'] = status
			jsonData['errorList'] = errorList
			jsonData['alerts']=alertList
			return jsonData
		}
	}

	static String convertTimeFromTwentyFourHoursToTwelveHours(String tripStartTime) {
		SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
		Date _24HourDt = _24HourSDF.parse(tripStartTime);
		SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
		return _12HourSDF.format(_24HourDt)
	}
}
