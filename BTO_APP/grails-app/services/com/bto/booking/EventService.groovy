package com.bto.booking

import com.bto.trip.EventDetails;
import com.bto.user.AlertTypes;
import com.bto.user.User;
import com.bto.user.UserAlerts

import grails.transaction.Transactional

import java.text.DateFormat
import java.text.SimpleDateFormat

@Transactional
class EventService {
	def notificationService
	def grailsApplication


	def serviceMethod() {
	}

	def createEvent(String userID,String eventName,String eventType,String eventVenue,String eventDate,String eventTime, String costCenter, String eventCode, String description){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		try {
			def user=User.get(userID)
			if(user){
				def eventDetails=new EventDetails()

				eventDetails.user=user
				eventDetails.eventName=eventName
				eventDetails.eventType=eventType
				eventDetails.eventVenue=eventVenue
				eventDetails.eventDate=eventDate
				eventDetails.eventTime=eventTime
				eventDetails.costCenter=costCenter
				eventDetails.eventCode=eventCode
				eventDetails.eventStatus="scheduled"
				eventDetails.description=description
				eventDetails.createdOn = new Date()
				eventDetails.lastUpdatedOn =new Date()

				if(eventDetails.save(flush:true,failOnError:true)){

					def event=EventDetails.get(eventDetails.id)
					def criteria = User.createCriteria()
					def result = criteria.list {
						projections { property 'id' }
					}
					if(result.size()== 0) {
						//errorList.add("Users not available to send event notification")
					}
					else {
						for(int i=0 ; i<result.size(); i++) {
							def singleUser=User.get(result[i])
							//Save Alert
							def userAlert=new UserAlerts()
							userAlert.sender=user
							userAlert.receiver=singleUser
							userAlert.type="EventAlert"
							userAlert.status="Unread"
							def alertId= AlertTypes.findByAlertType("EventCreated")
							if(alertId)
							userAlert.alertType=alertId
							userAlert.createdOn=new Date()
							userAlert.lastUpdatedOn=new Date()
							userAlert.event=event
							if(userAlert.save(flush:true,failOnError:true))
							{
								Map userData=new HashMap<String,String>()
								//userData.put("tripId", userAlert.trip.id)
								userData.put("alert_id", userAlert.alertType.id)
								userData.put("alertType", userAlert.alertType.alertType)
								userData.put("badge", UserAlerts.countByReceiver(singleUser))

								def message= alertId.message

								message=user.firstName+" "+user.lastName+" "+message
								notificationService.sendPushNotification(message, "notification_event_created",singleUser,userData)
							}
						}
					}

					status=true
					responseData="success"
					jsonData['eventId']=eventDetails.id
					jsonData['userId']=userID
				}else{
					status=false
					responseData="failed"
					errorList.add("Error in creating event")
				}
			}else{
				status=false
				responseData="failed"
				errorList.add("Invalid user id for creating event")
			}
		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}

	def getEvents(String userID){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def result
		List<HashMap<String, String>> eventList
		try {
			def uId=User.get(userID)
			if(uId){
				def criteria = EventDetails.createCriteria()
				result = criteria.list {

					projections {
						property 'id'
						property 'eventName'
						property 'eventType'
						property 'eventVenue'
						property 'eventDate'
						property 'eventTime'
						property 'costCenter'
						property 'eventCode'
						property 'eventStatus'
						property 'description'
					}
				}
				HashMap<String ,String> map

				if(result.size()== 0) {
					status = false
					responseData = "failed"
					errorList.add("Events not available")
				}
				else {
					eventList=new ArrayList()

					for(int i=0 ; i<result.size(); i++) {
						def event=result[i]
						def eventData=EventDetails.get(event[0])
						def userData=User.get(eventData.user.id)

						map =new HashMap<String,String>()
						map.put("eventID", event[0])
						map.put("eventName", event[1])
						map.put('eventType', event[2])
						map.put('eventVenue', event[3])
						map.put('eventDate', event[4])
						Calendar calendar = Calendar.getInstance();
						DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						Date date= df.parse(event[4]);
						calendar.setTime(date);
						map.put('day', new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()))
						map.put('eventTime', event[5])
						map.put('costCenter', event[6])
						map.put('eventCode', event[7])
						map.put('eventStatus', event[8])
						map.put('description', event[9])
						map.put('eventCreatedBy', userData.firstName+" "+userData.lastName)
						eventList.add(map)
					}

					jsonData["eventList"]=eventList
					status=true
					responseData="success"
				}
			}else{
				status = false
				responseData = "failed"
				errorList.add("Invalid user to get all events")
			}
		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}
	def cancelEvent(String eventId){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		try {
			def eventData=EventDetails.get(eventId)
			if(eventData){
				def eventAlert=UserAlerts.findAllByEvent(eventData)
				if(eventAlert){
					for(UserAlerts alert:eventAlert)
					{
						alert.delete(flush:true,failOnError:true)
					}
					eventData.delete(flush:true,failOnError:true)
					status=true
					responseData="success"
				}else{
				eventData.delete(flush:true,failOnError:true)
				status=true
				responseData="success"
				}
			}else{
				status=false
				responseData="failed"
				errorList.add("Event Not Found")
				}
			} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}
}
