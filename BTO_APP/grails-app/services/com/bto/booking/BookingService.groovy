package com.bto.booking

import com.bto.customer.CarType;
import com.bto.customer.VendorDetails;
import com.bto.trip.TripDetails
import com.bto.user.User;
import com.bto.user.UserAlerts;
import com.bto.utilities.DistanceAndLatLagFromAddress;
import com.bto.utilities.GetKMLFile;
import com.bto.utilities.UtilitiesService

import grails.transaction.Transactional

import java.text.DateFormat
import java.text.SimpleDateFormat

@Transactional
class BookingService {

	def dateTimeHelperService
	def getKMLFileService
	def serviceMethod() {
	}

	def bookTrip(String startLocation,String endLocation,String purpose,String tripDate,String tripTime,String userID, String vendorID, String carType, String costCenter, String eventCode, String note){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		def user,vendor
		try {
			user=User.get(userID)
			if(!user)
			{
				status=false
				responseData="failed"
				errorList.add("User Not Found")
			}
			else
			{
				vendor=VendorDetails.get(vendorID)
				if(!vendor)
				{
					status=false
					responseData="failed"
					errorList.add("Vendor Not Found")
				}				
			}
			if(status)
			{
				//get  mySql format timestamp
				def date=new UtilitiesService().newDate()
				def tripDetails=new TripDetails()
				//get start lat lag and end lat log and distance between start location and end location
				def distanceAndLatLagFromAddress=DistanceAndLatLagFromAddress.getDisatnceLatlagFromAddress(startLocation, endLocation)
				
				tripDetails.startLocation=startLocation
				tripDetails.startLat=Arrays.toString(distanceAndLatLagFromAddress[0]).replace("[", "").replace("]", "")
				tripDetails.startLog=Arrays.toString(distanceAndLatLagFromAddress[1]).replace("[", "").replace("]", "")
				tripDetails.endLocation=endLocation
				tripDetails.endLat=Arrays.toString(distanceAndLatLagFromAddress[2]).replace("[", "").replace("]", "")
				tripDetails.endLog=Arrays.toString(distanceAndLatLagFromAddress[3]).replace("[", "").replace("]", "")
				tripDetails.tripDistance=Float.parseFloat(distanceAndLatLagFromAddress[4])
				tripDetails.tripStatus="requested"
				tripDetails.purpose=purpose
				tripDetails.tripDate=tripDate
				HashMap timeMap=dateTimeHelperService.convertTimeFromTwelveToTwentyFourHours(tripTime)
				tripDetails.tripTime=timeMap.get("hour")+":"+timeMap.get("minute")
				tripDetails.costCenter=costCenter
				tripDetails.eventCode=eventCode				
				tripDetails.user=user
				tripDetails.vendor=vendor
				tripDetails.carType=carType			
				tripDetails.note=note			
				tripDetails.createdOn = date
				tripDetails.lastUpdatedOn =date
	
				if(tripDetails.save(flush:true,failOnError:true)){
					status=true
					responseData="success"
					jsonData['tripId']=tripDetails.id
					jsonData['userId']=userID
					
					def getTripID=tripDetails.id
					
					// Generating KML file with name trip id e.g. 1.kml
					def genarateKMLFile=getKMLFileService.genarateKML(startLocation, endLocation,getTripID.toString())
					if(genarateKMLFile){
						if(tripDetails.save(flush:true,failOnError:true)){
							tripDetails.routeName=genarateKMLFile
						}
						else{
							status=false
							responseData="failed"
							errorList.add("Error in saving KML File path")
						}
					}
					else{
						status=false
						responseData="failed"
						errorList.add("Error in creating KML File")
					}
				}
				else{
					status=false
					responseData="failed"
					errorList.add("Error in creating Trip")
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace()
		}
		finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}
	
	def updateTrip(String startLocation,String endLocation,String purpose,String tripDate,String tripTime,String userID, String vendorID, String carType, String costCenter, String eventCode,TripDetails tripDetails,String alertId){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		try {
			def date=new UtilitiesService().newDate()
			def distanceAndLatLagFromAddress=DistanceAndLatLagFromAddress.getDisatnceLatlagFromAddress(startLocation, endLocation)
			
			tripDetails.startLocation=startLocation
			tripDetails.startLat=Arrays.toString(distanceAndLatLagFromAddress[0]).replace("[", "").replace("]", "")
			tripDetails.startLog=Arrays.toString(distanceAndLatLagFromAddress[1]).replace("[", "").replace("]", "")
			tripDetails.endLocation=endLocation
			tripDetails.endLat=Arrays.toString(distanceAndLatLagFromAddress[2]).replace("[", "").replace("]", "")
			tripDetails.endLog=Arrays.toString(distanceAndLatLagFromAddress[3]).replace("[", "").replace("]", "")
			tripDetails.tripDistance=Float.parseFloat(distanceAndLatLagFromAddress[4])

			tripDetails.tripStatus="requested"
			tripDetails.purpose=purpose
			tripDetails.tripDate=tripDate
			HashMap timeMap=dateTimeHelperService.convertTimeFromTwelveToTwentyFourHours(tripTime)
			tripDetails.tripTime=timeMap.get("hour")+":"+timeMap.get("minute")
			tripDetails.costCenter=costCenter
			tripDetails.eventCode=eventCode
			
			def user=User.get(userID)
			def vendor=VendorDetails.get(vendorID)
			tripDetails.user=user
			tripDetails.vendor=vendor
			tripDetails.carType=carType			
			tripDetails.createdOn = date
			tripDetails.lastUpdatedOn =date

			if(tripDetails.save(flush:true,failOnError:true)){
				status=true
				responseData="success"
				jsonData['tripId']=tripDetails.id
				jsonData['userId']=userID
				
				def getTripID=tripDetails.id
				
				// Generating KML file with name trip id e.g. 1.kml
				def genarateKMLFile=getKMLFileService.genarateKML(startLocation, endLocation,getTripID.toString())
				if(genarateKMLFile){
					tripDetails.routeName=genarateKMLFile
					if(tripDetails.save(flush:true,failOnError:true)){
						jsonData['tripId']=tripDetails.id
						jsonData['userId']=userID
						def isAlert=UserAlerts.get(Long.parseLong(alertId))
						if(isAlert)
						{
							isAlert.status="Read"
							isAlert.lastUpdatedOn=new Date()
							if(isAlert.save(flush:true,failOnError:true))
							{
								status=true
								responseData="success"
							}
						}
					}
					else{
						status=false
						responseData="failed"
						errorList.add("Error in saving KML File path")
					}
				}
				else{
					status=false
					responseData="failed"
					errorList.add("Error in creating KML File")
				}
			}
			else{
				status=false
				responseData="failed"
				errorList.add("Error in creating Trip")
			}
		}
		catch (Exception e) {
			e.printStackTrace()
		}
		finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}
}
