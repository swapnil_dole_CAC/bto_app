package com.bto.Billing

import com.bto.customer.VendorDetails;
import com.bto.vendor.VendorBilling;
import grails.transaction.Transactional

@Transactional
class BillingService {

    def serviceMethod() {

    }
	
	def saveBillPackage(String packageType,String kilometer,String amount,VendorDetails vendor,String hour)
	{
		def jsonData=[:]
		def responseData
		def status=true
		def errorList=[]
		try{
			if(!kilometer)
			{
				status=false
				responseData="false"
				errorList.add("Invalid Kilometer Value")
			}
			else if(!amount)
			{
				status=false
				responseData="false"
				errorList.add("Invalid Amount")
			}
			else if(packageType=="2" && (hour=="" || hour==null))
			{
				status=false
				responseData="false"
				errorList.add("Invalid Hour Value")
			}
			if(status)
			{
				if(packageType.equals("1"))
				{
					def BillPackage=new VendorBilling()
					BillPackage.vendor=vendor
					BillPackage.kilometer=Double.parseDouble(kilometer)
					BillPackage.amount=Double.parseDouble(amount)
					BillPackage.packageType=1
					BillPackage.createdOn=new Date()
					BillPackage.lastUpdatedOn=new Date()
					if(BillPackage.save(flush:true,failOnError:true))
					{
						responseData="success"
					}
				}
				else if(packageType.equals("2"))
				{
					def BillPackage=new VendorBilling()
					BillPackage.vendor=vendor
					BillPackage.kilometer=Double.parseDouble(kilometer)
					BillPackage.amount=Double.parseDouble(amount)
					BillPackage.hour=Double.parseDouble(hour)
					BillPackage.packageType=2
					BillPackage.createdOn=new Date()
					BillPackage.lastUpdatedOn=new Date()
					if(BillPackage.save(flush:true,failOnError:true))
					{
						responseData="success"
					}
				}
			}
			else
			{
				status=false
				responseData="false"
				errorList.add("Internal Server Error")
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
		finally{
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}

	def updateBillPackage(VendorBilling billinstance,String packageType,String kilometer,String amount,VendorDetails vendor,String hour){
		def jsonData=[:]
		def responseData
		def status=true
		def errorList=[]
		try{
			if(!kilometer)
			{
				status=false
				responseData="false"
				errorList.add("Invalid Kilometer Value")
			}
			else if(!amount)
			{
				status=false
				responseData="false"
				errorList.add("Invalid Amount")
			}
			else if(packageType=="2" && (hour=="" || hour==null))
			{
				status=false
				responseData="false"
				errorList.add("Invalid Hour Value")
			}
			if(status)
			{
				if(packageType.equals("1"))
				{			
					billinstance.kilometer=Double.parseDouble(kilometer)
					billinstance.amount=Double.parseDouble(amount)
					billinstance.packageType=1
					billinstance.lastUpdatedOn=new Date()
					if(billinstance.save(flush:true,failOnError:true))
					{
						responseData="success"
					}
				}
				else if(packageType.equals("2"))
				{
					billinstance.kilometer=Double.parseDouble(kilometer)
					billinstance.amount=Double.parseDouble(amount)
					billinstance.hour=Double.parseDouble(hour)
					billinstance.packageType=2
					billinstance.lastUpdatedOn=new Date()
					if(billinstance.save(flush:true,failOnError:true))
					{
						responseData="success"
					}
				}
			}
			else
			{
				status=false
				responseData="false"
				errorList.add("Internal Server Error")
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
		finally{
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}	

	def deleteBillPackage(VendorBilling billInstance)
	{
		def jsonData=[:]
		def responseData
		def status=true
		def errorList=[]
		try{
			billInstance.delete(flush:true,failOnError:true)
			status=true
			responseData="success"
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
		finally{
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}	
}
