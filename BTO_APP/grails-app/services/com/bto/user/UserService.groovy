package com.bto.user

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Formatter.DateTime;

import javax.websocket.Session;

import org.springframework.mail.MailSender;

import grails.plugin.mail.MailService;

import org.apache.commons.lang.RandomStringUtils;

import com.bto.trip.InviteDetails;
import com.bto.trip.TripDetails;
import com.bto.utilities.SmsService;
import com.bto.utilities.UtilitiesService

import grails.converters.JSON
import grails.gsp.PageRenderer;
import grails.transaction.Transactional

@Transactional
class UserService {
	def smsService
	PageRenderer groovyPageRenderer

	def serviceMethod() {
	}

	def registerUser(String emailID,String password,String firstName,String lastName,String phone,String costCenter,String deviceType,String deviceToken){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		try{
			def corporateUser=CorporateUser.findByEmailIDAndStatus(emailID,1)
			if(corporateUser){
				/*jsonData['firstName']=corporateUser.firstName
				 jsonData['lastName']=corporateUser.lastName
				 jsonData['userId']=corporateUser.id
				 jsonData['status']=corporateUser.status*/

				def isExistsEmail=User.findByEmailID(emailID)
				if(!isExistsEmail) {
					def isExistsMob=User.findByPhone(phone)
					if(!isExistsMob) {

						def date=new UtilitiesService().newDate()
						Calendar calendar = new GregorianCalendar();
						TimeZone timeZone = calendar.getTimeZone();

						def BTOUser =new User()
						BTOUser.emailID=emailID
						BTOUser.password=password
						BTOUser.firstName=firstName
						BTOUser.lastName=lastName
						BTOUser.phone=phone
						BTOUser.costCenter=costCenter
						BTOUser.status=true
						BTOUser.department="-"
						BTOUser.designation="-"
						BTOUser.profilePicture=""
						BTOUser.timeZone=timeZone.getDefault().getID()
												
						
						BTOUser.createdOn =  new Date()
						BTOUser.lastUpdatedOn =new Date()


						if(BTOUser.save(flush:true,failOnError:true)) {

							status:true
							responseData="success"

							jsonData['emailID']=BTOUser.emailID
							jsonData['firstName']=BTOUser.firstName
							jsonData['lastName']=BTOUser.lastName
							jsonData['userId']=BTOUser.id
							jsonData['status']=BTOUser.status
							jsonData['phone']=BTOUser.phone
							jsonData['costCenter']=BTOUser.costCenter
							
							jsonData['gender']=BTOUser.gender
							jsonData['profilePicture']=BTOUser.profilePicture
							jsonData['department']=BTOUser.department
							jsonData['designation']=BTOUser.designation


							def userId=User.findByEmailID(emailID)
							if(userId){
								def randomInt=new UtilitiesService().getRandomNumber()
								def randomInt1=new UtilitiesService().getRandomNumber()
								//println "=================from verification============"

								//saving verification Details
								def verificationData=new VerificationDetails()
								verificationData.phoneVerificationCode=randomInt1
								verificationData.isPhoneVerified=false
								verificationData.createdOn=  new Date()
								verificationData.lastUpdatedOn =  new Date()
								verificationData.user=userId
								verificationData.emailID=emailID
								//sending verification code to user

								smsService.sendSMS("Dear "+firstName+", Welcome to Business Travel Optimizer ! Your mobile verification code is "+ randomInt1+". Thank you!"+" cloudacar.org Team.", phone,"91")

								if(verificationData.save(flush:true,failOnError:true)){
									status=true
									responseData="success"

									// device token service
									def charset = (('A'..'Z')).join()
									def length = 5
									def randomString = RandomStringUtils.random(length, charset.toCharArray())
									
									 def userDeviceToken=new UserDeviceToken()
									 userDeviceToken.deviceType=deviceType
									 userDeviceToken.deviceToken=deviceToken
									 userDeviceToken.status=false
									 userDeviceToken.createdOn=new Date()
									 userDeviceToken.lastUpdatedOn=new Date()
									 userDeviceToken.apitoken=randomString
									 userDeviceToken.user=userId
									if(userDeviceToken.save(flush:true,failOnError:true)){
										
									}else{
									status=false
									responseData="failed"
									errorList.add("Error in store device token")
								}

								}else{
									status=false
									responseData="failed"
									errorList.add("Error in user Verification")
								}
							}else{
								status=false
								responseData="failed"
								errorList.add("Error in getting userID")
							}
						}
						else{
							status=false
							responseData="failed"
							errorList.add("Error in Register user")
						}
					}
					else {
						status=false
						responseData="failed"
						errorList.add("Mobile Number Already Exists")
					}
				}
				else {
					status=false
					responseData="failed"
					errorList.add("Email Address Already Exists")
				}
			}else{
				status=false
				responseData="failed"
				errorList.add("User authentication failed")
			}
		}catch(Exception e) {
			e.printStackTrace()
		}
		finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}

	def authenticateUser(String emailID,String password,String deviceToken,String deviceType){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		try {
			def reguser=User.findByEmailID(emailID)
			if(reguser)
			{
				def user=User.findByEmailIDAndPassword(emailID,password)
				if(user)
				{
					def isDeviceExists=UserDeviceToken.findByDeviceTokenAndUser(deviceToken,reguser)
					if(isDeviceExists)
					{
						// device token service
						def charset = (('A'..'Z')).join()
						def length = 5
						def randomString = RandomStringUtils.random(length, charset.toCharArray())
						isDeviceExists.status=true
						isDeviceExists.lastUpdatedOn=new Date()
						isDeviceExists.apitoken=randomString
						isDeviceExists.save(flush:true,failOnError:true)
					}
					else
					{
						// device token service
						def charset = (('A'..'Z')).join()
						def length = 5
						def randomString = RandomStringUtils.random(length, charset.toCharArray())
						def userDeviceToken=new UserDeviceToken()
						userDeviceToken.deviceType=deviceType
						userDeviceToken.deviceToken=deviceToken
						userDeviceToken.status=true
						userDeviceToken.createdOn=new Date()
						userDeviceToken.lastUpdatedOn=new Date()
						userDeviceToken.apitoken=randomString
						userDeviceToken.user=reguser
					   if(userDeviceToken.save(flush:true,failOnError:true)){}
					}
					def userDetails=User.findByEmailID(emailID)
					def verifyUser=VerificationDetails.findByEmailIDAndIsPhoneVerified(emailID,true)
					if(userDetails)
					{
						status=true
						responseData="success"
						jsonData['emailID']=user.emailID
						jsonData['firstName']=user.firstName
						jsonData['lastName']=user.lastName
						jsonData['userId']=user.id
						jsonData['phone']=user.phone
						jsonData['costCenter']=user.costCenter
						jsonData['gender']=user.gender
						jsonData['profilePicture']=user.profilePicture
						jsonData['department']=user.department
						jsonData['designation']=user.designation
	
						if(verifyUser){
							jsonData['isPhoneVerified']=true
						}else{						
							jsonData['isPhoneVerified']=false
						}
	
					}else{
						status=false
						responseData="failed"
						errorList.add("Error in login user")
					}

				}else{
					status=false
					responseData="failed"
					errorList.add("Email address or password does not match")
				}
			}
			else
			{
				status=false
				responseData="failed"
				errorList.add("Email address not registered , please register to Login")
			}
		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}

	}

	def changePassword(String emailID,String oldPassword, String newPassword){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		try {
			def user=User.findByEmailIDAndPassword(emailID,oldPassword)

			if(user)
			{
				user.password=newPassword

				if(user.save(flush:true,failOnError:true))
				{
					status=true
					responseData="success"
					jsonData['firstName']=user.firstName
					jsonData['lastName']=user.lastName
					//jsonData['userId']=user.id
					//jsonData['status']=user.status
				}
				else
				{
					status=false
					responseData="failed"
					errorList.add("Problem in change password")
				}

			}
			else
			{
				status=false
				responseData="failed"
				errorList.add("Old password does not match")
			}
		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}

	}

	def forgetPassword(String emailID){
		def jsonData = [:]
		def requestData = [:]
		def responseData
		def errorList = []
		def status = true
		try
		{
			def user = User.findByEmailID(emailID)

			if(!user)
			{
				status = false
				requestData="failed"
				errorList.add("Invalid email address")
			}
			if(user && status==true && errorList?.size()<=0)
			{
				status = false
				def charset = (('A'..'Z')).join()
				def length = 5
				def randomString = RandomStringUtils.random(length, charset.toCharArray())
				//def randomInt=new UtilitiesService().getRandomNumber()

				//Saving password in database for that user
				user.password=randomString

				// failOnError:true
				if(user.save(flush: true))
				{
					// Sending new password to Email address
					Map data=new HashMap<String, String>()
					data.put("name", user.firstName)
					data.put("password", randomString)
					data.put("email", user.emailID)
					def content=groovyPageRenderer.render(view: '/htmlEmailTemplates/forgotPasswordEmailTemplate', model : [ userData : data] )
					sendMail {
						from 'info@cloudacar.org'
						to user.emailID
						subject ("BTO account new password")
						html content
					}
					status = true
					responseData = "success"
					requestData['message']="Password is Sent to your Email"

				}
				else
				{
					status = false
					responseData = "failed"
					errorList.add("Failed to verify data")
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
			status = false
			errorList.add("Internal server error")
		}
		finally
		{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}

	def authenticateCorporateUser(String emailID,String firstName,String lastName){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]

		try {

			def corporateUser=CorporateUser.findByEmailIDAndStatus(emailID,'true')
			if(corporateUser){
				//println "from corporate user service"
				status=true
				responseData="success"
				jsonData['firstName']=corporateUser.firstName
				jsonData['lastName']=corporateUser.lastName
				jsonData['userId']=corporateUser.id
				jsonData['status']=corporateUser.status
			}else{
				status=false
				responseData="failed"
				errorList.add("User authentication failed")
			}

		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}

	def userVerification(String emailID,String verificationCode, String phone, String firstName){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]


		try {
			def userVerification=VerificationDetails.findByEmailID(emailID)
			if(userVerification){
				if(userVerification.phoneVerificationCode.equalsIgnoreCase(verificationCode)){
					status=true
					responseData="success"
					//change status of verification details
					def date=new UtilitiesService().newDate()
					def phoneVerification=VerificationDetails.findByEmailID(emailID)
					phoneVerification.isPhoneVerified=true
					phoneVerification.lastUpdatedOn=new Date()

					if(phoneVerification.save(flush:true,failOnError:true)){
						status=true
						responseData="success"
						//println "print UserID from User service"+userVerification.user.id
						
						jsonData['userId']=userVerification.user.id
						jsonData['emailID']=userVerification.emailID
						jsonData['phone']=phone
						jsonData['firstName']=firstName
					}else{
						status=false
						responseData="failed"
						errorList.add("Error in updating phone verification details")
						jsonData['userId']=userVerification.user.id
						jsonData['emailID']=userVerification.emailID
						jsonData['phone']=phone
						jsonData['firstName']=firstName
					}

				}else{
					status=false
					responseData="failed"
					errorList.add("Invalid verification code")
					jsonData['emailID']=userVerification.emailID
					jsonData['phone']=phone
					jsonData['firstName']=firstName
				}

			}else{
				status=false
				responseData="failed"
				errorList.add("Invalid User ID in verification details")
			}

		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}

	}

	def resendVerificationCode(String emailID , String phone, String firstName){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]
		try {
			def randomInt=new UtilitiesService().getRandomNumber()

			def userVerification=VerificationDetails.findByEmailID(emailID)
			userVerification.phoneVerificationCode=randomInt

			smsService.sendSMS("Dear "+firstName+", Welcome to Business Travel Optimizer ! Your mobile verification code is "+ randomInt+". Thank you!"+" cloudacar.org Team.", phone,"91")


			if(userVerification.save(flush:true,failOnError:true)){
				status=true
				responseData="success"
				jsonData['emailID']=userVerification.emailID
				jsonData['phone']=phone
				jsonData['firstName']=firstName

			}else{
				status=false
				responseData="failed"
				errorList.add("Invalid User ID in verification details")
				jsonData['emailID']=userVerification.emailID
				jsonData['phone']=phone
				jsonData['firstName']=firstName
			}

		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}
	}

	def deleteUser(String emailID){
		def jsonData=[:]
		def requestData=[:]
		def responseData
		def status=true
		def errorList=[]


		try {
			def deleteUserFromVerification=VerificationDetails.executeUpdate("delete VerificationDetails where emailID='"+emailID+"'")
			if(deleteUserFromVerification){
				//println userVerification.phoneVerificationCode
				//Person.executeUpdate("delete Person where lastName='Doe'")
				status=true
				responseData="success"
				def userID=User.findByEmailID(emailID)
			//	println userID.id
				def deleteUserFromTrip=TripDetails.executeUpdate("delete TripDetails where user="+userID.id)
				if(deleteUserFromTrip){
					status=true
					responseData="success"
				}else{
					status=false
					responseData="failed"
					errorList.add("Invalid User ID in delete user from trip details")
				}
				
				def deleteUserFromInviteDetails=InviteDetails.executeUpdate("delete InviteDetails where invitedByID="+userID.id+ " or invitedToID="+userID.id)
				if(deleteUserFromInviteDetails){
					status=true
					responseData="success"
				}else{
					status=false
					responseData="failed"
					errorList.add("Invalid User ID in delete user from InviteDetails")
				}
				
				def deleteUserFromUserDevicetoken=UserDeviceToken.executeUpdate("delete UserDeviceToken where user="+userID.id)
				if(deleteUserFromUserDevicetoken){
					status=true
					responseData="success"
				}else{
					status=false
					responseData="failed"
					errorList.add("Invalid User ID in delete user from UserFromUserDevicetoken details")
				}
				
				
				def deleteUserFromUser=User.executeUpdate("delete User where emailID='"+emailID+"'")
				if(deleteUserFromUser){
					status=true
					responseData="success"

				}else{
					status=false
					responseData="failed"
					errorList.add("Invalid User ID in delete user")
				}



			}else{
				status=false
				responseData="failed"
				errorList.add("Invalid User ID in delete user from VerificationDetails")
			}

		} catch (Exception e) {
			e.printStackTrace()
		}finally{
			jsonData['request']=requestData
			jsonData['response']=responseData
			jsonData['status']=status
			jsonData['errorList']=errorList
			return jsonData
		}

	}

}
