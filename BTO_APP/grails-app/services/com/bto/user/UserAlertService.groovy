package com.bto.user

import grails.transaction.Transactional

@Transactional
class UserAlertService {

    def serviceMethod() {

    }
	
	//This Service formats the alert createdOn time(7.7 PM --> 07.07 PM)
	public String formatAlertCreatedOnTime(Calendar alertCreatedOnInUserTimeZone)
	{
		try
		{
			String AM_PM= alertCreatedOnInUserTimeZone.get(Calendar.AM_PM)==1 ? "PM" : "AM";
			String minutes
			if(alertCreatedOnInUserTimeZone.get(Calendar.MINUTE)>=0 && alertCreatedOnInUserTimeZone.get(Calendar.MINUTE)<=9)
			{
				minutes="0"+alertCreatedOnInUserTimeZone.get(Calendar.MINUTE)
			}
			else
			{
				minutes=alertCreatedOnInUserTimeZone.get(Calendar.MINUTE)
			}
			String hours=alertCreatedOnInUserTimeZone.get(Calendar.HOUR)==0? "12" : alertCreatedOnInUserTimeZone.get(Calendar.HOUR)+""
			return hours+":"+minutes+" " +AM_PM
			
		}
		catch(Exception e)
		{
			println "Exception occured in TripDetailsService.formatAlertCreatedOnTime"
			def AM_PM=alertCreatedOnInUserTimeZone.get(Calendar.AM_PM)==1 ? "PM" : "AM"
			return alertCreatedOnInUserTimeZone.get(Calendar.HOUR)+":"+alertCreatedOnInUserTimeZone.get(Calendar.MINUTE)+":"+AM_PM;
		}
		
	}
}
