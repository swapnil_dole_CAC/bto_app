package com.bto.utilities

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import org.springframework.web.context.request.RequestContextHolder;
import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONException;
import org.codehaus.groovy.grails.web.json.JSONObject;

import grails.transaction.Transactional

@Transactional
class GetKMLFileService {

	def grailsApplication
	
    def serviceMethod() {

    }
	
	// Declaring all parameters
	 String start_address = "";
	 String startCoordinates = "";
	 String listcoordinates = "";
	 String end_address = "";
	 String endCoordinates = "";
	 String polyline = "";
	 String summary = "";
	 String fileName = "";
	
	//read and convert input stream
	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	// passing url and getting json data and generating KML File
	public String genarateKML(String startLocation,String endLocation, String tripID) throws IOException,JSONException {
		String URL="https://maps.googleapis.com/maps/api/directions/json?origin="+URLEncoder.encode(startLocation, "UTF-8")+"&destination="+URLEncoder.encode(endLocation, "UTF-8")+"";
		InputStream is = new URL(URL).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			
			
			JSONObject json = new JSONObject(jsonText);
			JSONObject jsonObject = new JSONObject(jsonText);
			
			// reference http://stackoverflow.com/questions/7237290/json-parsing-of-google-maps-api-in-android-app
			
			// routesArray contains ALL routes
			JSONArray routesArray = jsonObject.getJSONArray("routes");
			
			// Grab the first route
			JSONObject route = routesArray.getJSONObject(0);
			
			// Take all legs from the route
			JSONArray legs = route.getJSONArray("legs");
			
			// Grab first leg
			JSONObject leg = legs.getJSONObject(0);
			// get start address and end address from leg
			start_address = leg.getString("start_address");
			end_address = leg.getString("end_address");
			
			// get lat lng of start location
			JSONObject start_locationObject = leg.getJSONObject("start_location");
			startCoordinates = start_locationObject.getString("lat") + ","+ start_locationObject.getString("lng");

			// get lat lng of end location
			JSONObject end_locationObject = leg.getJSONObject("end_location");
			endCoordinates = end_locationObject.getString("lat") + ","+ end_locationObject.getString("lng");
		
			// Grab route and get points
			JSONObject overview_polyline = route.getJSONObject("overview_polyline");
			polyline = overview_polyline.getString("points");
			
			// Grab route and get summary
			summary = route.getString("summary");
			
			// array to gate all coordinates from steps
			JSONArray steps = leg.getJSONArray("steps");

			for (int i = 0; i < steps.length(); i++) {
				JSONObject step = steps.getJSONObject(i);
				listcoordinates += step.getJSONObject("start_location").getString("lat")+ ","+ step.getJSONObject("start_location").getString("lng")+ "\n ";
				// System.out.println(step.getJSONObject("start_location").getString("lat")+","+step.getJSONObject("start_location").getString("lng"));
			}
			// System.out.println(listcoordinates);

			String xmlString=GetKMLFile.genarateKML(startLocation, endLocation,tripID)
			// write above string in KML file
			FileWriter out;
			try {
				fileName=tripID+".kml"
				def request = RequestContextHolder.currentRequestAttributes().request
				//out = new FileWriter(grailsApplication.config.grails.kmlFile+""+tripID+".kml");// file name and path of KML file
				out = new FileWriter(request.getSession().getServletContext().getRealPath("/")+"images/OtherFiles/routeFile/"+tripID+".kml");// file name and path of KML file
				out.write(xmlString);
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return fileName;
		} finally {
			is.close();
		}
	}
	
}
