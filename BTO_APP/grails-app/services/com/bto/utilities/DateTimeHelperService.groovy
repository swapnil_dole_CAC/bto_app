package com.bto.utilities

import java.text.SimpleDateFormat
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.TimeZone;

import com.bto.user.User;

import grails.transaction.Transactional

@Transactional
class DateTimeHelperService {

    def serviceMethod() {

    }
	
	
	/*service to
	1. Merge trip_date and Time
	2.Convert Trip dateTime from User's timeZone To Server's TimeZone
	
	Returns: HashMap with Date and Time
	*/
	HashMap<String, Object> convertDateToUTC(Date tripDate, String tripTime,String timeZone)
	{
		  HashMap<String, Object> tripDateMap=new HashMap<String, Object>();
		  HashMap<String, Integer> timeMap=convertTimeFromTwelveToTwentyFourHours(tripTime)
		  
		  //Trip date object with time
		  //In user's timeZone
		  //i.e before conversion
		  TimeZone.setDefault(TimeZone.getTimeZone(tripTime))
		  
		  Calendar localTime = new GregorianCalendar(TimeZone.getTimeZone(timeZone))
		  localTime.set(Calendar.DAY_OF_MONTH, tripDate.getDate());
		  localTime.set(Calendar.MONTH, tripDate.getMonth());
		  localTime.set(Calendar.YEAR, (tripDate.getYear()+1900));
		  localTime.set(Calendar.HOUR_OF_DAY, timeMap.get("hour"));
		  localTime.set(Calendar.MINUTE, timeMap.get("minute"));
		  
			
		  int hour = localTime.get(Calendar.HOUR_OF_DAY);
		  int minute = localTime.get(Calendar.MINUTE);
		  int second = localTime.get(Calendar.SECOND);
			
		   TimeZone.setDefault(TimeZone.getDefault())
		  
		  Calendar serverTime = new GregorianCalendar(TimeZone.getDefault())
		  serverTime.setTime(localTime.getTime());
		  hour = serverTime.get(Calendar.HOUR_OF_DAY);
		  minute = serverTime.get(Calendar.MINUTE);
		  second = serverTime.get(Calendar.SECOND);
			
		  tripDateMap.put("date",serverTime.getTime());
		  tripDateMap.put("time", hour+":"+minute);
			
		  return tripDateMap;
		  
	}
	
	
	//method converts 12 hour time into 24 hour time
	//returns map with "hour" and "minute" keys
	static HashMap<java.lang.String, Integer> convertTimeFromTwelveToTwentyFourHours(java.lang.String tripStartTime )
	{
			int tripStartHour,tripStartMinute;
			HashMap<java.lang.String, Integer> timeMap=new HashMap<java.lang.String, Integer>();
			
			StringTokenizer token=new StringTokenizer(tripStartTime, " ");
			
			java.lang.String  time=token.nextToken();
			java.lang.String AMORPM=token.nextToken();
			
			StringTokenizer tokenizer=new StringTokenizer(time, ":");
			tripStartHour=Integer.parseInt(tokenizer.nextToken());
			tripStartMinute= Integer.parseInt(tokenizer.nextToken());
			
			//Converting Start Time into 24 Hours Clock
			if(AMORPM.equalsIgnoreCase("pm"))
			{
				if(tripStartHour==12)
				{
					tripStartHour=0;
				}
				tripStartHour=12+tripStartHour;
				tripStartMinute=0+tripStartMinute;
			}
			else if(AMORPM.equalsIgnoreCase("am"))
			{
				if(tripStartHour==12)
				{
					tripStartHour=0;
				}
				tripStartMinute=0+tripStartMinute;
			}
			
			timeMap.put("hour", tripStartHour);
			timeMap.put("minute", tripStartMinute);
			
			return timeMap;
	}
	
	
	//Service to convert Time and Date in database in user's TimeZone
	HashMap<String, Object> convertDateToUserTimeZone(Date tripDate, String tripTime,String timeZone)
	{
		  HashMap<String, Object> tripDateMap=new HashMap<String, Object>();
		  Calendar localTime = new GregorianCalendar(TimeZone.getDefault())
		  try
		  {
			 StringTokenizer tokenizer=new StringTokenizer(tripTime, ":");
			 
			 //Trip date object with time
			 //In user's timeZone
			 //i.e before conversion
			 TimeZone.setDefault(TimeZone.getDefault())
			 
			 localTime.set(Calendar.DAY_OF_MONTH, tripDate.getDate());
			 localTime.set(Calendar.MONTH, tripDate.getMonth());
			 localTime.set(Calendar.YEAR, (tripDate.getYear()+1900));
			 localTime.set(Calendar.HOUR_OF_DAY,Integer.parseInt(tokenizer.nextToken()));
			 localTime.set(Calendar.MINUTE,Integer.parseInt(tokenizer.nextToken()));
			 
			   
		  }
		  catch(Exception e)
		  {
			  println "Exception Occured while Tokenizing the Start_Time"
		  }
		  
		  int hour = localTime.get(Calendar.HOUR_OF_DAY);
		  int minute = localTime.get(Calendar.MINUTE);
		  int second = localTime.get(Calendar.SECOND);
			
		  Calendar userTime = new GregorianCalendar(TimeZone.getTimeZone(timeZone))
		  userTime.setTime(localTime.getTime());
		  hour = userTime.get(Calendar.HOUR_OF_DAY);
		  minute = userTime.get(Calendar.MINUTE);
		  second = userTime.get(Calendar.SECOND);
			
		  tripDateMap.put("date",userTime);
		  tripDateMap.put("time", hour+":"+minute);
			
		  return tripDateMap;
		
		  
	}
	
	
	//method converts 24 hour time into 12 hour time
	//returns map with "hour" and "minute" and "AM_PM" Keys
	//It will be used for "Weekly Trip" time Conversion while sending in Response
	static HashMap<java.lang.String, Object> convertTimeFromTwentyFourHoursToTwelveHours(java.lang.String tripStartTime )
	{
			int tripStartHour,tripStartMinute;
			String AM_PM="";
			HashMap<java.lang.String, Object> timeMap=new HashMap<java.lang.String, Object>();
			
			StringTokenizer tokenizer=new StringTokenizer(tripStartTime, ":");
			tripStartHour=Integer.parseInt(tokenizer.nextToken());
			tripStartMinute= Integer.parseInt(tokenizer.nextToken());
			if(tripStartHour < 12)
			{
				tripStartHour=tripStartHour;
				tripStartMinute=tripStartMinute;
				AM_PM="AM";
			}
			else if(tripStartHour == 12)
			{
				 tripStartHour=tripStartHour;
				 tripStartMinute=tripStartMinute;
				 AM_PM="PM";

			}
			else if(tripStartHour > 12)
			{
				tripStartHour=tripStartHour-12;
				tripStartMinute=tripStartMinute;
				AM_PM="PM";
			}
			
			timeMap.put("hour", tripStartHour);
			timeMap.put("minute", tripStartMinute);
			timeMap.put("AM_PM", AM_PM);
			
			return timeMap;
	}

	String calculateCacUserDuration(User user)
	{
		
		String returnValue=""
		try
		{
					//1.set start Date
					Calendar start=new GregorianCalendar();
					start.setTime(user.createdOn)
					
					//2.set Current Date
					Calendar end=new GregorianCalendar();
					
					//3.Calculate Months
					int monthDiff= ((end.get(Calendar.YEAR)- start.get(Calendar.YEAR))*12)+(end.get(Calendar.MONTH)-start.get(Calendar.MONTH));
					
					//if more than One Year
					if(monthDiff>=12)
					{
							Float yearDiff=((float)monthDiff/12)
							returnValue=yearDiff.round(1)==1.0? yearDiff.intValue() +" Year" : yearDiff.round(1) +" Years"
							
					}
					else if(monthDiff==0)//If less than One Month
					{
							int dayDiff=end.get(Calendar.DAY_OF_MONTH)-start.get(Calendar.DAY_OF_MONTH)
							if(dayDiff==0)//If less than one Day
							{
									int hourDiff=end.get(Calendar.HOUR_OF_DAY)-start.get(Calendar.HOUR_OF_DAY)
									if(hourDiff==0)
									{
											int minuteDiff=end.get(Calendar.MINUTE)-start.get(Calendar.MINUTE)
											returnValue=minuteDiff==1? minuteDiff+" Minute": minuteDiff+" Minutes"
											
									}
									else
									{
											returnValue=hourDiff==1? hourDiff+" Hour" : hourDiff+" Hours"
									}
							}
							else
							{
									returnValue=dayDiff==1 ? dayDiff+" Day" : dayDiff+" Days"
							}
					}
					else
					{
						returnValue=monthDiff==1 ? monthDiff+" Month" : monthDiff+" Months"
					}
					
		}
		catch(Exception e)
		{
			println "Exception caused while calculating User_From"
		}
		finally
		{
			return returnValue
		}
		
	}
 
	//Service to calculate users age
	Integer calculateUserAge(User user)
	{
		Integer age=-1
		try
		{
			if(user.dob)
			{
				Calendar dob = Calendar.getInstance();
				SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");
				Calendar today = Calendar.getInstance();
				age = today.get(Calendar.YEAR) - Integer.parseInt(user.dob);
			}
			else
			{
				age=-1
			}
		}
		catch(Exception e)
		{
			println "Exception occured while calcluating age"
			e.printStackTrace()
		}
		finally
		{
			return age
		}
	}
	
}
