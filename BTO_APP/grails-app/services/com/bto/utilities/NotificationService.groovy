package com.bto.utilities

import org.codehaus.groovy.grails.commons.GrailsApplication;
import org.json.simple.JSONValue;

import com.bto.user.User
import com.bto.user.UserDeviceToken;
import com.google.android.gcm.server.Message
import com.google.android.gcm.server.Result
import com.google.android.gcm.server.Sender

import grails.converters.JSON;
import grails.transaction.Transactional
import javapns.Push
import javapns.devices.Device
import javapns.notification.Payload;
import javapns.notification.PushNotificationManager
import javapns.notification.PushNotificationPayload
/**
 * 
 * @author Amit Patil
 * Service to handle push notifications (ios,android) using APNS and Google Cloud Messaging  
 *
 */
@Transactional
class NotificationService {

	 GrailsApplication grailsApplication 
	
	
	def sendPushNotification(String body,String action, User user,HashMap<String, Object> customData)
	{
			//println customData
		//1. Fetch user's all ACTIVE devices from "UserDeviceTokens"
		def deviceTokenList= UserDeviceToken.findAllByUserAndStatus(user, true)
		
		//2.Iterate Over this list and send notification to all active devices.
		//Calling appropriate service by identifying type of device "android" or "Iphone"
		//if(deviceTokenList)
		for(UserDeviceToken deviceToken : deviceTokenList)
		{
				//Check Type of device
				if(deviceToken.deviceType.equalsIgnoreCase("IOS"))
				{	
					sendNotificationToiOS(deviceToken.deviceToken,body,action,customData)
				}
				else if(deviceToken.deviceType.equalsIgnoreCase("Android"))
				{
					sendNotificationToAndriod(createJsonForNotification(body, action, "android",customData),deviceToken.deviceToken)
				}	
		}
	}
	

	String createJsonForNotification(String body,String action,String deviceType,HashMap<String, Object> customData)
	{
				HashMap<String, Object> dataMap=new HashMap<String,Object>()
				HashMap<String, Object> androidMap=new HashMap<String,Object>()
				HashMap<String, Object> resultMap=new HashMap<String,Object>()
				
				//Creating dataMap
				dataMap.put("title", "BTO")
				dataMap.put("message", body)
				dataMap.put("customData", action)
				
				//adding custom data(if any) in Notification JSon response
				if(customData)
				{
					//println "Got Custom data in android"
					dataMap.putAll(customData)
				}
				
				//Creating androidMap
				androidMap.put("data", dataMap)
				
				//creating resultmap
				resultMap.put("Android", androidMap)
				 
				return JSONValue.toJSONString(resultMap);
				
			//}
	}
	
	def sendNotificationToAndriod(String textMessage,String deviceTokenAndroid)
	{
			try
			{
				Result result = null;
				Sender sender = new Sender(grailsApplication.config.grails.googleServerKey);
				Message message = new Message.Builder().timeToLive(30)
						.delayWhileIdle(true).addData("BTO", textMessage).build();
				
				result = sender.send(message, deviceTokenAndroid, 1);
			}
			catch(Exception e)
			{
				println "Exception occured at ANDROID  push notification"
				e.printStackTrace()
			}
		}
	
	
	def sendNotificationToiOS(String deviceTokeniOS,String body,String action,HashMap<String, Object> customData)
	{
		try
		{
			PushNotificationPayload payload = PushNotificationPayload.complex();
			//payload.addCustomAlertActionLocKey("view")
			//payload.addCustomAlertBody(body)
			//payload.addSound("default")
			Map invitemap
			if(customData && action=="invitation_trip")
			{				
				invitemap=new HashMap<String,Object>()
				invitemap.put("ti", customData.get("tripId").toString())
				invitemap.put("name", customData.get("sender_name").toString())
				invitemap.put("pic", customData.get("user_profile_pic").toString())
				invitemap.put("ai", customData.get("alert_id").toString())
				invitemap.put("sl", customData.get("source_location").toString().substring(0, 10))
				invitemap.put("dl", customData.get("destination_location").toString().substring(0, 10))
				invitemap.put("nt", customData.get("alertType").toString())
				invitemap.put("date", customData.get("date").toString())
				invitemap.put("time", customData.get("time").toString())
				for(Map.Entry<String, String> entry : invitemap.entrySet())
				{
					payload.addCustomDictionary(entry.getKey(), entry.getValue().toString())	
				}
				payload.addCustomDictionary("badge", customData.get("badge").toString())
			}
			//Production
			Push.payload(payload, grailsApplication.parentContext.servletContext.getRealPath("/")+"BTOPushNotification.p12", "admin123", true, deviceTokeniOS)
			
			//Push.payload(payload, grailsApplication.parentContext.servletContext.getRealPath("/")+"CAC_APNS_Cer_Dev.p12", "smartcloud", false, deviceTokeniOS)
		}
		catch(Exception e)
		{
			println "Exception at IOS Push Notification"
			e.printStackTrace()
		}		
	}
}
