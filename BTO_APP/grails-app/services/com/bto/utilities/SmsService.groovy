package com.bto.utilities

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import org.springframework.mail.MailSender;
import grails.transaction.Transactional
import grails.gsp.PageRenderer;
import grails.plugins.rest.client.RestBuilder


@Transactional
class SmsService {

	//Static API link for sms gateway.
	def smsGetwayAPIForIndia="http://www.smsjust.com/blank/sms/user/urlsms.php?username=paytech&pass=paytech&senderid=CLDCAR&dest_mobileno=USERMOBILENUMBER&message=ACTUALUSERMESSAGE&response=Y"
	def smsGetwayAPIForOther="http://smsapi.24x7sms.com/api_2.0/SendSMS.aspx?APIKEY=MYKEYAPI&MobileNo=USERMOBILENUMBER&SenderID=MYIDSENDER&Message=ACTUALUSERMESSAGE&ServiceName=MYNAMESERVICE"
	PageRenderer groovyPageRenderer
	def mailService
    
	def serviceMethod() 
	{

    }
	
	def sendSms(String URL)
	{
	
		def rest=new RestBuilder()
		def resp=resp.getAt()
			
	}
	
	/**Service to send any SMS in application.
	 * This service users two third party PAID APIs
	 * www.smsjust.com : for National SMS
	 * 24x7sms.com : for INTER-NATIONAL SMS
	 * 
	 * Note : Service determines users Country from Country code and uses appropriate SMS Gateway to send SMS 
	 */
	void sendSMS(String message,String phone,String countryCode)
	{
		def rest=new RestBuilder()
		def resp
		HashMap<String, Object> countryCodeMap=isCountryCodeOfIndia(countryCode)
		if(countryCodeMap.get("isIndian"))
		{
				String url= smsGetwayAPIForIndia
				url=url.replace("USERMOBILENUMBER",phone)
				url=url.replace("ACTUALUSERMESSAGE",message)
				resp=rest.get(url)
		}
		else
		{
				//Get GAteway Details from database
				String url= smsGetwayAPIForOther
				SMSGateway gateWay=SMSGateway.findByGatewayName("24x7sms")
				
				//If gate details found In database
				if(gateWay)
				{		println "Gate details foudn in Database"
						url=url.replace("MYKEYAPI",gateWay.apiKey)
						url=url.replace("MYIDSENDER",gateWay.senderId)
						url=url.replace("MYNAMESERVICE",gateWay.serviceName)
				}
				else //User default static details
				{
						println "Gate details NOT foudn in Database"
						url=url.replace("MYKEYAPI","Svw24DKz4fs")
						url=url.replace("MYIDSENDER","SMSMsg")
						url=url.replace("MYNAMESERVICE","INTERNATIONAL")
				}
				url=url.replace("USERMOBILENUMBER",countryCodeMap.get("countryCode")+phone)
				url=url.replace("ACTUALUSERMESSAGE",message)
				
				//send request
				resp=rest.get(url)
				
				//If Insufficient funds send aler email
				if(resp.getText().toString().equalsIgnoreCase("INSUFFICIENT_CREDIT"))
				{
						def content=groovyPageRenderer.render(view: '/htmlEmailTemplates/smsGatewayExpiryAlert')
						sendMail {
							from 'info@cloudacar.org'
							to "mandarlande@gmail.com"
							subject ("BTO- 24*7 SMS Gateway Expired")
							html content
						}
						sendMail {
							from 'info@cloudacar.org'
							to "iamitpatil1993@gmail.com"
							subject ("cloudacar- 24*7 SMS Gateway Insufficient funds")
							html content
						}
				}
		}
	}
	
	//Method to check country code is for INDIA or OTHER
	HashMap<String, Object> isCountryCodeOfIndia(String countryCode)
	{
			HashMap<String , Object> returnValue=new HashMap<String, Object>();
			countryCode=countryCode.trim();
			while (countryCode.startsWith("+"))
			{
				countryCode=countryCode.substring(1,countryCode.length());
			}
			if(countryCode.equalsIgnoreCase("91"))
			{
				returnValue.put("isIndian",true)
				returnValue.put("countryCode", countryCode)
			}
			else
			{
				returnValue.put("isIndian",false)
				returnValue.put("countryCode", countryCode)
			}
			
			return returnValue;
	}
}
