package com.bto.utilities

import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;

import com.sun.org.apache.bcel.internal.generic.RETURN;

import grails.transaction.Transactional

@Transactional
class UtilitiesService {

    def serviceMethod() {

    }
	
	public static Date newDate() {
		Date date = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateInString = "2016-06-10 11:34:36";

		try {

			date = formatter.parse(dateInString);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	
	// function to return random number for verification code
	public static String getRandomNumber(){
		//Generating Random characters as a Password
		def charset = (('A'..'Z')).join()
		def length = 5
		def randomString = RandomStringUtils.random(length, charset.toCharArray())
		def random = new Random()
		def randomInt = random.nextInt(2000-1000+1)+1000
		return randomInt;
	}
	
	//function to return string from BufferedReader Object
	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		
		int cp;
		while ((cp = rd.read()) != -1) {
		  sb.append((char) cp);
		}
		return sb.toString();
	  }
}
