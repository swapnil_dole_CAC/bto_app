package bto_app

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.bto.trip.TripDetails;


class ChangeTripStatusJob {
    static triggers = {
      //simple repeatInterval: 5000l // execute job once in 5 seconds
    }

    def execute() {
        try{
			def tripLists=TripDetails.list()
			for(TripDetails trip:tripLists)
			{ 
				def currentDate=new Date()
				Calendar currentCal=Calendar.getInstance()
				currentCal.setTime(currentDate)
				
				DateFormat formatter = new SimpleDateFormat("hh:mm");
				Date requestedDate = formatter.parse(trip.tripTime.toLowerCase());
				Calendar TripCal=Calendar.getInstance()
				TripCal.setTime(requestedDate)
				println trip.tripTime+"-"+TripCal.before(currentCal)
				if(TripCal.before(currentCal))
				{
					println "Hi"
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
		}
    }
}
